﻿using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace XynokUIToolkit.EventHandler
{
    public class KeyBoardEventListener : Manipulator
    {
        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<KeyDownEvent>(OnKeyDown);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback(new EventCallback<KeyDownEvent>(OnKeyDown));
        }

        internal void OnKeyDown(KeyDownEvent evt)
        {
            if (evt.modifiers == EventModifiers.Control && evt.keyCode == KeyCode.S)
                Debug.Log($"Saved !!!");


            evt.StopPropagation();
        }
    }
}