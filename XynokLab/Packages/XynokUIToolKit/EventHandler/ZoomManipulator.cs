﻿using UnityEngine;
using UnityEngine.UIElements;

namespace XynokUIToolkit.EventHandler
{
    public class ZoomManipulator : Manipulator
    {
        private float _minScale = .1f;
        private float _maxScale = 1f;
        private float _scaleStep = .1f;

        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<PointerDownEvent>(PointerDown);
            target.RegisterCallback<PointerMoveEvent>(PointerMove);
            target.RegisterCallback<PointerUpEvent>(PointerUp);
            target.RegisterCallback<WheelEvent>(OnWheel);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<PointerDownEvent>(PointerDown);
            target.UnregisterCallback<PointerMoveEvent>(PointerMove);
            target.UnregisterCallback<PointerUpEvent>(PointerUp);
            target.UnregisterCallback<WheelEvent>(OnWheel);
        }

        public ZoomManipulator()
        {
        }

        public ZoomManipulator(VisualElement visualElement)
        {
            this.target = visualElement;
        }


        void OnWheel(WheelEvent evt)
        {
            bool holdControl = evt.ctrlKey;
            if (!holdControl) return;
            if (evt.delta.y == 0) return;
            // evt.StopPropagation();
            bool zoomIn = evt.delta.y < 0;
            if (zoomIn)
            {
                ZoomIn(evt);
                return;
            }

            ZoomOut(evt);
        }

        void ZoomIn(WheelEvent wheelEvent)
        {
            target.transform.scale = GetZoomInScale();
            // Move(_scaleStep, wheelEvent.localMousePosition);
        }

        Vector3 GetZoomInScale()
        {
            var scale = target.transform.scale;
            scale += new Vector3(_scaleStep, _scaleStep, 0);
            var result = Vector3.Max(new Vector3(_minScale,_minScale,1), scale);
            result = Vector3.Min(_maxScale * Vector3.one, result);
            return result;
        }

        Vector3 GetZoomOutScale()
        {
            var scale = target.transform.scale;
            scale -= new Vector3(_scaleStep, _scaleStep, 0);
            var result = Vector3.Max(new Vector3(_minScale,_minScale,1), scale);
            result = Vector3.Min(_maxScale * Vector3.one, result);
            return result;
        }

        void ZoomOut(WheelEvent wheelEvent)
        {
            target.transform.scale = GetZoomOutScale();
            // Move(_scaleStep, wheelEvent.localMousePosition);
        }

        void PointerDown(PointerDownEvent evt)
        {
            evt.PreventDefault();
        }

        void PointerMove(PointerMoveEvent evt)
        {
            evt.PreventDefault();
        }

        void PointerUp(PointerUpEvent evt)
        {
        }


        void Move(float scaleDelta, Vector3 mousePosition)
        {
            var rect = target.localBound;
            var contentSize = target.parent.localBound;

            mousePosition.x /= rect.width;
            mousePosition.y /= rect.height;

            var shiftX = -scaleDelta * contentSize.width * (mousePosition.x - 0.5f);
            var shiftY = -scaleDelta * contentSize.height * (mousePosition.y - 0.5f);

            var currPos = target.parent.transform.position;
            var finalPos = new Vector3(currPos.x + shiftX, currPos.y + shiftY, currPos.z);
            target.transform.position = finalPos;
        }
    }
}