﻿using UnityEngine.UIElements;

namespace XynokUIToolkit.Utils
{
    public static class VisualElementExtensions
    {
        public static void  HideLabel(this VisualElement element)
        {
            var label = element.Q<Label>();
            label.style.display = DisplayStyle.None;
            label.style.visibility = Visibility.Hidden;
        }
    }
}