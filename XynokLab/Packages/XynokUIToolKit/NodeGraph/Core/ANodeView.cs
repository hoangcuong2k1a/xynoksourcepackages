﻿#if UNITY_EDITOR

using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;
using XynokConvention.APIs;
using XynokUIToolkit.Data;
using XynokUIToolkit.NodeGraph.Dialogue.Data;

namespace XynokUIToolkit.NodeGraph.Core
{
    // https://www.youtube.com/watch?v=nKpM98I7PeM&ab_channel=TheKiwiCoder
    public interface INodeData
    {
        Vector2 NodePosition { get; set; }
        string GuiID { get; }
        string[] NextNodeIDs { get; }
        void AddNextNodeID(string id);
        void RemoveNextNodeID(string id);
    }

    public interface INodeView<out T>
    {
        T GetData();
    }

    public abstract class ANodeView<T> : Node, IInjectable<T>, INodeView<T>
        where T : INodeData
    {
        protected T Data;
        public T GetData() => Data;
        public Port InputPort;
        public Port OutputPort;
        protected UIToolKitKeyManaged KeyManaged => UIToolKitKeyManaged.Instance;
        protected DialogueDatabase Database => DialogueDatabase.Instance;

        public void SetDependency(T dependency)
        {
            if (Data != null) Dispose();
            Data = dependency;
            viewDataKey = Data.GuiID;
            Init();
            style.left = Data.NodePosition.x;
            style.top = Data.NodePosition.y;
            CreateInputPort();
            CreateOutputPort();
            HideTop();
            RefreshExpandedState();
        }

        public override void SetPosition(Rect newPos)
        {
            base.SetPosition(newPos);
            if (Data == null) return;
            Data.NodePosition = new Vector2(newPos.xMin, newPos.yMin);
        }

        protected abstract void Init();
        protected abstract void OnDispose();

        void HideTop()
        {
            topContainer.style.minHeight = 0;
            topContainer.style.height = 15;
            topContainer.style.maxHeight = 15;
            topContainer.Query<Label>().ForEach(e => { e.style.display = DisplayStyle.None; });
        }

        protected virtual void CreateInputPort()
        {
            InputPort = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(bool));
            InputPort.portName = "Input";
            InputPort.name = "Input";


            inputContainer.Add(InputPort);
        }

        protected virtual void CreateOutputPort()
        {
            OutputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, typeof(bool));
            OutputPort.portName = "Output";
            OutputPort.name = "Output";


            outputContainer.Add(OutputPort);
        }

        public void Dispose()
        {
            OnDispose();
        }
    }
}
#endif
