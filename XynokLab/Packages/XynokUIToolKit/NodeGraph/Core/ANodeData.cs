﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using UnityEngine;

namespace XynokUIToolkit.NodeGraph.Core
{
    [Serializable]
    public class ANodeData : INodeData
    {
        [HideInInspector] public string guid;
        [HideInInspector] public Vector2 position;

        [HideInInspector] public List<string> nextNodeIDs;

        public Vector2 NodePosition
        {
            get => position;
            set => position = value;
        }

        public string GuiID => guid;

        public string[] NextNodeIDs => nextNodeIDs.ToArray();

        public void AddNextNodeID(string id)
        {
           
            nextNodeIDs ??= new List<string>();
            if (nextNodeIDs.Contains(id)) return;
            nextNodeIDs.Add(id);
        }

        public void RemoveNextNodeID(string id)
        {
            if (nextNodeIDs == null || nextNodeIDs.Count < 1) return;
            nextNodeIDs.Remove(id);
        }
    }
}
#endif
