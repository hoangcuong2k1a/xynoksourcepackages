﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
using XynokUIToolkit.EventHandler;

namespace XynokUIToolkit.NodeGraph.Core
{
    public abstract class AGridBoard : GraphView
    {
        protected virtual string UssPath => "Packages/com.xynok.uitoolkit/NodeGraph/NodeGraphEditor.uss";


        protected AGridBoard()
        {
            Init();
        }

      void Init()
        {
            var grid = new GridBackground();
            Insert(0, grid);
            grid.StretchToParentSize();
            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            this.AddManipulator(new KeyBoardEventListener());
            this.AddManipulator(new FreehandSelector());

            var styleSheet =
                AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.xynok.uitoolkit/NodeGraph/NodeGraphEditor.uss");
            styleSheets.Add(styleSheet);
        }

        protected void ZoomAt(VisualElement e)
        {
            // Vector2 destination = this.ChangeCoordinatesTo(this.contentViewContainer, e.transform.position);
            // Vector3 currentScale = contentViewContainer.transform.scale;
            // viewTransform.position += Vector3.Scale(destination, currentScale);
            //
            // Vector3 position = contentViewContainer.transform.position;
            // UpdateViewTransform(e.transform.position, currentScale);
            if (e is ISelectable selectable)
            {
                FrameSelection();
            }
        }
    }
}
#endif