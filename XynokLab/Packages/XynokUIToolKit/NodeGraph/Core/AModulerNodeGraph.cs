﻿#if UNITY_EDITOR

using UnityEngine.UIElements;
using XynokConvention.APIs;
using XynokUIToolkit.Data;

namespace XynokUIToolkit.NodeGraph.Core
{
    public  abstract class AModulerNodeGraph: IInjectable<VisualElement>
    {
        protected VisualElement Root;
        protected UIToolKitKeyManaged KeyManaged => UIToolKitKeyManaged.Instance;
        public void SetDependency(VisualElement dependency)
        {
            if(Root!=null) Dispose();
            Root = dependency;
            Init();
        }
        
        protected abstract void Init();
        protected abstract void OnDispose();

        public void Dispose()
        {
            OnDispose();
        }
    }
}
#endif
