﻿#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using XynokUIToolkit.NodeGraph.Core;
using XynokUIToolkit.NodeGraph.Dialogue.Data;

namespace XynokUIToolkit.NodeGraph.Dialogue
{
    public class DialogueModule : AModulerNodeGraph
    {
        private DialogueBoardEditor _dialogueBoardEditor;

        // new conversation
        private VisualElement _toolCreateNewConversation;
        private Button _btnCreateNewConversation;
        private TextField _txtCreateNewConversation;
        private VisualElement _saveConversationBtn;

        // language options
        private VisualElement _toolLanguageOptions;
        private RadioButtonGroup _languageOptions;

        // storage conversations
        private ListView _libraryConversations;

        protected override void Init()
        {
            _dialogueBoardEditor = Root.Q<DialogueBoardEditor>();
            // new conversation
            _toolCreateNewConversation = Root.Q(KeyManaged.idPopupCreateNewDialogue);
            _btnCreateNewConversation = _toolCreateNewConversation.Q<Button>(KeyManaged.idBtnCreate);
            _txtCreateNewConversation = _toolCreateNewConversation.Q<TextField>();
            _saveConversationBtn = Root.Q(KeyManaged.idBtnSave);

            _libraryConversations = _toolCreateNewConversation.Q(KeyManaged.idLibrary).Q<ListView>();

            _saveConversationBtn.AddManipulator(new Clickable(evt => { _dialogueBoardEditor.Save(); }));

            // language options
            _toolLanguageOptions = Root.Q(KeyManaged.idPopupDialogueLanguage);


            RegisterActiveMainView();
            RegisterBackdropClicked();
            RegisterCreateNewConversation();
            RegisterLanguageOptions();

            // library conversation
            SpawnStorageConversations();
        }


        protected override void OnDispose()
        {
        }

        void SpawnStorageConversations()
        {
            _libraryConversations.Clear();
            Func<VisualElement> makeItem = () =>
            {
                var item = KeyManaged.btnOpenConversation.Instantiate();
                var openBtn = item.Q<Button>(KeyManaged.idBtnOpen);
                var deleteBtn = item.Q<Button>(KeyManaged.idBtnDelete);
                openBtn.clicked += () =>
                {
                    var conversationName = item.Q<Label>().text;
                    var target = DialogueDatabase.Instance.GetConversationByName(conversationName);
                    if (target == null) return;
                    if (DialogueDatabase.Instance.lastEditedConversation != "???") _dialogueBoardEditor.Save();
                    _dialogueBoardEditor.LoadConversation(conversationName);
                    ActiveToolView(KeyManaged.idPopupCreateNewDialogue, false); // hide tool view
                };

                deleteBtn.clicked += () =>
                {
                    var conversationName = item.Q<Label>().text;
                    var confirm = EditorUtility.DisplayDialog("Dialogue",
                        $"Are you sure to delete conversation: {conversationName} ?", "Yes", "Cancel");
                    if (!confirm) return;
                    if (conversationName == DialogueDatabase.Instance.lastEditedConversation)
                    {
                        DialogueDatabase.Instance.lastEditedConversation = "???";
                        _dialogueBoardEditor.Dispose();
                    }

                    DialogueDatabase.Instance.RemoveConversation(conversationName);
                    _libraryConversations.Remove(item);
                    _libraryConversations.Rebuild();
                };
                return item;
            };
            Action<VisualElement, int> bindItem = (e, i) =>
            {
                e.Q<Label>().text = DialogueDatabase.Instance.conversations[i].conversationName;
            };
            _libraryConversations.bindItem = bindItem;
            _libraryConversations.makeItem = makeItem;
            _libraryConversations.itemsSource = DialogueDatabase.Instance.conversations;
            _libraryConversations.fixedItemHeight = 36;
        }

        void RegisterLanguageOptions()
        {
            _languageOptions = _toolLanguageOptions.Q<RadioButtonGroup>();
            _languageOptions.choices = DialogueDatabase.Instance.allLanguages;
            _languageOptions.RegisterValueChangedCallback(OnLanguageOptionsChanged);
        }

        void OnLanguageOptionsChanged(ChangeEvent<int> evt)
        {
            var index = evt.newValue;
            var lang = DialogueDatabase.Instance.allLanguages[index];
            _dialogueBoardEditor.UpdateLanguage(lang);
        }

        void RegisterCreateNewConversation()
        {
            void CreateNewConversation()
            {
                if (string.IsNullOrEmpty(_txtCreateNewConversation.value) ||
                    string.IsNullOrWhiteSpace(_txtCreateNewConversation.value) ||
                    _txtCreateNewConversation.value == "???")
                {
                    EditorUtility.DisplayDialog("Dialogue", "Fill a valid name for conversation !", "Ok");
                    return;
                }

                var succeed = DialogueDatabase.Instance.CreateNewConversation(_txtCreateNewConversation.value);
                if (!succeed)
                {
                    EditorUtility.DisplayDialog("Dialogue", "This conversation already existed !", "Ok");
                    return;
                }

                if (DialogueDatabase.Instance.lastEditedConversation != "???") _dialogueBoardEditor.Save();
                ActiveToolView(KeyManaged.idPopupCreateNewDialogue, false); // hide tool view
                EditorUtility.SetDirty(DialogueDatabase.Instance);
                DialogueDatabase.Instance.CreateNewConversation(_txtCreateNewConversation.value);
                _dialogueBoardEditor.LoadConversation(_txtCreateNewConversation.value);
                _dialogueBoardEditor.CreateNewSpeechForCurrentConversation(Vector2.zero);
                _dialogueBoardEditor.UpdateConversationName(_txtCreateNewConversation.value);
                _txtCreateNewConversation.SetValueWithoutNotify("???");
            }

            _btnCreateNewConversation.clicked -= CreateNewConversation;
            _btnCreateNewConversation.clicked += CreateNewConversation;
        }

        void RegisterBackdropClicked()
        {
            Root.Query(className: KeyManaged.classPopupDialogueToolView).ForEach((toolView) =>
            {
                var backDrop = toolView.Q(KeyManaged.idBtnBackdrop);
                backDrop.AddManipulator(new Clickable(evt => { toolView.style.visibility = Visibility.Hidden; }));
            });
        }

        void RegisterActiveMainView()
        {
            Root.Query(className: KeyManaged.classBtnDialogueHomeBar).ForEach((e) =>
            {
                e.AddManipulator(new Clickable(evt =>
                {
                    var toolViewName = KeyManaged.GetDialogueMappingToolView(e.name);
                    ActiveToolView(toolViewName);
                }));
            });
        }

        void ActiveToolView(string toolViewName, bool active = true)
        {
            Root.Query(className: KeyManaged.classPopupDialogueToolView).ForEach((toolView) =>
            {
                if (!active)
                {
                    toolView.style.visibility = Visibility.Hidden;
                    return;
                }

                toolView.style.visibility = toolView.name == toolViewName
                    ? Visibility.Visible
                    : Visibility.Hidden;
                if ( toolView.name == KeyManaged.idPopupCreateNewDialogue) _libraryConversations?.Rebuild();
            });
        }
    }
}
#endif
