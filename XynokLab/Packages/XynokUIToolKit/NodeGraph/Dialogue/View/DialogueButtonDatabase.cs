﻿#if UNITY_EDITOR

using UnityEngine.UIElements;

namespace XynokUIToolkit.NodeGraph.Dialogue.View
{
    public class DialogueButtonDatabase: VisualElement
    {   public class DialogueButtonDatabaseFactory : UxmlFactory<DialogueButtonDatabase, UxmlTraits>
        {
        }
        public Label Label;
        public VisualElement LoadBtn;
        public VisualElement DeleteBtn;
        public DialogueButtonDatabase()
        {
            Label = new Label();
            DeleteBtn = new ();
            LoadBtn = new ();
            Add(Label);
            Add(LoadBtn);
            Add(DeleteBtn);
        }
    }
}
#endif
