﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;
using XynokUIToolkit.Data;
using XynokUIToolkit.NodeGraph.Core;
using XynokUIToolkit.NodeGraph.Dialogue.Data;
using XynokUIToolkit.NodeGraph.Dialogue.Nodes;
using XynokUtils;

namespace XynokUIToolkit.NodeGraph.Dialogue
{
    [Serializable]
    public class TestNodeData
    {
        public string name;
        public string description;
    }

    public class DialogueBoardEditor : AGridBoard, IDisposable
    {
        public class GridFactory : UxmlFactory<DialogueBoardEditor, UxmlTraits>
        {
        }

        private Label _conversationNameLabel;

        public DialogueBoardEditor()
        {
            // GenerateMiniMap();
        }


        void GenerateMiniMap()
        {
            var miniMap = new MiniMap();
            var cords = contentViewContainer.WorldToLocal(new Vector2(54, 21));
            miniMap.SetPosition(new Rect(cords.x, cords.y, 200, 120));
            Insert(1, miniMap);
        }

        public void Save()
        {
            if (!edges.Any()) return;
            var connectionPorts = edges.Where(e => e.input.node != null).ToArray();

            EditorUtility.SetDirty(DialogueDatabase.Instance);
            // clear all nextNodeIDs
            foreach (var element in graphElements)
            {
                if (element is SpeechNode speechNode)
                    speechNode.GetData().nextNodeIDs.Clear();
            }

            for (int i = 0; i < connectionPorts.Length; i++)
            {
                var output = connectionPorts[i].output.node as INodeView<INodeData>;
                var input = connectionPorts[i].input.node as INodeView<INodeData>;
                if (input == null || output == null) continue;
                output.GetData().AddNextNodeID(input.GetData().GuiID);
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }


        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            var lastEditedConversation = DialogueDatabase.Instance.lastEditedConversation;
            if (lastEditedConversation == "???" || lastEditedConversation == null)
            {
                evt.menu.AppendAction("Tạo mới hội thoại hoặc chọn một hội thoại để bắt đầu edit !", null);
                return;
            }

            var nodeTypes = EnumHelper.GetAllValuesOf<DialogueNodeType>();
            foreach (var nodeType in nodeTypes)
            {
                evt.menu.AppendAction($"{nodeType}",
                    (dropdownMenuAction) => { CreateNode(dropdownMenuAction, nodeType); });
            }

            evt.menu.AppendSeparator();
            base.BuildContextualMenu(evt);
        }

        void CreateNode(DropdownMenuAction dropdownMenuAction, DialogueNodeType nodeType)
        {
            EditorUtility.SetDirty(DialogueDatabase.Instance);
            if (nodeType == DialogueNodeType.Speech)
                CreateNewSpeechForCurrentConversation(dropdownMenuAction.eventInfo.localMousePosition);

            if (nodeType == DialogueNodeType.Branch)
                CreateNewBranchForCurrentConversation(dropdownMenuAction.eventInfo.localMousePosition);
            if (nodeType == DialogueNodeType.Variable)
                CreateVariableNode(dropdownMenuAction.eventInfo.localMousePosition);
            
            if (nodeType == DialogueNodeType.EventEmitter)
                CreateEmitEventNode(dropdownMenuAction.eventInfo.localMousePosition);
        }

        void CreateEmitEventNode(Vector2 spawnPos)
        {
            var nodeView = new EmitEventNode();
            var data = new EventData
            {
                name = DialogueDatabase.Instance.events[0].name,
                position = spawnPos,
                guid = GUID.Generate().ToString(),
            };
            var conversation = DialogueDatabase.Instance.GetLastEditConversation();
            conversation.Add(data);
            nodeView.SetDependency(data);
            AddElement(nodeView);
        }
        void CreateVariableNode(Vector2 spawnPos)
        {
            var nodeView = new VariableNode();
            var first = DialogueDatabase.Instance.variables[0];
            var data = first.Clone();
            data.position = spawnPos;
            var conversation = DialogueDatabase.Instance.GetLastEditConversation();
            conversation.Add(data);
            nodeView.SetDependency(data);
            AddElement(nodeView);
        }

        void CreateNewBranchForCurrentConversation(Vector2 spawnPos)
        {
            var nodeView = new BranchNode();
            var data = new BranchData
            {
                position = spawnPos,
                guid = GUID.Generate().ToString()
            };
            var conversation = DialogueDatabase.Instance.GetLastEditConversation();
            conversation.Add(data);
            nodeView.SetDependency(data);
            AddElement(nodeView);
        }

        public void CreateNewSpeechForCurrentConversation(Vector2 spawnPos)
        {
            var nodeView = new SpeechNode();
            var data = new SpeechData
            {
                position = spawnPos,
            };
            data.Init();
#if UNITY_EDITOR
            data.guid = GUID.Generate().ToString();
#endif
            nodeView.SetDependency(data);

            DialogueDatabase.Instance.AddSpeechToCurrentConversation(data);
            AddElement(nodeView);
            ZoomAt(nodeView);
        }


        public Group CreateCommentBlock(Rect rect, CommentBlockData commentBlockData = null)
        {
            if (commentBlockData == null)
                commentBlockData = new CommentBlockData();
            var group = new Group
            {
                autoUpdateGeometry = true,
                title = commentBlockData.Title
            };
            AddElement(group);
            group.SetPosition(rect);
            return group;
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var compatiblePorts = new List<Port>();

            ports.ForEach((port) =>
            {
                if (startPort != port && startPort.node != port.node)
                    compatiblePorts.Add(port);
            });
            return compatiblePorts;
        }

        public void Dispose()
        {
            UpdateConversationName("???");
            DeleteElements(graphElements);
        }

        public void UpdateLanguage(string language)
        {
            DialogueDatabase.Instance.currentLanguage = language;
            foreach (var element in graphElements)
            {
                if (element is SpeechNode speechNode)
                    speechNode.UpdateContentByLanguage(language);
            }
        }

        public void OnEnable()
        {
            LoadCurrentNodes();
        }

        public void LoadConversation(string conversationName)
        {
            if (string.IsNullOrEmpty(conversationName) || string.IsNullOrWhiteSpace(conversationName))
            {
                Dispose();
                return;
            }

            var conversation = DialogueDatabase.Instance.GetConversationByName(conversationName);
            if (conversation == null) return;
            Dispose();
            EditorUtility.SetDirty(DialogueDatabase.Instance);
            DialogueDatabase.Instance.SetCurrentConversationEdit(conversationName);
            LoadCurrentNodes();
        }

        void LoadCurrentNodes()
        {
            var conversation = DialogueDatabase.Instance.GetLastEditConversation();
            if (conversation == null) return;

            var cacheNodesCreated = new List<SpeechNode>();
            if (conversation.speeches == null || conversation.speeches.Count < 1) return;
            // create nodes
            foreach (var speech in conversation.speeches)
            {
                var nodeView = new SpeechNode();
                nodeView.SetDependency(speech);
                AddElement(nodeView);
                cacheNodesCreated.Add(nodeView);
            }


            // create edges
            foreach (var speech in conversation.speeches)
            {
                var nodeView = cacheNodesCreated.FirstOrDefault(n => n.GetData().guid == speech.guid);
                if (nodeView == null) continue;
                if (speech.nextNodeIDs == null || speech.nextNodeIDs.Count < 1) continue;
                foreach (var nextSpeechGuid in speech.nextNodeIDs)
                {
                    var nextNodeView = cacheNodesCreated.FirstOrDefault(n => n.GetData().guid == nextSpeechGuid);
                    if (nextNodeView == null) continue;
                    if (nextNodeView.GetData().GuiID == nodeView.GetData().GuiID) continue;
                    var edge = nodeView.OutputPort.ConnectTo(nextNodeView.InputPort);
                    AddElement(edge);
                }
            }

            UpdateConversationName(conversation.conversationName);
        }

        public void UpdateConversationName(string conversationName)
        {
            _conversationNameLabel ??= parent.Q<Label>(UIToolKitKeyManaged.Instance.idCurrentConversationName);

            _conversationNameLabel.text = conversationName;
        }
    }
}
#endif
