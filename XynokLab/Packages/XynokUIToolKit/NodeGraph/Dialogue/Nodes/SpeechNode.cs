﻿#if UNITY_EDITOR

using System.Linq;
using UnityEngine;
using XynokUIToolkit.Data;
using XynokUIToolkit.NodeGraph.Dialogue.Data;
using UnityEngine.UIElements;
using XynokUIToolkit.NodeGraph.Core;

namespace XynokUIToolkit.NodeGraph.Dialogue.Nodes
{
    public class SpeechNode : ANodeView<SpeechData>
    {
        private VisualElement _root;
        private TextField _content;
        private DropdownField _languageOptions;
        private DropdownField _actorOptions;
        private string _currentLanguageSelected;

        protected override void Init()
        {
            style.maxWidth = 350;
            style.width = new StyleLength(StyleKeyword.Auto);
            _root = UIToolKitKeyManaged.Instance.dialogueNode.Instantiate();
            _content = _root.Q<TextField>();
            _languageOptions = _root.Q<DropdownField>();
            _actorOptions = _root.Q<DropdownField>("actor_dropdown");

            extensionContainer.Add(_root);

            SetupLanguageOptions();
            SetupTextField();
            SetupActors();

            titleContainer.style.minHeight = 36;
        }

       

        protected override void OnDispose()
        {
            if (_content != null)
            {
                _content.UnregisterValueChangedCallback(OnUpdateContentValue);
            }
        }

        void SetupActors()
        {
            _actorOptions.choices = DialogueDatabase.Instance.allSpeakers.ToList();

            _actorOptions.SetValueWithoutNotify(Data.speaker);
            titleContainer.style.backgroundColor = DialogueDatabase.Instance.GetActorByName(Data.speaker).theme;

            _actorOptions.RegisterValueChangedCallback((e) =>
            {
                titleContainer.style.backgroundColor = DialogueDatabase.Instance.GetActorByName(e.newValue).theme;
                Data.speaker = e.newValue;
            });
        }

        void SetupLanguageOptions()
        {
            var currentSupportedLanguages = DialogueDatabase.Instance.allLanguages.ToList();
            _languageOptions.choices = currentSupportedLanguages;
            _currentLanguageSelected = currentSupportedLanguages[0];
            _languageOptions.SetValueWithoutNotify(_currentLanguageSelected);
            _languageOptions.RegisterValueChangedCallback(UpdateContentLanguage);
        }

        void UpdateContentLanguage(ChangeEvent<string> evt)
        {
            UpdateContentByLanguage(evt.newValue);
        }

        public void UpdateContentByLanguage(string lan)
        {
            _currentLanguageSelected = lan;
            var content = Data.GetContentByLanguage(_currentLanguageSelected);
            UpdateTitleView(content);
            _content.SetValueWithoutNotify(content);
            _languageOptions.SetValueWithoutNotify(lan);
        }

        void SetupTextField()
        {
            _content.multiline = true;
            title = _content.text;
            var titleElement = this.Q<Label>("title-label");
            titleElement.style.textOverflow = TextOverflow.Clip;
            titleElement.style.whiteSpace = WhiteSpace.NoWrap;
            titleElement.style.fontSize = 18f;
            titleElement.style.maxWidth = 350;
            titleElement.style.unityFontStyleAndWeight = FontStyle.Bold;

            var content = Data.GetContentByLanguage(_currentLanguageSelected);
            title = content;
            _content.SetValueWithoutNotify(content);

            // binding
            _content.RegisterValueChangedCallback(OnUpdateContentValue);
        }

        void OnUpdateContentValue(ChangeEvent<string> evt)
        {
            UpdateTitleView(evt.newValue);
        }

        void UpdateTitleView(string newContent)
        {
            title = newContent;
            Data.UpdateContentByLanguage(_currentLanguageSelected, newContent);
        }
    }
}
#endif
