﻿#if UNITY_EDITOR

using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;
using XynokUIToolkit.NodeGraph.Core;
using XynokUIToolkit.NodeGraph.Dialogue.Data;

namespace XynokUIToolkit.NodeGraph.Dialogue.Nodes
{
    public class BranchNode : ANodeView<BranchData>
    {
        private VisualElement _root;
        private EnumField _branchTypeField;

        protected override void Init()
        {
            titleContainer.style.minHeight = 27;
            _root = KeyManaged.branchNode.Instantiate();
            _branchTypeField = _root.Q<EnumField>();
            _branchTypeField.Init(Data.branchType);

            extensionContainer.Add(_root);

           var branchLabel= _branchTypeField.Q<Label>();
           branchLabel.style.display = DisplayStyle.None;
           branchLabel.style.visibility = Visibility.Hidden;
            
            _branchTypeField.RegisterValueChangedCallback((e) =>
            {
                Data.branchType = (BranchType) e.newValue;
                title = Data.branchType.ToString();
            });
            
            
            title = Data.branchType.ToString();
            style.maxWidth = 150;
            var titleElement = this.Q<Label>("title-label");
            titleElement.style.unityTextAlign = TextAnchor.MiddleCenter;
            // titleElement.style.unityFontStyleAndWeight = FontStyle.Bold;
            titleElement.style.fontSize = 16;
            titleElement.style.color = Database.branchTitleColor;
            titleContainer.style.backgroundColor = Database.branchColor;
        }

        protected override void CreateOutputPort()
        {
            OutputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(bool));
            OutputPort.portName = "Output";
            OutputPort.name = "Output";


            outputContainer.Add(OutputPort);
        }

        protected override void OnDispose()
        {
        }
    }
}
#endif
