﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using UnityEngine.UIElements;
using XynokUIToolkit.Data;
using XynokUIToolkit.NodeGraph.Core;
using XynokUIToolkit.NodeGraph.Dialogue.Data;

namespace XynokUIToolkit.NodeGraph.Dialogue.Nodes
{
    public class VariableNode : ANodeView<VariableData>
    {
        private VisualElement _root;
        private DropdownField _variableName;
        private EnumField _actionType;
        private EnumField _comparisonType;
        private EnumField _dataType;
        private EnumField _booleanField;
        private TextField _txtField;


        protected override void Init()
        {
            style.maxWidth = 270;
            _root = UIToolKitKeyManaged.Instance.variableNode.Instantiate();

            _variableName = _root.Q<DropdownField>(KeyManaged.idVariableName);
            _actionType = _root.Q<EnumField>(KeyManaged.idActionType);
            _comparisonType = _root.Q<EnumField>(KeyManaged.idCompareOperator);
            _dataType = _root.Q<EnumField>(KeyManaged.idDataType);
            _booleanField = _root.Q<EnumField>(KeyManaged.idBooleanField);
            _txtField = _root.Q<TextField>();


            HideLabel(_variableName);
            HideLabel(_actionType);
            HideLabel(_comparisonType);
            // HideLabel(_dataType);
            HideLabel(_booleanField);


            InitAllDataView();

            _dataType.RegisterValueChangedCallback((e) =>
            {
                Data.dataType = (VariableType)e.newValue;
                InitOptions();
                UpdateTitle();
            });
            _actionType.RegisterValueChangedCallback((e) =>
            {
                Data.actionType = (VariableActionType)e.newValue;
                InitOptions();
                UpdateTitle();
            });
            _comparisonType.RegisterValueChangedCallback((e) =>
            {
                Data.compareType = e.newValue.ToString();
                UpdateTitle();
            });
            _booleanField.RegisterValueChangedCallback((e) =>
            {
                Data.value = e.newValue.ToString();
                UpdateTitle();
            });
            _txtField.RegisterValueChangedCallback((e) =>
            {
                Data.value = e.newValue;
                UpdateTitle();
            });

            _variableName.RegisterValueChangedCallback((e) =>
            {
                Data = Database.variables.First(x => x.name.Equals(e.newValue)).Clone();
                InitAllDataView();
                Data.compareType = _comparisonType.value.ToString();
                Data.value = _dataType.value.Equals(VariableType.Boolean)
                    ? _booleanField.value.ToString()
                    : _txtField.value;
                UpdateTitle();
            });

            UpdateTitle();

            var titleElement = this.Q<Label>("title-label");
            titleElement.style.fontSize = 16;
            titleContainer.style.backgroundColor = Database.variableColor;
            titleContainer.style.color = Database.variableTitleColor;
            Add(_root);
        }

        void InitAllDataView()
        {
            InitVariableChoices();
            _dataType.Init(Data.dataType);
            _dataType.pickingMode = PickingMode.Ignore;
            _dataType.SetEnabled(false);
            _actionType.Init(VariableActionType.Compare);
            _booleanField.Init(VariableBooleanValue.True);
            _txtField.SetValueWithoutNotify(Data.value);
            InitOptions();

            Data.compareType = _comparisonType.value.ToString();
        }

        void UpdateTitle()
        {
            bool comparing = _actionType.value.Equals(VariableActionType.Compare);
            if (comparing) title = $"{Data.actionType} {Data.name} {Data.compareType} {Data.value}";
            if (!comparing) title = $"{Data.actionType} {Data.value} for {Data.name}";
        }

        void InitOptions()
        {
            bool str = _dataType.value.Equals(VariableType.String);
            bool boolean = _dataType.value.Equals(VariableType.Boolean);
            bool comparing = _actionType.value.Equals(VariableActionType.Compare);

            if (comparing)
            {
                if (str || boolean) _comparisonType.Init(VariableComparisonBoolAndStringType.Equal);
                if (!str && !boolean) _comparisonType.Init(VariableComparisonNumberType.Greater);
            }


            _booleanField.style.display = boolean ? DisplayStyle.Flex : DisplayStyle.None;
            _booleanField.style.visibility = boolean ? Visibility.Visible : Visibility.Hidden;

            _txtField.style.display = !boolean ? DisplayStyle.Flex : DisplayStyle.None;
            _txtField.style.visibility = !boolean ? Visibility.Visible : Visibility.Hidden;

            _comparisonType.style.display = comparing ? DisplayStyle.Flex : DisplayStyle.None;
            _comparisonType.style.visibility = comparing ? Visibility.Visible : Visibility.Hidden;
        }

        void InitVariableChoices()
        {
            // data
            List<string> variableNames = new List<string>();
            var variables = DialogueDatabase.Instance.variables;
            foreach (var variable in variables)
            {
                variableNames.Add(variable.name);
            }

            _variableName.choices = variableNames;
            _variableName.SetValueWithoutNotify(Data.name);
        }

        void HideLabel(VisualElement element)
        {
            var label = element.Q<Label>();
            label.style.display = DisplayStyle.None;
            label.style.visibility = Visibility.Hidden;
        }

        protected override void OnDispose()
        {
        }
    }
}
#endif
