﻿#if UNITY_EDITOR

using XynokUIToolkit.NodeGraph.Core;
using XynokUIToolkit.NodeGraph.Dialogue.Data;

namespace XynokUIToolkit.NodeGraph.Dialogue.Nodes
{
    public class EventNode: ANodeView<EventData>
    {
        protected override void Init()
        {
            
        }

        protected override void OnDispose()
        {
            
        }
    }
}
#endif
