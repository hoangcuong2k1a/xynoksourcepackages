﻿#if UNITY_EDITOR

using System.Linq;
using UnityEngine.UIElements;
using XynokUIToolkit.NodeGraph.Core;
using XynokUIToolkit.NodeGraph.Dialogue.Data;
using XynokUIToolkit.Utils;

namespace XynokUIToolkit.NodeGraph.Dialogue.Nodes
{
    public class EmitEventNode : ANodeView<EventData>
    {
        private VisualElement _root;
        private DropdownField _eventField;

        protected override void Init()
        {
            titleContainer.style.minHeight = 27;
            _root = KeyManaged.emitEventNode.Instantiate();
            _eventField = _root.Q<DropdownField>();
            _eventField.choices = Database.allEvents.ToList();
            _eventField.SetValueWithoutNotify( Data.name);
            _eventField.HideLabel();
            //
            extensionContainer.Add(_root);


            _eventField.RegisterValueChangedCallback((e) =>
            {
                Data.name = e.newValue;
                UpdateTitleView();
            });

            UpdateTitleView();

            //
            // var branchLabel= _branchTypeField.Q<Label>();
            // branchLabel.style.display = DisplayStyle.None;
            // branchLabel.style.visibility = Visibility.Hidden;
            //
            // _branchTypeField.RegisterValueChangedCallback((e) =>
            // {
            //     Data.branchType = (BranchType) e.newValue;
            //     title = Data.branchType.ToString();
            // });
            //
            //
            // title = Data.branchType.ToString();
            style.maxWidth = 150;
            var titleElement = this.Q<Label>("title-label");
            // titleElement.style.unityTextAlign = TextAnchor.MiddleCenter;
            // titleElement.style.unityFontStyleAndWeight = FontStyle.Bold;
            titleElement.style.fontSize = 16;
            titleElement.style.color = Database.eventTitleColor;
            titleContainer.style.backgroundColor = Database.eventColor;
        }

        void UpdateTitleView()
        {
            title = $"Emit: {Data.name}";
        }

        protected override void CreateOutputPort()
        {
        }

        protected override void OnDispose()
        {
        }
    }
}
#endif
