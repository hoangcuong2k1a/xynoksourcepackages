﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    [CreateAssetMenu(fileName = "DialogueDatabase", menuName = "Xynok/Toolkit/DialogueDatabase")]
    public class DialogueDatabase : ScriptableObject
    {
#if UNITY_EDITOR
        public static DialogueDatabase Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<DialogueDatabase>("Xynok/Toolkit/DialogueDatabase");
#endif
        [ReadOnly] public string lastEditedConversation = "???";
        [ReadOnly] public string currentLanguage = "vi";

        public List<ConversationData> conversations;
        public List<VariableData> variables;
        public List<EventData> events;

        public ActorData[] allActors;
        
        public Color branchColor = Color.cyan;
        public Color branchTitleColor = Color.black;
      
        public Color eventColor = Color.red;
        public Color eventTitleColor = Color.white;
        
        public Color variableColor = Color.magenta;
        public Color variableTitleColor = Color.white;
        public string[] allLanguages = new[] { "vi", "en" };
        
        
        [HideInInspector] public string[] allSpeakers;
        [HideInInspector] public string[] allEvents;

        private void OnValidate()
        {
            allSpeakers = allActors.Select(a => a.name).ToArray();
            allEvents = events.Select(a => a.name).ToArray();

            if (conversations == null || conversations.Count < 1)
            {
                lastEditedConversation = "???";
                return;
            }

            bool exist = conversations.Any(c => c.conversationName == lastEditedConversation);
            if (!exist) lastEditedConversation = "???";
        }

        #region Method Externals

        public void RemoveConversation(string conversationName)
        {
            var conversation = GetConversationByName(conversationName);
            if (conversation == null) return;
            conversations.Remove(conversation);
        }

        public ActorData GetActorByName(string actorName)
        {
            var actor = allActors.FirstOrDefault(a => a.name == actorName);
            if (actor == null)
                Debug.LogError($"Actor {actorName} not found");

            return actor;
        }

        public ConversationData GetLastEditConversation()
        {
            if (string.IsNullOrEmpty(lastEditedConversation) || string.IsNullOrWhiteSpace(lastEditedConversation)
                                                             || lastEditedConversation == "???") return null;

            return GetConversationByName(lastEditedConversation);
        }

        public ConversationData GetConversationByName(string conversationName)
        {
            var conversation = conversations.FirstOrDefault(c => c.conversationName == conversationName);
            if (conversation == null)
                Debug.LogError($"Conversation {conversationName} not found");

            return conversation;
        }

        public bool CreateNewConversation(string conversationName)
        {
            if (ExistConversation(conversationName)) return false;

            conversations.Add(new ConversationData
            {
                conversationName = conversationName
            });

            return true;
        }

        public void SetCurrentConversationEdit(string conversationName)
        {
            lastEditedConversation = conversationName;
        }

        public void AddSpeechToCurrentConversation(SpeechData speechData)
        {
            var conversation = GetConversationByName(lastEditedConversation);
            conversation.AddSpeech(speechData);
        }

        #endregion


        bool ExistConversation(string conversationName)
        {
            var conversation = conversations.FirstOrDefault(c => c.conversationName == conversationName);
            return conversation != null;
        }
    }
}

#endif
