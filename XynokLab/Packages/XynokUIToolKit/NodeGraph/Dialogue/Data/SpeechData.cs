﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using XynokUIToolkit.NodeGraph.Core;


namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
    [Serializable]
    public class SpeechData : ANodeData
    {
        public string speaker;
        public string content;
        public List<LanguageContentPairData> languageContentPairs;

        private string _defaultSpeechContent = "???";

        public void Init()
        {
            speaker = DialogueDatabase.Instance.allSpeakers[0];
            languageContentPairs = new();
            for (int i = 0; i < DialogueDatabase.Instance.allLanguages.Length; i++)
            {
                AddLanguage(DialogueDatabase.Instance.allLanguages[i]);
            }

            content = languageContentPairs[0].content;
        }

        public string GetContentByLanguage(string language)
        {
            if (!ExistsLanguage(language))
            {
                AddLanguage(language);
                return _defaultSpeechContent;
            }

            var result = languageContentPairs.Find(l => l.language == language).content;

            return result;
        }

        public void UpdateContentByLanguage(string language, string value)
        {
            if (!ExistsLanguage(language)) AddLanguage(language);
            var target = languageContentPairs.Find(l => l.language == language);
            target.content = value;
        }

        public void AddLanguage(string language)
        {
            if (ExistsLanguage(language)) return;
            languageContentPairs.Add(new LanguageContentPairData
            {
                language = language,
                content = _defaultSpeechContent
            });
        }

        bool ExistsLanguage(string language)
        {
            if (languageContentPairs == null || languageContentPairs.Count == 0) return false;
            return languageContentPairs.Exists(l => l.language == language);
        }
    }
}
#endif
