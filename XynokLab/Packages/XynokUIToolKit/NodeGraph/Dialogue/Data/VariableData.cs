﻿#if UNITY_EDITOR

using System;
using Sirenix.OdinInspector;
using XynokUIToolkit.NodeGraph.Core;

namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
    [Serializable]
    public class VariableData : ANodeData
    {
        public string name;
        public VariableType dataType;
        [ReadOnly] public VariableActionType actionType;
        [ReadOnly] public string compareType;
        [ReadOnly] public string value;
        [ReadOnly] public string inGameValue;


        public VariableData Clone()
        {
            return new VariableData
            {
                name = name,
                dataType = dataType,
#if UNITY_EDITOR

                guid = UnityEditor.GUID.Generate().ToString()
#endif
            };
        }
    }
}
#endif
