﻿#if UNITY_EDITOR

using System;
using XynokUIToolkit.NodeGraph.Core;

namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
    [Serializable]
    public class EventData: ANodeData
    {
        public string name;
    }
}
#endif
