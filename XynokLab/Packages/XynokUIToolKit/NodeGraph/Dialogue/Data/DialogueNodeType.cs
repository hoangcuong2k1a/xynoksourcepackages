﻿namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
    public enum DialogueNodeType
    {
        Speech,
        Variable,
        EventEmitter,
        Branch,
    }

    public enum VariableType
    {
        Boolean,
        Integer,
        Float,
        String,
    }

    public enum VariableComparisonBoolAndStringType
    {
        Equal,
        NotEqual,
    }
    public enum VariableBooleanValue
    {
        True,
        False,
    }
    public enum VariableComparisonNumberType
    {
        Equal,
        Greater,
        Less,
        GreaterOrEqual,
        LessOrEqual,
    }
    public enum VariableActionType
    {
        Compare,
        Set,
        Add,
    }
    
    public enum BranchType
    {
        Sequence,
        Random,
        All,
    }
}