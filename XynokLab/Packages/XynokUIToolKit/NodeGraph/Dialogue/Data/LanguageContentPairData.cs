﻿using System;

namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
    [Serializable]
    public class LanguageContentPairData
    {
        public string language;
        public string content;
    }
}