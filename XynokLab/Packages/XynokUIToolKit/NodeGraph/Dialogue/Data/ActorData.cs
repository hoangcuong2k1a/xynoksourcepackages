﻿#if UNITY_EDITOR

using System;
using UnityEngine;

namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
    [Serializable]
    public class ActorData
    {
        public string name;
        public Color theme;
    }
}
#endif
