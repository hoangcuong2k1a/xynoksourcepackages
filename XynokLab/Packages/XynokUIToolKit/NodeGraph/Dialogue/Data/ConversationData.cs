﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;

namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
    [Serializable]
    public class ConversationData
    {
        public string conversationName;
        public List<SpeechData> speeches;
        public List<BranchData> branches;
        public List<VariableData> variables;
        public List<EventData> events;

        public void AddSpeech(SpeechData data)
        {
            speeches ??= new();
            speeches.Add(data);
        }

        public void Add(BranchData data)
        {
            branches ??= new();
            branches.Add(data);
        }

        public void Add(VariableData data)
        {
            variables ??= new();
            variables.Add(data);
        }

        public void Add(EventData data)
        {
            events ??= new();
            events.Add(data);
        }
    }
}
#endif
