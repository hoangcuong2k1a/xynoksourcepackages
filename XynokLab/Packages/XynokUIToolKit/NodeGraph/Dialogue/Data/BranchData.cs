﻿#if UNITY_EDITOR

using System;
using XynokUIToolkit.NodeGraph.Core;

namespace XynokUIToolkit.NodeGraph.Dialogue.Data
{
    [Serializable]
    public class BranchData: ANodeData
    {
        public BranchType branchType;
    }
}
#endif
