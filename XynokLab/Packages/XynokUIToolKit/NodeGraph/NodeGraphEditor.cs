#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using XynokUIToolkit.Data;
using XynokUIToolkit.NodeGraph.Dialogue;

namespace XynokUIToolkit.NodeGraph
{
    public class NodeGraphEditor : EditorWindow
    {
        [SerializeField] private UIToolKitKeyManaged keyManaged = default;
        [SerializeField] private VisualTreeAsset visualTreeAsset = default;

        private VisualElement _root;

        // big windows
        private VisualElement _dialogue;

        // modulers
        private DialogueModule _dialogueModule;
        private DialogueBoardEditor _dialogueBoardEditor;


        [MenuItem("Xynok/UI Toolkit")]
        public static void ShowExample()
        {
            NodeGraphEditor wnd = GetWindow<NodeGraphEditor>();
            wnd.titleContent = new GUIContent("Xynok UI Toolkit");
        }


        public void CreateGUI()
        {
            _root = visualTreeAsset.Instantiate();


            _dialogue = _root.Q(keyManaged.idMainViewDialogue);
            _dialogueModule ??= new();
            _dialogueModule.SetDependency(_dialogue);

            _dialogueBoardEditor = _dialogue.Q<DialogueBoardEditor>();


            RegisterActiveMainView();
            VersionView();
            ShowMainView(keyManaged.idMainViewHome);
            HandleDefaultFont();

            rootVisualElement.Add(_root);

            _dialogueBoardEditor?.OnEnable();
        }

        void HandleDefaultFont()
        {
            _root.Query<VisualElement>().ForEach(e =>
            {
                e.style.unityFont = keyManaged.defaultFont;
                e.style.unityFontDefinition = new StyleFontDefinition(keyManaged.defaultFontAsset);
            });
        }

        void VersionView()
        {
            var version = _root.Q<Label>(keyManaged.idVersionTxt);
            version.text = keyManaged.versionValue;
        }

        void RegisterActiveMainView()
        {
            _root.Query(className: keyManaged.classBtnHomeBarLeft).ForEach((e) =>
            {
                e.AddManipulator(new Clickable(evt =>
                {
                    var mappingMainView = keyManaged.GetMappingMainView(e.name);
                    ShowMainView(mappingMainView);
                }));
            });
        }

        void ShowMainView(string mainViewName)
        {
            _root.Query(className: keyManaged.classMainView).ForEach((mainView) =>
            {
                bool sameView = mainView.name == mainViewName;
                mainView.style.display = sameView
                    ? DisplayStyle.Flex
                    : DisplayStyle.None;
                mainView.style.visibility = sameView
                    ? Visibility.Visible
                    : Visibility.Hidden;
            });
        }
    }
}
#endif