﻿using UnityEditor;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.UIElements;

namespace XynokUIToolkit.Data
{
    [CreateAssetMenu(fileName = "UIToolKitKeyManaged", menuName = "Xynok/Toolkit/UIToolKitKeyManaged")]
    public class UIToolKitKeyManaged : ScriptableObject
    {
#if UNITY_EDITOR
        public static UIToolKitKeyManaged Instance =>
            AssetDatabase.LoadAssetAtPath<UIToolKitKeyManaged>(
                "Packages/com.xynok.uitoolkit/Data/UIToolKitKeyManaged.asset");
#endif
        public string versionValue = "v0.0.1";
        public VisualTreeAsset dialogueNode;
        public VisualTreeAsset branchNode;
        public VisualTreeAsset variableNode;
        public VisualTreeAsset emitEventNode;
        public VisualTreeAsset btnOpenConversation;
        public Font defaultFont;
        public FontAsset defaultFontAsset;


        [Header("MAIN VIEW")]
        // main view
        public string classMainView = "main_view";

        public string idMainViewHome = "main_view_home";
        public string idMainViewSetting = "main_view_setting";
        public string idMainViewDialogue = "main_view_dialogue";

        [Header("LEFT BAR")]
        // left bar
        public string classBtnHomeBarLeft = "btn_home_bar_left";

        public string idBtnHomeBarLeft = "btn_home_bar_left_home";
        public string idBtnHomeBarSetting = "btn_home_bar_left_setting";
        public string idBtnHomeBarDialogue = "btn_home_bar_left_dialogue";


        [Header("DIALOGUE BAR")] public string classBtnDialogueHomeBar = "btn_home_bar_dialogue";
        public string idDialogueHomeBarNewFile = "btn_home_bar_dialogue_new_file";
        public string idDialogueHomeBarVariable = "btn_home_bar_dialogue_variable";
        public string idDialogueHomeBarEvent = "btn_home_bar_dialogue_event";
        public string idDialogueHomeBarLanguage = "btn_home_bar_dialogue_language";
        public string idDialogueHomeBarStorage = "btn_home_bar_dialogue_storage";
        public string idDialogueHomeBarActor = "btn_home_bar_dialogue_actor";

        // popups
        public string classPopupDialogueToolView = "popup_dialogue_tool_view";
        public string idPopupCreateNewDialogue = "popup_new_conversation";
        public string idPopupDialogueVariable = "popup_dialogue_variable";
        public string idPopupDialogueEvent = "popup_dialogue_event";
        public string idPopupDialogueActor = "popup_dialogue_actor";
        public string idPopupDialogueLanguage = "popup_dialogue_language";
        public string idPopupDialogueStorage = "popup_dialogue_storage";


        [Header("BUTTONS")] public string idBtnBackdrop = "backdrop";
        public string idBtnCreate = "btn_create";
        public string idBtnSave = "btn_save";
        public string idBtnOpen = "btn_open";
        public string idBtnDelete = "btn_delete";

        [Header("MODULER")] public string idVariableName = "variable_name";
        public string idDataType = "data_type";
        public string idCompareOperator = "compare_operator";
        public string idActionType = "action_type";
        public string idBooleanField = "boolean_field";

        public string classGraphView = "graphView";
        public string classZoomAble = "zoom_able";
        public string idNode = "node";
        public string idCurrentConversationName = "current_conversation_name";
        public string idLibrary = "library";
        public string idRootHome = "root_home";
        public string idVersionTxt = "version_txt";

        public string GetMappingMainView(string leftBarBtn)
        {
            if (leftBarBtn == idBtnHomeBarLeft) return idMainViewHome;
            if (leftBarBtn == idBtnHomeBarSetting) return idMainViewSetting;
            if (leftBarBtn == idBtnHomeBarDialogue) return idMainViewDialogue;
            Debug.LogError($"not found mapping main view with : <color=cyan>{leftBarBtn}</color>");
            return "???";
        }

        public string GetDialogueMappingToolView(string dialogueHomeBarBtn)
        {
            if (dialogueHomeBarBtn == idDialogueHomeBarNewFile) return idPopupCreateNewDialogue;
            if (dialogueHomeBarBtn == idDialogueHomeBarVariable) return idPopupDialogueVariable;
            if (dialogueHomeBarBtn == idDialogueHomeBarActor) return idPopupDialogueActor;
            if (dialogueHomeBarBtn == idDialogueHomeBarLanguage) return idPopupDialogueLanguage;
            if (dialogueHomeBarBtn == idDialogueHomeBarStorage) return idPopupDialogueStorage;
            if (dialogueHomeBarBtn == idDialogueHomeBarEvent) return idPopupDialogueEvent;
            Debug.LogError($"not found mapping dialogue popup view with : <color=cyan>{dialogueHomeBarBtn}</color>");
            return "???";
        }
    }
}