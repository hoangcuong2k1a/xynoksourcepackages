﻿#if UNITY_EDITOR

using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
using XynokUIToolkit.Data;

namespace XynokUIToolkit.Tests
{
    public class NodeView : Node
    {
        public Port InputPort;
        public Port OutputPort;
        public NodeView( )
        {
             // title = node.name;
            // var customContainer = new VisualElement();
        
            var customContainer = UIToolKitKeyManaged.Instance.dialogueNode.Instantiate();
       
            // customContainer.style.width = 400;
            // customContainer.style.backgroundColor = new StyleColor(new Color(0.2f, 0.2f, 0.2f));
            // var txt = new TextField()
            // {
            //     label = "TestTextField",
            //     value = "TestTextField",
            //     multiline = true
            // };
            // txt.labelElement.style.minWidth = 3;
            // txt.labelElement.style.width = 10;
            // txt.style.height = 100;
            // RectField rectField = new RectField();
            // var objectField = new ObjectField()
            // {
            //     objectType = typeof(GameObject),
            //     allowSceneObjects = true,
            //     value = null,
            //     label = "TestObjectField"
            // };
            //
            // var enumField = new EnumField(KeyCode.A)
            // {
            //     label = "TestEnumField",
            //     
            // };
            // var toggle = new Toggle()
            // {
            //     text = "TestToggle"
            // };
            // var btn = new Button()
            // {
            //     text = "TestButton",
            // };
            // rectField.RegisterValueChangedCallback(evt =>
            // {
            //     Debug.Log($"RectField changed to {evt.newValue}");
            // });
            //
            // toggle.RegisterValueChangedCallback(evt =>
            // {
            //     Debug.Log($"Toggle changed to {evt.newValue}");
            // });
            //
            //
            // customContainer.Add(txt);
            // customContainer.Add(rectField);
            // customContainer.Add(toggle);
            // customContainer.Add(btn);
            // customContainer.Add(objectField);
            // customContainer.Add(enumField);
            
            extensionContainer.Add(customContainer);
            CreateInputPort();
            CreateOutputPort();
            // RefreshExpandedState();
        }
        
        void CreateInputPort()
        {
            InputPort = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(bool));
            // InputPort.portName = "Input";
            // InputPort.name = "Input";
            InputPort.style.position = Position.Absolute;
           
            inputContainer.Add(InputPort);
        }
        void CreateOutputPort()
        {
            OutputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, typeof(bool));
            // OutputPort.portName = "Output";
            // OutputPort.name = "Output";
            outputContainer.Add(OutputPort);
        }
        
        
    }
}
#endif
