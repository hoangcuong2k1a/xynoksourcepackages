﻿#if UNITY_EDITOR

using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

namespace XynokUIToolkit.Core
{
    [Flags]
    public enum DefineSymbolData
    {
        None = 0,
        XYNOK_DEBUGGER_ALLOW_LOG = 1,
        BAKINGSHEET_RUNTIME_GOOGLECONVERTER = 2,
        BAKINGSHEET_EXTERNAL_LOGGING_DLL = 4,
        BAKINGSHEET_RUNTIME_CSVCONVERTER = 8,
    }

    public enum BuildTarget
    {
        None = 0,
        Standalone = 1,
        Android = 2,
        IOS = 3,
        WebGL = 4,
    }

    [XynokAdmin.Editor.XynokAdminDashboardData]
    [CreateAssetMenu(fileName = "CompilerDashboard", menuName = "Xynok/Compiler/CompilerDashboard")]
    public class CompilerDashboard : ScriptableObject
    {
        [Title("Define Symbols", "allow some code can be ran", TitleAlignments.Centered)]
        [Space(15)]
        [SerializeField]
        private DefineSymbolData defineSymbols;

        [SerializeField] private BuildTarget buildTarget;


        [HorizontalGroup("Split", 0.5f)]
        [Button(ButtonSizes.Medium), GUIColor(179 / 255f, 66 / 255f, 245 / 255f)]
        void ActiveSymbols()
        {
            var symbols = GetFinalSymbols();

            PlayerSettings.SetScriptingDefineSymbols(GetBuildTarget(), symbols);

            Debug.Log(
                $"<color=green>Đã thêm các symbol:</color>{defineSymbols.ToString()} vào trong quá trình build <color=cyan>{GetBuildTarget().TargetName}</color>");
        }


        [HorizontalGroup("Split", 0.5f)]
        [Button(ButtonSizes.Medium), GUIColor(0.4f, 0.8f, 1)]
        void LogTest()
        {
            string result = string.Empty;

            var currentSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(GetBuildTargetGroup());

            var ourSymbolsLog = $"<color=#b342f5>our symbols we want to active:</color> {defineSymbols.ToString()}";

            var projectSymbolsLog = $"<color=yellow>current project symbols:</color> {currentSymbols}";

            var targetFinal = $"<color=cyan>final symbols:</color> {string.Join(";", GetFinalSymbols())}";

            result += ourSymbolsLog + "\n";
            result += projectSymbolsLog + "\n";
            result += targetFinal + "\n";

            Debug.Log(result);
        }

        NamedBuildTarget GetBuildTarget()
        {
            switch (buildTarget)
            {
                case BuildTarget.Android: return NamedBuildTarget.Android;
                case BuildTarget.WebGL: return NamedBuildTarget.WebGL;
                case BuildTarget.IOS: return NamedBuildTarget.iOS;
            }

            return NamedBuildTarget.Standalone;
        }

        BuildTargetGroup GetBuildTargetGroup()
        {
            switch (buildTarget)
            {
                case BuildTarget.Android: return BuildTargetGroup.Android;
                case BuildTarget.WebGL: return BuildTargetGroup.WebGL;
                case BuildTarget.IOS: return BuildTargetGroup.iOS;
            }

            return BuildTargetGroup.Standalone;
        }

        string[] GetFinalSymbols()
        {
            string[] ourSymbols = defineSymbols.ToString().Replace(" ", "").Split(',');

            var allOurSymbols = (DefineSymbolData[])Enum.GetValues(typeof(DefineSymbolData));

            var projectSymbols =
                PlayerSettings.GetScriptingDefineSymbolsForGroup(GetBuildTargetGroup()).Split(';').ToList();

            // clear all our symbols from project symbols
            for (int i = 0; i < allOurSymbols.Length; i++)
            {
                var symbol = allOurSymbols[i].ToString();
                if (projectSymbols.Contains(symbol)) projectSymbols.Remove(symbol);
            }

            // if our symbols is none, return project symbols only
            if (ourSymbols.Length == 1 && ourSymbols[0] == "None") return projectSymbols.ToArray();

            // else concat project symbols and our symbols
            return projectSymbols.Concat(ourSymbols).ToArray();
        }
    }
}
#endif