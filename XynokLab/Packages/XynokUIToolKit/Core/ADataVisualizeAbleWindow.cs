﻿#if UNITY_EDITOR

using System;
using System.Linq;
using Sirenix.OdinInspector.Editor;
using UnityEditor;

namespace Core
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public abstract class AVisualizeAbleDataAttribute : Attribute
    {
    }

    public abstract class ADataVisualizeAbleWindow<T1, T2> : OdinMenuEditorWindow
        where T1 : ADataVisualizeAbleWindow<T1, T2>
        where T2 : AVisualizeAbleDataAttribute
    {
        protected virtual string RootPath => "Assets/";

        protected static Type[] TypesToDisplay = TypeCache.GetTypesWithAttribute<T2>()
            .OrderBy(e => e.Name).ToArray();

        private Type selectedType;

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree();
            foreach (var type in TypesToDisplay)
            {
                tree.AddAllAssetsAtPath(type.Name, RootPath, type, true, true);
            }
            return tree;
        }
    }
}
#endif