﻿#if UNITY_EDITOR

using Core;
using UnityEditor;

namespace XynokAdmin.Editor
{
    public class XynokAdminDashboardData : AVisualizeAbleDataAttribute
    {
    }

    public class XynokAdminDashboard : ADataVisualizeAbleWindow<XynokAdminDashboard,XynokAdminDashboardData>
    {

        [MenuItem("Xynok/Xynok Admin Dashboard", priority = 0)]
        private static void OpenWindow()
        {
            GetWindow<XynokAdminDashboard>().Show();
        }

      
    }
}
#endif