namespace XynokDebugger
{
    public static class Xylog
    {
        public static void Log(string message)
        {
#if XYNOK_DEBUGGER_ALLOW_LOG
            UnityEngine.Debug.Log(message);
#endif
        }

        public static void LogWarning(string message)
        {
#if XYNOK_DEBUGGER_ALLOW_LOG
            UnityEngine.Debug.LogWarning(message);
#endif
        }

        public static void LogError(string message)
        {
#if XYNOK_DEBUGGER_ALLOW_LOG
            UnityEngine.Debug.LogError(message);
#endif
        }
    }
}