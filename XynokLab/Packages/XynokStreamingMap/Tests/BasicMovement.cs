﻿using System;
using UnityEngine;

namespace Xynok.StreamingMap.Tests
{
    public class BasicMovement : MonoBehaviour
    {
        [SerializeField] private Transform mover;
        [SerializeField] private Vector3 moveDir;
        [SerializeField] private MapStreamingController mapStreamingController;
        [SerializeField] [Range(0f, 100f)] private float moveSpeed;
        [SerializeField] [Range(0f, 100f)] private float rotateSpeed;
        private Transform GlobalTransform => mover.gameObject.transform;

        public event Action<Vector3> OnMoving;
        private bool _updateNextFrame;


        private void Update()
        {
            var dirX = Input.GetAxis("Horizontal");
            var dirZ = Input.GetAxis("Vertical");
            moveDir = new Vector3(dirX, 0f, dirZ);

            if (moveDir == Vector3.zero)
            {
                _updateNextFrame = false;
                return;
            }

            GlobalTransform.position += moveDir * moveSpeed * Time.deltaTime;
            GlobalTransform.rotation = Quaternion.Slerp(GlobalTransform.rotation, Quaternion.LookRotation(moveDir),
                Time.deltaTime * rotateSpeed);
            _updateNextFrame = true;
        }

        private void LateUpdate()
        {
            if (!_updateNextFrame) return;
            mapStreamingController.StreamingPos(GlobalTransform.position);
            _updateNextFrame = false;
        }
    }
}