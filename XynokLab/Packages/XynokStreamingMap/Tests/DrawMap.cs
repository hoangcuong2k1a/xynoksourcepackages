﻿using Sirenix.OdinInspector;
using Unity.Mathematics;
using UnityEngine;
using Xynok.StreamingMap.Data;
using XynokUtils;

namespace Xynok.StreamingMap.Tests
{
    public class DrawMap : MonoBehaviour
    {
        [SerializeField] private MapData data;
        [SerializeField] [Range(1, 19)] private int distanceDetect = 1;
        [SerializeField] private Transform cellPrefab;
        [SerializeField] private Color normalColor = Color.gray;
        [SerializeField] private Color detectColor = Color.green;
        [SerializeField] private Color centerColor = Color.white;
        [ReadOnly] [SerializeField] private GameObject con;

        private CellData[,] _cells;
        private MeshRenderer[,] _renderers;

        private MeshRenderer[] _currentDetect;
        private MeshRenderer _currentCenter;

        #region Estimation

        [SerializeField] private BasicMovement mover;

        private MatrixIndex _currentEstimate;

        private void Start()
        {
            Draw();
            mover.OnMoving += Detect;
        }

        private void OnDestroy()
        {
            mover.OnMoving -= Detect;
        }


        void Detect(Vector3 moverPos)
        {
            if (!InsideMap(moverPos)) return;


            var estimate = GetCellOf(moverPos);
            
            if (estimate.index.Equal(_currentEstimate)) return;
            _currentEstimate = estimate.index;
            DetectAround(estimate.index.row, estimate.index.col);
        }

        CellData GetCellOf(Vector3 pos)
        {
            var w = data.colAmount;
            var h = data.rowAmount;

            var size = data.cellSize;
            var sizeOffset = size / 2;

            var col = (pos.x + sizeOffset + w * sizeOffset) / size - 1;
            var row = (pos.z + sizeOffset + h * sizeOffset) / size - 1;

            return _cells[(int)row, (int)col];
        }

        MatrixIndex EstimateMatrixIndex(Vector3 pos)
        {
            var cellEstimate = GetCellOf(pos);

            var cellArounds = MatrixUtil.GetElementsAround8Dir(_cells, cellEstimate.index, distanceDetect, true);
            float minDistance = float.MaxValue;
            int minIndex = -1;
            for (int i = 0; i < cellArounds.Length; i++)
            {
                var cell = _cells[cellArounds[i].index.row, cellArounds[i].index.col];

                var distance = math.distancesq(pos, cell.position);

                bool closer = distance <= minDistance;
                if (closer)
                {
                    minIndex = i;
                    minDistance = distance;
                }
            }

            return minIndex < 0 ? cellEstimate.index : cellArounds[minIndex].index;
        }


        bool InsideMap(Vector3 currentPos)
        {
            var minX = data.cellSize * data.colAmount / 2;
            var minZ = data.cellSize * data.rowAmount / 2;

            bool xValid = currentPos.x > -minX && currentPos.x < minX;
            bool zValid = currentPos.z > -minZ && currentPos.z < minZ;

            bool valid = xValid && zValid;

            if (!valid) Debug.LogWarning("mover outside of map !");
            return valid;
        }

        #endregion

        [Button]
        void DetectAround(int row, int col)
        {
            var cell = _cells[row, col];
            var nodes = MatrixUtil.GetElementsAround8Dir(_cells, cell.index, distanceDetect);

            ResetCurrentDetect();

            _currentCenter = _renderers[row, col];


            _currentDetect = new MeshRenderer[nodes.Length];
            int count = 0;


            Coloring(_currentCenter, centerColor);

            foreach (var node in nodes)
            {
                var nodeCell = _renderers[node.index.row, node.index.col];
                _currentDetect[count] = nodeCell;
                Coloring(nodeCell, detectColor);
                count++;
            }
        }

        void ResetCurrentDetect()
        {
            if (_currentCenter != null) Coloring(_currentCenter, normalColor);
            if (_currentDetect == null) return;
            foreach (var node in _currentDetect)
            {
                Coloring(node, normalColor);
            }
        }

        void Coloring(MeshRenderer meshRenderer, Color color)
        {
            if (Application.isPlaying)
            {
                meshRenderer.material.color = color;
            }
            else
            {
                meshRenderer.sharedMaterial.color = color;
            }
        }

        void ResetData()
        {
            _currentCenter = null;
            _currentDetect = null;
            _renderers = null;
        }

        [Button]
        void Draw()
        {
            if (con != null) DestroyImmediate(con);
            ResetData();

            con = new GameObject("container");

            _cells = new CellData[data.rowAmount, data.colAmount];
            _renderers = new MeshRenderer[data.rowAmount, data.colAmount];

            con.transform.position = Vector3.zero;

            MatrixIndex cellIndexData = new();

            for (int i = 0; i < data.colAmount; i++)
            {
                for (int j = 0; j < data.rowAmount; j++)
                {
                    int row = j;
                    int col = i;
                    cellIndexData.row = row;
                    cellIndexData.col = col;

                    var cellPos = GetCellPos(cellIndexData, data);

                    var cell = Instantiate(cellPrefab, con.transform);
                    cell.gameObject.transform.localScale =
                        new Vector3(data.cellSize, 1f, data.cellSize);
                    cell.gameObject.transform.position = cellPos;


                    cell.gameObject.name = $"Cell row{row} col{col}";

                    cell.gameObject.transform.SetParent(con.transform);

                    _cells[row, col] = new CellData
                    {
                        index = new MatrixIndex
                        {
                            row = row,
                            col = col,
                        },
                        position = cellPos,
                    };
                    _renderers[row, col] = cell.GetComponent<MeshRenderer>();
                    Coloring(_renderers[row, col], normalColor);
                }
            }
        }


        private Vector3 GetCellPos(MatrixIndex cellIndexData, MapData mapData)
        {
            var w = mapData.colAmount;
            var h = mapData.rowAmount;
            var row = cellIndexData.row;
            var col = cellIndexData.col;
            var size = mapData.cellSize;
            var sizeOffset = size / 2;

            var x = -(w * sizeOffset) + (col + 1) * size - sizeOffset;
            var z = -(h * sizeOffset) + (row + 1) * size - sizeOffset;

            return new Vector3(x, mapData.yAxis, z);
        }
    }
}