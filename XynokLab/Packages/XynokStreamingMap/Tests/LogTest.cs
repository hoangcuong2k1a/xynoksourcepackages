﻿using System;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokAsync;

namespace Xynok.StreamingMap.Tests
{
    public class LogTest : MonoBehaviour
    {
        [SerializeField] [Range(1f, 10f)] private float heavyTime = 3f;
        [SerializeField] private bool x;
        private PromiseTask _task;

        private void Start()
        {
            _task = new PromiseTask("test",HeavyTask, () => Debug.Log("Task done !"), heavyTime);
        }

        bool HeavyTask()
        {
            return x;
        }

        [Button]
        void Async1()
        {
            // await UniTask.WaitUntil(()=>x);
            // Debug.Log("Task done !");
            _task.Run();
        }

        [Button]
        public void Cancel()
        {
            _task.Cancel();
        }
    }
}