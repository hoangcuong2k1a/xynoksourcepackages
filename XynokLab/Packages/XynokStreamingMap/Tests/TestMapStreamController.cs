﻿using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.StreamingMap.Data;
using XynokAssetBundle;
using XynokAssetBundle.Data;
using XynokUtils;

namespace Xynok.StreamingMap.Tests
{
    [RequireComponent(typeof(AssetBundleLoader))]
    public class TestMapStreamController : MonoBehaviour
    {
        [SerializeField] private AssetBundleLoader assetBundleLoader;
        [SerializeField] private BasicMovement mover;
        [SerializeField] private MeshRenderer cellPrefab;
        [SerializeField] private MapStreamingDrawer drawer;
        [SerializeField] [Range(1, 19)] private int distanceDetect = 1;
        [SerializeField] private Color normalColor = Color.gray;
        [SerializeField] private Color detectColor = Color.green;
        [SerializeField] private Color centerColor = Color.white;
        [ReadOnly] [SerializeField] private GameObject con;
        [ReadOnly] [SerializeField] private MapStreamingData mapData;
        private CellData _currentEstimate;
        [ReadOnly] [SerializeField] private string[] bundleNames;
        private MeshRenderer[,] _renderers;

        private MeshRenderer[] _currentDetect;
        private CellData[] _currentDetectCell;
        private MeshRenderer _currentCenter;

        private void Start()
        {
            mover.OnMoving += Detect;
            assetBundleLoader.OnLoadInstance += OnLoadInstance;
        }

        void OnLoadInstance(int bundleData, GameObjectData instanceData, GameObject instance)
        {
            var cell = _currentDetectCell.FirstOrDefault(e => e.GetHashCode() == bundleData);

            var spawnPos = instanceData.position - (Vector3.zero - cell.position);
            instance.transform.position = spawnPos;
            instance.transform.rotation = instanceData.rotation;
            instance.transform.localScale = instanceData.localScale;
            instance.SetActive(instanceData.active);
        }

        private void OnDestroy()
        {
            mover.OnMoving -= Detect;
        }

        void Detect(Vector3 moverPos)
        {
            if (!mapData.InsideMap(moverPos)) return;

            var estimate = mapData.GetExactlyCellOf(moverPos);

            if (estimate.Equals(_currentEstimate)) return;
            _currentEstimate = estimate;


            DetectAround(estimate.index.row, estimate.index.col);
        }


        void DetectAround(int row, int col)
        {
            var cell = mapData.cells[row, col];
            ResetCurrentDetectColor();
            var nodes = MatrixUtil.GetElementsAround8Dir(mapData.cells, cell.index, distanceDetect);

            if (_currentDetectCell != null)
            {
              var  combineOldAndNewCell = nodes.Union(_currentDetectCell).ToArray(); // merge old and new cell
                var passedCells = combineOldAndNewCell.Except(nodes).ToArray();
                var newCells = combineOldAndNewCell.Except(_currentDetectCell).ToArray();
                _currentDetectCell = nodes;
                SpawnCells(newCells);
                ReleaseCells(passedCells);
            }
            else
            {
                _currentDetectCell = nodes;
                SpawnCells(_currentDetectCell);
            }

          

       
            _currentCenter = _renderers[row, col];
            _currentDetect = new MeshRenderer[nodes.Length];
            int count = 0;


            _currentCenter.Coloring(centerColor);

            foreach (var node in nodes)
            {
                var nodeCell = _renderers[node.index.row, node.index.col];
                _currentDetect[count] = nodeCell;
                nodeCell.Coloring(detectColor);
                count++;
            }
        }


        void SpawnCells(CellData[] cells)
        {
            foreach (var cell in cells)
            {
                assetBundleLoader.LoadBundle(cell.GetHashCode());
            }
        }

        void ReleaseCells(CellData[] cells)
        {
            foreach (var cell in cells)
            {
                assetBundleLoader.ReleaseBundle(cell.GetHashCode());
            }
        }

        void ResetCurrentDetectColor()
        {
            if (_currentCenter != null) _currentCenter.Coloring(normalColor);
            if (_currentDetect != null)
            {
                foreach (var node in _currentDetect)
                {
                    node.Coloring(normalColor);
                }
            }
        }

        [Button]
        void Draw()
        {
            if (con) DestroyImmediate(con);
            con = RuntimeUtils.CreateGameObject("map_container");
            mapData = drawer.Draw();
            bundleNames = new string[mapData.cells.Length];
            _renderers = new MeshRenderer[mapData.mapData.rowAmount, mapData.mapData.colAmount];

            int count = 0;
            for (int i = 0; i < mapData.cells.GetLength(0); i++)
            {
                for (int j = 0; j < mapData.cells.GetLength(1); j++)
                {
                    bundleNames[count] = mapData.cells[i, j].ToString();

                    RenderCell(mapData.cells[i, j]);
                    count++;
                }
            }
        }

        private void RenderCell(CellData cellData)
        {
            var cell = Instantiate(cellPrefab, cellData.position, Quaternion.identity, con.transform);
            cell.gameObject.name = cellData.ToString();
            cell.transform.localScale = new Vector3(mapData.mapData.cellSize, 1, mapData.mapData.cellSize);
            cell.Coloring(normalColor);
            _renderers[cellData.index.row, cellData.index.col] = cell;
        }
    }
}