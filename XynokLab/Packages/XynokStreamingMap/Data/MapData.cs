﻿using System;
using UnityEngine;
using XynokUtils;

namespace Xynok.StreamingMap.Data
{
    [Serializable]
    public struct MapData
    {
        public int rowAmount;
        public int colAmount;
        public float cellSize;
        public float yAxis;
    }


    [Serializable]
    public struct CellData
    {
        public MatrixIndex index;
        public Vector3 position;

        public bool Equals(CellData other)
        {
            return index.Equals(other.index) && position.Equals(other.position);
        }

        public override string ToString()
        {
            return index.ToString();
        }

        public override int GetHashCode()
        {
            return Animator.StringToHash(ToString());
        }
    }
}