﻿using System;
using Unity.Mathematics;
using UnityEngine;
using XynokUtils;

namespace Xynok.StreamingMap.Data
{
    [Serializable]
    public class MapStreamingData
    {
        public readonly MapData mapData;
        public readonly CellData[,] cells;

        public MapStreamingData(CellData[,] cells, MapData mapData)
        {
            this.cells = cells;
            this.mapData = mapData;
        }

        public CellData GetLikelyCellOf(Vector3 pos)
        {
            var w = mapData.colAmount;
            var h = mapData.rowAmount;

            var size = mapData.cellSize;
            var sizeOffset = size / 2;

            var col = (pos.x + sizeOffset + w * sizeOffset) / size - 1;
            var row = (pos.z + sizeOffset + h * sizeOffset) / size - 1;

            return cells[(int)row, (int)col];
        }

        public CellData GetExactlyCellOf(Vector3 pos)
        {
            var cellEstimate = GetLikelyCellOf(pos);

            var around = MatrixUtil.GetElementsAround8Dir(cells, cellEstimate.index, 1, true);
            float minDistance = float.MaxValue;
            int minIndex = -1;
            for (int i = 0; i < around.Length; i++)
            {
                var cell = cells[around[i].index.row, around[i].index.col];

                var distance = math.distancesq(pos, cell.position);

                bool closer = distance <= minDistance;
                if (closer)
                {
                    minIndex = i;
                    minDistance = distance;
                }
            }

            return minIndex < 0 ? cellEstimate : around[minIndex];
        }

        public bool InsideMap(Vector3 pos)
        {
            var minX = mapData.cellSize * mapData.colAmount / 2;
            var minZ = mapData.cellSize * mapData.rowAmount / 2;

            bool xValid = pos.x > -minX && pos.x < minX;
            bool zValid = pos.z > -minZ && pos.z < minZ;

            bool valid = xValid && zValid;

            // if (!valid) Debug.LogWarning($"pos {pos} outside of map !");
            return valid;
        }
    }
}