﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.StreamingMap.Data;
using XynokAssetBundle;
using XynokAssetBundle.Data;
using XynokUtils;

namespace Xynok.StreamingMap
{
    public enum DetectZoneAroundType
    {
        Likely,
        Exactly,
    }

    [RequireComponent(typeof(AssetBundleLoader))]
    public class MapStreamingController : MonoBehaviour
    {
        [SerializeField] [ReadOnly] private string namingCell = @"mapCell_{row}_{col}";
        [SerializeField] private MapStreamingDrawer drawer;
        [SerializeField] private DetectZoneAroundType detectType;
        [SerializeField] [Range(1, 19)] private int distanceDetect = 1;

        [HideInInspector] [SerializeField] private AssetBundleLoader assetBundleLoader;
        [HideInInspector] [SerializeField] private GameObject con;

        private CellData[] _currentDetectCell;
        private MapStreamingData mapData;
        private CellData _currentEstimate;

        private void OnValidate()
        {
            if (!assetBundleLoader) assetBundleLoader = GetComponent<AssetBundleLoader>();
        }


        public void StreamingPos(Vector3 moverPos, Action callOnOutsideMap = null)
        {
            if (!mapData.InsideMap(moverPos))
            {
                callOnOutsideMap?.Invoke();
                return;
            }

            var estimate = detectType == DetectZoneAroundType.Exactly
                ? mapData.GetExactlyCellOf(moverPos)
                : mapData.GetLikelyCellOf(moverPos);

            if (estimate.Equals(_currentEstimate)) return;
            _currentEstimate = estimate;


            DetectAround(estimate.index.row, estimate.index.col);
        }

        [Button]
        public void DrawMap()
        {
            if (con) DestroyImmediate(con);
            con = RuntimeUtils.CreateGameObject("streaming_map_container");
            mapData = drawer.Draw();
            assetBundleLoader.OnLoadInstance -= OnLoadInstance;
            assetBundleLoader.OnLoadInstance += OnLoadInstance;
        }

        void DetectAround(int row, int col)
        {
            var cell = mapData.cells[row, col];

            var nodes = MatrixUtil.GetElementsAround8Dir(mapData.cells, cell.index, distanceDetect, true);

            if (_currentDetectCell != null)
            {
                var combineOldAndNewCell = nodes.Union(_currentDetectCell).ToArray(); // merge old and new cell
                var passedCells = combineOldAndNewCell.Except(nodes).ToArray();
                var newCells = combineOldAndNewCell.Except(_currentDetectCell).ToArray();
                _currentDetectCell = nodes;
                SpawnCells(newCells);
                ReleaseCells(passedCells);
            }
            else
            {
                _currentDetectCell = nodes;
                SpawnCells(_currentDetectCell);
            }
        }

        void SpawnCells(CellData[] cells)
        {
            foreach (var cell in cells)
            {
                assetBundleLoader.LoadBundle(cell.GetHashCode());
            }
        }

        void ReleaseCells(CellData[] cells)
        {
            if (cells is { Length: < 1 }) return;
            foreach (var cell in cells)
            {
                assetBundleLoader.ReleaseBundle(cell.GetHashCode());
            }
        }

        void OnLoadInstance(int bundleData, GameObjectData instanceData, GameObject instance)
        {
            var cell = _currentDetectCell.FirstOrDefault(e => e.GetHashCode() == bundleData);
            
            if (cell.Equals(default)) return;
            
            var spawnPos = instanceData.position - (Vector3.zero - cell.position);
            instance.transform.position = spawnPos;
            instance.transform.rotation = instanceData.rotation;
            instance.transform.localScale = instanceData.localScale;
            instance.SetActive(instanceData.active);
        }

        private void OnDestroy()
        {
            if (assetBundleLoader) assetBundleLoader.OnLoadInstance -= OnLoadInstance;
        }
    }
}