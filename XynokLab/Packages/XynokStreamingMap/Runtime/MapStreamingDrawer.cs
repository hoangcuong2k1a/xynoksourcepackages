﻿using System;
using UnityEngine;
using Xynok.StreamingMap.Data;
using XynokUtils;

namespace Xynok.StreamingMap
{
    /// <summary>
    /// handles the drawing of the map
    /// </summary>
    [Serializable]
    public class MapStreamingDrawer
    {
        [SerializeField] private MapData data;

        private CellData[,] _cells;

        public MapStreamingData Draw()
        {
            _cells = new CellData[data.rowAmount, data.colAmount];

            MatrixIndex cellIndexData = new();

            for (int i = 0; i < data.colAmount; i++)
            {
                for (int j = 0; j < data.rowAmount; j++)
                {
                    int row = j;
                    int col = i;
                    cellIndexData.row = row;
                    cellIndexData.col = col;

                    var cellPos = GetCellPos(cellIndexData, data);

                    _cells[row, col] = new CellData
                    {
                        index = new MatrixIndex
                        {
                            row = row,
                            col = col,
                        },
                        position = cellPos,
                    };
                }
            }

            return new MapStreamingData(_cells, data);
        }


        Vector3 GetCellPos(MatrixIndex cellIndexData, MapData mapData)
        {
            var w = mapData.colAmount;
            var h = mapData.rowAmount;
            var row = cellIndexData.row;
            var col = cellIndexData.col;
            var size = mapData.cellSize;
            var sizeOffset = size / 2;

            var x = -(w * sizeOffset) + (col + 1) * size - sizeOffset;
            var z = -(h * sizeOffset) + (row + 1) * size - sizeOffset;

            return new Vector3(x, mapData.yAxis, z);
        }
    }
}