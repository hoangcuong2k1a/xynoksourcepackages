﻿using Unity.Entities;
using Unity.Physics.Systems;
using Unity.Transforms;

namespace XynokECS.SystemGroup
{
    [UpdateInGroup(typeof(InitializationSystemGroup), OrderLast = true)]
    public partial class XynokInitializationGroup : ComponentSystemGroup
    {
    }

    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    public partial class XynokSimulationGroup : ComponentSystemGroup
    {
    }

    [UpdateInGroup(typeof(PresentationSystemGroup))]
    [UpdateAfter(typeof(BeginPresentationEntityCommandBufferSystem))]
    public partial class XynokPresentationGroup : ComponentSystemGroup
    {
    }


    [UpdateInGroup(typeof(PhysicsSimulationGroup), OrderLast = true)]
    [UpdateAfter(typeof(PhysicsSolveAndIntegrateGroup))]
    public partial class XynokPhysicGroup : ComponentSystemGroup
    {
    }

    [UpdateInGroup(typeof(XynokInitializationGroup), OrderFirst = true)]
    public partial class XynokStructuralChangeGroup : ComponentSystemGroup
    {
    }
// /*------------------------------------- SYSTEMS ---------------------------------------------*/
//
//     [UpdateInGroup(typeof(XynokInitializationGroup))]
//     public partial struct MyInitSystem : ISystem
//     {
//         public void OnCreate(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Created");
//         }
//
//         public void OnUpdate(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Updating");
//         }
//
//         public void OnDestroy(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Destroyed");
//         }
//     }
//
//     [UpdateInGroup(typeof(XynokSimulationGroup))]
//     public partial struct MySimulationSystem : ISystem
//     {
//         public void OnCreate(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Created");
//         }
//         public void OnUpdate(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Updating");
//         }
//         public void OnDestroy(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Destroyed");
//         }
//     }
//
//     [UpdateInGroup(typeof(XynokPresentationGroup))]
//     public partial struct MyPresentationSystem : ISystem
//     {
//         public void OnCreate(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Created");
//         }
//         public void OnUpdate(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Updating");
//         }
//         public void OnDestroy(ref SystemState state)
//         {
//             Debug.Log($"{GetType().Name} Destroyed");
//         }
//     }
}