﻿using Unity.Collections;
using Unity.Entities;
using XynokSourceGenMarker.SourceGenMarkers;

namespace XynokECS.Tests
{
    public class MyBuffer
    {
        
    }
    public enum EntityGroup
    {
        None,
        Character,
        Item,
        Environment,
    }
    
    public enum EntityID
    {
        None,
        Hero01,
        Enemy01,
        Boss01,
    }
    [StatComponentCreator]
    public enum StatID
    {
        None,
        Health,
    }
    [InternalBufferCapacity(27)]
    public struct ExampleBufferComponent : IBufferElementData
    {
        public Entity target;
        public FixedString32Bytes component;
        public bool boolValue;
        public float floatValue;
    }
}