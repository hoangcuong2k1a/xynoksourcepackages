﻿using System;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace XynokECS.Tests
{
    public enum PrefabID
    {
        None = 0,
        Player = 1,
        System = 2,
        Bear = 3,
    }
    public struct TestBlobAsset
    {
        public int Value;
        
    }

    public struct BlobComponent : IComponentData
    {
        public BlobAssetReference<TestBlobAsset >Blob;
    }
    public struct PrefabCentral
    {
        public BlobArray<PrefabCentralChild> prefabs;
    }

    public class PrefabCentralStorage : IComponentData
    {
        public PrefabCentralChild[] prefabs;
    }

    [Serializable]
    public struct PrefabCentralChild
    {
        public PrefabID prefabID;
        public Entity Entity;
    }

    [Serializable]
    public class PrefabData
    {
        public PrefabID prefabID;
        public GameObject prefab;
    }

    public class WorldPrefabAuthor : MonoBehaviour
    {
        //public PrefabData[] prefabs;
public int Value;
        public class WorldPrefabBaker : Baker<WorldPrefabAuthor>
        {
            public override void Bake(WorldPrefabAuthor authoring)
            {
                // var entity = GetEntity(authoring, TransformUsageFlags.Dynamic);
                // var storage = new PrefabCentralStorage();
                //
                // for (int i = 0; i < authoring.prefabs.Length; i++)
                // {
                //     var child = new PrefabCentralChild
                //     {
                //         prefabID = authoring.prefabs[i].prefabID,
                //         Entity = GetEntity(authoring.prefabs[i].prefab, TransformUsageFlags.Dynamic)
                //     };
                //     storage.prefabs[i] = child;
                // }
                //
                // AddComponentObject(entity, storage);
                var customHash = new Unity.Entities.Hash128(
                    (uint) authoring.Value.GetHashCode(),
                    (uint) authoring.Value.GetHashCode()-1, 0, 0);
                
                if (!TryGetBlobAssetReference(customHash,
                        out BlobAssetReference<TestBlobAsset> blobReference))
                {
                    // Create a new builder that will use temporary memory to construct the blob asset
                    var builder = new BlobBuilder(Allocator.Temp);

                    // Construct the root object for the blob asset. Notice the use of `ref`.
                    ref TestBlobAsset marketData = ref builder.ConstructRoot<TestBlobAsset>();

                    // Now fill the constructed root with the data:
                    // Apples compare to Oranges in the universally accepted ratio of 2 : 1 .
                    marketData.Value = authoring.Value;

                    // Now copy the data from the builder into its final place, which will
                    // use the persistent allocator
                    blobReference =
                        builder.CreateBlobAssetReference<TestBlobAsset>(Allocator.Persistent);

                    // Make sure to dispose the builder itself so all internal memory is disposed.
                    builder.Dispose();

                    // Register the Blob Asset to the Baker for de-duplication and reverting.
                    AddBlobAssetWithCustomHash<TestBlobAsset>(ref blobReference, customHash);
                }

                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new BlobComponent() {Blob = blobReference});
            }
        }
    }
}