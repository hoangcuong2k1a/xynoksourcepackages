﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using Input = UnityEngine.Input;

namespace XynokECS.Tests
{
    // main thread
    public partial struct MySystemTestBinding2 : ISystem
    {
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<BlobComponent>();
        }

        public void OnUpdate(ref SystemState state)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log($"{MySingleton.Instance.Value}");
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                var eBlob = SystemAPI.GetSingleton<BlobComponent>();
                Debug.Log($"blob value: {eBlob.Blob.Value.Value}");
            }
        }
    }


    public readonly partial struct MyAspect : IAspect
    {
        // Your Aspect code
        readonly Entity _self;
        readonly RefRW<MyComponent> _data;
        readonly RefRO<MyComponentChanged> _dataChanged;

        public void Update(ref SystemState state)
        {
            _data.ValueRW.Value = _dataChanged.ValueRO.NewValue;
            state.SetActive<MyComponentChanged>(_self, false);
            state.SetActive<MyComponentEventCallbackEmitter>(_self, true);
        }

        public void Update(EntityCommandBuffer ecb)
        {
            _data.ValueRW.Value = _dataChanged.ValueRO.NewValue;
            ecb.SetComponentEnabled<MyComponentChanged>(_self, false);
            ecb.SetComponentEnabled<MyComponentEventCallbackEmitter>(_self, true);
            // state.SetActive<MyComponentChanged>(_self, false);
            // state.SetActive<MyComponentEventCallbackEmitter>(_self, true);
        }
    }

    [BurstCompile]
    public partial struct MyJob : IJobEntity
    {
        public EntityCommandBuffer Ecb;

        [BurstCompile]
        void Execute(MyAspect myAspect)
        {
            myAspect.Update(Ecb);
        }
    }

    // [UpdateAfter(typeof(MySystemTestBinding2))]
    public partial struct MySystemTestBinding : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            // foreach (var myAspect in SystemAPI.Query<MyAspect>())
            // {
            //     myAspect.Update(ref state);
            // }

            var ecb = new EntityCommandBuffer(Allocator.TempJob);
            MyJob job = new MyJob
            {
                Ecb = ecb
            };
            state.Dependency = job.Schedule(state.Dependency);

            state.Dependency.Complete();
            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }

    readonly partial struct MyAspectCallback : IAspect
    {
        // Your Aspect code
        readonly Entity _self;
        readonly RefRO<MyComponent> _data;
        readonly RefRO<MyComponentEventCallbackEmitter> _emitter;

        public void Update(ref SystemState state)
        {
            state.SetActive<MyComponentEventCallbackEmitter>(_self, false);
        }
    }

    [UpdateAfter(typeof(MySystemTestBinding))]
    public partial struct MySystemTestCallback : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            foreach (var myAspectCallback in SystemAPI.Query<MyAspectCallback>())
            {
                myAspectCallback.Update(ref state);
            }
        }
    }
}