﻿using Unity.Entities;

namespace XynokECS.Tests
{
    public static class SystemStateExtensions
    {
        public static void SetDataAndActive<T1>(this SystemState state, Entity entity, T1 data,
            bool activeRaiserValue = true)
            where T1 : unmanaged, IComponentData, IEnableableComponent
        {
            state.EntityManager.SetComponentData(entity, data);
            state.EntityManager.SetComponentEnabled<T1>(entity, activeRaiserValue);
        }

        public static void SetActive<T>(this SystemState state, Entity entity, bool activeValue)
            where T :
#if UNITY_DISABLE_MANAGED_COMPONENTS
            unmanaged,
#endif
            IEnableableComponent
        {
            state.EntityManager.SetComponentEnabled<T>(entity, activeValue);
        }
    }
}