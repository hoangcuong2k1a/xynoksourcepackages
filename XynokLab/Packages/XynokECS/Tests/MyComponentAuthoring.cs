﻿using Unity.Entities;
using UnityEngine;

namespace XynokECS.Tests
{
    public struct MyComponentEventCallbackEmitter : IComponentData, IEnableableComponent
    {
    }

    public struct MyComponent : IComponentData
    {
        public int Value;
    }

    public struct MyComponentChanged : IComponentData, IEnableableComponent
    {
        public int NewValue;
    }

    public class MyComponentAuthoring : MonoBehaviour
    {
        public int Value;
        public Entity entity;

        public class MyComponentBaker : Baker<MyComponentAuthoring>
        {
            public override void Bake(MyComponentAuthoring authoring)
            {
                authoring.entity = GetEntity(TransformUsageFlags.Dynamic);
                var activator = new MyComponentChanged
                {
                    NewValue = authoring.Value,
                };
                var data = new MyComponent
                {
                    Value = authoring.Value,
                };


                var emitter = new MyComponentEventCallbackEmitter();

                AddComponent( authoring.entity, data);
                AddComponent( authoring.entity, activator);
                AddComponent( authoring.entity, emitter);
            }
        }
    }
}