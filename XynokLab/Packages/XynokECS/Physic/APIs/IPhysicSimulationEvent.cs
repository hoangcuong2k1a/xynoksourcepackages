using Unity.Entities;
using Unity.Physics;
using XynokECS.Physic.Data;

namespace XynokECS.Physic.APIs
{

    public interface IPhysicSimulationEvent<T> : IBufferElementData, ISimulationEvent<T>
    {
        public PhysicState State { get; set; }
    }
}
