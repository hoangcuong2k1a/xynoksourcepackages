﻿using Unity.Entities;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.Components
{
    public struct PhysicTag: IComponentData
    {
        
    }
    
    public struct TriggerDetectTag : IComponentData
    {
    }

    public struct CollisionDetectTag : IComponentData
    {
    }
}