﻿using System;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace XynokECS.Physic.Components
{
    [Serializable]
    public struct EntityTag : IComponentData
    {
        [SerializeField] private FixedString32Bytes resourceID;
        public FixedString32Bytes Name => resourceID;

        public EntityTag(FixedString32Bytes resourceID)
        {
            this.resourceID = resourceID;
        }
    }
}