﻿using Unity.Collections;
using Unity.Entities;
using Unity.Physics;
using UnityEngine;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Components;

namespace XynokECS.Physic.Collision
{
    public struct CollisionEventJob: ICollisionEventsJob
    {

        [ReadOnly] public ComponentLookup<CollisionDetectTag> collisionEventImpulseData;
        [ReadOnly] public ComponentLookup<PhysicsVelocity> physicsVelocityData;

        public void Execute(CollisionEvent collisionEvent)
        {
            Entity entityA = collisionEvent.EntityA;
            Entity entityB = collisionEvent.EntityB;

            bool isBodyADynamic = physicsVelocityData.HasComponent(entityA);
            bool isBodyBDynamic = physicsVelocityData.HasComponent(entityB);

            bool isBodyARepulser = collisionEventImpulseData.HasComponent(entityA);
            bool isBodyBRepulser = collisionEventImpulseData.HasComponent(entityB);

            if (isBodyARepulser && isBodyBDynamic)
            {
                Debug.Log($" hit something First");
                 
            }

            if (isBodyBRepulser && isBodyADynamic)
            {
                Debug.Log($" hit something Second");
            }
        }
    }
}