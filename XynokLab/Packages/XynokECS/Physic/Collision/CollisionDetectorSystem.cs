﻿using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Components;

namespace XynokECS.Physic.Collision
{
    
// This system applies an impulse to any dynamic that collides with a Repulsor.
// A Repulsor is defined by a PhysicsShapeAuthoring with the `Raise Collision Events` flag ticked and a
// CollisionEventImpulse behaviour added.
    [RequireMatchingQueriesForUpdate]
    [UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
    [UpdateAfter(typeof(PhysicsSystemGroup))]
    public partial struct CollisionDetectorSystem: ISystem
    {
        internal ComponentDataHandles mComponentDataHandles;

        internal struct ComponentDataHandles
        {
            public ComponentLookup<CollisionDetectTag> collisionEventImpulseData;
            public ComponentLookup<PhysicsVelocity> physicsVelocityData;

            public ComponentDataHandles(ref SystemState systemState)
            {
                collisionEventImpulseData = systemState.GetComponentLookup<CollisionDetectTag>(true);
                physicsVelocityData = systemState.GetComponentLookup<PhysicsVelocity>(true);
            }

            public void Update(ref SystemState systemState)
            {
                collisionEventImpulseData.Update(ref systemState);
                physicsVelocityData.Update(ref systemState);
            }
        }

        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<SimulationSingleton>();
            state.RequireForUpdate(state.GetEntityQuery(ComponentType.ReadOnly<CollisionDetectTag>()));
            mComponentDataHandles = new ComponentDataHandles(ref state);
        }

        public void OnUpdate(ref SystemState state)
        {
            mComponentDataHandles.Update(ref state);

            CollisionEventJob collisionEventImpulseJob = new CollisionEventJob
            {
                collisionEventImpulseData = mComponentDataHandles.collisionEventImpulseData,
                physicsVelocityData = mComponentDataHandles.physicsVelocityData,
            };
            state.Dependency =
                collisionEventImpulseJob.Schedule(SystemAPI.GetSingleton<SimulationSingleton>(), state.Dependency);

        }

     
    }
}