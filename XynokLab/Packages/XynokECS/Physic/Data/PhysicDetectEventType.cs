﻿namespace XynokECS.Physic.Data
{
    public enum PhysicDetectEventType
    {
        None,
        
        /// <summary>
        /// source with raise trigger event and another body
        /// </summary>
        Trigger,
        
        /// <summary>
        /// emit when 2 bodies collide(Collide or Collide raise events)
        /// </summary>
        Collision,
    }
}