﻿using Unity.Entities;

namespace XynokECS.Physic.Data
{
    public struct CollisionEventDetails : IComponentData
    {
        public bool CalculateDetails;
    }
}