using Unity.Assertions;
using Unity.Entities;
using Unity.Physics;
using XynokECS.Physic.APIs;

namespace XynokECS.Physic.Data
{
    // Trigger Event that can be stored inside a DynamicBuffer
    public struct PhysicTriggerEvent : IPhysicSimulationEvent<PhysicTriggerEvent>
    {
        public Entity EntityA { get; set; }
        public Entity EntityB { get; set; }
        public int BodyIndexA { get; set; }
        public int BodyIndexB { get; set; }
        public ColliderKey ColliderKeyA { get; set; }
        public ColliderKey ColliderKeyB { get; set; }
        public PhysicState State { get; set; }

        public PhysicTriggerEvent(TriggerEvent triggerEvent) 
        {
            EntityA = triggerEvent.EntityA;
            EntityB = triggerEvent.EntityB;
            BodyIndexA = triggerEvent.BodyIndexA;
            BodyIndexB = triggerEvent.BodyIndexB;
            ColliderKeyA = triggerEvent.ColliderKeyA;
            ColliderKeyB = triggerEvent.ColliderKeyB;
            State = default;
        }

        // Returns other entity in EntityPair, if provided with one
        public Entity GetOtherEntity(Entity entity)
        {
            Assert.IsTrue((entity == EntityA) || (entity == EntityB));
            return (entity == EntityA) ? EntityB : EntityA;
        }

        // get source raises trigger
        public Entity GetRootEntity(Entity entity)
        {
            Assert.IsTrue((entity == EntityA) || (entity == EntityB));
            return (entity == EntityA) ? EntityA : EntityB;
        }

        public int CompareTo(PhysicTriggerEvent other) => ISimulationEventUtilities.CompareEvents(this, other);
    }
}