﻿using System;
using Unity.Assertions;
using Unity.Collections;
using Unity.Entities;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Components;
using XynokECS.Physic.Components;
using XynokECS.Physic.Core;
using XynokECS.Physic.Data;

namespace XynokECS.Physic.Trigger
{
    [UpdateInGroup(typeof(PhysicsSystemGroup))]
    [UpdateAfter(typeof(TriggerEventBufferSystem))]
    public partial class TriggerDetectorSystem : SystemBase
    {
        private EndFixedStepSimulationEntityCommandBufferSystem _commandBufferSystem;
        private EntityQuery _nonTriggerQuery;
        private EntityQueryMask _nonTriggerMask;
        public event Action<Vector3, FixedString32Bytes> OnTriggerEnter;

        protected override void OnCreate()
        {
            _commandBufferSystem = World.GetOrCreateSystemManaged<EndFixedStepSimulationEntityCommandBufferSystem>();

            _nonTriggerQuery =
                GetEntityQuery(new EntityQueryDesc
                {
                    None = new ComponentType[]
                    {
                        typeof(PhysicTriggerEvent)
                    }
                });
            Assert.IsFalse(_nonTriggerQuery.HasFilter(),
                "The use of EntityQueryMask in this system will not respect the query's active filter settings.");
            _nonTriggerMask = _nonTriggerQuery.GetEntityQueryMask();

            RequireForUpdate<TriggerDetectTag>();
        }

        protected override void OnUpdate()
        {
            // Need this extra variable here so that it can
            // be captured by Entities.ForEach loop below
            var nonTriggerMask = _nonTriggerMask;


            foreach (var (triggerEventBuffer, entity) in SystemAPI.Query<DynamicBuffer<PhysicTriggerEvent>>()
                         .WithEntityAccess())
            {
                for (int i = 0; i < triggerEventBuffer.Length; i++)
                {
                    var triggerEvent = triggerEventBuffer[i];
                    var otherEntity = triggerEvent.GetOtherEntity(entity);
                    var rootEntity = triggerEvent.GetRootEntity(entity);

                    // exclude other triggers and processed events
                    if (triggerEvent.State == PhysicState.Stay || !nonTriggerMask.MatchesIgnoreFilter(otherEntity))
                    {
                        continue;
                    }

                    if (triggerEvent.State == PhysicState.Enter)
                    {
                        if (SystemAPI.HasComponent<EntityTag>(rootEntity))
                        {
                            var tag = SystemAPI.GetComponent<EntityTag>(rootEntity);
                            OnTriggerEnter?.Invoke(SystemAPI.GetComponent<LocalTransform>(otherEntity).Position, tag.Name);
                        }

                      
                        //Debug.Log("hitted trigger");
                    }
                    else
                    {
                        //Debug.Log("out trigger");
                    }
                }
            }

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}