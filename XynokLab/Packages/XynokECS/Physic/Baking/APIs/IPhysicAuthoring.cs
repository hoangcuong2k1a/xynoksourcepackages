﻿using XynokECS.Physic.Data;

namespace XynokECS.Physic.Baking.APIs
{
    public interface IPhysicAuthoring
    {
        PhysicDetectEventType DetectEventType { get; }
    }
}