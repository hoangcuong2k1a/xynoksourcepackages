﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace XynokInput
{
    public class InputDeviceClickListener : MonoBehaviour
    {
        [ValueDropdown("Maps")] [SerializeField]
        private string targetMap;

        [ValueDropdown("Actions")] [SerializeField]
        private string action;

        [SerializeField] private UnityEvent onclick;

        private ActionInput _inputAction;

#if UNITY_EDITOR
        private static string[] Maps => EmbedInputMap.Instance.inputMaps;
        private static string[] Actions => EmbedInputMap.Instance.inputActions;
#endif
        private void OnEnable()
        {
            _inputAction = InputManager.Instance.GetInput(targetMap, action);
            _inputAction.OnStarted += InvokeAction;
        }

        void InvokeAction()
        {
            onclick?.Invoke();
        }

        private void OnDisable()
        {
            if (_inputAction != null) _inputAction.OnStarted -= InvokeAction;
        }
    }
}