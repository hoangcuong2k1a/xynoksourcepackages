﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using XynokConvention;
using XynokConvention.Patterns;
using XynokConvention.Procedural;

namespace XynokInput
{
    [RequireComponent(typeof(PlayerInput))]
    public class InputManager : ASingleton<InputManager>
    {
        [ReadOnly] [SerializeField] private PlayerInput playerInput;
        [SerializeField] private InputActionAsset inputActionAsset;


        private InputMap[] _inputMaps;
        private Action _onDispose;
        private string _currentMap;
        [ShowInInspector] public string CurrentControlScheme => _currentControlScheme;
        [ShowInInspector] public string CurrentMap => _currentMap;
        public PlayerInput PlayerInput => playerInput;

        private string _currentControlScheme="???";
        public event Action<string> OnControlSchemeChanged;

        private void OnValidate()
        {
            if (!playerInput) playerInput = GetComponent<PlayerInput>();
            if (playerInput && inputActionAsset) playerInput.actions = inputActionAsset;
        }

#if UNITY_EDITOR

        [Button]
        void Reload()
        {
            EmbedInputMap.Instance.Reload();
        }
#endif

        protected override void Awake()
        {
            base.Awake();
            Init();
            _currentMap = playerInput.currentActionMap.name;
        }


        /// <summary>
        /// active or deactive input response of current map only
        /// </summary>
        public void SetActiveInput(bool value)
        {
            if (value) playerInput.ActivateInput();
            else playerInput.DeactivateInput();
        }

        private void Start()
        {
            playerInput.actions.Enable();
        }

        public void ActiveInputMap(string map)
        {
            _currentMap = map;
            playerInput.SwitchCurrentActionMap(map);
            EventManager.EmitEvent(XynokGuiEvent.SwapInputMap.ToString());
        }

        public ActionInput GetInput(string map, string action)
        {
            var inputMap = _inputMaps.FirstOrDefault(e => e.Name == map);
            if (inputMap != null) return inputMap.GetInputAction(action);
#if XYNOK_DEBUGGER_ERROR_LOG

            Debug.LogError($"inputMap {map} does not exist");
#endif
            return null;
        }

        void Init()
        {
            if (inputActionAsset == null)
            {
#if XYNOK_DEBUGGER_ERROR_LOG
                Debug.LogError("inputActionAsset is null");
#endif
                return;
            }

            // init maps
            _inputMaps = new InputMap[inputActionAsset.actionMaps.Count];

            for (int i = 0; i < inputActionAsset.actionMaps.Count; i++)
            {
                int index = i;
                var map = inputActionAsset.actionMaps[index];

                _inputMaps[index] ??= new();
                _inputMaps[index].SetDependency(map);

                _onDispose += _inputMaps[i].Dispose;
            }
        }

         void Update()
        {
            if (_currentControlScheme == playerInput.currentControlScheme) return;
            _currentControlScheme = playerInput.currentControlScheme;
            OnControlSchemeChanged?.Invoke(_currentControlScheme);
        }

        private void OnDestroy()
        {
            _onDispose?.Invoke();
        }
    }
}