﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using XynokDebugger;

namespace XynokInput
{
    /// <summary>
    /// đã lỗi thời, chỉ dùng cho các input cố định ko thay đổi
    /// </summary>
#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    [CreateAssetMenu(fileName = "InputTextureManaged", menuName = "Xynok/Input/InputTextureManaged")]
    public class InputTextureManaged : ScriptableObject
    {
        [SerializeField] private InputActionAsset inputActionAsset;
        [SerializeField] private Sprite defaultTexture;
        [ValueDropdown("maps")] public string targetMap = "???";
        public InputSchemeTextureData[] schemes;

#if UNITY_EDITOR
        public static InputTextureManaged Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<InputTextureManaged>("Xynok/Input/InputTextureManaged");
        private static string[] maps => EmbedInputMap.Instance.inputMaps;
#endif
        public Sprite GetTexture(string scheme, string actionName)
        {
            var target = schemes.FirstOrDefault(x => x.scheme == scheme);
            if (target == null)
            {
                Xylog.LogError($"InputTextureManaged: scheme {scheme} not found");
                return defaultTexture;
            }

            var result = target.GetTexture(actionName);
            return result ? result : defaultTexture;
        }

#if UNITY_EDITOR
        [Button]
        void InitSchemeWithTargetMap()
        {
            var mapTarget = inputActionAsset.FindActionMap(targetMap);
            if (mapTarget == null)
            {
                Xylog.LogError($"InputTextureManaged: map {targetMap} not found");
                return;
            }

            var allSchemes = inputActionAsset.controlSchemes;
            if (schemes == null || schemes.Length < allSchemes.Count)
                schemes = new InputSchemeTextureData[allSchemes.Count];
            for (int i = 0; i < allSchemes.Count; i++)
            {
                // init scheme
                int index = i;
                schemes[index] ??= new InputSchemeTextureData();
                schemes[index].scheme = allSchemes[index].name;


                // init input textures
                if (schemes[index].inputTextures == null ||
                    schemes[index].inputTextures.Length < mapTarget.actions.Count)
                {
                    schemes[index].inputTextures = new InputTextureData[mapTarget.actions.Count];
                }

                for (int j = 0; j < mapTarget.actions.Count; j++)
                {
                    // init input texture
                    int index2 = j;
                    schemes[index].inputTextures[index2] ??= new InputTextureData();

                    // var group = schemes[index].scheme;
                    var actionName = mapTarget.actions[index2].name;
                    // var displayString = mapTarget.actions[index2].GetBindingDisplayString(group: group);
                    // var displayStringFormated = string.IsNullOrEmpty(displayString)?actionName: displayString;

                    var key = actionName;

                    schemes[index].inputTextures[index2].actionName = key;
                }
            }
        }
#endif
    }

    [Serializable]
    public class InputSchemeTextureData
    {
        [HorizontalGroup] [HideLabel] public string scheme;
        [HorizontalGroup] [HideLabel] public InputTextureData[] inputTextures;

        public Sprite GetTexture(string actionName)
        {
            var target = inputTextures.FirstOrDefault(x => x.actionName == actionName);
            if (target == null)
            {
                Xylog.LogError($"input [scheme {scheme}]: {actionName} not found");
                return null;
            }

            return target.texture;
        }
    }

    [Serializable]
    public class InputTextureData
    {
        [HorizontalGroup] [HideLabel] public string actionName;
        [HorizontalGroup] [HideLabel] public Sprite texture;
    }
}