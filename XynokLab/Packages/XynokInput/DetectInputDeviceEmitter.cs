﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using XynokConvention.Patterns;

namespace XynokInput
{
    public class DetectInputDeviceEmitter: ASingleton<DetectInputDeviceEmitter>
    {
        [SerializeField] [ReadOnly] private string currentDeviceName;
        public bool CurrentIsPCInput => _isKeyboardAndMouse;
        public event Action OnKeyboardAndMouse;
        public event Action OnGamepad;

        private bool _isKeyboardAndMouse;
        private void Start()
        {
            InputSystem.onActionChange += InputActionChangeCallback;
        }

        void InputActionChangeCallback(object obj, InputActionChange change)
        {
            void Detect()
            {
                InputAction receivedInputAction = (InputAction)obj;
                InputDevice lastDevice = receivedInputAction.activeControl.device;
                currentDeviceName = lastDevice.name;
                bool isPCInput = lastDevice.name.Equals("Keyboard") || lastDevice.name.Equals("Mouse");
                if (_isKeyboardAndMouse != isPCInput)
                {
                    _isKeyboardAndMouse = isPCInput;
                    if (isPCInput) OnKeyboardAndMouse?.Invoke();
                    if (!isPCInput) OnGamepad?.Invoke();
                }
            }
            if (change == InputActionChange.ActionStarted) Detect();
            if (change == InputActionChange.ActionPerformed) Detect();
        }
    }
}