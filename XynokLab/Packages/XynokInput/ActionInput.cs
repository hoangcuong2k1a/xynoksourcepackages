﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using XynokConvention.APIs;

namespace XynokInput
{
    public class ActionInput : IInjectable<InputAction>
    {
        public string name;
        public event Action OnStarted;
        public event Action OnPerformed;
        public event Action OnCanceled;
        public event Action<InputAction.CallbackContext> OnContextStarted;
        public event Action<InputAction.CallbackContext> OnContextPerformed;
        public event Action<InputAction.CallbackContext> OnContextCanceled;

        public InputAction source;
        private bool _started;
        private bool _canceled;
        private Action _overrider;

        public void SetDependency(InputAction dependency)
        {
            if (source != null) Dispose();
            source = dependency;
            if (source == null)
            {
                Debug.LogError($"InputAction {name} has Source null");
                return;
            }

            Init();
        }


        protected virtual void Init()
        {
            name =(source.name);

            source.started -= Started;
            source.started += Started;

            source.performed -= Performed;
            source.performed += Performed;

            source.canceled -= Canceled;
            source.canceled += Canceled;
        }

        /// <summary>
        /// dangerous method, use with caution
        /// </summary>
        public void AddOverride(Action context)
        {
            _overrider = default;
            _overrider += context;
        }

        public void ResetOverride()
        {
            _overrider = default;
        }

        void Started(InputAction.CallbackContext context)
        {
            if (InvokeOverride()) return;
            _started = true;
            _canceled = false;
            OnContextStarted?.Invoke(context);
            OnStarted?.Invoke();
        }

        bool InvokeOverride()
        {
            if (_overrider != default)
            {
                _overrider?.Invoke();
                return true;
            }

            return false;
        }

        void Performed(InputAction.CallbackContext context)
        {
            if (InvokeOverride()) return;
            OnContextPerformed?.Invoke(context);
            OnPerformed?.Invoke();
        }

        void Canceled(InputAction.CallbackContext context)
        {
            if (InvokeOverride()) return;
            _started = false;
            _canceled = true;
            OnContextCanceled?.Invoke(context);
            OnCanceled?.Invoke();
        }

        protected virtual void OnDispose()
        {
            _started = false;
            _canceled = false;

            source.started -= Started;
            source.performed -= Performed;
            source.canceled -= Canceled;

            OnStarted = default;
            OnContextStarted = default;

            OnCanceled = default;
            OnContextCanceled = default;

            OnPerformed = default;
            OnContextPerformed = default;
            name = default;

        }

        public void Dispose()
        {
            OnDispose();
        }
    }
}