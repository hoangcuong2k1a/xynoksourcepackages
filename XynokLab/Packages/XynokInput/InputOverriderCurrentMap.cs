﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace XynokInput
{
    public class InputOverriderCurrentMap : MonoBehaviour
    {

        [ValueDropdown("Actions")] [SerializeField]
        private string inputOverride;
        [SerializeField] private UnityEvent overrideEvent;

        private ActionInput _inputAction;

#if UNITY_EDITOR
        private static string[] Actions => EmbedInputMap.Instance.inputActions;
#endif
        private void Start()
        {
            overrideEvent.AddListener(OnDisable);
        }

        private void OnEnable()
        {
            _inputAction ??= InputManager.Instance.GetInput(InputManager.Instance.CurrentMap, inputOverride);
            _inputAction.AddOverride(overrideEvent.Invoke);
        }

        private void OnDisable()
        {
            _inputAction.ResetOverride();
        }
    }
}