﻿using System;
using Cathei.BakingSheet;
using Cathei.BakingSheet.Raw;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokBakingSheet.Runner
{
    public abstract class SheetPageScriptable : ScriptableObject
    {
        [SerializeField] [ReadOnly] protected TextAsset keyCredential;
        public event Action OnPullCompleted;

        public void SetKeyCredential(TextAsset keyText)
        {
            keyCredential = keyText;
        }
// #if UNITY_EDITOR
        public abstract void Pull();
        protected void AfterPullEventBase()
        {
            OnPullCompleted?.Invoke();
        }

        public void ClearEventAfterPull()
        {
            OnPullCompleted = default;
        }
// #endif
    }

    public abstract class ASheetPage<T> : SheetPageScriptable
        where T : SheetContainerBase
    {
        [Title("Google Sheet file", "tất cả các property của class này sẽ là một sheet nhỏ trong file GG Sheet",
            TitleAlignments.Centered)]
        [HideLabel]
        [SerializeField]
        protected SheetRawData setupData;


// #if UNITY_EDITOR

        [Button]
        public override async void Pull()
        {
            var container = Activator.CreateInstance<T>();
            if (container == null)
            {
                Debug.LogError($"{GetType().Name}: Container is null");
                return;
            }

            var ggSheetConverter = new GoogleSheetConverter(setupData.sheetId, keyCredential.text);
            var result = await container.Bake(ggSheetConverter);

            AfterPull(result, container);
            AfterPullEventBase();
        }

        /// <summary>
        /// do something after pull data
        /// </summary>
        /// <param name="pullResult">true if succeed</param>
        protected abstract void AfterPull(bool pullResult, T container);
// #endif
    }
}