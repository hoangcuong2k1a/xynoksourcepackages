﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokBakingSheet.Runner
{
#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    [CreateAssetMenu(fileName = "BakingSheetManaged", menuName = "Xynok/AssetBundle/BakingSheetManaged")]
    public class BakingSheetManaged : ScriptableObject
    {
        [Title("Google Credential", "which gmail is allowed to access gg sheet file (json format file)",
            TitleAlignments.Centered)]
        [SerializeField]
        protected TextAsset keyCredential;

        [Space(18)]
        [Title("Sheet Files", "", TitleAlignment = TitleAlignments.Centered)]
        [Space(10)]
        [HideLabel]
        [SerializeField]
        private SheetPageScriptable[] sheetPages;

        public string KeyCredential => keyCredential.text;
#if UNITY_EDITOR
        public static BakingSheetManaged Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<BakingSheetManaged>("Xynok/AssetBundle/BakingSheetManaged");

        [Button]
        void PullAll()
        {
            foreach (var sheetPage in sheetPages)
            {
                if (sheetPage)
                {
                    sheetPage.SetKeyCredential(keyCredential);
                    sheetPage.Pull();
                }
            }
        }
#endif
    }
}