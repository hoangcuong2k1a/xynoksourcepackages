﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace XynokCombatOop.Test
{
    public class NavMeshTest : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private Transform mover;
        [SerializeField] private NavMeshAgent agent;
        [SerializeField] private KeyCode moveKey = KeyCode.Space;
        [SerializeField] private KeyCode stopKey = KeyCode.A;

        private void Update()
        {
            if (Input.GetKeyDown(moveKey)) agent.SetDestination(target.position);
            if (Input.GetKeyDown(stopKey)) agent.isStopped = true;
        }
    }
}