﻿using UnityEngine;
using XynokGUI.Behaviors;
using XynokUtils.Physics;

namespace XynokCombatOop.HitBox.IteractiveObj
{
    public abstract class AInteractiveObject: DetectCallback2D
    {
        [Header("INTERACTIVE OBJECT SETUP")]
        [SerializeField] protected GuiInputBehavior swapInputOnEnter;
        [SerializeField] protected GuiInputBehavior swapInputOnExit;
    }
}