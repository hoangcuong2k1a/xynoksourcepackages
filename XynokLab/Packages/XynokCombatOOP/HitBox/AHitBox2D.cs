﻿using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle;
using XynokAssetBundle.Pooling;
using XynokCombatOop.APIs;
using XynokCombatOop.Data;
using XynokUtils.Physics;

namespace XynokCombatOop.HitBox
{
   

    public abstract class AHitBox2D : DetectCallback2D
    {
        [Header("HIT BOX DATA")] [SerializeField]
        protected bool spawnImpactOnHit;

        [ShowIf("spawnImpactOnHit")] [SerializeField]
        protected ImpactData hitImpact;

        protected virtual void OnValidate()
        {
#if UNITY_EDITOR
            FxPrefabManaged.Instance.EmitCurrentCategory(hitImpact.category);
#endif
        }

        protected virtual void Awake()
        {
           onCollisionEnter.EventCallback += SpawnImpact;
        }

        protected virtual void OnDestroy()
        {
            onCollisionEnter.EventCallback -= SpawnImpact;
        }

        void SpawnImpact(Collision2D col)
        {
            if (spawnImpactOnHit)
            {
                var impactAbleObj = col.gameObject.GetComponent<IImpactAble>();
                if (impactAbleObj != null) FxPool.Instance.Spawn(hitImpact.fxName, col.contacts[0].point);
            }
        }
    }
}