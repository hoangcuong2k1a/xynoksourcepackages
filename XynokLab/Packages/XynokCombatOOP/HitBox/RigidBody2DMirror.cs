﻿using UnityEngine;
using XynokConvention.Procedural;

namespace XynokCombatOop.HitBox
{
    public class RigidBody2DMirror : MonoBehaviour
    {
        [Header(" RigidBody 2D Mirror")] [SerializeField]
        protected MonoUpdateMode updateMode = MonoUpdateMode.FixedUpdate;

        [SerializeField] protected Rigidbody2D target;

        [SerializeField] protected Rigidbody2D mirror;
        [SerializeField] protected bool includeRotation = true;
        [SerializeField] protected bool includeScaleDirection = true;
        private Vector3 _originalMirrorScale;

        private void Start()
        {
            _originalMirrorScale = mirror.transform.localScale;
            mirror.gravityScale = 0;
        }

        protected virtual void Update()
        {
            if (updateMode == MonoUpdateMode.Update) Mirror();
        }

        protected virtual void FixedUpdate()
        {
            if (updateMode == MonoUpdateMode.FixedUpdate) Mirror();
        }

        protected virtual void LateUpdate()
        {
            if (updateMode == MonoUpdateMode.LateUpdate) Mirror();
        }

        void Mirror()
        {
            mirror.transform.position = target.transform.position;
            mirror.velocity = Vector2.zero;
            mirror.angularVelocity = target.angularVelocity;
            if (mirror.gravityScale != 0) mirror.gravityScale = 0;
            
            if(!includeRotation && !includeScaleDirection) return;
            
            var direction = target.transform.localScale.x > 0 ? 1 : -1;
            if (includeRotation)
            {
                mirror.transform.eulerAngles = target.transform.eulerAngles;
            }

            if (includeScaleDirection)
            {
                mirror.transform.localScale = new Vector3(direction * _originalMirrorScale.x, _originalMirrorScale.y,
                    _originalMirrorScale.z);
            }
        }
    }
}