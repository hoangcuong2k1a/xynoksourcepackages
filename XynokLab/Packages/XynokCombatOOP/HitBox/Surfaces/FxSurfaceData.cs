﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle;

namespace XynokCombatOop.HitBox.Surfaces
{
    public enum FxSurfaceDetectType
    {
        Collision,
        Trigger,
    }

    public enum FxSurfaceDetectMomentType
    {
        In,
        Out,
    }

    [Serializable]
    public class FxSurfaceData
    {
        [ValueDropdown("GetCategories")] public string category = "???";
        [ValueDropdown("GetNames")] public string fxName = "???";
        public FxSurfaceDetectType detectType;
        public FxSurfaceDetectMomentType detectMomentType;
       [ShowIf("detectType", FxSurfaceDetectType.Trigger)] public Vector3 spawnOffset;


#if UNITY_EDITOR
        private static string[] GetCategories => FxPrefabManaged.Instance.categories;
        private static string[] GetNames => FxPrefabManaged.Instance.currentPrefabs;

        [Button]
        void Sync()
        {
            FxPrefabManaged.Instance.EmitCurrentCategory(category);
        }
#endif
    }
}