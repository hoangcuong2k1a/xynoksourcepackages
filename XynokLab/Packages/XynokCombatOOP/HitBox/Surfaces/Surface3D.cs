﻿using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle.Pooling;
using XynokUtils.Physics;

namespace XynokCombatOop.HitBox.Surfaces
{
    [RequireComponent(typeof(DetectCallback3D))]
    public class Surface3D: MonoBehaviour
    {
        [SerializeField] [ReadOnly] protected DetectCallback3D detector;

        [Title("SURFACE COLLISION DATA", "Lưu ý: mọi thay đổi khi runtime sẽ ko có tác dụng")] [SerializeField]
        private FxSurfaceData impactData;

        protected virtual void OnValidate()
        {
            detector = GetComponent<DetectCallback3D>();
        }

        void OnEnable()
        {
            RegisterCallback();
        }

        void OnDisable()
        {
            DisposeCallback();
        }


        void RegisterCallback()
        {
            bool triggerIn = impactData.detectType == FxSurfaceDetectType.Trigger &&
                             impactData.detectMomentType == FxSurfaceDetectMomentType.In;
            bool triggerOut = impactData.detectType == FxSurfaceDetectType.Trigger &&
                              impactData.detectMomentType == FxSurfaceDetectMomentType.Out;
            bool collisionIn = impactData.detectType == FxSurfaceDetectType.Collision &&
                               impactData.detectMomentType == FxSurfaceDetectMomentType.In;
            bool collisionOut = impactData.detectType == FxSurfaceDetectType.Collision &&
                                impactData.detectMomentType == FxSurfaceDetectMomentType.Out;
            
            if (triggerIn) detector.onTriggerEnter.EventCallback += SpawnImpact;
            if (triggerOut) detector.onTriggerExit.EventCallback += SpawnImpact;
            if (collisionIn) detector.onCollisionEnter.EventCallback += SpawnImpact;
            if (collisionOut) detector.onCollisionExit.EventCallback += SpawnImpact;
           
        }

       void DisposeCallback()
        {
             
            detector.onTriggerEnter.EventCallback -= SpawnImpact;
             detector.onTriggerExit.EventCallback -= SpawnImpact;

             detector.onCollisionEnter.EventCallback -= SpawnImpact;
            detector.onCollisionExit.EventCallback -= SpawnImpact;
        }


        protected virtual void SpawnImpact(Collision col)
        {
            FxPool.Instance.Spawn(impactData.fxName, col.contacts[0].point);
        }

        protected virtual void SpawnImpact(Collider col)
        {
            FxPool.Instance.Spawn(impactData.fxName, col.transform.position);
        }
    }
}