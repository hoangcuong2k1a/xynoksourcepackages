﻿using System;
using Sirenix.OdinInspector;
using XynokAssetBundle;

namespace XynokCombatOop.Data
{
    [Serializable]
    public class ImpactData
    {
        [ValueDropdown("GetCategories")] public string category = "???";
        [ValueDropdown("GetNames")] public string fxName = "???";

#if UNITY_EDITOR
        private static string[] GetCategories => FxPrefabManaged.Instance.categories;
        private static string[] GetNames => FxPrefabManaged.Instance.currentPrefabs;
#endif
    }
}