﻿using System;
using UnityEngine;

namespace XynokCombatOop.Physic.Raycast
{
    [Serializable]
    public class Raycast2dHitData
    {
        public RaycastHit2D hit;
        public Collider2D hitCollider;
    }
}