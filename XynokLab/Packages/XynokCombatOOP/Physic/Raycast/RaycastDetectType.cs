﻿namespace XynokCombatOop.Physic.Raycast
{
    public enum RaycastDetectType
    {
        Distance,
        Infinite
    }
}