﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokCombatOop.Physic.Raycast
{
    [Serializable]
    public class Raycaster2d
    {
        [SerializeField] protected LayerMask interactable;
        [SerializeField] protected RaycastDetectType detectType = RaycastDetectType.Infinite;

        [ShowIf("detectType", RaycastDetectType.Distance)] [SerializeField]
        protected float rayDistance = 1f;

        private Raycast2dHitData _hitData;

        public event Action<Raycast2dHitData> OnHit;

        public bool Raycast(Vector2 origin, Vector2 direction)
        {
            bool hitSomething = false;
            _hitData ??= new();

            _hitData.hitCollider = null;
            if (detectType == RaycastDetectType.Distance)
            {
                _hitData.hit = Physics2D.Raycast(origin, direction, rayDistance, interactable);
                _hitData.hitCollider = _hitData.hit.collider;
            }

            if (detectType == RaycastDetectType.Infinite)
            {
                _hitData.hit = Physics2D.Raycast(origin, direction, Mathf.Infinity, interactable);
                _hitData.hitCollider = _hitData.hit.collider;
            }

            hitSomething = _hitData.hitCollider != null;

            if (hitSomething) OnHit?.Invoke(_hitData);

            return hitSomething;
        }
    }
}