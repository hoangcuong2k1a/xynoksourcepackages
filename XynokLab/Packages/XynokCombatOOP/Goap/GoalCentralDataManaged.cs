﻿using UnityEngine;

namespace XynokCombatOop.Goap
{
#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    // [CreateAssetMenu(fileName = "GoalCentralDataManaged", menuName = "Xynok/Combat/GoalCentralDataManaged")]
    public class GoalCentralDataManaged : ScriptableObject
    {
        public string[] orderExecutes = new[] { "Low", "Medium", "High", "Very High" };
#if UNITY_EDITOR
        public static GoalCentralDataManaged Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<GoalCentralDataManaged>("Xynok/Combat/GoalCentralDataManaged");
#endif
        
        
        public int GetOrderExecuteIndex(string orderExecute)
        {
            for (int i = 0; i < orderExecutes.Length; i++)
            {
                if (orderExecutes[i] == orderExecute) return i;
            }

            return -1;
        }
    }
}