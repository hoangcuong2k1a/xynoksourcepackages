﻿using Sirenix.OdinInspector;

namespace XynokCombatOop.Goap.Data
{
    public enum GoalStateType
    {
        None = 0,
        Executing = 1,
        Stopped = 2,
        Completed = 3,
    }

    public enum GoalConditionType
    {
        [LabelText("chỉ số", SdfIconType.SortNumericUp)]
        Stat = 0,

        [LabelText("trạng thái", SdfIconType.EmojiSmileFill)]
        State = 1,

        [LabelText("nội suy", SdfIconType.Person)]
        StatSelfCompare = 2,

        [LabelText("đối chiếu", SdfIconType.PersonPlus)]
        StatCompareAnotherOne = 3,

        [LabelText("Environment Stat", SdfIconType.ImageAlt)]
        EnvironmentStat = 4,

        [LabelText("Environment State", SdfIconType.ImageAlt)]
        EnvironmentState = 5,
    }
}