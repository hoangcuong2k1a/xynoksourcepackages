﻿using Sirenix.OdinInspector;

namespace XynokCombatOop.Goap.Data
{
    public enum ExecutorType
    {
        [LabelText("Tuần tự", SdfIconType.Calendar)]  Sequence,
        [LabelText("By Cost", SdfIconType.CurrencyDollar)] ByCost,
        [LabelText("Random", SdfIconType.Dice6)]Random,
    }
    
    public enum BargainType
    {
        Expensive,
        Cheapest,
    }
    
    public enum ResponsibilityType
    {
        [LabelText("Làm hết giờ thì nghỉ", SdfIconType.Clock)]  TimeOut,
        [LabelText("Until Completed", SdfIconType.CheckCircle)] UntilCompleted,
    }
    public enum ActiveHandlerType
    {
        [LabelText("một lần")]  OnceTime,
        [LabelText("nhiều lần")] MultipleTimes,
    }
}