﻿using System;
using Sirenix.OdinInspector;
using XynokConvention.Data;

namespace XynokCombatOop.Goap.Data
{
    [Serializable]
    public class AGoalCondition<T1, T2, T3, T4>
        where T1 : Enum
        where T2 : Enum
        where T3 : APairData<T1, float>
        where T4 : APairData<T2, bool>
    {
        [HorizontalGroup] [HideLabel] public GoalConditionType conditionType;

        [HorizontalGroup] [ShowIf("conditionType", GoalConditionType.Stat)] [HideLabel]
        public AGoalStatOperator<T1, T3> stat;

        [HorizontalGroup] [ShowIf("conditionType", GoalConditionType.State)] [HideLabel]
        public AGoalStateOperator<T2, T4> state;

        public bool Satisfy()
        {
            switch (conditionType)
            {
                case GoalConditionType.Stat:
                    return stat.Satisfy();
                case GoalConditionType.State:
                    return state.Satisfy();
                default:
                    return true;
            }
        }
    }
}