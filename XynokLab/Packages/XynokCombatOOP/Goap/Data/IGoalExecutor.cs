﻿using System;
using XynokCombatOop.APIs;

namespace XynokCombatOop.Goap.Data
{
    public interface IGoalExecutor: IChain<IGoalExecutor>
    {
        void Execute();
        void StopExecute();
        event Action OnStart;
        event Action OnStop;
        event Action OnCompleted;
    }
}