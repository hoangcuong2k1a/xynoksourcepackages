﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokConvention.APIs;
using XynokConvention.Data;

namespace XynokCombatOop.Goap.Data
{
    [Serializable]
    public class AGoalStatOperator<T1, T2> : IInjectable<T2>
        where T1 : Enum
        where T2 : APairData<T1, float>
    {
        [HorizontalGroup] [HideLabel] public T1 stat;
        [HorizontalGroup] [HideLabel] public Operator compareType;
        [HorizontalGroup] [HideLabel] public float compareValue;
        private T2 _statData;

        public bool Satisfy()
        {
            if (_statData == null)
            {
                Debug.LogError($"{stat} data is null, please check if it is injected correctly");
                return false;
            }

            switch (compareType)
            {
                case Operator.Equal:
                    return Math.Abs(_statData.Value - compareValue) < Mathf.Epsilon;
                case Operator.Greater:
                    return _statData.Value > compareValue;
                case Operator.LessThan:
                    return _statData.Value < compareValue;
                case Operator.LessThanOrEqual:
                    return _statData.Value <= compareValue;
                case Operator.GreaterOrEqual:
                    return _statData.Value >= compareValue;
            }

            return false;
        }

        public void SetDependency(T2 dependency)
        {
            _statData = dependency;
        }

        public void Dispose()
        {
        }
    }

    

    [Serializable]
    public class AGoalStateOperator<T1, T2> : IInjectable<T2>
        where T1 : Enum
        where T2 : APairData<T1, bool>
    {
        [HorizontalGroup] [HideLabel] public T1 state;
        [HorizontalGroup] [HideLabel] public bool compareValue;

        private T2 _stateData;

        public bool Satisfy()
        {
            if (_stateData == null) return false;
            return _stateData.Value == compareValue;
        }

        public void SetDependency(T2 dependency)
        {
            _stateData = dependency;
        }

        public void Dispose()
        {
        }
    }
    
    /*---------------------------------------- ADVANCEDs -------------------------------------------*/
    
    
    [Serializable]
    public class AStatSelfCompareOperator<T1, T2> : IInjectable<T2>
        where T1 : Enum
        where T2 : APairData<T1, float>
    {
        [HorizontalGroup] [HideLabel] public T1 stat;
        [HorizontalGroup] [HideLabel] public Operator compareType;

        [HorizontalGroup] [HideLabel][SuffixLabel("% giá trị gốc", Overlay = true)]
        public float percent;

        private T2 _statData;

        public bool Satisfy()
        {
            if (_statData == null)
            {
                Debug.LogError($"{stat} data is null, please check if it is injected correctly");
                return false;
            }

            var compareValue= _statData.BaseValue * percent;
            switch (compareType)
            {
                case Operator.Equal:
                    return Math.Abs(compareValue - _statData.Value) < Mathf.Epsilon;
                case Operator.Greater:
                    return _statData.Value > compareValue;
                case Operator.LessThan:
                    return _statData.Value < compareValue;
                case Operator.LessThanOrEqual:
                    return _statData.Value <= compareValue;
                case Operator.GreaterOrEqual:
                    return _statData.Value >= compareValue;
            }

            return false;
        }

        public void SetDependency(T2 dependency)
        {
            _statData = dependency;
        }

        public void Dispose()
        {
        }
    }

    [Serializable]
    public abstract class AStatCompareAnotherOneOperatorProperties<T1,T2>
        where T1 : Enum
        where T2 : APairData<T1, float>
    {
        public T2 stat;
        public T2 another;
    }
    [Serializable]
    public class AStatCompareAnotherOneOperator<T1, T2> : IInjectable<AStatCompareAnotherOneOperatorProperties<T1,T2>>
        where T1 : Enum
        where T2 : APairData<T1, float>
    {
        [HorizontalGroup] [HideLabel] public T1 stat;
        [HorizontalGroup] [HideLabel] public Operator compareType;
        [HorizontalGroup] [HideLabel] public T1 another;


        private AStatCompareAnotherOneOperatorProperties<T1,T2> _properties;

        public bool Satisfy()
        {
            if (_properties == null)
            {
                Debug.LogError($"{stat} compare with {another} data is null, please check if it is injected correctly");
                return false;
            }

            var value1 = _properties.stat.Value;
            var value2 = _properties.another.Value;
            switch (compareType)
            {
                case Operator.Equal:
                    return Math.Abs(value1 - value2) < Mathf.Epsilon;
                case Operator.Greater:
                    return value1 > value2;
                case Operator.LessThan:
                    return value1 < value2;
                case Operator.LessThanOrEqual:
                    return value1 <= value2;
                case Operator.GreaterOrEqual:
                    return value1 >= value2;
            }

            return false;
        }

        public void SetDependency(AStatCompareAnotherOneOperatorProperties<T1,T2> dependency)
        {
            _properties = dependency;
        }

        public void Dispose()
        {
        }
    }
}