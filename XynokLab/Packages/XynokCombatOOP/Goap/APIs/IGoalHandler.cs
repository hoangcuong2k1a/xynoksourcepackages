﻿using System;
using XynokUtils.APIs;

namespace XynokCombatOop.Goap.APIs
{
    /// <summary>
    /// implement this interface to create a goal handler
    /// </summary>
    /// <typeparam name="TEnum">ability</typeparam>
    /// <typeparam name="THandler">implementor of handler</typeparam>
    public interface IGoalHandler<out TEnum, out THandler>: IExecuteLite
        where TEnum : Enum
    {
        TEnum HandleID { get; }
        event Action<THandler> OnExecute;
        event Action<THandler> OnStop;
        event Action<THandler> OnReset;
    }
}