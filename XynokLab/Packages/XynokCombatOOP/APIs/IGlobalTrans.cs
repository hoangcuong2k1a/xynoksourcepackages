﻿using UnityEngine;

namespace XynokCombatOop.APIs
{
    public interface IGlobalTrans
    {
        Transform GlobalTrans { get; }
    }
}