﻿using System;


namespace XynokCombatOop.APIs
{
    public interface IMonitor<in T1, T2, in T3>
        where T1 : Enum
        where T3 : IControllable<T1, T2>
    {
        void GetIn(T3 member);
        void GetOut(T3 member);
        void RequestFor(T3 member, Action<bool> request);
    }

    public interface IControllable<out T, out T2>
        where T : Enum
    {
        T IdControllable { get; }
        T2 ControllableData { get; }
    }
}