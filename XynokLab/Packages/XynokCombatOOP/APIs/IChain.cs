﻿
namespace XynokCombatOop.APIs
{
    public interface IChain<T>
    {
        void SetNext(T next);
        T Next { get; }    
    }
}