﻿namespace XynokCombatOop.AI.Data
{
    public interface IStrategy
    {
        void Execute();
    }
    public class ACharacteristic<T> where T : IStrategy
    {
        
    }
}