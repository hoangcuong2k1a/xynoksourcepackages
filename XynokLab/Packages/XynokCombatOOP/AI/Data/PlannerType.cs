﻿namespace XynokCombatOop.AI.Data
{
    /// <summary>
    /// cách AI sắp xếp thứ tự thực hiện
    /// </summary>
    public enum PlannerType
    {
        Linear,
        Priority,
        Instinct,
    }
    public enum PersonalityType
    {
        None,
        Instinct, // bản năng
        Experience, // kinh nghiệm
    }
    // /// <summary>
    // /// cách AI thực hiện hành vi
    // /// </summary>
    // public enum ExecutorType
    // {
    //     Single,
    //     Polymorphism,
    //     Saver,
    //     Abstract
    // }
}