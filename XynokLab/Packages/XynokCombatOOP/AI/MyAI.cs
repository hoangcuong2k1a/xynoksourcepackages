﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokCombatOop.AI
{
    public interface IBehaviour
    {
        void Execute();
    }
    
    public class MyAI: MonoBehaviour, IBehaviour
    {
        [SerializeField] private IBehaviour _behaviour;
        [SerializeField] private Type randomType;

        [Button]
        void Test()
        {
            _behaviour = this;
        }
        
        public void Execute()
        {
            
        }
    }
}