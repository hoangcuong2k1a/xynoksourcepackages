﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace XynokCombatOop.Modules
{
    [Serializable]
    public class MoveToPosition
    {
        [Title("Move To Position", "", TitleAlignments.Centered)] [SerializeField]
        private Transform mover;

        [SerializeField] private float speed = 12;
        [SerializeField] private Ease easeMove = Ease.Linear;
        [SerializeField] private bool forceCompleteOnStackExecute;

        [FoldoutGroup("EVENTs")] [SerializeField]
        private UnityEvent onMoveStart;

        [FoldoutGroup("EVENTs")] [SerializeField]
        private UnityEvent onMoving;

        [FoldoutGroup("EVENTs")] [SerializeField]
        private UnityEvent onMoveCompleted;

        private Tween _tweenMove;
        protected virtual Vector3 TargetPosition => _targetPosition;

        private Vector3 _targetPosition;

        public void SetTargetPosition(Vector3 targetPosition)
        {
            _targetPosition = targetPosition;
        }
        [Button]
        public void Move()
        {
            var distance = Vector3.Distance(mover.gameObject.transform.position, TargetPosition);

            _tweenMove?.Kill(forceCompleteOnStackExecute);

            _tweenMove = mover.DOMove(TargetPosition, distance / speed).SetEase(easeMove)
                .OnStart(() => { onMoveStart.Invoke(); })
                .OnUpdate(() => { onMoving.Invoke(); })
                .OnComplete(() => { onMoveCompleted.Invoke(); });
        }

        public void StopMove(bool complete = false)
        {
            _tweenMove?.Kill(complete);
        }

        private void OnDestroy()
        {
            _tweenMove?.Kill();
        }
    }
}