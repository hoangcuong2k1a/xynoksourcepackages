﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace XynokCombatOop.Modules._2D.KnockBack
{
    public class KnockBackReceivier : MonoBehaviour, IKnockBackAble
    {
        [Header("DEFAULT SETUP")] [SerializeField]
        protected Rigidbody2D rb;

        [SerializeField] protected bool resetVelocityOnComplete = true;
        [SerializeField] protected GameObject knockBackReceiver;
        [SerializeField] protected UnityEvent onKnockBack;
        [SerializeField] protected UnityEvent onKnockBackCompleted;

        private KnockBackData2D _knockBackData2D;
        private Coroutine _coroutine;


        public virtual void KnockBack(KnockBackData2D knockBackData2D)
        {
            _knockBackData2D = knockBackData2D;
            StopKnockBack();

            _coroutine = StartCoroutine(KnockBack());
        }

        public void StopKnockBack(bool forceCompleted = true)
        {
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
                _coroutine = null;
            }

            if (forceCompleted) Complete();
        }

        protected virtual void OnDisable()
        {
            StopKnockBack();
        }

        protected virtual  void Complete()
        {
            if (resetVelocityOnComplete) rb.velocity = Vector2.zero;
            onKnockBackCompleted?.Invoke();
        }
        protected virtual IEnumerator KnockBack()
        {
            onKnockBack?.Invoke();
            float startTime = Time.time;
            while (Time.time - startTime <= _knockBackData2D.AccelForceDuration)
            {
                rb.velocity = _knockBackData2D.GetForceAccel(knockBackReceiver.gameObject.transform);
                yield return null;
            }

            startTime = Time.time;


            while (Time.time - startTime <= _knockBackData2D.DeccelForceDuration)
            {
                rb.velocity = _knockBackData2D.GetForceDeccel(knockBackReceiver.gameObject.transform);
                yield return null;
            }

            Complete();
        }
    }
}