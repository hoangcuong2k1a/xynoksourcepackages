﻿using Sirenix.OdinInspector;
using UnityEngine;
using XynokCombatOop.HitBox;

namespace XynokCombatOop.Modules._2D.KnockBack
{
    /// <summary>
    /// Khi hitbox collision với obj, nếu obj đó là một IKnockBackAble thì sẽ knockBack
    /// </summary>
    public class KnockBacker : MonoBehaviour
    {
        [Title("DEFAULT SETUP", "Khi hitbox collision với obj, nếu obj đó là một IKnockBackAble thì sẽ knockBack")]
        [SerializeField]
        protected GameObject knockBacker;

        [SerializeField] protected AHitBox2D hitBox;
        [SerializeField] protected KnockBackData2D knockBackData2D;

        protected virtual void Start()
        {
            hitBox.onCollisionEnter.EventCallback += KnockBack;
            knockBackData2D.SetKnockBacker(knockBacker.transform);
        }

        protected virtual void KnockBack(Collision2D col)
        {
            if (!col.gameObject.activeSelf) return;
            var knockbackReceiver = col.gameObject.GetComponent<IKnockBackAble>();
            knockbackReceiver?.KnockBack(knockBackData2D);
        }
    }
}