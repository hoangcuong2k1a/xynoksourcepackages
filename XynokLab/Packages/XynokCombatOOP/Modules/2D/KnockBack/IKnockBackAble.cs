﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokCombatOop.Modules._2D.KnockBack
{
    public interface IKnockBackAble
    {
        void KnockBack(KnockBackData2D knockBackData2D);
    }

    public enum KnockBackType
    {
        None = 0,
        Position = 1,
        LocalScale = 2,
    }

    [Serializable]
    public class KnockBackData2D
    {
        [SerializeField] private Vector2 direction;
        [SerializeField] private KnockBackType knockBackType;


        [Range(0f, 500f)] [SerializeField] private float forceAccelPower;
        [Range(0f, 500f)] [SerializeField] private float forceDeccelPower;
        [Range(0f, 3f)] [SerializeField] private float accelForceDuration;
        [Range(0f, 3f)] [SerializeField] private float deccelForceDuration;

        private Transform _knockBacker;

        public float AccelForceDuration => accelForceDuration;
        public float DeccelForceDuration => deccelForceDuration;

        public void SetKnockBacker(Transform knockBacker)
        {
            _knockBacker = knockBacker;
        }


        public Vector2 GetForceAccel(Transform knockBackerReceiver) => GetDir(forceAccelPower, knockBackerReceiver);
        public Vector2 GetForceDeccel(Transform knockBackerReceiver) => GetDir(forceDeccelPower, knockBackerReceiver);

        Vector2 GetDir(float forceValue, Transform knockBackerReceiver)
        {
            Vector2 result = direction * forceValue;

            if (!CanSyncDirection()) return result;

            if (knockBackType == KnockBackType.Position)
            {
                var dir = (knockBackerReceiver.position - _knockBacker.position).normalized;
                var dirInt = dir.x > 0 ? 1 : -1;

                var finalDir = new Vector2(dirInt * direction.x, dirInt * direction.y);

                result = finalDir * forceValue;
            }

            if (knockBackType == KnockBackType.LocalScale)
            {
                var dirInt = knockBackerReceiver.localScale.x > 0 ? 1 : -1;

                var finalDir = new Vector2(dirInt * direction.x, dirInt * direction.y);

                result = finalDir * forceValue;
            }

            return result;
        }


        bool CanSyncDirection()
        {
            if (knockBackType == KnockBackType.None) return false;
            if (knockBackType == KnockBackType.Position && !_knockBacker)
            {
                Debug.LogError($"[{nameof(KnockBackData2D)}]:knockBacker is null");
                return false;
            }

            return true;
        }
    }
}