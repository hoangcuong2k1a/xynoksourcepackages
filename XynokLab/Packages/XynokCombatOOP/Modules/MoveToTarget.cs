﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace XynokCombatOop.Modules
{
    [Serializable]
    public class MoveToTarget
    {
        [Title("Move To Target", "", TitleAlignments.Centered)] [SerializeField]
        private Transform mover;

        [SerializeField] private Transform target;
        [SerializeField] private float speed = 12;
        [SerializeField] private Ease easeMove = Ease.Linear;
        [SerializeField] private bool forceCompleteOnStackExecute;

        [FoldoutGroup("EVENTs")] [SerializeField]
        private UnityEvent onMoveStart;

        [FoldoutGroup("EVENTs")] [SerializeField]
        private UnityEvent onMoving;

        [FoldoutGroup("EVENTs")] [SerializeField]
        private UnityEvent onMoveCompleted;

        private Tween _tweenMove;

        [Button]
        public void Move()
        {
            var targetPosition = target.position;
            var distance = Vector3.Distance(mover.position, targetPosition);

            _tweenMove?.Kill(forceCompleteOnStackExecute);

            _tweenMove = mover.DOMove(targetPosition, distance / speed).SetEase(easeMove)
                .OnStart(() => { onMoveStart.Invoke(); })
                .OnUpdate(() => { onMoving.Invoke(); })
                .OnComplete(() => { onMoveCompleted.Invoke(); });
        }

        public void StopMove(bool complete = false)
        {
            _tweenMove?.Kill(complete);
        }

        private void OnDestroy()
        {
            _tweenMove?.Kill();
        }
    }
}