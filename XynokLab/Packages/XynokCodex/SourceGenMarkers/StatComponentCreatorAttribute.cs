﻿using System;


namespace XynokSourceGenMarker.SourceGenMarkers
{
    public class TestSourceGenAttribute : Attribute
    {
    }

    /// <summary>
    /// this is use for ecs component
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum)]
    public class StatComponentCreatorAttribute : Attribute
    {
    }

    /// <summary>
    /// used by xynok convention, so don't use it
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class PrimitiveDataAttribute : Attribute
    {
    }

    /// <summary>
    /// use this attribute to mark an enum as a factory for a group of entities (e.g. HeroA, HeroB, etc.)
    /// suffix the enum name with "Id" or "Ids" or "ID" or "IDs"(e.g. EffectIDs, HeroIds, etc.)
    /// </summary>
    /// <typeparam name="abilityGroup">`abilityGroup` must be an enum.
    ///abilityGroup is the group of abilities that this entity has (e.g. attack, defend, etc.)
    /// </typeparam>
    /// <typeparam name="statGroup">`statGroup` must be an enum.
    ///statGroup is the group of stats that this entity has (e.g. health, mana, etc.)
    /// </typeparam>
    /// <typeparam name="stateGroup">`stateGroup` must be an enum.
    ///stateGroup is the group of states that this entity has (e.g. stunned, poisoned, etc.)
    /// </typeparam>
    /// <typeparam name="triggerGroup">`triggerGroup` must be an enum.
    ///triggerGroup is the group of triggers that this entity has be (e.g. forceAttack, forceJump, etc.)
    /// </typeparam>
    [AttributeUsage(AttributeTargets.Enum)]
    public class EntityMakerAttribute : Attribute
    {
        public Type abilityGroup;
        public Type statGroup;
        public Type stateGroup;
        public Type triggerGroup;

        public EntityMakerAttribute(Type abilityGroup, Type statGroup, Type stateGroup, Type triggerGroup)
        {
            this.abilityGroup = abilityGroup;
            this.statGroup = statGroup;
            this.stateGroup = stateGroup;
            this.triggerGroup = triggerGroup;
        }
    }


    /// <summary>
    /// use this attribute to mark an enum as a factory for a group of item entities (e.g. Sword, Shield, etc.)
    /// suffix the enum name with "Id" or "Ids" or "ID" or "IDs"(e.g. MeleeIDs, RangeWeaponIds, etc.)
    /// </summary>
    /// <typeparam name="ownerType">`ownerType` must be an enum.
    ///ownerType is the group of owner Id that this entity has be owned (e.g. hero, enemy, minion, etc.)
    /// </typeparam>
    /// <typeparam name="abilityGroup">`abilityGroup` must be an enum.
    ///abilityGroup is the group of abilities that this entity has (e.g. attack, defend, etc.)
    /// </typeparam>
    /// <typeparam name="statGroup">`statGroup` must be an enum.
    ///statGroup is the group of stats that this entity has (e.g. health, mana, etc.)
    /// </typeparam>
    /// <typeparam name="stateGroup">`stateGroup` must be an enum.
    ///stateGroup is the group of states that this entity has (e.g. stunned, poisoned, etc.)
    /// </typeparam>
    /// <typeparam name="triggerGroup">`triggerGroup` must be an enum.
    ///triggerGroup is the group of triggers that this entity has be (e.g. forceAttack, forceJump, etc.)
    /// </typeparam>
    [AttributeUsage(AttributeTargets.Enum)]
    public class EntityItemMakerAttribute : Attribute
    {
        public Type ownerGroup;
        public Type abilityGroup;
        public Type statGroup;
        public Type stateGroup;
        public Type triggerGroup;

        public EntityItemMakerAttribute(Type ownerGroup, Type abilityGroup, Type statGroup, Type stateGroup,
            Type triggerGroup)
        {
            this.ownerGroup = ownerGroup;
            this.abilityGroup = abilityGroup;
            this.statGroup = statGroup;
            this.stateGroup = stateGroup;
            this.triggerGroup = triggerGroup;
        }
    }

    #region Combat GOAP

    [AttributeUsage(AttributeTargets.Enum)]
    public class GoalMakerAttribute : Attribute
    {
        public Type statGroup;
        public Type stateGroup;
        public Type abilityGroup;

        public GoalMakerAttribute(Type statGroup, Type stateGroup, Type abilityGroup)
        {
            this.abilityGroup = abilityGroup;
            this.statGroup = statGroup;
            this.stateGroup = stateGroup;
        }
    }

    #endregion

    #region ECS

    [AttributeUsage(AttributeTargets.Enum)]
    public class DOTEntityMakerAttribute : Attribute
    {
        public Type abilityGroup;
        public Type statGroup;
        public Type stateGroup;
        public Type triggerGroup;

        public DOTEntityMakerAttribute(Type abilityGroup, Type statGroup, Type stateGroup, Type triggerGroup)
        {
            this.abilityGroup = abilityGroup;
            this.statGroup = statGroup;
            this.stateGroup = stateGroup;
            this.triggerGroup = triggerGroup;
        }
    }

    [AttributeUsage(AttributeTargets.Enum)]
    public class DOTEntitySystemMakerAttribute : Attribute
    {
        public Type entityGroup;
        public Type abilityGroup;
        public Type statGroup;
        public Type stateGroup;
        public Type triggerGroup;
        public string savePath;

        public DOTEntitySystemMakerAttribute(Type entityGroup, Type abilityGroup, Type statGroup, Type stateGroup,
            Type triggerGroup, string savePath)
        {
            this.entityGroup = entityGroup;
            this.abilityGroup = abilityGroup;
            this.statGroup = statGroup;
            this.stateGroup = stateGroup;
            this.triggerGroup = triggerGroup;
            this.savePath = savePath;
        }
    }

    #endregion

    #region Serialization

    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class ASerializableAttribute : Attribute
    {
    }

    #endregion
}