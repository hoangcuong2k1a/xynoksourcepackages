﻿using System;
using System.Collections.Generic;

namespace XynokConvention.Procedural
{
    /// <summary>
    /// specific type for each event is registered
    /// </summary>
    public abstract class AEventManager<T>
    {
        private static Dictionary<string, Action<T>> eventDict = new();
        private static Dictionary<string, T> eventDataDict = new();
        private static Dictionary<string, Action> eventVoidDict = new();

        #region LISTEN

        public static void StartListen(string eventName, Action<T> action)
        {
            if (eventDict.ContainsKey(eventName))
            {
                eventDict[eventName] -= action;
                eventDict[eventName] += action;
            }
            else
            {
                eventDict.Add(eventName, action);
            }
        }

        public static void StartListen(string eventName, Action action)
        {
            if (eventVoidDict.ContainsKey(eventName))
            {
                eventVoidDict[eventName] -= action;
                eventVoidDict[eventName] += action;
            }
            else
            {
                eventVoidDict.Add(eventName, action);
            }
        }

        public static void StopListen(string eventName, Action<T> action)
        {
            if (!eventDict.ContainsKey(eventName)) return;
            eventDict[eventName] -= action;
        }

        #endregion


        #region EMIT

        public static void SetEventData(string eventName, T data)
        {
            if (!eventDataDict.ContainsKey(eventName))
            {
                eventDataDict.Add(eventName, data);
                return;
            }

            eventDataDict[eventName] = data;
        }

        static void EmitEvent(string eventName)
        {
            if (!eventDataDict.ContainsKey(eventName)) return;
            
            if (eventDict.TryGetValue(eventName, out var value1)) value1?.Invoke(eventDataDict[eventName]);
            
            if (eventVoidDict.TryGetValue(eventName, out var value)) value?.Invoke();
            
            eventDataDict[eventName] = default; // reset data
        }


        public static void EmitEvent(string eventName, T data)
        {
            SetEventData(eventName, data);
            EmitEvent(eventName);
        }

        #endregion

        public static void Dispose()
        {
            eventDict.Clear();
            eventDataDict.Clear();
            eventVoidDict.Clear();
        }
    }
}