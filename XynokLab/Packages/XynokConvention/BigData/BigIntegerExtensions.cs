﻿using System.Numerics;

namespace XynokConvention.BigData
{
    public static class BigIntegerExtensions
    {
        public static string GetStringRepresentation(this BigInteger number, int decimalCount = 3)
        {
            string[] suffixes = new[]
            {
                "", "k", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion",
                "octillion",
                "nonillion", "decillion", "undecillion", "duodecillion", "tredecillion", "quattuordecillion",
                "quin decillion"
            };

            var result = GetRepresentation(number, suffixes, decimalCount);
            return $"{result.Item1}{result.Item2}";
        }


        /// <summary>
        /// display a big number in a human readable format
        /// </summary>
        /// <param name="number">big number</param>
        /// <param name="suffixes">suffix format for that value</param>
        /// <param name="decimalCount">number of decimal will be displayed</param>
        public static (string, string) GetRepresentation(this BigInteger number, string[] suffixes,
            int decimalCount = 3)
        {
            int validDecimalCount = decimalCount <= 0 ? 3 : decimalCount;
            if (number < 1000)
            {
                var integerTxt = number.ToString();
                if (integerTxt.Contains("."))
                {
                    var x = integerTxt.Split('.');
                    if (x[1].Length > decimalCount)
                    {
                        x[1] = x[1].Substring(0, validDecimalCount);
                        integerTxt = $"{x[0]}.{x[1]}";
                    }
                }
                var zeroTxt = $".{GetZeroString(decimalCount)}";
                if (integerTxt.Contains(zeroTxt)) integerTxt = integerTxt.Replace(zeroTxt, "");
                return (integerTxt, "");
            }

            bool over1M = number >= 1000000;
            string decimalChar = over1M ? "." : ",";

            for (int i = 1;; i += 1)
            {
                BigInteger val = BigInteger.Pow(1000, i);

                if (i >= suffixes.Length) break;

                if (val > number)
                {
                    var integer = number / BigInteger.Pow(1000, i - 1);
                    var remainder = BigInteger.Remainder(number, BigInteger.Pow(1000, i - 1));

                    var remainderStr = remainder.ToString();
                    var remainderTxt = remainderStr.Length <= validDecimalCount
                        ? remainderStr
                        : remainderStr.Substring(0, validDecimalCount);

                    var thapPhan = remainder != 0
                        ? $"{remainderTxt}"
                        : $"{GetZeroString(decimalCount)}";

                    if (thapPhan.Length < decimalCount)
                        thapPhan += GetZeroString(validDecimalCount - thapPhan.Length);
                    else if (thapPhan.Length > validDecimalCount) thapPhan = thapPhan.Substring(0, validDecimalCount);
                    
                    thapPhan = $"{decimalChar}{thapPhan}";

                    var integerTxt = $"{integer.ToString()}{thapPhan}".Trim();
                    var suffix = over1M ? $" {suffixes[i - 1]}" : "";


                    return (integerTxt, suffix);
                }
            }

            return ("number's too big !", "");
        }

        static string GetZeroString(int count)
        {
            string result = "";
            for (int i = 0; i < count; i++)
            {
                result += "0";
            }

            return result;
        }
    }
}