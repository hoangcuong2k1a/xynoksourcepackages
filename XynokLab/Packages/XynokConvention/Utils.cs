﻿
namespace XynokConvention
{
    public class Utils
    {
        public static bool Equal<T>(T a, T b) where T : class
        {
            return a == b;
        }
    }
}