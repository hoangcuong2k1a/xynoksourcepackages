﻿using UnityEngine;

namespace XynokConvention.Patterns
{
    public abstract class ALazySingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                _instance ??= FindObjectOfType<T>();
                if (_instance) return _instance;
                GameObject singleton = new GameObject($"singleton_{typeof(T).Name}");
                var dontDestroy = FindObjectOfType<DontDestroyOnLoad>();
                if (dontDestroy) singleton.transform.SetParent(dontDestroy.transform);
                _instance = singleton.AddComponent<T>();

                return _instance;
            }
        }
    }
}