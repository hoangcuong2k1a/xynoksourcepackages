﻿namespace XynokConvention.Data
{
    public enum ListenerTriggerType
    {
        None = 0,
        Stat = 1,
        State = 2
    }
}