﻿using Sirenix.OdinInspector;

namespace XynokConvention.Data
{
    public enum ResourceDataConditionType
    {
        None = 0,
        [LabelText("chỉ số", SdfIconType.SortNumericUp)]
        Stat = 0,

        [LabelText("trạng thái", SdfIconType.EmojiSmileFill)]
        State = 1,

        [LabelText("nội suy", SdfIconType.Person)]
        StatSelfCompare = 2,

        [LabelText("đối chiếu", SdfIconType.PersonPlus)]
        StatCompareAnotherOne = 3,
        
    }
}