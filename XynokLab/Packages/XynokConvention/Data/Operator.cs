﻿namespace XynokConvention.Data
{
    public enum Operator
    {
        Equal = 0,
        Greater = 1,
        LessThan = 2,
        LessThanOrEqual = 3,
        GreaterOrEqual = 4,
    }
}