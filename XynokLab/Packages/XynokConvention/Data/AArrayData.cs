﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace XynokConvention.Data
{
    [Serializable]
    public class AArrayData<T>
    {
        public T[] elements;


        /// <summary>
        /// T1 is old value, T2 is new value, int is index
        /// </summary>
        public event Action<T, T, int> OnChanged;


        public int Length => elements.Length;

        public T this[int index]
        {
            get => elements[index];
            set
            {
                OnChanged?.Invoke(elements[index], value, index);
                elements[index] = value;
            }
        }

        public T FirstOrDefault(Func<T, bool> func) => elements.FirstOrDefault(func);
        public IEnumerable<T> Where(Func<T, bool> func) => elements.Where(func);
        public int IndexOf(T item) => Array.IndexOf(elements, item);
        public bool Contains(T item) => IndexOf(item) != -1;


        public void RemoveAllListener()
        {
            OnChanged = default;
        }
    }
}