﻿using System;

namespace XynokConvention.Data
{
    [Serializable]
    public abstract class ATriggerData<T> : APairData<T, bool> where T : Enum
    {
        public Func<bool> canTrigger;

        protected ATriggerData(T key, bool baseValue) : base(key, baseValue)
        {
        }

        protected ATriggerData(bool baseValue) : base(baseValue)
        {
        }

        public override bool Value
        {
            get => currentValue;
            set
            {
                if (!canTrigger?.Invoke() ?? false)
                {
                    currentValue = false;
                    return;
                }

                lastValue = currentValue;
                currentValue = value;
                EmitEventChanged();
            }
        }

        public bool SetTrigger()
        {
            Value = true;
            return Value;
        }
    }
}