﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XynokConvention.Data
{
    public  class AStack<T>
    {
        private Stack<T> _stack = new();
        public event Action<T> OnPop;
        public event Action<T> OnPush;

        #region Defaults

        public int Count => _stack.Count;
        public bool IsEmpty => _stack.Count == 0;
        public List<T> ToList() => _stack.ToList();
        public T[] ToArray() => _stack.ToArray();
        public bool Contains(T item) => _stack.Contains(item);
        public void CopyTo(T[] array, int arrayIndex) => _stack.CopyTo(array, arrayIndex);
        public T Peek() => _stack.Peek();
        public void TrimExcess() => _stack.TrimExcess();
        public bool TryPeek(out T result) => _stack.TryPeek(out result);

        #endregion

        public bool TryPop(out T result)
        {
            var success = _stack.TryPop(out result);
            if (success) OnPop?.Invoke(result);
            return success;
        }

        public T Pop()
        {
            var value = _stack.Pop();
            OnPop?.Invoke(value);
            return value;
        }

        public void Push(T value)
        {
            _stack.Push(value);
            OnPush?.Invoke(value);
        }

        public void Clear() => _stack.Clear();
    }
}