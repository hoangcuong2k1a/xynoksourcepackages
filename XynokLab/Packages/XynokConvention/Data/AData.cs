﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokSourceGenMarker.SourceGenMarkers;

namespace XynokConvention.Data
{
    [PrimitiveData]
    [Serializable]
    public class AData<T>
    {
        [HideInInspector] [SerializeField] protected T baseValue;
        [HideInInspector] [SerializeField] protected T lastValue;
        [HideInInspector] [SerializeField] protected T currentValue;

        public AData(T baseValue, T lastValue, T currentValue)
        {
            this.baseValue = baseValue;
            this.lastValue = lastValue;
            this.currentValue = currentValue;
        }

        public event Action<T> OnChanged;

        /// <summary>
        /// T1 is base value, T2 is last value, T3 is current value
        /// </summary>
        public event Action<T, T, T> OnDeepChanged;


        [ShowInInspector]
        [HorizontalGroup]
        [HideLabel]
        public T BaseValue => baseValue;

        [ShowInInspector]
        [HorizontalGroup]
        [HideLabel]
        public T LastValue => lastValue;

        [ShowInInspector]
        [HorizontalGroup]
        [HideLabel]
        public virtual T Value
        {
            get => currentValue;
            set
            {
                lastValue = currentValue;
                currentValue = value;
                EmitEventDeepChanged();
                EmitEventChanged();
            }
        }

        public AData(T baseValue)
        {
            this.baseValue = baseValue;
            lastValue = this.baseValue;
            currentValue = this.baseValue;
        }

        [Button]
        public void SetBaseValue(T value, bool emmitEvent = true)
        {
            baseValue = value;
            lastValue = baseValue;
            currentValue = baseValue;
            if (emmitEvent) EmitEventDeepChanged();
            if (emmitEvent) EmitEventChanged();
        }


        public void SetCurrentValue(T value, bool emmitEvent = true)
        {
            lastValue = currentValue;
            currentValue = value;
            if (emmitEvent) EmitEventDeepChanged();
            if (emmitEvent) EmitEventChanged();
        }

        protected virtual void EmitEventChanged()
        {
            OnChanged?.Invoke(currentValue);
        }

        protected virtual void EmitEventDeepChanged()
        {
            OnDeepChanged?.Invoke(baseValue, lastValue, currentValue);
        }
    }
}