﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokConvention.Data
{
    [Serializable]
    public abstract class APairData<T1, T2> : AData<T2> where T1 : Enum
    {
        [SerializeField][HorizontalGroup] [HideLabel]  private T1 key;
        public T1 Key => key;

        private int _hashkey;

        public int HashKey
        {
            get
            {
                if (_hashkey == default) _hashkey = Animator.StringToHash(key.ToString());
                return _hashkey;
            }
        }

        public APairData(T1 key, T2 baseValue) : base(baseValue)
        {
            _hashkey = Animator.StringToHash(key.ToString());
            this.key = key;
        }

        protected APairData(T2 baseValue) : base(baseValue)
        {
        }
    }
}