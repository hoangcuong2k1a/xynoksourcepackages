﻿using Newtonsoft.Json;
using UnityEngine;
using XynokDebugger;

namespace XynokConvention.Data
{
    public struct DataHelper
    {
        public static void SaveData<T>(T data, string key)
        {
            var json = JsonConvert.SerializeObject(data, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }); 
            PlayerPrefs.SetString(key, json);
            PlayerPrefs.Save();
            Xylog.Log($"DATA SAVED - <color=cyan>{key}</color>: {json}");
        }

        public static T LoadSavedData<T>(string key)
        {
            return JsonConvert.DeserializeObject<T>(PlayerPrefs.GetString(key));
        }
    }
}