﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XynokConvention.Data
{
    public class AQueue<T>
    {
        private Queue<T> _queue = new();
        public Queue<T> Collection=>_queue;
        public event Action<T> OnEnqueue;
        public event Action<T> OnDequeue;
        public event Action OnEmpty;
        public event Action<T> OnPeekSuccess;

        #region Defaults

        public int Count => _queue.Count;
        public bool IsEmpty => _queue.Count == 0;
        public List<T> ToList() => _queue.ToList();
        public T[] ToArray() => _queue.ToArray();
        public bool Contains(T item) => _queue.Contains(item);
        public void CopyTo(T[] array, int arrayIndex) => _queue.CopyTo(array, arrayIndex);
        public T Peek() => _queue.Peek();


        public void TrimExcess() => _queue.TrimExcess();

        #endregion

        public void Enqueue(T value)
        {
            _queue.Enqueue(value);
            OnEnqueue?.Invoke(value);
        }

        public bool TryDequeue(out T result)
        {
            var success = _queue.TryDequeue(out result);
            if (success)
            {
                OnDequeue?.Invoke(result);
                if (IsEmpty) OnEmpty?.Invoke();
            }

            return success;
        }

        public bool TryPeek(out T result)
        {
            var success = _queue.TryPeek(out result);
            if (success)
            {
                OnPeekSuccess?.Invoke(result);
            }

            return success;
        }

        public T Dequeue()
        {
            var value = _queue.Dequeue();
            OnDequeue?.Invoke(value);
            if (IsEmpty) OnEmpty?.Invoke();
            return value;
        }
    }
}