﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XynokDebugger;

namespace XynokConvention.Data
{
    [Serializable]
    public class AListData<T>
    {
        public AListData(T[] arr)
        {
            elements = new List<T>(arr);
        }

        [SerializeField] protected List<T> elements;

        public event Action<T> OnAdd;
        public event Action<T> OnRemove;

        public event Action<T> OnChanged;

        /// <summary>
        /// T1 is base value, T2 is last value, T3 is current value
        /// </summary>
        public event Action<T, T, int> OnDeepChanged;

        public int Count => elements.Count;

        public T this[int index]
        {
            get => elements[index];
            set
            {
                OnDeepChanged?.Invoke(elements[index], value, index);
                OnChanged?.Invoke(value);
                elements[index] = value;
            }
        }

        public T FirstOrDefault(Func<T, bool> func) => elements.FirstOrDefault(func);
        public IEnumerable<T> Where(Func<T, bool> func) => elements.Where(func);
        public void Clear() => elements.Clear();
        public int IndexOf(T item) => elements.IndexOf(item);
        public bool Contains(T item) => elements.Contains(item);


        public void RemoveListener(Action<T> action)
        {
            OnAdd -= action;
            OnRemove -= action;
        }

        public void RemoveAllListener()
        {
            OnAdd = default;
            OnRemove = default;
            OnChanged = default;
        }

        public virtual bool Remove(T item)
        {
            if (elements == null) return false;
            if (!elements.Contains(item)) return false;

            elements.Remove(item);
            OnRemove?.Invoke(item);
            return true;
        }

        public virtual void Add(T item)
        {
            elements ??= new List<T>();
            if (elements.Contains(item))
            {
                Xylog.LogWarning(
                    $"[{GetType().Name} - Add()]: duplicated element <color=cyan>{item.GetType().Name}</color> !");
                return;
            }

            elements.Add(item);
            OnAdd?.Invoke(item);
        }

        public void AddRange(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                Add(item);
            }
        }
    }
}