﻿using UnityEngine;
using XynokDebugger;

namespace XynokConvention.Data.SaveAble
{
    public abstract class ADataSaveAble<T> 
    {
        public abstract string SaveKey { get; }
        public string version;

        public bool IsVersionValid()
        {
            if (Application.version != version)
            {
                Xylog.LogWarning(
                    $"application's version of <color=cyan>{SaveKey}</color> does not matched saved version !");
                return false;
            }

            return true;
        }

        public void Save()
        {
            version = Application.version;
            DataHelper.SaveData(this, SaveKey);
        }

        public T Load()
        {
            return DataHelper.LoadSavedData<T>(SaveKey);
        }
    }
}