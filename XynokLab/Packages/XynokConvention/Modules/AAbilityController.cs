﻿using System;
using System.Collections.Generic;
using UnityEngine;
using XynokConvention.APIs;

namespace XynokConvention.Modules
{
    public abstract class AAbilityController<T1, T2, T3> : IInjectable<T2>
        where T1 : Enum
        where T2 : IAbilityOwner<T1>
        where T3 : IInjectable<T2>, ITypeOnFactory<T1>
    {
        private T2 _owner;
        private Action _onDispose;
        private List<T3> _abilities;
        public T3[] Abilities => _abilities.ToArray();

        protected abstract T3 GetAbility(T1 key);

        public void SetDependency(T2 dependency)
        {
            if (_owner != null) Dispose();
            if (dependency == null)
            {
                Debug.LogError($"[AbilityController of {typeof(T2)}]: dependency is null!");
                return;
            }

            _owner = dependency;
            _abilities ??= new();

            _owner.Abilities.OnAdd += InitNewAbility;
            _owner.Abilities.OnRemove += RemoveAbility;
            _owner.Abilities.OnDeepChanged += OnChangedAbility;

            InitCurrentAbilities();
        }

        void InitCurrentAbilities()
        {
            for (int i = 0; i < _owner.Abilities.Count; i++)
                InitNewAbility(_owner.Abilities[i]);
        }

        void OnChangedAbility(T1 oldAbility, T1 newAbility, int index)
        {
            if (EqualityComparer<T1>.Default.Equals(oldAbility, newAbility)) return;

            if ((int)(object)oldAbility > 0) RemoveAbility(oldAbility);

            if ((int)(object)newAbility > 0) InitNewAbility(newAbility);
        }

        protected virtual void InitNewAbility(T1 abilityType)
        {
            if ((int)(object)abilityType <= 0) return;

            bool duplicatedAbility =
                _abilities.Find(e => EqualityComparer<T1>.Default.Equals(abilityType, e.GetProductType())) != null;

            if (duplicatedAbility)
            {
                Debug.LogError($"owner {_owner.GetType().Name} duplicated ability {abilityType}!");
                return;
            }

            var ability = GetAbility(abilityType);
            ability.SetDependency(_owner);
            _onDispose += ability.Dispose;
            _abilities.Add(ability);
        }

        void RemoveAbility(T1 abilityType)
        {
            if ((int)(object)abilityType <= 0) return;

            var ability = _abilities.Find(e => EqualityComparer<T1>.Default.Equals(abilityType, e.GetProductType()));
            bool existed = ability != null;
            if (!existed)
            {
                Debug.LogError($"owner {_owner.GetType().Name} does not contain ability {abilityType} to be removed !");
                return;
            }

            _onDispose -= ability.Dispose;
            ability.Dispose();
            _abilities.Remove(ability);
        }

        public void Dispose()
        {
            _owner.Abilities.OnAdd -= InitNewAbility;
            _owner.Abilities.OnRemove -= RemoveAbility;
            _owner.Abilities.OnDeepChanged -= OnChangedAbility;
            _onDispose?.Invoke();
            _onDispose = default;
            _abilities?.Clear();
            _owner = default;
        }
    }
}