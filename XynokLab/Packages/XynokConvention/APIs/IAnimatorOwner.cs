﻿using UnityEngine;

namespace XynokConvention.APIs
{
    public interface IAnimatorOwner
    {
        Animator Animator { get; }
    }
}