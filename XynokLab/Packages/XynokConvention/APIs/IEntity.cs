﻿using System;

namespace XynokConvention.APIs
{
    public interface IEntity<out T1>   where T1 : Enum

    {
        T1 EntityId { get; }
    }
}