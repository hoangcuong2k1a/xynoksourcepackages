﻿using System;
using XynokConvention.Data;

namespace XynokConvention.APIs
{
    /// <typeparam name="T">group of abilities</typeparam>
    public interface IAbilityOwner<T> where T: Enum
    {
        AListData<T> Abilities { get; }
    }
}