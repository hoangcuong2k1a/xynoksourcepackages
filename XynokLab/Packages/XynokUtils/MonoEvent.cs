﻿using UnityEngine;
using UnityEngine.Events;

namespace XynokUtils
{
    public class MonoEvent : MonoBehaviour
    {
        [Header("Mono Events")] [SerializeField]
        protected UnityEvent onEnable;

        [SerializeField] protected UnityEvent onDisable;
        [SerializeField] protected UnityEvent onStart;
        [SerializeField] protected UnityEvent onAwake;
        [SerializeField] protected UnityEvent onDestroy;

        protected virtual void OnEnable()
        {
            onEnable?.Invoke();
        }

        protected virtual void OnDisable()
        {
            onDisable?.Invoke();
        }

        protected virtual void Start()
        {
            onStart?.Invoke();
        }

        protected virtual void Awake()
        {
            onAwake?.Invoke();
        }

        protected virtual void OnDestroy()
        {
            onDestroy?.Invoke();
        }
    }
}