﻿using System;
using UnityEngine;
using UnityEngine.Events;
using XynokConvention.Procedural;

namespace XynokUtils.Schedule
{

    [Serializable]
    public class ScheduleData
    {
        public UpdateMode updateMode;
        [SerializeField] private UnityEvent unityEvent;
        public event Action onSchedule;
        public event Action onScheduleComplete;
    }
}