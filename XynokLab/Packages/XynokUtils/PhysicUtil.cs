﻿using UnityEngine;

namespace XynokUtils
{
    public static class PhysicUtil
    {
        #region Layer Detection

        public static bool IsLayer(this Transform transform, LayerMask layerMask)
        {
            return layerMask == (layerMask | (1 << transform.gameObject.layer));
        }

        public static bool IsLayer(this GameObject gameObject, LayerMask layerMask)
        {
            return layerMask == (layerMask | (1 << gameObject.layer));
        }

        // 2D
        public static bool IsLayer(this Collider2D col, LayerMask layerMask)
        {
            return layerMask == (layerMask | (1 << col.gameObject.layer));
        }

        public static bool IsLayer(this Collision2D col, LayerMask layerMask)
        {
            return layerMask == (layerMask | (1 << col.gameObject.layer));
        }
        
        // 3D
        public static bool IsLayer(this Collider col, LayerMask layerMask)
        {
            return layerMask == (layerMask | (1 << col.gameObject.layer));
        }
        public static bool IsLayer(this Collision col, LayerMask layerMask)
        {
            return layerMask == (layerMask | (1 << col.gameObject.layer));
        }

        #endregion

        public static bool HitLayer2D(LayerMask layerMask, Vector2 pos, Vector2 checkSize, float angle)
        {
            return Physics2D.OverlapBox(pos, checkSize, angle, layerMask);
        }
        
    }
}