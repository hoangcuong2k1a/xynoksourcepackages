﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using TMPro;

namespace XynokUtils
{
    public static class TextMeshProGUIExtensions
    {
        public static TweenerCore<string, string, StringOptions> DoText(this TextMeshProUGUI target, string endValue,
            float duration, bool richTextEnabled = true, ScrambleMode scrambleMode = ScrambleMode.None,
            string scrambleChars = null)
        {
            if (endValue == null)
            {
                if (Debugger.logPriority > 0)
                    Debugger.LogWarning(
                        "You can't pass a NULL string to DOText: an empty string will be used instead to avoid errors");
                endValue = "";
            }

            TweenerCore<string, string, StringOptions> t = DOTween.To(() => target.text, x => target.text = x, endValue,
                duration);
            t.SetOptions(richTextEnabled, scrambleMode, scrambleChars)
                .SetTarget(target).SetUpdate(true);
            return t;
        }

        public static string AddSpaces(this string input)
        {
            string[] words = SplitCamelCase(input);
            string output = string.Join(" ", words);
            return output;
        }

        static string[] SplitCamelCase(string input)
        {
            return System.Text.RegularExpressions.Regex.Split(input, @"(?<!^)(?=[A-Z])");
        }
    }
}