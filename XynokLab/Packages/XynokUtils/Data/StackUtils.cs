﻿using System.Collections.Generic;
using UnityEngine;

namespace XynokUtils.Data
{
    public struct StackUtils
    {
        /// <summary>
        /// remove element from stack
        /// </summary>
        /// <returns> return new stack without that element</returns>
        public static Stack<T> Remove<T>(Stack<T> stack, T element)
        {
            if (stack == null)
            {
#if XYNOK_DEBUGGER_ERROR_LOG
                Debug.LogError($"[StackUtils]: Remove element from stack failed! stack is null!");
#endif
                return null;
            }

            if (element == null)
            {
#if XYNOK_DEBUGGER_WARNING_LOG
                Debug.LogWarning("[StackUtils]: Remove element from stack failed! element is null!");
#endif
                return stack;
            }

            if (stack.Count < 1)
            {
#if XYNOK_DEBUGGER_WARNING_LOG
                Debug.LogWarning("[StackUtils]: Remove element from stack failed! stack is empty!");
#endif
                return stack;
            }

            var newStack = new Stack<T>();

            var list = new List<T>(stack);

            if (!list.Contains(element))
            {
#if XYNOK_DEBUGGER_WARNING_LOG
                Debug.LogWarning(
                    "[StackUtils]: Remove element from stack failed! stack does not contain that element!");
#endif
                return stack;
            }

            list.Remove(element);
            for (int i = 0; i < list.Count; i++)
            {
                newStack.Push(list[i]);
            }

            return newStack;
        }
    }
}