﻿using UnityEngine;
using XynokUtils.Physics.Data;

namespace XynokUtils.Physics
{
    [RequireComponent(typeof(Collider))]
    public class DetectCallback3D: MonoBehaviour
    {
        [Header("COLLISION SETTING")] [SerializeField]
        private LayerMask interactableLayer;

        [SerializeField] private OrderInvokeCallback executeOrder;
        public TriggerCallback3D onTriggerEnter;
        public TriggerCallback3D onTriggerExit;
        public CollisionCallback3D onCollisionEnter;
        public CollisionCallback3D onCollisionExit;


        private void OnTriggerEnter(Collider other)
        {
            if (!other.IsLayer(interactableLayer)) return;

            onTriggerEnter.Invoke(other, executeOrder);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.IsLayer(interactableLayer)) return;

            onTriggerExit.Invoke(other, executeOrder);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (!other.IsLayer(interactableLayer)) return;

            onCollisionEnter.Invoke(other, executeOrder);
        }

        private void OnCollisionExit(Collision other)
        {
            if (!other.IsLayer(interactableLayer)) return;
            onCollisionExit.Invoke(other, executeOrder);
        }
    }
}