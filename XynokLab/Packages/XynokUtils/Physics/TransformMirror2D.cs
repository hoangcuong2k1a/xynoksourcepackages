﻿using System;
using UnityEngine;
using XynokConvention.Procedural;

namespace XynokUtils.Physics
{
    public class TransformMirror2D : MonoBehaviour
    {
        [Header("Transform Mirror")] [SerializeField]
        protected GameObject target;

        [SerializeField] protected GameObject mirror;
        [SerializeField] protected MonoUpdateMode updateMode = MonoUpdateMode.FixedUpdate;
        [SerializeField] protected bool includeRotation = true;
        [SerializeField] protected bool includeScaleDirection = true;
        private Vector3 _originalMirrorScale;

        private void Start()
        {
            _originalMirrorScale = mirror.transform.localScale;
        }

        protected virtual void Update()
        {
            if (updateMode == MonoUpdateMode.Update) Mirror();
        }

        protected virtual void FixedUpdate()
        {
            if (updateMode == MonoUpdateMode.FixedUpdate) Mirror();
        }

        protected virtual void LateUpdate()
        {
            if (updateMode == MonoUpdateMode.LateUpdate) Mirror();
        }

        void Mirror()
        {
            mirror.transform.position = target.transform.position;

            if (!includeRotation && !includeScaleDirection) return;

            var direction = target.transform.localScale.x > 0 ? 1 : -1;

            if (includeRotation)
            {
                mirror.transform.eulerAngles = target.transform.eulerAngles;
            }

            if (includeScaleDirection)
            {
                mirror.transform.localScale = new Vector3(direction * _originalMirrorScale.x, _originalMirrorScale.y,
                    _originalMirrorScale.z);
            }
        }
    }
}