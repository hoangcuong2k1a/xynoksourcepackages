﻿using UnityEngine;

namespace XynokUtils
{
    public class RuntimeUtils
    {
        public static GameObject CreateGameObject(string name, Transform parent=null)
        {
            GameObject gameObject = new GameObject(name);
            gameObject.transform.SetParent(parent);
            gameObject.transform.localPosition = Vector3.zero;
            return gameObject;
        }
    }
}