﻿using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace XynokUtils.Gacha
{
    public class ProportionalRandomSelector<T>
    {
        private Dictionary<T, int> _percentageItemsDict;

        public void Init(Dictionary<T, float> source)
        {
            _percentageItemsDict?.Clear();
            _percentageItemsDict = GetRates(source);
        }

        public T SelectItem()
        {
            // Calculate the summa of all portions.
            int poolSize = 0;
            foreach (int i in _percentageItemsDict.Values)
            {
                poolSize += i;
            }

            // Get a random integer from 1 to PoolSize.
            int randomNumber = Random.Range(1, poolSize);

            // Detect the item, which corresponds to current random number.
            int accumulatedProbability = 0;
            foreach (KeyValuePair<T, int> pair in _percentageItemsDict)
            {
                accumulatedProbability += pair.Value;
                if (randomNumber <= accumulatedProbability)
                    return pair.Key;
            }

            return default; // this code will never come while you use this programm right :)
        }

        Dictionary<T, int> GetRates(Dictionary<T, float> source)
        {
            double minValue = double.MaxValue;

            // Find the minimum value in the array
            foreach (var pair in source)
            {
                if (pair.Value < minValue) minValue = pair.Value;
            }

            Dictionary<T, int> myResult = new();

            foreach (var pair in source)
            {
                myResult.Add(pair.Key, (int)Math.Round((pair.Value / minValue) * 100));
            }

            return myResult;
        }
    }
}