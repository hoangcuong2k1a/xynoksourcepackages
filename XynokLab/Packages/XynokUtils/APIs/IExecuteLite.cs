﻿namespace XynokUtils.APIs
{
    public interface IExecuteLite
    {
        void Execute();
        void Continue();
        void Stop();
        void Reset();
    }
}