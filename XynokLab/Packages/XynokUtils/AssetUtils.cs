﻿#if UNITY_EDITOR
using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;


namespace XynokUtils
{
    public enum PrefabType
    {
        None = 0,
        Original = 1,
        Instance = 2,
        DisconnectedFromInstance = 3,
    }

    public class AssetUtils
    {
        public static bool IsFolderExist(string path, bool createIfNotExist = false)
        {
            bool exist = Directory.Exists(path);
            if (!exist && createIfNotExist)
            {
                Directory.CreateDirectory(path);
                Debug.LogWarning($"Created folder:<color=cyan>{path}</color> !");
                return Directory.Exists(path);
            }

            if (!exist) Debug.LogError($"[{path}] is not exist!");

            return exist;
        }

        [Obsolete("Obsolete")]
        public static PrefabType GetPrefabType(GameObject obj)
        {
            var prefabParent = PrefabUtility.GetPrefabParent(obj);
            var prefabObject = PrefabUtility.GetPrefabObject(obj.transform);

            bool isPrefabInstance = prefabParent != null && prefabObject != null;
            bool isPrefabOriginal = prefabParent == null && prefabObject != null;
            bool isDisconnectedPrefabInstance = prefabParent != null && prefabObject == null;
            if (isPrefabInstance) return PrefabType.Instance;
            if (isPrefabOriginal) return PrefabType.Original;
            if (isDisconnectedPrefabInstance) return PrefabType.DisconnectedFromInstance;

            return PrefabType.None;
        }

        public static string GetOriginPrefabPath(GameObject obj)
        {
            var prefabParent = PrefabUtility.GetPrefabParent(obj);
            return GetAssetPath(prefabParent);
        }

        public static string GetAssetPath(Object obj)
        {
            string path = AssetDatabase.GetAssetPath(obj);
            return path;
        }

        public static bool ExistAssetAtPath(string path)
        {
            var asset = AssetDatabase.LoadAssetAtPath<Object>(path);
            bool exist = asset != null;
            return exist;
        }

        public static void CreateTag(string tag) => UnityEditorInternal.InternalEditorUtility.AddTag(tag);

        public static bool ExistTag(string tag)
        {
            string[] tags = UnityEditorInternal.InternalEditorUtility.tags;
            for (int i = 0; i < tags.Length; i++)
            {
                if (tags[i] == tag)
                {
                    return true;
                }
            }

            return false;
        }


        public static string[] GetAllSubFolder(string path = "Assets")
        {
            if (string.IsNullOrEmpty(path)) return null;
            if (!Directory.Exists(path)) return null;
            var folders = Directory.GetDirectories(path);
            if (folders.Length == 0) return null;

            for (int i = 0; i < folders.Length; i++)
            {
                folders[i] = folders[i].Replace("\\", "/");
            }

            return folders;
        }

        public static string[] GetAllAssetAtPath(string file = "prefab", string path = "Assets",
            bool includePath = true)
        {
            if (string.IsNullOrEmpty(path)) return null;

            string[] guids2 = AssetDatabase.FindAssets($"t:{file}", new[] { path });
            string[] result = new string[guids2.Length];
            for (int i = 0; i < guids2.Length; i++)
            {
                result[i] = AssetDatabase.GUIDToAssetPath(guids2[i]);
                if (!includePath) result[i] = Path.GetFileNameWithoutExtension(result[i]);
            }

            return result;
        }

        public static void CreateSo<T>(string path) where T : ScriptableObject
        {
            var so = ScriptableObject.CreateInstance<T>();
            AssetDatabase.CreateAsset(so, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static bool IsSoExist<T>(Type type) where T : ScriptableObject
        {
            return AssetDatabase.FindAssets($"t:{type.Name}").Length > 0;
        }

        public static T GetInstanceOfSo<T>() where T : ScriptableObject
        {
            string typeName = typeof(T).Name;
            var guidList = AssetDatabase.FindAssets($"t:{typeName}");
            T so;
            if (guidList.Length > 0)
            {
                var path = Path.Combine(
                    Path.GetDirectoryName(AssetDatabase.GUIDToAssetPath(guidList[0])) ?? string.Empty,
                    typeName + ".asset");

                so = AssetDatabase.LoadAssetAtPath<T>(path);
                if (so != null) return so;
            }

            Debug.LogError($"not found instance of {typeName} ");
            return null;
        }

        public static T GetInstanceOfSo<T>(string hintCreate)
            where T : ScriptableObject
        {
            string typeName = typeof(T).Name;
            var guidList = AssetDatabase.FindAssets($"t:{typeName}");
            T so;
            if (guidList.Length > 0)
            {
                var path = Path.Combine(
                    Path.GetDirectoryName(AssetDatabase.GUIDToAssetPath(guidList[0])) ?? string.Empty,
                    typeName + ".asset");

                so = AssetDatabase.LoadAssetAtPath<T>(path);
                if (so != null) return so;
            }

            Debug.LogError(
                $"not found instance of {typeName} \n To Create:<color=cyan> Right Click > Create > {hintCreate}</color>");
            return null;
        }
    }
}
#endif