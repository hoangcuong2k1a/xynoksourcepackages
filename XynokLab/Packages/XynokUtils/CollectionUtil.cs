﻿using System;

namespace XynokUtils
{
    public struct CollectionUtil
    {
      public  static T[] Shuffle<T>(T[] array)
        {
            Random random = new Random();
            for (int i = array.Length - 1; i > 0; i--)
            {
                int j = random.Next(i + 1);
                (array[i], array[j]) = (array[j], array[i]);
            }

            return array;
        }
    }
}