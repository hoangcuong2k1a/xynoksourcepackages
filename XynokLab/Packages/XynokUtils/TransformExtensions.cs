﻿using UnityEngine;

namespace XynokUtils
{
    public static class TransformExtensions
    {
        public static void SetRelativePosition(this Transform transform, Vector3 anotherLikeParentPos)
        {
            transform.gameObject.transform.position -= anotherLikeParentPos;
        }
        
     
    }
}