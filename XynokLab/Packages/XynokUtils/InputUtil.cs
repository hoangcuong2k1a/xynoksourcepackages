﻿
namespace XynokUtils
{
    public static class InputUtil
    {
        public const float OFFSET_GAMEPAD_AXIS = .75f;
        public static bool InputAxisClearly(float axis)
        {
            bool greaterThanZero = axis is > 0 and >= .1f;
            bool lessThanZero = axis is < 0 and <= -.1f;
            return greaterThanZero || lessThanZero;
        }
    }
}