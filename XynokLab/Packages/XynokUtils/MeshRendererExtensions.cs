﻿using UnityEngine;

namespace XynokUtils
{
    public static class MeshRendererExtensions
    {
        public static void Coloring(this MeshRenderer meshRenderer, Color color)
        {
            if (Application.isPlaying)
            {
                meshRenderer.material.color = color;
            }
            else
            {
                meshRenderer.sharedMaterial.color = color;
            }
        }
    }
}