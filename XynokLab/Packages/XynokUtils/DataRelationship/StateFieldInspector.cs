﻿using System;

namespace XynokUtils.DataRelationship
{
    public enum BooleanDefaultValue
    {
        None = 0,
        False = 1,
        True = 2,
    }

    public enum FieldType
    {
        None = 0,
        Title = 1,
        Valuable = 2,
        Controller = 3
    }


    [Serializable]
    public abstract class AFieldInspector<T>
    {
        public string fieldName;
        public T value;
        public FieldType fieldType;
    }

    [Serializable]
    public class StateFieldInspector : AFieldInspector<BooleanDefaultValue>
    {
    }

    [Serializable]
    public class StateBondData
    {
        public string stateName;
        public StateData[] whenStateTrue = { };
        public StateData[] whenStateFalse = { };
    }

    [Serializable]
    public class StateData
    {
        public string stateName;
        public bool value;
    }
}