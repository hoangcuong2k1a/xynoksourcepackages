﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace XynokUtils
{
    [Serializable]
    public struct MatrixIndex
    {
        public int col;
        public int row;

        public bool Equal(MatrixIndex other)
        {
            return col == other.col && row == other.row;
        }

        public override string ToString()
        {
            return $"mapCell_{row}_{col}";
        }
    }

    public struct MatrixUtil
    {
        public static T[] GetElementsAround4Dir<T>(T[,] matrix, MatrixIndex target, int cellDistance,
            bool includeTarget = false)
        {
            var elementsAroundTarget = includeTarget ? new List<T> { matrix[target.row, target.col] } : new List<T>();
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);
            int maxDistance = cellDistance;
            int targetCol = target.col;
            int targetRow = target.row;


            for (int r = Math.Max(0, targetRow - maxDistance); r <= Math.Min(rows - 1, targetRow + maxDistance); r++)
            {
                for (int c = Math.Max(0, targetCol - maxDistance);
                     c <= Math.Min(cols - 1, targetCol + maxDistance);
                     c++)
                {
                    int distance = Math.Abs(targetRow - r) + Math.Abs(targetCol - c);

                    if (distance <= maxDistance)
                    {
                        elementsAroundTarget.Add(matrix[r, c]);
                    }
                }
            }

            return elementsAroundTarget.ToArray();
        }

        public static T[] GetElementsAround8Dir<T>(T[,] matrix, MatrixIndex target, int maxDistance,
            bool includeTarget = false)
        {
            int targetRow = target.row;
            int targetCol = target.col;
            List<T> elementsAroundTarget = new List<T>();
            HashSet<(int, int)> visited = new HashSet<(int, int)>();

            Queue<(int, int, int)> queue = new Queue<(int, int, int)>();
            queue.Enqueue((targetRow, targetCol, 0));

            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);

            int[] dr = { -1, 1, 0, 0, -1, -1, 1, 1 }; // Offsets for rows in 8 directions
            int[] dc = { 0, 0, -1, 1, -1, 1, -1, 1 }; // Offsets for columns in 8 directions

            while (queue.Count > 0)
            {
                var (row, col, distance) = queue.Dequeue();

                if (row < 0 || row >= rows || col < 0 || col >= cols || distance > maxDistance ||
                    visited.Contains((row, col)))
                    continue;

                elementsAroundTarget.Add(matrix[row, col]);
                visited.Add((row, col));

                if (distance < maxDistance)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        int newRow = row + dr[i];
                        int newCol = col + dc[i];
                        queue.Enqueue((newRow, newCol, distance + 1));
                    }
                }
            }

            if (!includeTarget) elementsAroundTarget.Remove(matrix[target.row, target.col]);
            return elementsAroundTarget.ToArray();
        }


        public static T[] GetColumn<T>(T[,] matrix, int columnNumber)
        {
            return Enumerable.Range(0, matrix.GetLength(1))
                .Select(x => matrix[columnNumber, x])
                .ToArray();
        }

        public static T[] GetRow<T>(T[,] matrix, int rowNumber)
        {
            
            return Enumerable.Range(0, matrix.GetLength(0))
                .Select(x => matrix[x, rowNumber])
                .ToArray();
        
        }

        public static (int, int) IndexOf<T>(T[,] matrix, T element)
        {
            
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    if (matrix[i, j].Equals(element)) return (j, i);
                } 
            }

            Debug.LogError($"element not found on matrix {typeof(T).Name}");
            return (-1, -1);
        }
    }
}