﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokConvention.APIs;
using XynokConvention.Procedural;

namespace XynokAsync
{
    [Serializable]
    public class TaskDurationData
    {
        private Action _action;

        [SuffixLabel("tính = giây", Overlay = true)] [InfoBox("deadline tối đa", SdfIconType.Calendar)] [HideLabel]
        public float duration;

        [InfoBox("recorder", SdfIconType.CameraReelsFill)] [HideLabel]
        public UpdateMode updateMode = UpdateMode.Update;

        [ShowIf("updateMode", UpdateMode.Custom)]
        [SuffixLabel("tính = giây", Overlay = true)]
        [InfoBox("năng suất làm việc(bao lâu 1 lần làm)", SdfIconType.Activity)]
        [HideLabel]
        public float executeIterate;

        public void AddAction(Action action)
        {
            _action -= action;
            _action += action;
        }

        public void RemoveAction(Action action)
        {
            _action -= action;
        }

        public void Execute()
        {
            _action?.Invoke();
        }
    }

    public class TaskDuration : IInjectable<TaskDurationData>
    {
        private TaskDurationData _data;
        private float _time;
        private float _timeIterate;
        private bool _canRun;
        private bool _completed;
        public bool Completed => _completed;
        public event Action OnCompleted;

        public void SetDependency(TaskDurationData dependency)
        {
            if (_data != null) Dispose();
            _data = dependency;

            if (_data.updateMode == UpdateMode.RealtimeUpdate)
            {
                Debug.LogError($"[XynokAsync]: {UpdateMode.RealtimeUpdate} is not supported.");
                return;
            }

            Reset();
            TimeCycle.Instance.AddInvoker(Track, _data.updateMode);
        }

        public void Activate(bool value)
        {
            if (_data != null && value) Reset();
            _canRun = value;
        }

        /// <summary>
        /// reset only, not dispose, not replay. If you want to replay, use Activate(true)
        /// </summary>
        public void Reset()
        {
            _completed = false;
            _canRun = false;
            if (_data == null) return;
            _time = _data.duration;
            _timeIterate = _data.executeIterate;
        }

        void Track()
        {
            if (_completed) return;
            if (!_canRun) return;
            if (_data == null) return;
            if (_time > 0) _time -= Time.deltaTime;
            else
            {
                OnCompleted?.Invoke();
                Reset(); // reset if you want to replay without call Reset()
                _completed = true;
                return;
            }

            if (_data.updateMode != UpdateMode.Custom)
            {
                _data.Execute();
                return;
            }

            if (_timeIterate > 0)
            {
                _timeIterate -= Time.deltaTime;
                return;
            }

            _data.Execute();
            _timeIterate = _data.executeIterate;
        }

        public void Dispose()
        {
            _canRun = false;
            _data = default;
            _completed = false;
            OnCompleted = default;
            TimeCycle.Instance.RemoveInvoker(Track);
        }
    }
}