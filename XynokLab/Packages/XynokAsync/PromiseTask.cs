﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace XynokAsync
{
    public class PromiseTask
    {
        private Func<bool> _condition;
        private Action _callback;
        private bool _canRunStack;
        private bool _isRunning;
        private CancellationTokenSource _cts;
        private float _timeOut;
        private string _taskName; // for debug
        public PromiseTask(string name,Func<bool> condition, Action callback, float timeOut)
        {
            _taskName = name;
            _condition = condition;
            _callback = callback;
            _isRunning = false;
            _timeOut = timeOut;
            _cts = new();
        }

        public void Cancel()
        {
            if (!_isRunning) return;
            _cts.Dispose();
            _isRunning = false;
        }

        public async void Run()
        {
            if (_isRunning)
            {
                Debug.LogWarning($"[PromiseTask]: {_taskName} is already running !");
                return;
            }
            _cts = new();
            _isRunning = true;
            await UniTask.WaitUntil(_condition, cancellationToken: _cts.Token).Timeout(TimeSpan.FromSeconds(_timeOut));
            if (!_cts.IsCancellationRequested) _callback?.Invoke();
            _isRunning = false;
        }
    }
}