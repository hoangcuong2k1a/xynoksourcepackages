﻿using UnityEngine;

namespace XynokSaveSystem.Database
{
    /// <summary>
    /// lưu trữ slot, category, settings, ... sử dụng cho mục đích quản lý save data và config chúng
    /// - Slot: user có thể tạo nhiều slot để chơi game, mỗi slot sẽ có lưu trữ 1 user profile
    /// - Category: nằm trong scope của Slot(1 slot có nhiều categories ), mỗi category sẽ chứa 1 loại data riêng
    /// - Settings: chưa define, chưa code tính năng cho phần này, về sau có thể mở rộng thêm compress, encrypt, ...
    /// </summary>
    [CreateAssetMenu(fileName = "SaveDataManaged", menuName = "Xynok/SaveSystem/SaveDataManaged")]
    public class SaveDataManaged: ScriptableObject
    {
#if UNITY_EDITOR
        public static SaveDataManaged Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<SaveDataManaged>("Xynok/SaveSystem/SaveDataManaged");
#endif
        public string[] categories;
    }
}