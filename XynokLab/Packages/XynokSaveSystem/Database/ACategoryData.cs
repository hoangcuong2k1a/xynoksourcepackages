﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokSaveSystem.APIs;

namespace XynokSaveSystem.Database
{
    [Serializable]
    public abstract class ACategoryData<T> : ISaveAble
    {
        [SerializeField] [ValueDropdown("CategoryNames")]
        private string saveKey;

        public string SaveKey => saveKey;
        
#if UNITY_EDITOR
        private static string[] CategoryNames => SaveDataManaged.Instance.categories;
#endif
    }
}