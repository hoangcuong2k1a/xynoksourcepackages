﻿namespace XynokSaveSystem.APIs
{
    public interface ISaveAble
    {
        string SaveKey { get; }
    }
}