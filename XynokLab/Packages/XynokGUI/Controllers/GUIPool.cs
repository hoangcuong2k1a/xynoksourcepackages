﻿using UnityEngine;
using XynokAssetBundle.Pooling;

namespace XynokGUI.Controllers
{
    public class GUIPool : APool<string>
    {
        public void DeSpawnView(string viewName)
        {
            if (!pools.ContainsKey(viewName))
            {
                Debug.Log("does not exist pool of " + viewName);
                return;
            }

            var pool = pools[viewName];
            pool.DespawnOldest();
        }
    }
}