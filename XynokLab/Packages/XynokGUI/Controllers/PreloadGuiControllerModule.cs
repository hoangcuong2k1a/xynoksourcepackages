﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using XynokGUI.Core.Data;

namespace XynokGUI.Controllers
{
    /// <summary>
    /// use for preload a Gui on start game
    /// </summary>
    [Serializable]
    public class PreloadGuiControllerModule
    {
        [SerializeField] private GuiViewNamingData[] preloadGui;
        [SerializeField] [Range(0f, 10f)] private float delay = 1f;

        public async void Execute()
        {
            await UniTask.Delay(TimeSpan.FromSeconds(delay));
            if (preloadGui == null || preloadGui.Length < 1) return;
            for (int i = 0; i < preloadGui.Length; i++)
            {
                var gui = preloadGui[i];
                GUIController.Instance.Load(gui.category, gui.viewName);
            }
        }
    }
}