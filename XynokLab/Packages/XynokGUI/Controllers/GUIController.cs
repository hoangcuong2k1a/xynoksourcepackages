﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle;
using XynokAssetBundle.Manager;
using XynokConvention.Patterns;
using XynokDebugger;
using XynokGUI.Core.Data.EventData;
using XynokGUI.Entities;
using XynokInput;
using XynokUtils.Data;
using GUISettings = XynokGUI.Core.Data.GUISettings;

namespace XynokGUI.Controllers
{
    /// <summary>
    /// quản lý các thực thể liên quan đến GUI
    /// </summary>
    [RequireComponent(typeof(Canvas))]
    public class GUIController : ASingleton<GUIController>, IBundleHolder
    {
        [SerializeField] [ReadOnly] private Canvas canvas;
        [SerializeField] private GUISettings settings;
        [SerializeField] private SceneManaged sceneManaged;
        [SerializeField] private bool useContainerForLayer;

        [SerializeField] [ShowIf("useContainerForLayer")]
        private Transform layerContainer;

        [SerializeField] private bool loadOnStart;

        [ShowIf("loadOnStart")] [SerializeField]
        private PreloadGuiControllerModule preloadSetting;

        [SerializeField] [ReadOnly] private GUILayerEntity currentLayerInteractive;

        /// <summary>
        /// use for handle interactable for layer.
        /// it can have duplicate elements, because of layer can have multiple views
        /// </summary>
        private Stack<GUILayerEntity> _layerHistory = new();


        private GUILayerEntity[] _layers;
        public GUISettings GUIPrefabManaged => settings;
        public SceneManaged SceneManaged => sceneManaged;
        private bool _isLoading;

        private Action<string> _bundleStartLoad;
        private Action<string> _variantLoad;
        private Action<IBundleHolder> _bundleLoadComplete;
#if UNITY_EDITOR

        private void OnValidate()
        {
            if (!canvas) canvas = GetComponent<Canvas>();
        }
#endif
        
        #region Bundle Load

        public void Register(Action<string> bundleStartLoad, Action<string> variantLoad,
            Action<IBundleHolder> bundleLoadComplete)
        {
            _bundleStartLoad = bundleStartLoad;
            _variantLoad = variantLoad;
            _bundleLoadComplete = bundleLoadComplete;
        }

        public void LoadBundle()
        {
            InitLayer();
        }

        public int AssetCount
        {
            get
            {
                int count = 0;

                foreach (var layerData in settings.layers)
                {
                    if (settings.excludeCategories.Length > 0)
                    {
                        if (settings.excludeCategories.Contains(layerData.targetCategory)) continue;
                    }

                    count += layerData.viewData.Length;
                }

                return count;
            }
        }

        #endregion

        #region Externals

        public async void LoadAndSendData<T>(string category, string viewName, T dataDeliver) where T : IGuiDataDeliver
        {
            await UniTask.WaitUntil(() => !_isLoading);

            var layer = GetGUILayerEntity(category);
            if (!layer)
            {
                Xylog.LogError($"not exist layer for category {category} !");
                return;
            }

            if (_layerHistory.Count > 0)
            {
                var currentOnTop = _layerHistory.Peek();
                if (currentOnTop) currentOnTop.Interactable = false;
            }

            _isLoading = true;

            var targetView = await layer.LoadAndSendData(viewName, dataDeliver);

            if (targetView != null)
            {
                _layerHistory.Push(layer);
                ActiveLayerInteractAfterLoad();
            }
            else
            {
                Xylog.LogError(
                    $"can not load <color=cyan>{viewName}</color> of category <color=cyan>{category}</color> !");
            }

            _isLoading = false;
        }

        [Button]
        public async void Load(string category, string viewName)
        {
            await UniTask.WaitUntil(() => !_isLoading);

            var layer = GetGUILayerEntity(category);
            if (!layer)
            {
                Xylog.LogError($"not exist layer for category {category} !");
                return;
            }

            InputManager.Instance.SetActiveInput(false);
            if (_layerHistory.Count > 0)
            {
                var currentOnTop = _layerHistory.Peek();
                if (currentOnTop) currentOnTop.Interactable = false;
            }

            _isLoading = true;

            var succeed = await layer.Load(viewName);

            if (succeed)
            {
                _layerHistory.Push(layer);
                ActiveLayerInteractAfterLoad();
            }
            else
            {
                Xylog.LogError(
                    $"can not load <color=cyan>{viewName}</color> of category <color=cyan>{category}</color> !");
            }


            _isLoading = false;
        }

        [Button]
        public async void Pop(string category)
        {
            await UniTask.WaitUntil(() => !_isLoading);
            var layer = GetGUILayerEntity(category);
            if (!layer)
            {
                Xylog.LogError($"not exist layer for category {category} !");
                return;
            }

            InputManager.Instance.SetActiveInput(false);
            if (_layerHistory.Count > 0)
            {
                var currentOnTop = _layerHistory.Pop();
                if (currentOnTop) currentOnTop.Interactable = false;
            }

            _isLoading = true;

            var succeed = await layer.Pop();

            if (succeed)
            {
                ActiveLayerInteractAfterLoad();
            }
            else
            {
                Xylog.LogError(
                    $"can not pop <color=cyan>{layer.CurrentView.ViewData.viewName}</color> of category <color=cyan>{category}</color> !");
                return;
            }

            _isLoading = false;
        }


        [Button]
        public void PopAll(string category)
        {
            var layer = GetGUILayerEntity(category);
            if (!layer)
            {
                Xylog.LogError($"not exist layer for category {category} !");
                return;
            }

            if (layer.CurrentViewCount < 1) return;

            layer.PopAll();
            _layerHistory = StackUtils.Remove(_layerHistory, layer);
            ActiveLayerInteractAfterLoad();
        }

        void ActiveLayerInteractAfterLoad()
        {
            currentLayerInteractive = _layerHistory.Count > 0 ? _layerHistory.Peek() : null;

            // handle interactable for layer
            for (int i = 0; i < _layers.Length; i++)
            {
                _layers[i].Interactable = currentLayerInteractive == _layers[i];
            }

            InputManager.Instance.SetActiveInput(true);
        }

        #endregion

        #region Internals

        async void InitLayer()
        {
            _layers = new GUILayerEntity[settings.layers.Length];
            for (int i = 0; i < settings.layers.Length; i++)
            {
                var layerData = settings.layers[i];
                var layerName = layerData.layerName;

                if (settings.excludeCategories.Length > 0)
                {
                    if (settings.excludeCategories.Contains(layerData.targetCategory)) continue;
                }

                var layer = Instantiate(settings.layerPrefab,
                    useContainerForLayer && layerContainer ? layerContainer : canvas.gameObject.transform);

                layer.gameObject.name = layerName;

                _bundleStartLoad?.Invoke(layerName);

                layer.Init(layerData, _variantLoad);
                await UniTask.WaitUntil(() => layer.IsReady);
                _layers[i] = layer;
            }

            _bundleLoadComplete?.Invoke(this);
        }

        public void Preload()
        {
            if (!loadOnStart) return;
            preloadSetting.Execute();
        }

        GUILayerEntity GetGUILayerEntity(string category)
        {
            return _layers.FirstOrDefault(e => e.Category == category);
        }

        #endregion
    }
}