﻿using System;
using UnityEngine;
using XynokConvention.Procedural;

namespace XynokGUI.Behaviors
{
    [Serializable]
    public class GuiTimeScaleBehavior
    {
        [Range(0, 1f)] public float timeScale = 0f;

        public void Execute()
        {
            TimeCycle.Instance.SetTimeScale(timeScale);
        }
    }
}