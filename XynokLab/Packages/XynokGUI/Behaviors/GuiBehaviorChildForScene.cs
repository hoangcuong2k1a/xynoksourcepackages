﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using XynokGUI.Controllers;
using XynokInput;

namespace XynokGUI.Behaviors
{
    [Serializable]
    public class GuiBehaviorChildForScene
    {
        [SerializeField] private bool useUnityEvent;
        [SerializeField] private GuiBehaviorType actionName;

        [ShowIf("useUnityEvent")] [SerializeField]
        private UnityEvent unityEvent;

        [SerializeField] [ShowIf("actionName", GuiBehaviorType.InputHandle)]
        private GuiInputBehavior inputHandle;


        [ShowIf("actionName", GuiBehaviorType.ActiveGui)]
        public ActiveGuiBehavior activeGuiBehavior;

        
        [ShowIf("actionName", GuiBehaviorType.QuitGame)]
        public QuitGameHandler quitGameHandler;
        
        private Action _action;

        public void Execute()
        {
            switch (actionName)
            {
                case GuiBehaviorType.InputHandle:
                    inputHandle.Execute();
                    break;

                case GuiBehaviorType.ActiveGui:
                    activeGuiBehavior.Execute();
                    break;
                
                case GuiBehaviorType.QuitGame:
                    quitGameHandler.Execute();
                    break;
            }

            if (useUnityEvent) unityEvent?.Invoke();
            _action?.Invoke();
        }

        public void AddInvoker(Action action)
        {
            _action -= action;
            _action += action;
        }

        public void RemoveInvoker(Action action)
        {
            _action -= action;
        }
    }
}