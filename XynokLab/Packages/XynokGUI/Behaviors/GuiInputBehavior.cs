﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokInput;

namespace XynokGUI.Behaviors
{
    public enum GuiInputBehaviorType
    {
        None = 0,
        DisableCurrentInput = 1000,
        EnableCurrentInput = 1001,
        SwapInputMap = 1002,
    }

    [Serializable]
    public class GuiInputBehavior
    {
        public GuiInputBehaviorType actionType;

        [ShowIf("actionType", GuiInputBehaviorType.SwapInputMap)] [SerializeField] [ValueDropdown("InputMapNames")]
        private string targetInputMap = "???";

#if UNITY_EDITOR
        private static string[] InputMapNames => EmbedInputMap.Instance.inputMaps;

#endif
        public void Execute()
        {
            switch (actionType)
            {
                case GuiInputBehaviorType.DisableCurrentInput:
                    DisableInput();
                    break;
                case GuiInputBehaviorType.EnableCurrentInput:
                    EnableInput();
                    break;
                case GuiInputBehaviorType.SwapInputMap:
                    InputManager.Instance.ActiveInputMap(targetInputMap);
                    break;
            }
        }

        void DisableInput()
        {
            InputManager.Instance.SetActiveInput(false);
        }

        void EnableInput()
        {
            InputManager.Instance.SetActiveInput(true);
        }
    }
}