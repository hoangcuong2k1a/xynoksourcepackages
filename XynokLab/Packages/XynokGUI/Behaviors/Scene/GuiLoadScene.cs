﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using XynokAssetBundle;
using XynokConvention.Procedural;
using XynokGUI.Controllers;

namespace XynokGUI.Behaviors.Scene
{
    [Serializable]
    public class GuiLoadScene
    {
        [SerializeField] [ValueDropdown("GetScenes")]
        private string targetScene = "???";

        public event Action OnStartLoad;
        public event Action<float> LoadProgress;
        public event Action OnLoadComplete;
        [SerializeField] private GuiBehaviorChildForScene[] onStartLoad;
        [SerializeField] private UnityEvent<float> onLoadProgress;
        [SerializeField] private GuiBehaviorChildForScene[] onSceneLoaded;

        private SceneManaged SceneManaged => GUIController.Instance.SceneManaged;
#if UNITY_EDITOR
        private static string[] GetScenes => SceneManaged.Instance.scenes;
#endif

        public void Execute()
        {
            if (targetScene == "???")
            {
                Debug.LogError($"cannot load scene <color=cyan>{targetScene}</color>!!!");
                return;
            }


            TimeCycle.Instance.StartCoroutine(StartLoad());
        }

        public void SetTargetScene(string sceneName)
        {
            targetScene = sceneName;
        }


        IEnumerator StartLoad()
        {
            var totalProgress = 1 + .9f;
            InvokeOnStartLoad();

            // maybe "onStartLoad" has some action which is async, so we need to wait for it.
            yield return new WaitForSeconds(SceneManaged.delayOnChangeScene);

            if (targetScene == SceneConst.currentScene) targetScene = SceneManager.GetActiveScene().name;


            AsyncOperationHandle<SceneInstance> handle =
                Addressables.LoadSceneAsync(targetScene, LoadSceneMode.Single, false);

            LoadProgress?.Invoke(handle.PercentComplete / totalProgress);
            onLoadProgress?.Invoke(handle.PercentComplete / totalProgress);

            yield return handle;

            //...

            //One way to handle manual scene activation.
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                var sceneProgress = handle.Result.ActivateAsync();
                LoadProgress?.Invoke((handle.PercentComplete + sceneProgress.progress) / totalProgress);
                onLoadProgress?.Invoke((handle.PercentComplete + sceneProgress.progress) / totalProgress);
                yield return sceneProgress;
            }

            //...
            InvokeOnSceneLoaded();
        }

        void InvokeOnStartLoad()
        {
            OnStartLoad?.Invoke();
            if (onStartLoad == null || onStartLoad.Length < 1) return;
            for (int i = 0; i < onStartLoad.Length; i++)
            {
                onStartLoad[i].Execute();
            }
        }

        void InvokeOnSceneLoaded()
        {
            OnLoadComplete?.Invoke();
            if (onSceneLoaded == null || onSceneLoaded.Length < 1) return;
            for (int i = 0; i < onSceneLoaded.Length; i++)
            {
                onSceneLoaded[i].Execute();
            }
        }
    }
}