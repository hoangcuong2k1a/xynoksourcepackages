﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle;
using XynokConvention;
using XynokConvention.Procedural;
using XynokGUI.Modulers.Loadings;

namespace XynokGUI.Behaviors.Scene
{
    [Serializable]
    public class GuiLoadSceneGuide
    {
        [SerializeField] [ValueDropdown("GetScenes")]
        private string targetScene = "???";

        [SerializeField] [ValueDropdown("GetGuiLoadingSceneCategory")]
        private string loadingSceneGuiCategory = "???";

        [SerializeField] [ValueDropdown("GetNames")]
        private string loadingSceneGui = "???";

        [SerializeField] private ActiveGuiBehavior[] afterSceneLoaded;

#if UNITY_EDITOR
        private static string[] GetScenes => SceneManaged.Instance.scenes;
        private static string[] GetGuiLoadingSceneCategory => Core.Data.GUISettings.Instance.categories;
        private static string[] GetNames => Core.Data.GUISettings.Instance.currentPrefabs;
#endif

        public string TargetScene => targetScene;

#if UNITY_EDITOR
        [Button]
        void SyncData()
        {
            Core.Data.GUISettings.Instance.EmitCurrentCategory(loadingSceneGuiCategory);
        }
#endif

        public void SetTargetScene(string sceneName)
        {
            targetScene = sceneName;
        }

        public void Execute()
        {
            var data = new LoadSceneEventData
            {
                sceneName = targetScene,
                afterSceneLoaded = afterSceneLoaded
            };
            // đăng ký event sau khi load scene
            EventManager.EmitEventData(XynokGuiEvent.StartLoadScene.ToString(), data);

            Controllers.GUIController.Instance.Load(loadingSceneGuiCategory, loadingSceneGui);
        }
    }
}