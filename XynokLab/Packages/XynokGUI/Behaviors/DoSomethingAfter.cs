﻿using UnityEngine;
using XynokConvention.Procedural;

namespace XynokGUI.Behaviors
{
 
    public class DoSomethingAfter : MonoBehaviour
    {
        [SerializeField] [Range(0, 1000f)] private float duration = 1;
        [SerializeField] private GuiBehaviorContainer action;
        private bool _canExecute;
        private float _t;

        private void Start()
        {
            TimeCycle.Instance.AddInvoker(Track);
        }

        public void Execute()
        {
            _canExecute = true;
        }

        public void Stop()
        {
            _canExecute = false;
        }

        public void Reset()
        {
            _canExecute = false;
            _t = 0;
        }

        void Track()
        {
            if (!_canExecute) return;
            if (_t < duration)
                _t += Time.deltaTime;
            else
            {
                action.Execute();
                _canExecute = false;
                _t = 0;
            }
        }
    }
}