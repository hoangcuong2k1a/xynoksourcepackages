﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using XynokGUI.Behaviors.Scene;

namespace XynokGUI.Behaviors
{
    [Serializable]
    public class GuiBehavior
    {
        [SerializeField] private GuiBehaviorType actionName;

        [Tooltip("nếu true thì sẽ chạy UnityEvent kể cả khi ko có action, orderExecute: action > unityEvent")]
        [SerializeField] 
        private bool useUnityEvent;

        [ShowIf("useUnityEvent")] [SerializeField] 
        private UnityEvent unityEvent;

        [SerializeField] [ShowIf("actionName", GuiBehaviorType.InputHandle)]
        private GuiInputBehavior inputHandle;

        [ShowIf("actionName", GuiBehaviorType.LoadScene)] [HideLabel]
        public GuiLoadSceneGuide loadSceneGuide;

        [ShowIf("actionName", GuiBehaviorType.ActiveGui)] [HideLabel]
        public ActiveGuiBehavior activeGuiBehavior;

        [ShowIf("actionName", GuiBehaviorType.QuitGame)] [HideLabel]
        public QuitGameHandler quitGameHandler;

        [ShowIf("actionName", GuiBehaviorType.ChangeTimeScale)] [HideLabel]
        public GuiTimeScaleBehavior timeScaleBehavior;

        [ShowIf("actionName", GuiBehaviorType.ActiveSheetGui)] [HideLabel]
        public GuiSheetBehavior guiSheetBehavior;

        private Action _action;

        public void Execute()
        {
            switch (actionName)
            {
                case GuiBehaviorType.InputHandle:
                    inputHandle.Execute();
                    break;

                case GuiBehaviorType.LoadScene:
                    loadSceneGuide.Execute();
                    break;

                case GuiBehaviorType.ActiveGui:
                    activeGuiBehavior.Execute();
                    break;

                case GuiBehaviorType.QuitGame:
                    quitGameHandler.Execute();
                    break;

                case GuiBehaviorType.ChangeTimeScale:
                    timeScaleBehavior.Execute();
                    break;
                case GuiBehaviorType.ActiveSheetGui:
                    guiSheetBehavior.Execute();
                    break;
            }

            if (useUnityEvent) unityEvent?.Invoke();
            _action?.Invoke();
        }


        public void AddInvoker(Action action)
        {
            _action -= action;
            _action += action;
        }

        public void RemoveInvoker(Action action)
        {
            _action -= action;
        }
    }
}