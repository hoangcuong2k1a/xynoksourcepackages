﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokGUI.Entities.Sheet;

namespace XynokGUI.Behaviors
{
    public enum SheetBehaviorType
    {
        Show = 1,
        Hide = 2,
    }

    [Serializable]
    public class GuiSheetBehavior
    {
        public GUISheetContainer sheetContainer;
        public SheetBehaviorType actionName;
        [ShowIf("actionName", ActionActiveGui.Show)] [ValueDropdown("GetNames")]
        public string sheetName = "???";
#if UNITY_EDITOR
        private static string[] GetNames => Core.Data.GUISettings.Instance.currentPrefabs;
#endif


#if UNITY_EDITOR
        [Button, GUIColor(0.4f, 0.8f, 1)]
        void SyncData()
        {
            if (sheetContainer)
            {
                Core.Data.GUISettings.Instance.EmitCurrentCategory(sheetContainer.Category);
                return;
            }

            Debug.LogError("Sheet container is null");
        }
#endif

        public void Execute()
        {
            if (!sheetContainer)
            {
                Debug.LogError("Sheet container is null");
                return;
            }

            switch (actionName)
            {
                case SheetBehaviorType.Show:
                    sheetContainer.ShowByName(sheetName);
                    break;
                case SheetBehaviorType.Hide:
                    sheetContainer.Pop();
                    break;
            }
        }
    }
}