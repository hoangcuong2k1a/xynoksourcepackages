﻿using Cysharp.Threading.Tasks;
using XynokGUI.Core.Data;

namespace XynokGUI.Behaviors.APIs
{
    public interface IGuiView
    {
        GuiBehaviorContainer OnInteractive { get; }
        GuiViewData ViewData { get; set; }
        ViewTransitionData TransitionData { get; set; }
        
        UniTask<bool> Show();
        UniTask<bool> Hide();

        // anim events
        void OnShowCompleted();
        void OnHideCompleted();
    }
}