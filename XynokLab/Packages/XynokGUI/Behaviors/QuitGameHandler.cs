﻿using System;
using UnityEngine;

namespace XynokGUI.Behaviors
{
    [Serializable]
    public struct QuitGameHandler
    {
        public void Execute()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                UnityEditor.EditorApplication.isPlaying = false;
                return;
            }
#endif
            Application.Quit();
        }
    }
}