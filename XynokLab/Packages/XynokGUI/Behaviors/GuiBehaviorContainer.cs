﻿using System;

namespace XynokGUI.Behaviors
{
    [Serializable]
    public class GuiBehaviorContainer
    {
        public GuiBehavior[] behaviors;

        private Action _action;

        public void Execute()
        {
            _action?.Invoke();
            if (behaviors == null || behaviors.Length < 1) return;
            foreach (var behavior in behaviors)
            {
                behavior.Execute();
            }
        }

        public void AddInvoker(Action action)
        {
            _action -= action;
            _action += action;
        }

        public void RemoveInvoker(Action action)
        {
            _action -= action;
        }
    }
}