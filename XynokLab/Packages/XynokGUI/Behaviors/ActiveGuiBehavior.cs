﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;


namespace XynokGUI.Behaviors
{
    public enum ActionActiveGui
    {
        None = 0,
        [LabelText("ẩn đi")]  Hide = 1,
        [LabelText("hiển thị")]  Show = 2,
      [LabelText("ẩn toàn bộ category")]  HideAllUiOfCategory = 3,
    }

    [Serializable]
    public class ActiveGuiBehavior
    {
        [SerializeField] private ActionActiveGui actionName;

        [ValueDropdown("GetCategories")] public string category = "???";
        [ShowIf("actionName", ActionActiveGui.Show)] [ValueDropdown("GetNames")] public string viewName = "???";
#if UNITY_EDITOR
        private static string[] GetCategories => Core.Data.GUISettings.Instance.categories;
        private static string[] GetNames => Core.Data.GUISettings.Instance.currentPrefabs;
#endif


#if UNITY_EDITOR
        [Button, GUIColor(0.4f, 0.8f, 1)]
        void SyncData()
        {
            Core.Data.GUISettings.Instance.EmitCurrentCategory(category);
        }
#endif

        public void Execute()
        {
            switch (actionName)
            {
                case ActionActiveGui.Show:
                    Controllers.GUIController.Instance.Load(category, viewName);
                    break;
                case ActionActiveGui.Hide:
                    Controllers.GUIController.Instance.Pop(category);
                    break;
                case ActionActiveGui.HideAllUiOfCategory:
                    Controllers.GUIController.Instance.PopAll(category);
                    break;
            }
        }
    }
}