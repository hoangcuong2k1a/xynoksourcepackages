﻿namespace XynokGUI.Behaviors
{
    public enum GuiBehaviorType
    {
        None = 0,
        InputHandle = 1000,
        LoadScene = 1001,
        ActiveGui = 1002,
        QuitGame = 1003,
        ChangeTimeScale = 1004,
        ActiveSheetGui = 1005,
        
    }
}