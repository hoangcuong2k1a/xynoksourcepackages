﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokGUI.Behaviors.Event
{
    [Serializable]
    public class EventManagedData
    {
        public string category;
        public string[] events;
    }


#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    [CreateAssetMenu(fileName = "EventManaged", menuName = "Xynok/GUI/EventManaged")]
    public class EventManaged : ScriptableObject
    {
#if UNITY_EDITOR

        public static EventManaged Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<EventManaged>("Xynok/GUI/EventManaged");

#endif

        [SerializeField] private EventManagedData[] events;

        [ReadOnly] public string[] categories;

        private void OnValidate()
        {
            if (events == null || events.Length < 1)
            {
                categories = Array.Empty<string>();
                return;
            }

            categories = new string[events.Length];
            for (int i = 0; i < events.Length; i++)
            {
                categories[i] = events[i].category;
            }
        }

        public string[] GetEvents(string category)
        {
            foreach (var e in events)
            {
                if (e.category == category)
                {
                    return e.events;
                }
            }

            return null;
        }
    }
}