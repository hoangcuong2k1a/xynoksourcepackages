﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokConvention.Procedural;

namespace XynokGUI.Behaviors.Event
{
    public class EventListener : MonoBehaviour
    {
        [ValueDropdown("GetCategories")] [SerializeField]
        private string category = "???";

        [ValueDropdown("_getEvents")] [SerializeField]
        private string eventName = "???";

        [SerializeField] private GuiBehaviorContainer action;
#if UNITY_EDITOR
        private static string[] GetCategories => EventManaged.Instance.categories;
#endif
        private static string[] _getEvents;

        private void OnValidate()
        {
#if UNITY_EDITOR
            _getEvents = EventManaged.Instance.GetEvents(category);
#endif
        }

        private void Start()
        {
            EventManager.StartListening($"{category}_{eventName}", Execute);
        }

        private void OnDestroy()
        {
            EventManager.StopListening($"{category}_{eventName}", Execute);
        }

        void Execute()
        {
            action.Execute();
        }

        public void ForceExecute() => action.Execute();
        public void AddInvoker(Action act) => action.AddInvoker(act);

        public void RemoveInvoker(Action act) => action.RemoveInvoker(act);
    }
}