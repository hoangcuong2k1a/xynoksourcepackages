﻿using Sirenix.OdinInspector;
using UnityEngine;
using XynokConvention.Procedural;

namespace XynokGUI.Behaviors.Event
{
    public class EventEmitter : MonoBehaviour
    {
        [ValueDropdown("GetCategories")] [SerializeField]
        private string category = "???";

        [ValueDropdown("_getEvents")] [SerializeField]
        private string eventName = "???";

        #if UNITY_EDITOR
        private static string[] GetCategories => EventManaged.Instance.categories;
        #endif
        private static string[] _getEvents;


        private void OnValidate()
        {
#if UNITY_EDITOR
            _getEvents = EventManaged.Instance.GetEvents(category);
#endif
        }

        public void EmitEvent()
        {
            EventManager.EmitEvent($"{category}_{eventName}");
        }
    }
}