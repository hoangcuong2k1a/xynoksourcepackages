﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokGUI.Sound.Data
{
    public enum SoundRepeatType
    {
        Once = 0,
        Loop = 1,
    }

    public enum SoundPlayMode
    {
        Independence = 0,
        Replace = 1,
    }

    public enum SoundTransitionType
    {
        Immediately = 0,
        FadeIn = 1,
    }

    public enum SoundVolumeSetting
    {
        Value = 0,
        Graph = 1,
    }

    public enum SoundPitchSetting
    {
        None = 0,
        RandomOnPlay = 1,
    }

    public interface ISound
    {
        SoundData SoundData { get; }
    }

    [Serializable]
    public class SoundData
    {
        [ReadOnly] public AudioClip audioClip;
        [ReadOnly] public AudioSource audioSource;

        [Header("COMMON")] public SoundMasterType soundType = SoundMasterType.SoundFx;

        [ShowIf("soundType", SoundMasterType.Background)]
        public SoundPlayMode playMode;


        [Header("TRANSITION")] public SoundTransitionType transitionType;

        [ShowIf("transitionType", SoundTransitionType.FadeIn)] [Range(0, 10f)]
        public float fadeInDuration = 2f;


        [Header("VOLUME")] public SoundVolumeSetting volumeSetting;

        [ShowIf("volumeSetting", SoundVolumeSetting.Graph)]
        public AnimationCurve volumeGraph = AnimationCurve.Linear(0, 0, 1, 1);


        [Header("PITCH")] public SoundPitchSetting pitchSetting;

        [ShowIf("pitchSetting", SoundPitchSetting.RandomOnPlay)]
        public MinMaxPitchData randomPitch;

        [HideInInspector] public bool isPlayingTween;
        [HideInInspector] public float originalPitch;
        [HideInInspector] public float originalVolume;


        public SoundData Clone()
        {
            return new SoundData
            {
                audioClip = audioClip,
                audioSource = audioSource,
                soundType = soundType,
                playMode = SoundPlayMode.Independence,
                transitionType = transitionType,
                fadeInDuration = fadeInDuration,
                volumeSetting = volumeSetting,
                volumeGraph = volumeGraph,
                pitchSetting = pitchSetting,
                randomPitch = randomPitch,
            };
        }
    }

    [Serializable]
    public class MinMaxPitchData
    {
        [Range(-3f, 3f)] public float min = .9f;
        [Range(-3f, 3f)] public float max = 1.1f;
    }
}