﻿using XynokConvention.Procedural;

namespace XynokGUI.Sound.Data
{
  
    public enum SoundMasterType
    {
        SoundFx = 0,
        Background = 1,
        Master = 999,
    }
    public struct EventSoundData
    {
        public SoundMasterType SoundType;
        public float Volume;
    }

    /// <summary>
    /// sử dụng để thông báo cho các sound player biết khi nào thì có sự thay đổi về volume
    /// </summary>
    public class SoundEventManager : AEventManager<EventSoundData>
    {
    }
}