﻿using UnityEngine;
using UnityEngine.UI;
using XynokGUI.Modulers.Txt;
using XynokGUI.Sound.Data;

namespace XynokGUI.Sound
{
    public class VolumeSlider : MonoBehaviour
    {
        public Slider slider;
        public TextBase valueTxt;
        public SoundMasterType type;

        private bool _started;

        private void Start()
        {
            _started = true;
            slider.onValueChanged.AddListener(OnValueChanged);
            Init();
        }

        private void OnEnable()
        {
            if (!_started) return;
            Init();
        }

        void Init()
        {
            var value = SoundManager.Instance.GetMasterVol(type).Value;
            slider.value = value;
            valueTxt.SetText(Mathf.RoundToInt(value * 100).ToString());
        }

        private void OnDestroy()
        {
            slider.onValueChanged.RemoveListener(OnValueChanged);
        }

        void OnValueChanged(float value)
        {
            SoundManager.Instance.GetMasterVol(type).Value = value;
            valueTxt.SetText(Mathf.RoundToInt(value * 100).ToString());
        }
    }
}