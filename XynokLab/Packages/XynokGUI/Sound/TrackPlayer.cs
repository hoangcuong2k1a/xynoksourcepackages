﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using XynokGUI.Sound.Data;
using XynokSourceGenerator.Generated.Data.PrimitiveData;
using Random = UnityEngine.Random;

namespace XynokGUI.Sound
{
    [RequireComponent(typeof(AudioSource))]
    public class TrackPlayer : MonoBehaviour, ISound
    {
        [SerializeField] private bool playOnEnable;
        [SerializeField] protected SoundData soundData;
        public virtual SoundData SoundData => soundData;

        protected string EventListener => soundData.soundType.ToString();
        protected string EventListenerMaster => SoundMasterType.Master.ToString();

        protected float OriginalVolume => soundData.originalVolume;
        protected float OriginalPitch => soundData.originalPitch;
        protected float currentVolume;
        protected float volumeOverrider = -1f;
        protected AudioSource AudioSource => soundData.audioSource;
        private FloatValue _controlValue;

        private Tween _upTween;

        protected bool IsPlayingTween
        {
            get => soundData.isPlayingTween;
            set => soundData.isPlayingTween = value;
        }

        private void OnValidate()
        {
            soundData.audioSource = GetComponent<AudioSource>();
            soundData.audioClip = AudioSource.clip;
            soundData.originalPitch = AudioSource.pitch;
            soundData.originalVolume = AudioSource.volume;
            soundData.audioSource.playOnAwake = playOnEnable;
        }


        private void OnEnable()
        {
            Init();
            if (playOnEnable) Play();
        }

        protected void Init()
        {
            SoundEventManager.StartListen(EventListener, OnVolumeChanged);
            SoundEventManager.StartListen(EventListenerMaster, OnVolumeChanged);
        }

        protected virtual void Dispose()
        {
            volumeOverrider = -1f;
            SoundEventManager.StopListen(EventListener, OnVolumeChanged);
            SoundEventManager.StopListen(EventListenerMaster, OnVolumeChanged);
            KillUpTween();
        }

        void OnDisable()
        {
            Dispose();
        }


        void OnVolumeChanged(EventSoundData data)
        {
            if (data.SoundType == SoundMasterType.Master)
            {
                volumeOverrider = OriginalVolume * SoundManager.Instance.GetMasterVol(soundData.soundType).Value *
                                  data.Volume;
            }

            if ((int)data.SoundType == (int)soundData.soundType)
            {
                volumeOverrider = OriginalVolume * SoundManager.Instance.GetMasterVol(SoundMasterType.Master).Value *
                                  data.Volume;
            }

            currentVolume = volumeOverrider;
            UpdateVol();
        }

        void UpdateVol()
        {
            AudioSource.volume = currentVolume;
        }

        protected void UpdateVol(float value)
        {
            AudioSource.volume = value;
        }

        public virtual void Stop()
        {
            AudioSource.Stop();
        }
        public virtual void Play()
        {
            if (!AudioSource) return;
            if (!AudioSource.clip) return;
            if (soundData.soundType == SoundMasterType.Background)
            {
                PlayBackground();
                return;
            }

            PlayInternal();
        }

        #region Get Volume and Pitch

        float GetVolumeOnStart()

        {
            float vol = 1f;
            if (soundData.volumeSetting == SoundVolumeSetting.Value)
            {
                vol = OriginalVolume * SoundManager.Instance.GetMasterVol(soundData.soundType).Value;
                return vol;
            }

            if (soundData.volumeSetting == SoundVolumeSetting.Graph)
            {
                vol = OriginalVolume * soundData.volumeGraph.Evaluate(0);
                return vol;
            }

            return vol;
        }

        float GetPitchOnStart()
        {
            float pitch;
            if (soundData.pitchSetting == SoundPitchSetting.RandomOnPlay)
            {
                pitch = Random.Range(soundData.randomPitch.min, soundData.randomPitch.max);
                return pitch;
            }

            pitch = OriginalPitch;
            return pitch;
        }

        #endregion

        #region Play types

        protected virtual async void PlayBackground()

        {
            if (soundData.playMode == SoundPlayMode.Independence) PlayInternal();

            if (soundData.playMode == SoundPlayMode.Replace)
            {
                AudioSource.Stop();
                await UniTask.WaitUntil(() => SoundManager.Instance);
                SoundManager.Instance.ReplaceBackground(this);
            }
        }

        protected async void PlayInternal()
        {
            await UniTask.WaitUntil(() => SoundManager.Instance);

            // volume
            currentVolume = GetVolumeOnStart();
            UpdateVol();

            // pitch
            AudioSource.pitch = GetPitchOnStart();

            // transition
            if (soundData.transitionType == SoundTransitionType.Immediately)
            {
                if (AudioSource.gameObject.activeSelf) AudioSource.Play();
            }

            if (soundData.transitionType == SoundTransitionType.FadeIn) FadeIn();
        }

        #endregion

        #region Fade

        void FadeIn()
        {
            KillUpTween();
            _upTween = DOTween.To(() => 0f, UpdateVol, currentVolume, soundData.fadeInDuration).OnStart(() =>
            {
                IsPlayingTween = true;
                AudioSource.volume = 0;
                AudioSource.Play();
            }).OnComplete(() => { IsPlayingTween = false; });
            _upTween.Play();
        }

        protected void KillUpTween()
        {
            _upTween?.Kill(true);
        }

        #endregion


        private void Update()
        {
            if (IsPlayingTween) return;
            if (!AudioSource) return;
            if (!AudioSource.isPlaying) return;
            if (!AudioSource.clip) return;
            if (soundData.volumeSetting != SoundVolumeSetting.Graph) return;
            currentVolume = GetVolumeSeed() *
                            soundData.volumeGraph.Evaluate(AudioSource.time / AudioSource.clip.length);
            UpdateVol();
        }

        float GetVolumeSeed() => volumeOverrider >= 0 ? volumeOverrider : OriginalVolume;
    }
}