﻿using UnityEngine;
using XynokConvention.Patterns;
using XynokGUI.Sound.Data;
using XynokSourceGenerator.Generated.Data.PrimitiveData;

namespace XynokGUI.Sound
{
  

    public class SoundManager : ASingleton<SoundManager>
    {
        [SerializeField] private TrackPlayerMainBackground backgroundPlayer;
        public FloatValue master;
        public FloatValue background;
        public FloatValue soundFx;

        private string EventUpdateMaster => SoundMasterType.Master.ToString();
        private string EventUpdateBackground => SoundMasterType.Background.ToString();
        private string EventUpdateSoundFx => SoundMasterType.SoundFx.ToString();

        private void Start()
        {
            master.OnChanged += UpdateMaster;
            background.OnChanged += UpdateBackground;
            soundFx.OnChanged += UpdateSoundFx;
        }

        public void ReplaceBackground(ISound sound)
        {
            backgroundPlayer.SetSound(sound);
        }
        public FloatValue GetMasterVol(SoundMasterType soundType)
        {
            if (soundType == SoundMasterType.Background) return background;
            if (soundType == SoundMasterType.SoundFx) return soundFx;
            return master;
        } 
    

        void UpdateMaster(float newVol)
        {
            SoundEventManager.EmitEvent(EventUpdateMaster, new EventSoundData
            {
                SoundType = SoundMasterType.Master,
                Volume = newVol
            });
        }

        void UpdateBackground(float newVol)
        {
            SoundEventManager.EmitEvent(EventUpdateBackground, new EventSoundData
            {
                SoundType = SoundMasterType.Background,
                Volume = newVol
            });
        }

        void UpdateSoundFx(float newVol)
        {
            SoundEventManager.EmitEvent(EventUpdateSoundFx, new EventSoundData
            {
                SoundType = SoundMasterType.SoundFx,
                Volume = newVol
            });
        }

    }
}