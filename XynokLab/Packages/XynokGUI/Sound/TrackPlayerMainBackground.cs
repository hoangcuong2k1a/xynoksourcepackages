﻿using System;
using DG.Tweening;
using XynokGUI.Sound.Data;

namespace XynokGUI.Sound
{
    /// <summary>
    /// only use for background sound and <b>ALL GAME MUST HAS ONLY ONE OF THIS !!!</b>
    /// if you want to play sound effect or another background, use <see cref="TrackPlayer"/>
    /// </summary>
    public class TrackPlayerMainBackground : TrackPlayer
    {
        private SoundData _overriderData;
        private Tween _downTween;

        public void SetSound(ISound sound)
        {
            _overriderData = sound.SoundData;
            if (AudioSource && AudioSource.isPlaying && _overriderData.transitionType == SoundTransitionType.FadeIn)
            {
                IsPlayingTween = true;
                FadeOut(_overriderData.fadeInDuration / 2, PlayNewBackground);
                return;
            }

            PlayNewBackground();
        }

        protected override void PlayBackground()
        {
            PlayInternal();
        }

        void PlayNewBackground()
        {
            if (AudioSource) AudioSource.Stop();
            CopyAudioSource();
            Dispose();
            Init();
            Play();
        }

        void CopyAudioSource()
        {
            // general
            soundData.audioSource.clip = _overriderData.audioClip;
            soundData.audioSource.volume = _overriderData.audioSource.volume;
            soundData.audioSource.pitch = _overriderData.audioSource.pitch;
            soundData.audioSource.loop = _overriderData.audioSource.loop;
            soundData.audioSource.priority = _overriderData.audioSource.priority;
            soundData.audioSource.playOnAwake = _overriderData.audioSource.playOnAwake;
            soundData.audioSource.panStereo = _overriderData.audioSource.panStereo;
            soundData.audioSource.spatialBlend = _overriderData.audioSource.spatialBlend;
            soundData.audioSource.reverbZoneMix = _overriderData.audioSource.reverbZoneMix;
            // 3d
            soundData.audioSource.dopplerLevel = _overriderData.audioSource.dopplerLevel;
            soundData.audioSource.spread = _overriderData.audioSource.spread;
            soundData.audioSource.rolloffMode = _overriderData.audioSource.rolloffMode;
            soundData.audioSource.minDistance = _overriderData.audioSource.minDistance;
            soundData.audioSource.maxDistance = _overriderData.audioSource.maxDistance;

            // custom
            soundData.soundType = _overriderData.soundType;
            soundData.playMode = _overriderData.playMode;
            soundData.transitionType = _overriderData.transitionType;
            soundData.fadeInDuration = _overriderData.fadeInDuration;
            soundData.volumeSetting = _overriderData.volumeSetting;
            soundData.volumeGraph = _overriderData.volumeGraph;
            soundData.pitchSetting = _overriderData.pitchSetting;
            soundData.randomPitch = _overriderData.randomPitch;
            soundData.isPlayingTween = _overriderData.isPlayingTween;
            soundData.originalPitch = _overriderData.originalPitch;
            soundData.originalVolume = _overriderData.originalVolume;
            
            _overriderData = null;
        }

        void FadeOut(float duration, Action callback)
        {
            KillUpTween();
            KillDownTween();
            _downTween = DOTween.To(() => currentVolume, UpdateVol, 0, duration).OnStart(() =>
                {
                    IsPlayingTween = true;
                })
                .OnComplete(() =>
                {
                    callback?.Invoke();
                    IsPlayingTween = false;
                });

            _downTween.Play();
        }

        void KillDownTween()
        {
            _downTween?.Kill(true);
        }

        protected override void Dispose()
        {
            base.Dispose();
            KillDownTween();
        }
    }
}