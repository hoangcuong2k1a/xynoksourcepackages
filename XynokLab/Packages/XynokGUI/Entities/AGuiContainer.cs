﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using XynokGUI.Behaviors;

namespace XynokGUI.Entities
{
    public enum GuiViewEvent
    {
        OnShow,
        OnShowCompleted,
        OnHide,
        OnHideCompleted
    }

    [RequireComponent(typeof(CanvasGroup))]
    [RequireComponent(typeof(RectMask2D))]
    public abstract class AGuiContainer : MonoBehaviour
    {
        [ReadOnly] [SerializeField] private CanvasGroup canvasGroup;

        [Header("COMMON EVENTS")] [SerializeField]
        protected GuiBehaviorContainer onShow;

        [SerializeField] protected GuiBehaviorContainer onShowCompleted;
        [SerializeField] protected GuiBehaviorContainer onInteractable;
        [SerializeField] protected GuiBehaviorContainer onHide;
        [SerializeField] protected GuiBehaviorContainer onHideCompleted;

        [ShowInInspector]
        public bool Interactable
        {
            get => canvasGroup.interactable;
            set
            {
                canvasGroup.interactable = value;
                if (value) onInteractable.Execute();
            }
        }

        protected virtual void OnValidate()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public void AddInvoker(Action action, GuiViewEvent eventType)
        {
            if (eventType == GuiViewEvent.OnShow) onShow.AddInvoker(action);
            if (eventType == GuiViewEvent.OnShowCompleted) onShowCompleted.AddInvoker(action);
            if (eventType == GuiViewEvent.OnHide) onHide.AddInvoker(action);
            if (eventType == GuiViewEvent.OnHideCompleted) onHideCompleted.AddInvoker(action);
        }

        public void RemoveInvoker(Action action, GuiViewEvent eventType)
        {
            if (eventType == GuiViewEvent.OnShow) onShow.RemoveInvoker(action);
            if (eventType == GuiViewEvent.OnShowCompleted) onShowCompleted.RemoveInvoker(action);
            if (eventType == GuiViewEvent.OnHide) onHide.RemoveInvoker(action);
            if (eventType == GuiViewEvent.OnHideCompleted) onHideCompleted.RemoveInvoker(action);
        }
    }
}