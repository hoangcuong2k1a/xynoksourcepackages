﻿using System;
using UnityEngine;
using UnityEngine.UI;
using XynokConvention.Data;
using XynokGUI.Modulers.Txt;

namespace XynokGUI.Entities.StatView
{
    public abstract class AEntityStateView<T1, T2> : MonoBehaviour, IDisposable
        where T1 : Enum
        where T2 : APairData<T1, bool>
    {
        [Header("DEFAULT STATE VIEW")]
        [SerializeField] private Toggle valueToggle;
        [SerializeField] private TextBase stateNameTxt;
        [SerializeField] private T1 targetState;

        protected abstract T2 GetSourceData(T1 key);
        protected abstract string GetStateName(T1 key);
        protected T2 StateData;

        protected virtual void OnEnable()
        {
            StateData = GetSourceData(targetState);
            if (StateData == null)
            {
                stateNameTxt.SetText("???");
                Debug.LogError(
                    $"[{name} - <color=cyan>{GetType().Name}</color>]: {targetState} does not exist");
                return;
            }

            stateNameTxt.SetText(GetStateName(targetState));
            StateData.OnChanged += OnStateChanged;
            valueToggle.onValueChanged.AddListener(OnToggleChanged);
            OnStateChanged(StateData.Value);
        }

        void OnToggleChanged(bool value)
        {
            StateData.Value = value;
        }

        void OnStateChanged(bool value)
        {
            valueToggle.isOn = value;
        }

        public virtual void Dispose()
        {
            if (StateData == null) return;
            StateData.OnChanged -= OnStateChanged;
            valueToggle.onValueChanged.RemoveListener(OnToggleChanged);
        }
    }
}