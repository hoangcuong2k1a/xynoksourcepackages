﻿using System;
using TMPro;
using UnityEngine;
using XynokConvention.Data;
using XynokGUI.Modulers.ImgFills;
using XynokGUI.Modulers.Txt;

namespace XynokGUI.Entities.StatView
{
    public abstract class AEntityStatView<T1, T2> : ImageFillBase
        where T1 : Enum
        where T2 : APairData<T1, float>
    {
        [Header("DEFAULT STAT VIEW")] [SerializeField]
        protected T1 targetStat;

        [SerializeField] protected TextBase valueTxt;
        [SerializeField] protected TextBase statNameTxt;
        [SerializeField] protected TMP_InputField valueField;
        protected abstract T2 GetSourceData(T1 key);
        protected abstract string GetStatName(T1 key);
        protected T2 StatData;

        protected override void Awake()
        {
            base.Awake();
            StatData = GetSourceData(targetStat);
            if (StatData == null)
            {
                statNameTxt.SetText("???");
                Debug.LogError(
                    $"[{name} - <color=cyan>{GetType().Name}</color>]: {targetStat} does not exist");
                return;
            }

            statNameTxt.SetText(GetStatName(targetStat));
            StatData.OnChanged += OnStatChanged;
            valueField.onValueChanged.AddListener(OnInputChanged);
            fillValue.SetBaseValue(StatData.BaseValue);
            OnStatChanged(StatData.Value);
        }

        void OnInputChanged(string value)
        {
            if (string.IsNullOrEmpty(value)) return;
            float result = float.Parse(value);
            StatData.Value = result;
        }

        public override void Dispose()
        {
            base.Dispose();
            if (StatData == null) return;
            StatData.OnChanged -= OnStatChanged;
            valueField.onValueChanged.RemoveListener(OnInputChanged);
        }

        void OnStatChanged(float value)
        {
            fillValue.Value = value;
            var viewValue = value < 0 ? 0 : value;
            valueTxt.SetText($"{viewValue}/{fillValue.BaseValue}");
            valueField.text = $"{viewValue}";
        }
    }
}