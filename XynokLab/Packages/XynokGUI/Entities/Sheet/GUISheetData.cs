﻿using System;
using Sirenix.OdinInspector;
using XynokGUI.Core.Data;
using GUISettings = XynokGUI.Core.Data.GUISettings;

namespace XynokGUI.Entities.Sheet
{
    [Serializable]
    public class GUISheetData
    {
        [ValueDropdown("GetCategories")] public string targetCategory;
        [ReadOnly] public GuiViewData[] viewData;


#if UNITY_EDITOR
        private static string[] GetCategories => GUISettings.Instance.categories;

        
       public void SyncCategory()
        {
            viewData = GUISettings.Instance.GetGUIPoolData(targetCategory);
        }
#endif

        public GUILayerData GetGUILayerData()
        {
            var data = new GUILayerData
            {
                layerName = targetCategory,
                targetCategory = targetCategory,
                viewData = viewData
            };
            return data;
        }
    }
}