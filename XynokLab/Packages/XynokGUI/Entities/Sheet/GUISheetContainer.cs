﻿using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokGUI.Entities.Sheet
{
    [RequireComponent(typeof(GUILayerEntity))]
    public class GUISheetContainer : MonoBehaviour
    {
        [SerializeField] [ReadOnly] protected GUILayerEntity sheetContainer;
        [SerializeField] protected GUISheetData sheetData;

        private bool _isTransitioning;
        public string Category => sheetData.targetCategory;

        protected virtual void OnValidate()
        {
            sheetContainer = GetComponent<GUILayerEntity>();
#if UNITY_EDITOR
            sheetData.SyncCategory();
#endif
        }

        private void Start()
        {
            sheetContainer.Init(sheetData.GetGUILayerData());
        }

        public async void ShowByIndex(int viewIndex)
        {
            await UniTask.WaitUntil(() => !_isTransitioning);
            _isTransitioning = true;
            await sheetContainer.Load(sheetData.viewData[viewIndex].viewName);
            _isTransitioning = false;
        }

        public async void ShowByName(string viewName)
        {
            await UniTask.WaitUntil(() => !_isTransitioning);
            _isTransitioning = true;
            await sheetContainer.Load(viewName);
            _isTransitioning = false;
        }

        public void PopAll() => sheetContainer.PopAll();

        public async void Pop()
        {
            await UniTask.WaitUntil(() => !_isTransitioning);
            _isTransitioning = true;
            await sheetContainer.Pop();
            _isTransitioning = false;
        }
    }
}