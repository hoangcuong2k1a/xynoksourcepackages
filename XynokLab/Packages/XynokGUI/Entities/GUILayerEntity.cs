﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using XynokAssetBundle.Data;
using XynokDebugger;
using XynokGUI.Behaviors.APIs;
using XynokGUI.Controllers;
using XynokGUI.Core.Data;
using XynokGUI.Core.Data.EventData;

namespace XynokGUI.Entities
{
    public class GUILayerEntity : AGuiContainer
    {
        [Header("LAYER DATA")] [SerializeField]
        private GUIPool pool;

        [SerializeField] private Transform content;

        public string Category => _layerData.targetCategory;
        public IGuiView CurrentView => _history.Peek();
        [ShowInInspector] [ReadOnly] public int CurrentViewCount => _currentViewCount;
        public bool IsReady => _isReady;
        private GUILayerData _layerData;

        private Stack<IGuiView> _history = new();
        private bool _isTransitioning;
        private bool _isPoolIniting;
        private bool _isReady;
        private int _currentViewCount;
        private Action<string> _loadViewCallback;

        private PoolData _defaultPool = new()
        {
            preload = 0,
            capacity = 1,
            recycle = true
        };


        public void Init(GUILayerData guiLayerData, Action<string> loadViewCallback = null)
        {
            _layerData = guiLayerData;
            _loadViewCallback = loadViewCallback;
            CreatePool();
        }


        #region Externals

        public async UniTask<IGuiView> LoadAndSendData<T>(string viewName, T dataDeliver) where T : IGuiDataDeliver
        {
            if (!CanLoadView(viewName)) return null;

            StartShow();

            if (_history.Count > 0 && !_layerData.stackable)
            {
                _isTransitioning = true;
                await HideCurrentView();
                _isTransitioning = false;
            }


            var obj = pool.Spawn(viewName, Vector3.zero, content);
            var view = obj.GetComponent<IGuiView>();
            view.ViewData ??= _layerData.GetGuiViewData(viewName);

            _history.Push(view);

            // send data
            if (view is IGuiDataReceiver<T> receiver)
            {
                receiver.SetDependency(dataDeliver);
            }
            else
            {
                Xylog.LogError(
                    $"view <color=cyan>{viewName}</color> is not an: {typeof(IGuiDataReceiver<T>).Name}");
                return null;
            }

            _isTransitioning = true;

            await view.Show();

            EndShow(view);

            return view;
        }

        public async UniTask<bool> Load(string viewName)
        {
            if (!CanLoadView(viewName)) return false;

            StartShow();

            if (_history.Count > 0 && !_layerData.stackable)
            {
                _isTransitioning = true;
                await HideCurrentView();
                _isTransitioning = false;
            }


            var obj = pool.Spawn(viewName, Vector3.zero, content);
            var view = obj.GetComponent<IGuiView>();
            view.ViewData ??= _layerData.GetGuiViewData(viewName);

            _history.Push(view);

            _isTransitioning = true;

            await view.Show();

            EndShow(view);

            return true;
        }


        public void PopAll()
        {
            while (_history.Count > 0)
            {
                var view = _history.Pop();
                pool.DeSpawnView(view.ViewData.viewName);
                onInteractable.RemoveInvoker(view.OnInteractive.Execute);
            }

            _currentViewCount = 0;
        }

        public async UniTask<bool> Pop()
        {
            if (!CanPop()) return false;

            StartPop();

            var view = _history.Pop();
            _isTransitioning = true;

            // next view is not exist remove invoker of interactable
            onInteractable.RemoveInvoker(view.OnInteractive.Execute);

            await view.Hide();

            // clear invoker of interactable
            _isTransitioning = false;
            pool.DeSpawnView(view.ViewData.viewName);
            _currentViewCount--;

            if (_history.Count < 1)
            {
                return true;
            }

            if (!_layerData.stackable)
            {
                _isTransitioning = true;
                var lastView = _history.Peek();
                var obj = pool.Spawn(lastView.ViewData.viewName, Vector3.zero, content);
                var viewToShow = obj.GetComponent<IGuiView>();
                await viewToShow.Show();
            }

            // view and show last view 
            EndPop(_history.Peek());
            return true;
        }

        #endregion

        #region Events

        void StartShow()
        {
        }

        void EndShow(IGuiView view)
        {
            // next time if layer is interactable, view will be interactable too
            onInteractable.AddInvoker(view.OnInteractive.Execute);

            _isTransitioning = false;
            _currentViewCount++;
        }


        void StartPop()
        {
        }

        void EndPop(IGuiView viewToShow)
        {
            _isTransitioning = false;
            onInteractable.AddInvoker(viewToShow.OnInteractive.Execute);
            viewToShow.OnInteractive.Execute();
            _currentViewCount++;
        }

        #endregion

        #region Validate

        bool CanPop()
        {
            if (_isTransitioning)
            {
                Debug.LogWarning($"[{gameObject.name}]: can not pop: is on transition !");
                return false;
            }

            if (_history.Count < 1)
            {
                Debug.LogWarning($"[{gameObject.name}]: can not pop: history is empty !");
                return false;
            }

            return true;
        }

        bool CanLoadView(string viewName)
        {
            if (_isTransitioning)
            {
                Debug.LogWarning($"[{gameObject.name}]: can not load {viewName}: is on transition !");
                return false;
            }

            if (_history.Count > 0 && _history.Peek().ViewData.viewName == viewName)
            {
                Debug.LogWarning($"[{gameObject.name}]: can not load {viewName} : Duplicated !");
                return false;
            }


            return true;
        }

        #endregion

        #region Internals

        async UniTask<bool> HideCurrentView()
        {
            var lastView = _history.Peek();
            bool hideSucceed = await lastView.Hide();
            onInteractable.RemoveInvoker(lastView.OnInteractive.Execute);
            pool.DeSpawnView(lastView.ViewData.viewName);
            _currentViewCount--;
            return hideSucceed;
        }

        async void CreatePool()
        {
            var viewsData = _layerData.viewData;
            for (int i = 0; i < viewsData.Length; i++)
            {
                int index = i;
                var viewData = viewsData[index];
                var assetReference =
                    GUIController.Instance.GUIPrefabManaged.GetGuiAssetReference(_layerData.targetCategory,
                        viewData.viewName);

                if (assetReference == null)
                {
                    Xylog.LogError($"[{name}]: can not find asset reference of: {viewData.viewName} !");
                    continue;
                }

                await UniTask.WaitUntil(() => !_isPoolIniting);
                _isPoolIniting = true;

                _loadViewCallback?.Invoke(viewData.viewName);
                if (assetReference.IsValid()) assetReference.ReleaseAsset();
                assetReference.LoadAssetAsync<GameObject>().Completed += OnInstantiateCompleted;

                void OnInstantiateCompleted(AsyncOperationHandle<GameObject> source)
                {
                    var guiView = source.Result.GetComponent<IGuiView>();
                    guiView.ViewData = viewData;
                    _isPoolIniting = false;
                    pool.CreatePool(viewData.viewName, source.Result, _defaultPool);
                }
            }

            await UniTask.WaitUntil(() => !_isPoolIniting);
            _isReady = true;
        }

        #endregion
    }
}