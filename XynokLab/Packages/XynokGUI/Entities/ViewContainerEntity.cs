﻿using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokGUI.Behaviors;
using XynokGUI.Behaviors.APIs;
using XynokGUI.Core.Data;
using XynokUtils;

namespace XynokGUI.Entities
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(RectTransform))]
    public class ViewContainerEntity : AGuiContainer, IGuiView
    {
        [Header("TRANSITION DATA")] [ReadOnly] [SerializeField]
        private RectTransform rootTrans;

        [SerializeField] private RectTransform viewTrans;
        [SerializeField] private ViewTransitionData transitionData;

        private GuiViewData _runtimeData;
        private bool _isTransitioning;

        public GuiBehaviorContainer OnInteractive => onInteractable;

        public GuiViewData ViewData
        {
            get => _runtimeData;
            set => _runtimeData = value;
        }

        public ViewTransitionData TransitionData
        {
            get => transitionData;
            set => transitionData = value;
        }

        [Button]
        void InitOnInspector()
        {
            if (!transitionData.rootAnimator) transitionData.rootAnimator = GetComponent<Animator>();
            transitionData.OnValidate();

            rootTrans = GetComponent<RectTransform>();

            if (!viewTrans) viewTrans = GuiConst.SetupRootView(transform);
        }


        public async UniTask<bool> Show()
        {
            if (_isTransitioning) return false;
            StretchView();
            _isTransitioning = true;
            onShow.Execute();

            if (ViewData.animateOnShow)
            {
                TransitionData.Show();
                await UniTask.WaitUntil(() => !_isTransitioning);
            }

            if (!ViewData.animateOnShow) OnShowCompleted();

            return _isTransitioning;
        }

        public async UniTask<bool> Hide()
        {
            if (_isTransitioning) return false;

            _isTransitioning = true;

            onHide.Execute();

            if (ViewData.animateOnHide)
            {
                TransitionData.Hide();
                await UniTask.WaitUntil(() => !_isTransitioning);
            }

            if (!ViewData.animateOnHide) OnHideCompleted();

            return _isTransitioning;
        }

        void StretchView()
        {
            rootTrans.SetAnchor(AnchorPresets.StretchAll);
            rootTrans.SetPivot(PivotPresets.MiddleCenter);
        }

        public void OnShowCompleted()
        {
            OnShowed();
            onShowCompleted.Execute();
            _isTransitioning = false;
        }

        public void OnHideCompleted()
        {
            OnHidden();
            onHideCompleted.Execute();
            _isTransitioning = false;
        }

        #region Internals

        protected virtual void OnShowed()
        {
        }

        protected virtual void OnHidden()
        {
        }

        #endregion
    }
}