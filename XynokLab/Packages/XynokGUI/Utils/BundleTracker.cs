﻿
using UnityEngine;
using UnityEngine.Events;
using XynokAssetBundle.Manager;
using XynokConvention.Patterns;
using XynokGUI.Modulers.ImgFills;
using XynokGUI.Modulers.Txt;

namespace XynokGUI.Utils
{
    public class BundleTracker : MonoBehaviour
    {
        [SerializeField] private TextBase loadingTxt;
        [SerializeField] private ImageFillBase progressFillBar;
        [SerializeField] private UnityEvent onCompleteLoading;
        private BundleManager BundleManager => Singleton.Get<BundleManager>();

         void Start()
        {
            BundleManager.loadProgress.OnChanged += DisplayProgress;
            BundleManager.OnAllLoadComplete += LoadComplete;
        }

         void OnDestroy()
        {
            BundleManager.loadProgress.OnChanged -= DisplayProgress;
            BundleManager.OnAllLoadComplete -= LoadComplete;
        }

        void LoadComplete()
        {
            onCompleteLoading?.Invoke();
        }

        void DisplayProgress(BundleLoadData loadData)
        {
            if (loadingTxt && loadingTxt.gameObject.activeSelf) loadingTxt.SetText(loadData.message);
            if (progressFillBar && progressFillBar.gameObject.activeSelf)
                progressFillBar.SetNewBaseValue(BundleManager.AssetCount, loadData.progress);
        }
    }
}