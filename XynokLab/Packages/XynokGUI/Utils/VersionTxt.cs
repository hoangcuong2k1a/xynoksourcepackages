﻿using UnityEngine;
using XynokGUI.Modulers.Txt;

namespace XynokGUI.Utils
{
    public class VersionTxt : MonoBehaviour
    {
        [SerializeField] private string prefix = "v";
        [SerializeField] private TextBase txt;

        private void OnEnable()
        {
            var version = Application.version;
            txt.SetText($"{prefix}{version}");
        }
    }
}