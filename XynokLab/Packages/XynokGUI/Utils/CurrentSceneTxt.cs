﻿using UnityEngine;
using XynokGUI.Modulers.Txt;
using UnityEngine.SceneManagement;
using XynokConvention;
using XynokConvention.Procedural;
using XynokGUI.Modulers.Loadings;

namespace XynokGUI.Utils
{
    public class CurrentSceneTxt : MonoBehaviour
    {
        [SerializeField] private TextBase txt;
        private string EventName => XynokGuiEvent.LoadSceneCompleted.ToString();

        private void OnEnable()
        {
            EventManager.StartListening(EventName, UpdateSceneName);
            Scene scene = SceneManager.GetActiveScene();
            txt.SetText(scene.name);
        }

        private void OnDisable()
        {
            EventManager.StopListening(EventName, UpdateSceneName);
        }

        void UpdateSceneName()
        {
            var data = EventManager.GetData(EventName);
            if (data is LoadSceneEventData loadSceneEventData)
            {
                // txt.SetText(loadSceneEventData.sceneName);
                txt.SetText(SceneManager.GetActiveScene().name);
            }
        }
    }
}