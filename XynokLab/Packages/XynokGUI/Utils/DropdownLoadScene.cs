﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using XynokAssetBundle;
using XynokGUI.Behaviors;
using XynokGUI.Behaviors.Scene;
using XynokGUI.Modulers.Dropdowns;

namespace XynokGUI.Utils
{
    /// <summary>
    /// a list all scene managed by SceneManaged, click to load scene
    /// </summary>
    public class DropdownLoadScene : DropdownBase
    {
        [ReadOnly] [SerializeField] private string[] scenes;
        [SerializeField] private ActiveGuiBehavior[] onclick;
        [SerializeField] private GuiLoadSceneGuide guiLoadScene;
#if UNITY_EDITOR
        private void OnValidate()
        {
            scenes = SceneManaged.Instance.scenes;
        }
#endif

        private void Start()
        {
            var dropdownProperties = new DropdownProperties
            {
                options = scenes,
                onValueChanged = LoadScene
            };
            SetDependency(dropdownProperties);
            LoadDefaultScene();
        }

        void LoadDefaultScene()
        {
            Scene scene = SceneManager.GetActiveScene();
            dropdown.SetValueWithoutNotify(Array.IndexOf(scenes, scene.name));
        }

        protected virtual void LoadScene(int sceneIndex)
        {
            if (onclick != null && onclick.Length > 0)
            {
                foreach (var behavior in onclick)
                {
                    behavior.Execute();
                }
            }

            guiLoadScene.SetTargetScene(scenes[sceneIndex]);
            guiLoadScene.Execute();
        }
    }
}