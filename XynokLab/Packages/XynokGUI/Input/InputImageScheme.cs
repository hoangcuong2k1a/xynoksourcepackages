﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using XynokInput;

namespace XynokGUI.Input
{
    public class InputImageScheme : MonoBehaviour
    {
        [SerializeField] private InputTextureManaged inputTextureManaged;
        [SerializeField] private Image img;
        
        [SerializeField] [ValueDropdown("ActionNames")]
        protected string actionName = "???";
        
#if UNITY_EDITOR
        private static string[] ActionNames => EmbedInputMap.Instance.inputActions;

#endif
        private void OnValidate()
        {
#if UNITY_EDITOR
            if (!inputTextureManaged)
                inputTextureManaged = InputTextureManaged.Instance;
#endif
        }

        private void Start()
        {
            OnControlSchemeChanged(InputManager.Instance.CurrentControlScheme);
            InputManager.Instance.OnControlSchemeChanged += OnControlSchemeChanged;
        }

        private void OnDestroy()
        {
            InputManager.Instance.OnControlSchemeChanged -= OnControlSchemeChanged;
        
        }

        void OnControlSchemeChanged(string scheme)
        {
           
            var texture = inputTextureManaged.GetTexture(scheme, actionName);
            img.sprite = texture;

           
        }
    }
}