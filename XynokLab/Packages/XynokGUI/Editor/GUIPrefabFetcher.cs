﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using XynokGUI.Core.Data;

namespace XynokGUI.Editor
{
    /// <summary>
    /// cập  nhật các thay đổi của các root folder GUIPrefabManaged và các prefab trong đó
    /// </summary>
    public class GUIPrefabFetcher : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            if (!IsConfigurationPass(importedAssets, deletedAssets)) return;
           
            EditorUtility.SetDirty(GUISettings.Instance);
            GUISettings.Instance.SyncPoolDataAndLayerData();
           
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }

        static bool IsConfigurationPass(string[] importedAssets, string[] deletedAssets)
        {
            EditorUtility.SetDirty(GUISettings.Instance);
            GUISettings.Instance.Refresh();
            
            var folderConfigs = GUISettings.Instance.categories;
            
            foreach (var folder in folderConfigs)
            {
                if (importedAssets.Any(x => x.StartsWith(folder)) || deletedAssets.Any(x => x.StartsWith(folder)))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
#endif