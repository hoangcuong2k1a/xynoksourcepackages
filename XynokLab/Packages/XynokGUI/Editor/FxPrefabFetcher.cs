﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using XynokAssetBundle;

namespace XynokGUI.Editor
{
    public class FxPrefabFetcher: AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            if (!IsConfigurationPass(importedAssets, deletedAssets)) return;
            
            EditorUtility.SetDirty(FxPrefabManaged.Instance);
            FxPrefabManaged.Instance.ReloadPrePools();
            
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }

        static bool IsConfigurationPass(string[] importedAssets, string[] deletedAssets)
        {
            var folderConfigs = FxPrefabManaged.Instance.rootFolder.folderPath;
            foreach (var folder in folderConfigs)
            {
                if (importedAssets.Any(x => x.StartsWith(folder)) || deletedAssets.Any(x => x.StartsWith(folder)))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
#endif