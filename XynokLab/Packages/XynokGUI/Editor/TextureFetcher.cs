﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using XynokAssetBundle;


namespace XynokGUI.Editor
{
    public class TextureFetcher: AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            if (!IsConfigurationPass(importedAssets, deletedAssets)) return;
            
            EditorUtility.SetDirty(TextureManaged.Instance);
            TextureManaged.Instance.ReloadPreloadTextures();
            
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }

        static bool IsConfigurationPass(string[] importedAssets, string[] deletedAssets)
        {
            var folderConfigs = TextureManaged.Instance.rootFolder.folderPath;
            foreach (var folder in folderConfigs)
            {
                if (importedAssets.Any(x => x.StartsWith(folder)) || deletedAssets.Any(x => x.StartsWith(folder)))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
#endif
