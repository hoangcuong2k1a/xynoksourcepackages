﻿#if UNITY_EDITOR

using System.Linq;
using UnityEditor;
using XynokAssetBundle;
using XynokGUI.Core.Data;

namespace XynokGUI.Editor
{
    public class ScenePrefabFetcher: AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            if (!IsConfigurationPass(importedAssets, deletedAssets)) return;
            EditorUtility.SetDirty(SceneManaged.Instance);
            SceneManaged.Instance.Refresh();
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        static bool IsConfigurationPass(string[] importedAssets, string[] deletedAssets)
        {
            GUISettings.Instance.Refresh();
            var folderConfigs = SceneManaged.Instance.folderPath;
            foreach (var folder in folderConfigs)
            {
                if (importedAssets.Any(x => x.StartsWith(folder)) || deletedAssets.Any(x => x.StartsWith(folder)))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
#endif
