﻿using System;
using UnityEngine;
using XynokConvention;
using XynokConvention.Procedural;
using XynokGUI.Behaviors;
using XynokGUI.Behaviors.Scene;
using XynokGUI.Entities;
using XynokUtils;

namespace XynokGUI.Modulers.Loadings
{
    /// <summary>
    /// gôm các action sẽ được thực hiện sau khi load scene completed và scene sẽ load
    /// </summary>
    [Serializable]
    public class LoadSceneEventData
    {
        public string sceneName;
        public ActiveGuiBehavior[] afterSceneLoaded;

        public void ExecuteAfterSceneLoaded()
        {
            EventManager.EmitEventData(XynokGuiEvent.LoadSceneCompleted.ToString(), this);

            if (afterSceneLoaded == null || afterSceneLoaded.Length < 1) return;
            foreach (var behavior in afterSceneLoaded)
            {
                behavior.Execute();
            }
        }
    }

    [RequireComponent(typeof(MonoEvent))]
    public class GuiLoadingScene : MonoBehaviour
    {
        public ViewContainerEntity rootView;
        public GuiLoadScene guiLoadScene;
        private string EventNameRegisterAfterSceneLoad => XynokGuiEvent.StartLoadScene.ToString();
        private LoadSceneEventData _loadSceneEventData;


        private void OnEnable()
        {
            rootView.AddInvoker(LoadScene, GuiViewEvent.OnShowCompleted);

            if (_loadSceneEventData != null)
                guiLoadScene.OnLoadComplete -= _loadSceneEventData.ExecuteAfterSceneLoaded;

            var data = EventManager.GetData(EventNameRegisterAfterSceneLoad);

            if (data is LoadSceneEventData loadSceneEventData)
            {
                _loadSceneEventData = loadSceneEventData;
                guiLoadScene.OnLoadComplete += _loadSceneEventData.ExecuteAfterSceneLoaded;
                EventManager.Dispose(EventNameRegisterAfterSceneLoad);
                return;
            }

            throw new Exception(
                $"[{nameof(GuiLoadingScene)}]: data of event {EventNameRegisterAfterSceneLoad} is not {nameof(LoadSceneEventData)}");
        }

        private void OnDisable()
        {
            rootView.RemoveInvoker(LoadScene, GuiViewEvent.OnShowCompleted);
        }

        void LoadScene()
        {
            guiLoadScene.SetTargetScene(_loadSceneEventData.sceneName);
            guiLoadScene.Execute();
        }
    }
}