﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using XynokConvention.APIs;

namespace XynokGUI.Modulers.Dropdowns
{
    [Serializable]
    public class DropdownProperties
    {
        public string[] options;
        public UnityEvent<int> onValueChangedUnityEvent;
        public Action<int> onValueChanged;
    }

    public class DropdownBase : MonoBehaviour, IInjectable<DropdownProperties>
    {
        [Header("DROPDOWN BASE")] public TMP_Dropdown dropdown;

        private DropdownProperties _dependency;

        public void SetDependency(DropdownProperties dependency)
        {
            if (_dependency != null) Dispose();
            
            // remove old data
            dropdown.ClearOptions();
            dropdown.onValueChanged.RemoveAllListeners();
            dropdown.options = new List<TMP_Dropdown.OptionData>();
            _dependency = dependency;
            
            // add new data
            dropdown.AddOptions(_dependency.options.ToList());
            
            dropdown.onValueChanged.AddListener(_dependency.onValueChanged.Invoke);
            if (_dependency is { onValueChangedUnityEvent: not null })
                dropdown.onValueChanged.AddListener(_dependency.onValueChangedUnityEvent.Invoke);
            OnSetDependency();
        }

        public void Dispose()
        {
            if (_dependency == null) return;
            dropdown.onValueChanged.RemoveAllListeners();
            dropdown.ClearOptions();
            OnDisable();
        }

        protected virtual void OnSetDependency()
        {
        }

        protected void OnDisable()
        {
        }
    }
}