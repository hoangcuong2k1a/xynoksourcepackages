﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using XynokGUI.Behaviors;
using XynokUtils;

namespace XynokGUI.Modulers.Buttons
{
    [RequireComponent(typeof(Button))]
    public class BtnBase : MonoEvent, ISelectHandler, IDeselectHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [Header("BUTTON DEFAULT EVENTS")] [SerializeField]
        public Button btn;

        public GuiBehaviorContainer onClicked;
        public GuiBehaviorContainer onSelected;
        public GuiBehaviorContainer onDeselected;
        public GuiBehaviorContainer onHoverEnter;
        public GuiBehaviorContainer onHoverExit;

        protected virtual void OnValidate()
        {
            if (!btn) btn = GetComponent<Button>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            btn.onClick.AddListener(onClicked.Execute);
        }

        protected virtual void Dispose()
        {
            btn.onClick.RemoveListener(onClicked.Execute);
        }

        public void Select()
        {
            btn.Select();
            onSelected.Execute();
        }

        public void DeSelect()
        {
            onDeselected.Execute();
        }

        public void Click()
        {
            onClicked?.Execute();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Dispose();
        }

        public void OnSelect(BaseEventData eventData)
        {
            onSelected.Execute();
        }

        public void OnDeselect(BaseEventData eventData)
        {
            onDeselected.Execute();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            onHoverEnter.Execute();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onHoverExit.Execute();
        }
    }
}