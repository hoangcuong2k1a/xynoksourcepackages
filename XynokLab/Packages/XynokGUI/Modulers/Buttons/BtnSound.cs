﻿using UnityEngine;
using XynokGUI.Sound;

namespace XynokGUI.Modulers.Buttons
{
    [RequireComponent(typeof(BtnBase))]
    public class BtnSound : MonoBehaviour
    {
        [SerializeField] protected BtnBase btnBase;
        [SerializeField] protected TrackPlayer clickSound;
        [SerializeField] protected TrackPlayer selectSound;
        [SerializeField] protected TrackPlayer hoverSound;


        protected virtual void OnValidate()
        {
            btnBase = GetComponent<BtnBase>();
        }

        protected virtual void OnEnable()
        {
            if (clickSound) btnBase.onClicked.AddInvoker(clickSound.Play);
            if (selectSound) btnBase.onSelected.AddInvoker(selectSound.Play);
            if (hoverSound) btnBase.onHoverEnter.AddInvoker(hoverSound.Play);
        }

        protected virtual void OnDisable()
        {
            if (clickSound)
            {
                clickSound.Stop();
                btnBase.onClicked.RemoveInvoker(clickSound.Play);
            }

            if (selectSound)
            {
                selectSound.Stop();
                btnBase.onSelected.RemoveInvoker(selectSound.Play);
            }

            if (hoverSound)
            {
                hoverSound.Stop();
                btnBase.onHoverEnter.RemoveInvoker(hoverSound.Play);
            }
        }
    }
}