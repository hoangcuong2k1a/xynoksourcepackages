﻿using Sirenix.OdinInspector;
using UnityEngine;
using XynokConvention;
using XynokConvention.Procedural;
using XynokGUI.Behaviors;
using XynokGUI.Core.Data;
using XynokInput;

namespace XynokGUI.Modulers.Listeners
{
    public class InputListener : MonoBehaviour
    {
        [Title("INPUT LISTENER", "Lưu ý: ko được thay đổi data khi đã runtime, chỉ có hiệu lực khi Enabled",
            TitleAlignments.Centered)]
        [SerializeField]
        protected InputListenType listenType;

        [ShowIf("listenType", InputListenType.SpecificMap)] [SerializeField] [ValueDropdown("InputMapNames")]
        protected string targetMap = "???";

        [SerializeField] [ValueDropdown("ActionNames")]
        protected string actionName = "???";

        [SerializeField] protected GuiBehavior[] onPerformed;
        private string EventName => XynokGuiEvent.SwapInputMap.ToString();
        private string _currentMapListening;

#if UNITY_EDITOR
        private static string[] InputMapNames => EmbedInputMap.Instance.inputMaps;
        private static string[] ActionNames => EmbedInputMap.Instance.inputActions;

#endif
        void OnEnable()
        {
            if (listenType == InputListenType.AllMap)
            {
                RegisterInputActionCurrentMap();
                EventManager.StartListening(EventName, RegisterInputActionCurrentMap);
            }

            if (listenType == InputListenType.SpecificMap) RegisterInputActionForSpecificMap();
        }

        void OnDisable()
        {
            DisposeListener();
        }

        void RegisterInputActionForSpecificMap()
        {
            var action = InputManager.Instance.GetInput(targetMap, actionName);
            if (action == null) return;
            action.OnPerformed += Execute;
            _currentMapListening = targetMap;
        }

        void RegisterInputActionCurrentMap()
        {
            targetMap = InputManager.Instance.CurrentMap;
            var action = InputManager.Instance.GetInput(targetMap, actionName);
            if (action == null) return;

            // dispose current listener
            if (!string.IsNullOrEmpty(_currentMapListening)) action.OnPerformed -= Execute;

            // register new listener
            action.OnPerformed += Execute;

            // update current map listening
            _currentMapListening = targetMap;
        }

        void DisposeListener()
        {
            if (listenType == InputListenType.AllMap)
                EventManager.StopListening(EventName, RegisterInputActionCurrentMap);

            var action = InputManager.Instance.GetInput(targetMap, actionName);

            if (action == null) return;

            action.OnPerformed -= Execute;
        }

        public virtual void Execute()
        {
            if (onPerformed == null || onPerformed.Length < 1) return;
            for (int i = 0; i < onPerformed.Length; i++)
            {
                onPerformed[i].Execute();
            }
        }
    }
}