﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using XynokUtils;

namespace XynokGUI.Modulers.Txt
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextBase : MonoBehaviour
    {
        [Header("TEXT BASE")] [SerializeField] protected TextMeshProUGUI txt;
        [SerializeField] protected bool useDuration;

        [ShowIf("useDuration")] [SerializeField] [Range(0, 5)]
        protected float duration = 0.5f;

        protected virtual void OnValidate()
        {
            txt = GetComponent<TextMeshProUGUI>();
        }

        [Button]
        public void SetText(string text)
        {
            if (useDuration) txt.DoText(text, duration);
            else txt.text = text;
        }
    }
}