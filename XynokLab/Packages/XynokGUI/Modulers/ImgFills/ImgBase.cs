﻿using UnityEngine;
using UnityEngine.UI;
using XynokAssetBundle.Pooling;

namespace XynokGUI.Modulers.ImgFills
{
    [RequireComponent(typeof(Image))]
    public class ImgBase : MonoBehaviour
    {
        public Image image;

        protected virtual void OnValidate()
        {
            image = GetComponent<Image>();
        }

        public async void SetImage(string category, string textureName)
        {
            image.sprite = await TexturePool.Instance.GetSprite(category, textureName);
        }
    }
}