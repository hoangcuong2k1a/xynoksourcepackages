﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using XynokSourceGenerator.Generated.Data.PrimitiveData;

namespace XynokGUI.Modulers.ImgFills
{
    public class ImageFillBase : MonoBehaviour, IDisposable
    {
        [Header("IMAGE FILL")] [SerializeField]
        protected Image rootValueImg;

        [SerializeField] protected Image changedValueImg;
        [SerializeField] protected bool useAnimation = true;

        [ShowIf(nameof(useAnimation))] [SerializeField] [Range(0f, 5f)]
        protected float durationChanged = 0.5f;

        [ShowIf(nameof(useAnimation))] [SerializeField]
        protected bool completeAnimationOnKill;

        [SerializeField] protected bool useGradient;

        [ShowIf("useGradient")] [SerializeField]
        protected Gradient gradientForRoot;

        [ShowIf("useGradient")] [SerializeField]
        protected Gradient gradientForChanged;

        public FloatValue fillValue;
        protected Tween tweenChangedValueImg;


        protected virtual void Awake()
        {
            fillValue.OnChanged += UpdateFill;
        }


        public void SetNewBaseValue(float baseValue, float value)
        {
            fillValue.OnChanged -= UpdateFill;
            fillValue.SetBaseValue(baseValue);
            fillValue.Value = value;
            UpdateFill(value);
            fillValue.OnChanged += UpdateFill;
        }

        void UpdateFill(float value)
        {
            if (!useAnimation)
            {
                SetValueImmediately(value);
                return;
            }

            if (value > fillValue.LastValue) Boost(value);
            if (value < fillValue.LastValue) Reduce(value);
        }

        public void SetValueImmediately(float value)
        {
            BoostImmediately(value);
            ReduceImmediately(value);
        }

        void ReduceImmediately(float value)
        {
            rootValueImg.fillAmount = value / fillValue.BaseValue;
            changedValueImg.fillAmount = rootValueImg.fillAmount;
            if (useGradient) changedValueImg.color = gradientForChanged.Evaluate(changedValueImg.fillAmount);
        }

        void BoostImmediately(float value)
        {
            var endValue = value / fillValue.BaseValue;
            changedValueImg.fillAmount = endValue;
            rootValueImg.fillAmount = endValue;
            if (useGradient) rootValueImg.color = gradientForRoot.Evaluate(rootValueImg.fillAmount);
        }

        void Reduce(float value)
        {
            rootValueImg.fillAmount = value / fillValue.BaseValue;
            if (useGradient) rootValueImg.color = gradientForRoot.Evaluate(rootValueImg.fillAmount);
            StopTween();
            tweenChangedValueImg = changedValueImg.DOFillAmount(rootValueImg.fillAmount, durationChanged).OnUpdate(() =>
            {
                if (useGradient) changedValueImg.color = gradientForChanged.Evaluate(changedValueImg.fillAmount);
            });
            tweenChangedValueImg.Play();
        }

        void Boost(float value)
        {
            var endValue = value / fillValue.BaseValue;
            StopTween();

            tweenChangedValueImg = changedValueImg.DOFillAmount(endValue, durationChanged).OnUpdate(() =>
            {
                if (useGradient) changedValueImg.color = gradientForChanged.Evaluate(changedValueImg.fillAmount);
            }).OnComplete(() =>
            {
                tweenChangedValueImg = rootValueImg.DOFillAmount(endValue, durationChanged).OnUpdate(() =>
                {
                    if (useGradient) rootValueImg.color = gradientForRoot.Evaluate(rootValueImg.fillAmount);
                });
                tweenChangedValueImg.Play();
            });

            tweenChangedValueImg.Play();
        }

        void StopTween()
        {
            if (tweenChangedValueImg == null) return;
            if (tweenChangedValueImg.IsActive() && tweenChangedValueImg.IsPlaying())
                tweenChangedValueImg.Kill(completeAnimationOnKill);
            tweenChangedValueImg = null;
        }

        protected virtual void OnDisable()
        {
            StopTween();
        }

        protected virtual void OnDestroy()
        {
            Dispose();
        }

        public virtual void Dispose()
        {
            fillValue.OnChanged -= UpdateFill;
            StopTween();
        }
    }
}