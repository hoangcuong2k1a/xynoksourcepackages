﻿using TMPro;
using XynokGUI.InfinityScroller.Implementation.ListView;

namespace XynokGUI.InfinityScroller.Test
{
    public class TestItemList: AItemListView<TestScrollerInfinityItemData>
    {
        public TextMeshProUGUI txt;
       
        protected override void Init()
        {
            txt.text = itemData.data;
        }

        protected override void OnDispose()
        {
          
        }
    }
}