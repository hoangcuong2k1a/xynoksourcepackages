﻿using TMPro;
using XynokGUI.InfinityScroller.Implementation.GridView;

namespace XynokGUI.InfinityScroller.Test
{
    public class TestItemGrid: AItemGridView<TestScrollerInfinityItemData>
    {
        public TextMeshProUGUI txt;
        protected override void Init()
        {
            txt.text = itemData.data;
        }

        protected override void OnDispose()
        {
            
        }
    }
}