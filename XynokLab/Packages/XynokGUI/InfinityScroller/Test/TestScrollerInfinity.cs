﻿using System;
using Sirenix.OdinInspector;
using XynokGUI.InfinityScroller.Implementation.ListView;


namespace XynokGUI.InfinityScroller.Test
{
    [Serializable]
    public class TestScrollerInfinityItemData
    {
        public string data;
    }

    public class TestScrollerInfinity : AListViewController<TestScrollerInfinityItemData, TestItemList>
    {
        public TestScrollerInfinityItemData[] data;
        
        void Start()
        {
            SetNewData(data);
        }

        [Button]
        void ClearAll()
        {
            listView.ClearAll();
            
        } [Button]
        void InitView()
        {
            SetNewData(data);
            
        }
        [Button]
        void InitRandomData(int amount=100)
        {
            data = new TestScrollerInfinityItemData[amount];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = new TestScrollerInfinityItemData();
                data[i].data = "Item " + UnityEngine.Random.Range(0, amount);
            }
        }
    }
}