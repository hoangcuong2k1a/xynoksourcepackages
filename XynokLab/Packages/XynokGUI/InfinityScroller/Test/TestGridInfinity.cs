﻿using Sirenix.OdinInspector;
using XynokGUI.InfinityScroller.Implementation.GridView;

namespace XynokGUI.InfinityScroller.Test
{
    public class TestGridInfinity: AGridViewController<TestScrollerInfinityItemData, TestItemGrid>
    {
        public TestScrollerInfinityItemData[] data;
        
        void Start()
        {
            SetNewData(data);
        }

        [Button]
        void ClearAll()
        {
            loopGridView.ClearAll();
            
        }
        [Button]
        void InitView()
        {
            SetNewData(data);
            
        }
        [Button]
        void InitRandomData(int amount=100)
        {
            data = new TestScrollerInfinityItemData[amount];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = new TestScrollerInfinityItemData();
                data[i].data = "Item " + UnityEngine.Random.Range(0, amount);
            }
        }
    }
}