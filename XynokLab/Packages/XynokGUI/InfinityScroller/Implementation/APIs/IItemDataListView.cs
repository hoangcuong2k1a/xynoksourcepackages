﻿using UnityEngine;

using XynokGUI.InfinityScroller.ListView;

namespace XynokGUI.InfinityScroller.Implementation.APIs
{
    /// <summary>
    /// This interface is used to inject data into the item.
    /// </summary>
    public interface IItemDataListView
    {
        int ItemIndex { get; }
        int ItemId { get; }
        LoopListView ParentListView { get; set; }
        bool IsInitHandlerCalled { get; set; }
        RectTransform RectTransformCached { get; }
        float Padding { get; }
    }
}