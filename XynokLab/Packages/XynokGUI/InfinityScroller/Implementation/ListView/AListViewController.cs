﻿using UnityEngine;
using XynokGUI.InfinityScroller.ListView;

namespace XynokGUI.InfinityScroller.Implementation.ListView
{
    /// <summary>
    /// infinite list view controller
    /// </summary>
    /// <typeparam name="T1">data</typeparam>
    /// <typeparam name="T2">item view</typeparam>
    [RequireComponent(typeof(LoopListView))]
    public abstract class AListViewController<T1, T2> : MonoBehaviour where T2 : AItemListView<T1>
    {
        public LoopListView listView;
        public T2 prefab;
        public T1[] allData;
        protected virtual void OnValidate()
        {
            listView = GetComponent<LoopListView>();
        }
        public void SetNewData(T1[] data)
        {
            listView.ClearAll();
            allData = data;
            listView.InitListView(allData.Length, ItemHandle);
        }

        LoopListViewItem ItemHandle(LoopListView viewRoot, int itemIndex)
        {
            if (itemIndex < 0 || itemIndex >= allData.Length)
            {
                return null;
            }

            LoopListViewItem item = viewRoot.NewListViewItem(prefab.gameObject.name);
            item.GetComponent<T2>().SetDependency(allData[itemIndex]);
            return item;
        }
    }
}