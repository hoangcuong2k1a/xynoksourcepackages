﻿using UnityEngine;
using XynokConvention.APIs;
using XynokGUI.InfinityScroller.ListView;

namespace XynokGUI.InfinityScroller.Implementation.ListView
{
    [RequireComponent(typeof(LoopListViewItem))]
    public abstract class AItemListView<T> : MonoBehaviour, IInjectable<T> 
    {
        protected T itemData;
        public void SetDependency(T dependency)
        {
            Dispose();
            itemData = dependency;
            Init();
        }

        public void Dispose()
        {
            OnDispose();
        }
        
        protected abstract void Init();
        protected abstract void OnDispose();
        
        
    }
}