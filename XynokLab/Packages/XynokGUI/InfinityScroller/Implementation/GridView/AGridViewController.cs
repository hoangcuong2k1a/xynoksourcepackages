﻿using UnityEngine;
using XynokGUI.InfinityScroller.GridView;


namespace XynokGUI.InfinityScroller.Implementation.GridView
{
    [RequireComponent(typeof(LoopGridView))]
    public abstract class AGridViewController<T1, T2> : MonoBehaviour where T2 : AItemGridView<T1>
    {
        public LoopGridView loopGridView;
        public T2 prefab;
        public T1[] allData;

        protected virtual void OnValidate()
        {
            loopGridView = GetComponent<LoopGridView>();
        }

        public void SetNewData(T1[] data)
        {
            loopGridView.ClearAll();
            allData = data;
            loopGridView.InitGridView(allData.Length, ItemHandle);
        }

        LoopGridViewItem ItemHandle(LoopGridView gridView, int itemIndex, int row, int column)
        {
            if (itemIndex < 0 || itemIndex >= allData.Length)
            {
                return null;
            }

            LoopGridViewItem item = gridView.NewListViewItem(prefab.gameObject.name);
            if (item.IsInitHandlerCalled == false)
            {
                item.IsInitHandlerCalled = true;
                item.GetComponent<T2>().InitFirstTime();
            }

            item.GetComponent<T2>().SetDependency(allData[itemIndex]);
            return item;
        }
    }
}