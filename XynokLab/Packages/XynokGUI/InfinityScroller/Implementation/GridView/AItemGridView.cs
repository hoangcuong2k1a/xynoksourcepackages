﻿using UnityEngine;
using XynokConvention.APIs;
using XynokGUI.InfinityScroller.GridView;

namespace XynokGUI.InfinityScroller.Implementation.GridView
{
    [RequireComponent(typeof(LoopGridViewItem))]
    public abstract class AItemGridView<T> : MonoBehaviour, IInjectable<T>
    {
        protected T itemData;

        public void SetDependency(T dependency)
        {
            Dispose();
            itemData = dependency;
            Init();
        }

        /// <summary>
        /// something that only need to be called once
        /// </summary>
        public virtual void InitFirstTime()
        {
        }

        public void Dispose()
        {
            OnDispose();
        }

        protected abstract void Init();
        protected abstract void OnDispose();
    }
}