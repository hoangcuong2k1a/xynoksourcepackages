﻿using XynokSourceGenMarker.SourceGenMarkers;

namespace XynokGUI.Core.Enums
{
    public enum ViewType
    {
        None = 0,
        Layer = 1,
        Fullscreen = 5,
        Popup = 3,
        Content = 4,
    }
}