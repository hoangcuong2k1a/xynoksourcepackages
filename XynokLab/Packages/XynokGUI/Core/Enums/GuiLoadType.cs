﻿namespace XynokGUI.Core.Enums
{
    public enum GuiLoadType
    {
        Immediate = 0,
        Async = 1
    }
}