﻿using System;
using Sirenix.OdinInspector;

namespace XynokGUI.Core.Data
{
    [Serializable]
    public class GuiViewNamingData
    {
        [ValueDropdown("GetCategories")] public string category = "???";
        [ValueDropdown("GetNames")] public string viewName = "???";
#if UNITY_EDITOR
        private static string[] GetCategories => GUISettings.Instance.categories;
        private static string[] GetNames => GUISettings.Instance.currentPrefabs;
        
        [Button]
        void Sync()
        {
            GUISettings.Instance.EmitCurrentCategory(category);
        }
#endif
        
    }

    [Serializable]
    public class GuiViewData
    {
        [ValueDropdown("GetNames")] public string viewName = "???";
        public bool animateOnShow;
        public bool animateOnHide;

#if UNITY_EDITOR
        private static string[] GetCategories => GUISettings.Instance.categories;
        private static string[] GetNames => GUISettings.Instance.currentPrefabs;
#endif
    }
    
    [Serializable]
    public class GuiViewDataWrapper
    {
        [ValueDropdown("GetCategories")] 
        public string category = "???";
        public GuiViewData[] viewData;
        
#if UNITY_EDITOR
        private static string[] GetCategories => GUISettings.Instance.categories;

        [Title("Lưu ý:", "reload sẽ load lại pool cho toàn bộ các fx trong category này, nên cẩn thận khi dùng")]
        [Button]
        void Reload()
        {   UnityEditor.EditorUtility.SetDirty(GUISettings.Instance);
            
            GUISettings.Instance.ReloadCategory(this);
            
            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.SaveAssets();
        }
#endif
    }
}