﻿using XynokConvention.APIs;

namespace XynokGUI.Core.Data.EventData
{
    public interface IGuiDataDeliver
    {
    }

    /// <summary>
    /// inject will be call before running show animation or show events
    /// </summary>
    /// <typeparam name="T">data want to inject to view</typeparam>
    public interface IGuiDataReceiver<in T> : IInjectable<T> where T : IGuiDataDeliver
    {
    }
}