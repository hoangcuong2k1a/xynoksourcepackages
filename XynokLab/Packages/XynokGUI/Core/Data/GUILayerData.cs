﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokGUI.Core.Data
{
    [Serializable]
    public class GUILayerData
    {
        public string layerName;
        [ValueDropdown("GetCategories")] public string targetCategory;
        public bool stackable;
        [HideInInspector] [ReadOnly] public GuiViewData[] viewData;


#if UNITY_EDITOR
        private static string[] GetCategories => GUISettings.Instance.categories;
#endif

        public GuiViewData GetGuiViewData(string viewName)
        {
            return viewData.FirstOrDefault(e => e.viewName == viewName);
        }
    }
}