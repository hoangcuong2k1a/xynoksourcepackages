﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle.RuntimeEditor.AssetRepo;
using XynokGUI.Entities;

namespace XynokGUI.Core.Data
{
#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    [CreateAssetMenu(fileName = "GUISettings", menuName = "Xynok/GUI/GUISettings")]
    public class GUISettings : AAssetManagedContainer
    {
        [Space(10)] [Header("LAYER SETTINGS")] public GUILayerEntity layerPrefab;
        public GUILayerData[] layers;
        
        [Tooltip("category của layer này sẽ không được tạo ra khi start game")]
        [ValueDropdown("categories")] public string[] excludeCategories;
        
        [Space(10)] [Header("VIEW SETTINGS")] public GuiViewDataWrapper[] preloadConfig;


#if UNITY_EDITOR


        public static GUISettings Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<GUISettings>("Xynok/GUI/GUISettings");


        [Button]
        void ReloadAllCategory()
        {
            UnityEditor.EditorUtility.SetDirty(Instance);
            Refresh();

            // create preload data
            preloadConfig = new GuiViewDataWrapper[rootFolder.subFolders.Length];

            // assign preload data
            for (int i = 0; i < preloadConfig.Length; i++)
            {
                // reload all view in category
                var folder = rootFolder.subFolders[i];
                folder.Refresh();

                var preloadData = new GuiViewDataWrapper()
                {
                    category = folder.folderName,
                    viewData = new GuiViewData[folder.prefabs.Length]
                };

                // get all variant names
                var variantNames = new string[folder.prefabs.Length];


                // assign variants to pool data
                for (int j = 0; j < variantNames.Length; j++)
                {
                    variantNames[j] = folder.prefabs[j].sourceName;
                    preloadData.viewData[j] = new GuiViewData
                    {
                        viewName = variantNames[j],
                        animateOnHide = true,
                        animateOnShow = true,
                    };
                }

                // final assign
                preloadConfig[i] = preloadData;
            }

            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.SaveAssets();
        }

        [Button]
        public void SyncPoolDataAndLayerData()
        {
            UnityEditor.EditorUtility.SetDirty(this);
            for (int i = 0; i < layers.Length; i++)
            {
                var poolData = GetGUIPoolData(layers[i].targetCategory);
                layers[i].viewData = poolData ?? Array.Empty<GuiViewData>();
            }

            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.SaveAssets();
        }

        public GuiViewData[] GetGUIPoolData(string category)
        {
            var target = preloadConfig.FirstOrDefault(e => e.category == category);
            if (target == null)
            {
                Debug.LogError($"[{nameof(GUISettings)}]: does not exist pool data for category: {category}");
                return null;
            }

            return target.viewData;
        }

        public void ReloadCategory(GuiViewDataWrapper guiViewData)
        {
            var folder = rootFolder.subFolders.FirstOrDefault(e => e.folderName == guiViewData.category);
            if (folder == null)
            {
                Debug.LogError("does not have category: " + guiViewData.category);
                return;
            }

            folder.Refresh();

            var variants = new GuiViewData[folder.prefabs.Length];
            for (int i = 0; i < variants.Length; i++)
            {
                int index = i;
                var variantName = folder.prefabs[index].sourceName;
                variants[i] = new GuiViewData
                {
                    viewName = variantName,
                    animateOnHide = true,
                    animateOnShow = true,
                };
            }

            guiViewData.viewData = variants;
        }
#endif
    }
}