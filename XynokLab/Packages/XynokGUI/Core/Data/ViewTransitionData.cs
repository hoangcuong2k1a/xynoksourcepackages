﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace XynokGUI.Core.Data
{
    [Serializable]
    public class ViewTransitionData
    {
        [ValidateInput("NoneValidAnimBinding", "Root does not have valid child binding with this animation !")]
        public Animator rootAnimator;

        // [ValidateInput("ValidTrigger", "trigger cannot be empty or root does not have this trigger !")]
        [ValueDropdown("triggerParameters")] public string showTrigger = "???";

        //[ValidateInput("ValidTrigger", "trigger cannot be empty or root does not have this trigger !")]
        [ValueDropdown("triggerParameters")] public string hideTrigger = "???";


        [ReadOnly] [SerializeField] private string[] triggerParameters;

        private string _hideEventName = "OnHideCompleted";
        private string _showEventName = "OnShowCompleted";

        [Button]
        public void OnValidate()
        {
            triggerParameters = rootAnimator.parameters
                .Select(e => e.type == AnimatorControllerParameterType.Trigger ? e.name : "???").ToArray();
        }


        public void Show()
        {
            if (!rootAnimator.enabled) rootAnimator.enabled = true;
            rootAnimator.SetTrigger(showTrigger);
        }

        public void Hide()
        {
            if (!rootAnimator.enabled) rootAnimator.enabled = true;
            rootAnimator.SetTrigger(hideTrigger);
        }


#if UNITY_EDITOR

        bool ValidTrigger(string trigger)
        {
            if (!rootAnimator) return false;
            if (!rootAnimator.gameObject.activeSelf) return false;


            bool emptyOrNull = string.IsNullOrEmpty(trigger);

            bool validShowParam = rootAnimator.parameters.FirstOrDefault(e => e.name == trigger) != null;

            return !emptyOrNull && validShowParam;
        }

        bool ValidAnimator()
        {
            return rootAnimator;
        }


        bool NoneValidAnimBinding(Animator animator)
        {
            if (!animator) return false;
            var animatorController = animator.runtimeAnimatorController;
            if (!animatorController) return false;

            Transform[] childs = rootAnimator.gameObject.GetComponentsInChildren<Transform>();

            // check if animation has event binding with view transition
            for (int i = 0; i < animatorController.animationClips.Length; i++)
            {
                var clip = animatorController.animationClips[i];
                if (clip.name == "Empty" || clip.name == "empty") continue;
                var eventValid = clip.events.ToList().FirstOrDefault(e =>
                    e.functionName == _showEventName || e.functionName == _hideEventName);
                if (eventValid == null)
                {
                    Debug.LogError(
                        $"AnimationClip: <color=red>{clip.name}</color> does not have event binding with view transition ! (<color=cyan>{_hideEventName} or {_showEventName}</color>)");
                    return false;
                }
            }


            // check if animator has valid child binding
            var curves = new List<UnityEditor.EditorCurveBinding>();

            for (int i = 0; i < animatorController.animationClips.Length; i++)
            {
                curves.AddRange(UnityEditor.AnimationUtility.GetCurveBindings(animatorController.animationClips[i]));
            }

            for (int i = 0; i < curves.Count; i++)
            {
                if (childs.FirstOrDefault(e => e.name == curves[i].path) == null)
                {
                    Debug.LogWarning(
                        $"{rootAnimator.gameObject.name}  does not have a child named <color=cyan>{curves[i].path}</color> for running anim <color=cyan>{animatorController.name}</color>!");
                    return false;
                }
            }

            return true;
        }
#endif
    }
}