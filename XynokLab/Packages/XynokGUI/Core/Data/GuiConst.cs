﻿using UnityEngine;
using UnityEngine.UI;
using XynokUtils;

namespace XynokGUI.Core.Data
{
    public class GuiConst
    {
        public const string ROOT_VIEW_NAME = "--- container ---"; // used for animated views


        public static RectTransform SetupRootView(Transform parent)
        {
            var viewTrans = new GameObject(ROOT_VIEW_NAME).AddComponent<RectTransform>();
           
            viewTrans.SetParent(parent);
            viewTrans.SetLeft(0);
            viewTrans.SetTop(0);
            viewTrans.SetBottom(0);
            viewTrans.SetRight(0);
            viewTrans.SetAnchor(AnchorPresets.StretchAll);
            viewTrans.gameObject.AddComponent<CanvasGroup>();
            viewTrans.gameObject.AddComponent<RectMask2D>();
            
            return viewTrans;
        }
    }
}