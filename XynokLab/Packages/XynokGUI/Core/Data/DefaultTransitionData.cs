﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokGUI.Core.Enums;

namespace XynokGUI.Core.Data
{
    [Serializable]
    public class DefaultTransitionData
    {
        public ViewType viewType;
        [ValueDropdown("getAbilityTypes")] public ViewAbilityType abilityType = ViewAbilityType.Show;
        public RuntimeAnimatorController transition;

        private static ViewAbilityType[] getAbilityTypes = new ViewAbilityType[]
            { ViewAbilityType.Show, ViewAbilityType.Hide };
    }
}