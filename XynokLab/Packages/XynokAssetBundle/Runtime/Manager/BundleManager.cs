﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XynokConvention.Data;
using XynokConvention.Patterns;
using XynokDebugger;

namespace XynokAssetBundle.Manager
{
    /// <summary>
    /// 1 implementor sẽ đăng ký vào quá trình load bundle ở đầu Game
    /// </summary>
    public interface IBundleHolder
    {
        /// <summary>
        /// Đăng ký tham gia vào quá trình load bundle
        /// </summary>
        /// <param name="bundleStartLoad">callback khi bundle bắt đầu laod</param>
        /// <param name="variantLoad">callback khi 1 variant của bundle được load</param>
        /// <param name="bundleLoadComplete">callback khi bundle load xong</param>
        /// <returns>khi đăng ký xong manger cần biết bundle này có bao nhiêu request(variant)</returns>
        void Register(Action<string> bundleStartLoad, Action<string> variantLoad,
            Action<IBundleHolder> bundleLoadComplete);

        void LoadBundle();
        int AssetCount { get; }
    }

    [Serializable]
    public class BundleLoadData
    {
        public string message;
        public float progress;
    }

    public class BundleManager : ASingleton<BundleManager>
    {
        [Range(0f, 10f)] [SerializeField] private float delayLoadOnStart = 0.5f;
        public AData<BundleLoadData> loadProgress;
        public event Action OnAllLoadComplete;
        private Queue<IBundleHolder> _bundleHolders = new();


        public int AssetCount
        {
            get
            {
                if (_assetAmount < 0)
                {
                    _assetAmount = 0;
                    foreach (var bundleHolder in _bundleHolders)
                    {
                        _assetAmount += bundleHolder.AssetCount;
                    }
                }

                return _assetAmount;
            }
        }

        private int _assetAmount = -1;
        private float _progress;
        private string _currentBundle = "[???]";

        protected override void Awake()
        {
            base.Awake();
            loadProgress = new(new BundleLoadData
            {
                message = "Loading...",
                progress = 0
            });
            var bundlerHolders = FindObjectsOfType<MonoBehaviour>().OfType<IBundleHolder>();
            foreach (var bundlerHolder in bundlerHolders)
            {
                if (_bundleHolders.Contains(bundlerHolder)) continue;
                Xylog.Log($"{bundlerHolder.GetType().Name} join bundle load process.");
                bundlerHolder.Register(StartLoadABundle, LoadVariant, LoadBundleComplete);
                _bundleHolders.Enqueue(bundlerHolder);
            }
            Xylog.Log($"Estimate progress completed: total progress is {AssetCount}");
        }

        private void Start()
        {
            Invoke(nameof(LoadBundle), delayLoadOnStart);
        }

        void LoadBundle()
        {
            _bundleHolders.Dequeue()?.LoadBundle();
        }

        void StartLoadABundle(string bundleName)
        {
            _currentBundle = bundleName;
            loadProgress.Value.message = $"Start load [{_currentBundle}]...";
            loadProgress.Value = loadProgress.Value; // trigger update
        }

        void LoadVariant(string variant)
        {
            loadProgress.Value.message = $"[{_currentBundle}: {variant}...";
            loadProgress.Value.progress = ++_progress;
            loadProgress.Value = loadProgress.Value; // trigger update
        }

        void LoadBundleComplete(IBundleHolder bundleHolder)
        {
            loadProgress.Value.message = $"[{_currentBundle}] load completed.";
            loadProgress.Value = loadProgress.Value; // trigger update

            if (_bundleHolders.Count < 1)
            {
                OnAllLoadComplete?.Invoke();
                return;
            }

            _bundleHolders.Dequeue()?.LoadBundle();
        }
    }
}