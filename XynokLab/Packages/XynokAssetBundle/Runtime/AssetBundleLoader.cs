﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using XynokAssetBundle.Data;


namespace XynokAssetBundle
{
    /// <summary>
    /// chịu trách nhiệm load, preload, unload asset bundle
    /// </summary>
    public class AssetBundleLoader : MonoBehaviour, IDisposable
    {
        [SerializeField] private AssetBundleVault vault;
        public event Action<int, GameObjectData, GameObject> OnLoadInstance;
        public event Action<int, GameObjectData, GameObject> OnReleaseInstance;

        private class BundlePrefabContainer
        {
            public GameObject[] Instances;
            public Action Dispose;
        }

        private Dictionary<int, BundlePrefabContainer[]> _bundleLoaded = new();

        private bool _isBundleLoading;
        private bool _isInstanceLoading;

        public async void LoadBundle(int bundleID)
        {
            await UniTask.WaitUntil(() => !_isBundleLoading);


            // detect if bundle is already loaded
            var bundleData = vault.GetBundleData(bundleID);
            if (bundleData == null) return;
            bool exist = _bundleLoaded.ContainsKey(bundleID);
            if (exist) return;


            // add new bundle
            var bundlePrefabs = new BundlePrefabContainer[bundleData.prefabs.Length];
            _bundleLoaded.Add(bundleID, bundlePrefabs);

            // init prefab instances of bundle
            for (int i = 0; i < bundleData.prefabs.Length; i++)
            {
                int indexPrefabSource = i;
                int instanceAmount = bundleData.prefabs[indexPrefabSource].instances.Length;

                bundlePrefabs[indexPrefabSource] = new BundlePrefabContainer
                {
                    Instances = new GameObject[instanceAmount]
                };

                for (int j = 0; j < instanceAmount; j++)
                {
                    await UniTask.WaitUntil(() => !_isInstanceLoading);

                    // start loading
                    _isInstanceLoading = true;

                    var indexInstance = j;
                    var reference = bundleData.prefabs[indexPrefabSource].source;

                    void AssignInstance(AsyncOperationHandle<GameObject> obj)
                    {
                        obj.Result.SetActive(false);
                        OnLoadInstance?.Invoke(bundleID,
                            bundleData.prefabs[indexPrefabSource].instances[indexInstance], obj.Result);

                        bundlePrefabs[indexPrefabSource].Instances[indexInstance] = obj.Result;

                        // end loading
                        _isInstanceLoading = false;
                    }

                    // assign callbacks

                    reference.InstantiateAsync().Completed += AssignInstance;
                    bundlePrefabs[indexPrefabSource].Dispose += () =>
                    {
                        var instance = bundlePrefabs[indexPrefabSource].Instances[indexInstance];
                        if (!instance) return;
                        OnReleaseInstance?.Invoke(bundleID,
                            bundleData.prefabs[indexPrefabSource].instances[indexInstance], instance);
                        reference.ReleaseInstance(instance);
                    };
                }
            }

            _isBundleLoading = false;
        }


        public void ReleaseBundle(int bundleID)
        {
            bool exist = _bundleLoaded.ContainsKey(bundleID);
            if (!exist)
            {
                Debug.LogError($"bundle {bundleID} is not loaded");
                return;
            }

            var bundle = _bundleLoaded[bundleID];
            _bundleLoaded.Remove(bundleID);
            for (int i = 0; i < bundle.Length; i++)
            {
                if (bundle[i] == null) continue;
                bundle[i].Dispose?.Invoke();
            }
            
        }

        public void Dispose()
        {
            foreach (var bundle in _bundleLoaded)
            {
                foreach (var container in bundle.Value)
                {
                    container.Dispose?.Invoke();
                }
            }

            _bundleLoaded.Clear();
        }

        private void OnDestroy()
        {
            Dispose();
        }
    }
}


/*
 using System;
using System.Collections.Generic;
using Lean.Pool;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using XynokAssetBundle.Data;
using XynokConvention.Patterns;
using XynokUtils;

namespace XynokAssetBundle
{
    /// <summary>
    /// chịu trách nhiệm load, preload, unload asset bundle
    /// </summary>
    public class AssetBundleLoader : ASingleton<AssetBundleLoader>, IDisposable
    {
        [ValueDropdown("BundleIDs")] [SerializeField]
        private string bundlePreloadID;

#if UNITY_EDITOR
        private static string[] BundleIDs => AssetBundleVault.Instance.BundleIDs;
#endif
        
        private Dictionary<int, LeanGameObjectPool> _pools = new();

        private Action _onDispose;
        private AsyncOperationHandle<IList<GameObject>> _currentBundleHandle;
        private Dictionary<string, GameObject> _currentPrefabNameDict; // cache prefab name for fast lookup

        private void Start()
        {
            if (!string.IsNullOrEmpty(bundlePreloadID)) LoadBundle(bundlePreloadID);
        }

        public void LoadBundle(string bundleID)
        {
            var bundleData = AssetBundleVault.Instance.GetBundleData(bundleID);
            if (bundleData == null)
            {
                Debug.LogError($"not found bundle data for {bundleID}");
                return;
            }

            CreatePoolsForBundle(bundleData);
        }


        /// <summary>
        /// khởi tạo các pool cho các prefab trong bundle. Max capacity của pool = số lượng instance trong bundle.
        /// Preload mặc định =1/3 số lượng instance trong bundle
        /// </summary>
        [Obsolete("Obsolete")]
        void CreatePoolsForBundle(RuntimeAssetBundleData bundleData)
        {
            InitCurrentBundle(bundleData);

            var poolAmount = bundleData.prefabs.Length;

            var container = RuntimeUtils.CreateGameObject($"BundlePool-{bundleData.bundleID}", gameObject.transform);

            for (int i = 0; i < poolAmount; i++)
            {
                var prefabData = bundleData.prefabs[i];
                var poolData = new PoolData
                {
                    recycle = true,
                    preload = prefabData.instances.Length / 3,
                    capacity = prefabData.instances.Length,
                };

                var runtimePoolData = new RuntimePoolData
                {
                    sourceAddress = prefabData.sourceAddress,
                    poolData = poolData,
                    sourceName = prefabData.sourceName,
                };
                CreatePool(container, runtimePoolData);
            }
        }

        [Obsolete("Obsolete")]
        void InitCurrentBundle(RuntimeAssetBundleData bundleData)
        {
            var addresses = bundleData.GetAllPrefabAddresses();

            _currentPrefabNameDict ??= new();
            _currentPrefabNameDict.Clear();
            _currentBundleHandle = default;
            for (int i = 0; i < bundleData.prefabs.Length; i++)
            {
                _currentPrefabNameDict.Add(bundleData.prefabs[i].sourceName, null);
            }

            _currentBundleHandle =
                Addressables.LoadAssetsAsync<GameObject>(addresses, CachePrefab, Addressables.MergeMode.Union);
        }

        void CachePrefab(GameObject obj)
        {
            if (_currentPrefabNameDict.ContainsKey(obj.name)) _currentPrefabNameDict[obj.name] = obj;
        }


        void CreatePool(GameObject parent, RuntimePoolData runtimePoolData)
        {
            if (!CanCreatePool(runtimePoolData)) return;

            var newPool = new GameObject($"pool_{runtimePoolData.Key}").AddComponent<LeanGameObjectPool>();
            newPool.transform.SetParent(parent.transform);

            newPool.Prefab = _currentPrefabNameDict[runtimePoolData.sourceName];
            newPool.Capacity = runtimePoolData.poolData.capacity;
            newPool.Preload = runtimePoolData.poolData.preload;
            newPool.Recycle = runtimePoolData.poolData.recycle;


            _pools.Add(runtimePoolData.Key, newPool);
            _onDispose += newPool.DespawnAll;
        }

        bool CanCreatePool(RuntimePoolData runtimePoolData)
        {
            bool exist = _pools.ContainsKey(runtimePoolData.Key);

            if (exist)
            {
                Debug.LogError(
                    $"<color=cyan>[AssetBundleCollector]<color=cyan>:pool with key {runtimePoolData.Key} already exist");
            }

            return exist;
        }

        public void Dispose()
        {
            _onDispose?.Invoke();
            _onDispose = default;
            _pools.Clear();
        }
    }
}
 */