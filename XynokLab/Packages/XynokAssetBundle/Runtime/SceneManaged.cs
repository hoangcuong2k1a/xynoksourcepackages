﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;


namespace XynokAssetBundle
{
    [Serializable]
    public class SceneAddressData
    {
        public string sceneName;
        public AssetReference sceneAddress;
    }

    public class SceneConst
    {
        public const string currentScene = "$current scene$";
    }
#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    [CreateAssetMenu(fileName = "SceneManaged", menuName = "Xynok/AssetBundle/SceneManaged")]
    public class SceneManaged : ScriptableObject
    {
        [Range(0, 10f)] public float delayOnChangeScene = 0.5f;
#if UNITY_EDITOR
        public static SceneManaged Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<SceneManaged>("Xynok/AssetBundle/SceneManaged");
#endif

        public string folderPath;
        [ReadOnly] public string[] scenes;
        [ReadOnly] public SceneAddressData[] sceneAddress;


        public AssetReference GetSceneAssetReference(string sceneName)
        {
            return sceneAddress.FirstOrDefault(x => x.sceneName == sceneName)?.sceneAddress;
        }

#if UNITY_EDITOR

        [Button]
        public void Refresh()
        {
            var allAddress = XynokUtils.AssetUtils.GetAllAssetAtPath("scene", path: folderPath, includePath: true)
                .ToList();
            // tham chiếu only names
            var allScenes = XynokUtils.AssetUtils.GetAllAssetAtPath("scene", path: folderPath, includePath: false)
                .ToList();
            sceneAddress = new SceneAddressData[allAddress.Count];


            for (int i = 0; i < allAddress.Count; i++)
            {
                if (allScenes[i] == SceneConst.currentScene) continue;
                sceneAddress[i] = new SceneAddressData
                {
                    sceneName = allScenes[i],
                    sceneAddress = new AssetReference(UnityEditor.AssetDatabase.AssetPathToGUID(allAddress[i]))
                };
            }

            allScenes.Add(SceneConst.currentScene);
            scenes = allScenes.ToArray();
        }
#endif
    }
}