﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle.Data;
using XynokAssetBundle.RuntimeEditor.AssetRepo;

namespace XynokAssetBundle
{
    [Serializable]
    public class FxCategoryPoolData
    {
        [ValueDropdown("GetCategories")] public string category = "???";
        public FxVariantPoolData[] variants;

#if UNITY_EDITOR
        private static string[] GetCategories => FxPrefabManaged.Instance.categories;

        [Title("Lưu ý:", "reload sẽ load lại pool cho toàn bộ các fx trong category này, nên cẩn thận khi dùng")]
        [Button]
        void Reload()
        {   UnityEditor.EditorUtility.SetDirty(FxPrefabManaged.Instance);
            
            FxPrefabManaged.Instance.ReloadCategory(this);
            
            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.SaveAssets();
        }
#endif
    }

    [Serializable]
    public class FxVariantPoolData
    {
        public string fxName = "???";
        public PoolData poolData;
    }

    

#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    [CreateAssetMenu(fileName = "FxPrefabManaged", menuName = "Xynok/AssetBundle/FxPrefabManaged")]
    public class FxPrefabManaged : AAssetManagedContainer
    {
#if UNITY_EDITOR
        public static FxPrefabManaged Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<FxPrefabManaged>("Xynok/AssetBundle/FxPrefabManaged");
#endif
        [Header("PRELOAD FXs")] public FxCategoryPoolData[] preload;


      
        

#if UNITY_EDITOR

        [Button]
       public void ReloadPrePools()
        {
            UnityEditor.EditorUtility.SetDirty(Instance);
            Refresh();


            // create preload data
            preload = new FxCategoryPoolData[rootFolder.subFolders.Length];
            
            
            // assign preload data
            for (int i = 0; i < rootFolder.subFolders.Length; i++)
            {
                // reload all fx in category
                var folder = rootFolder.subFolders[i];
                folder.Refresh();
                
                // get all variant names
                var variantNames = new string[folder.prefabs.Length]; 

                // create category pool data
                var categoryPoolData = new FxCategoryPoolData
                {
                    category = folder.folderName,
                    variants = new FxVariantPoolData[variantNames.Length]
                };
                
                // assign variants to pool data
                for (int j = 0; j < variantNames.Length; j++)
                {
                    variantNames[j] = folder.prefabs[j].sourceName;
                    categoryPoolData.variants[j] = new FxVariantPoolData
                    {
                        fxName = variantNames[j],
                        poolData = new PoolData
                        {
                            preload = 1,
                            recycle = true,
                            capacity = 3
                        }
                    };
                }
                
                // final assign
                preload[i] = categoryPoolData;
            }
            
            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.SaveAssets();
        }

        public void ReloadCategory(FxCategoryPoolData category)
        {
         
            var folderName = category.category;
            var targetFolder = rootFolder.subFolders.FirstOrDefault(e => e.folderName == folderName);
            if (targetFolder == null)
            {
                Debug.LogError($"[{nameof(FxPrefabManaged)}]does not have category: {folderName}");
                return;
            }
             
            targetFolder.Refresh();
            var variants = new FxVariantPoolData[targetFolder.prefabs.Length];
            for (int i = 0; i < variants.Length; i++)
            {
                int index = i;
                var variantName = targetFolder.prefabs[index].sourceName;
                variants[i] = new FxVariantPoolData
                {
                    fxName = variantName,
                    poolData = new PoolData
                    {
                        preload = 1,
                        recycle = true,
                        capacity = 3
                    }
                };
            }
            category.variants = variants;
            
         
        }
#endif
    }
}