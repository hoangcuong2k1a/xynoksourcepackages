﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle.RuntimeEditor.AssetRepo;

namespace XynokAssetBundle
{
    [Serializable]
    public class PreloadTextureData
    {
        public string category;
        public string[] textureNames;
    }

#if UNITY_EDITOR
    [XynokAdmin.Editor.XynokAdminDashboardData]
#endif
    [CreateAssetMenu(fileName = "TextureManaged", menuName = "Xynok/AssetBundle/TextureManaged")]
    public class TextureManaged : AAssetManagedContainer
    {
        protected override string AssetType => "Texture2D";
#if UNITY_EDITOR
        public static TextureManaged Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<TextureManaged>("Xynok/AssetBundle/TextureManaged");
#endif

        [Header("PRELOAD TEXTURES")] public PreloadTextureData[] preload;


#if UNITY_EDITOR
        [Button]
        public void ReloadPreloadTextures()
        {
            UnityEditor.EditorUtility.SetDirty(Instance);
            Refresh();
            preload = new PreloadTextureData[rootFolder.subFolders.Length];

            for (int i = 0; i < rootFolder.subFolders.Length; i++)
            {
                preload[i] = new PreloadTextureData
                {
                    category = rootFolder.subFolders[i].folderName,
                };

                for (int j = 0; j < rootFolder.subFolders[i].prefabs.Length; j++)
                {
                    preload[i].textureNames = rootFolder.subFolders[i].prefabs.Select(e => e.sourceName).ToArray();
                }
            }

            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.SaveAssets();
        }
#endif
    }
}