﻿using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokAssetBundle.Data;

namespace XynokAssetBundle
{
    /// <summary>
    /// store map objs
    /// </summary>
    [CreateAssetMenu(fileName = "AssetBundleVault", menuName = "Xynok/AssetBundle/AssetBundleVault")]
    public class AssetBundleVault : ScriptableObject
    {
        [SerializeField] private string rootObjTag = "Prefab";
        [SerializeField] [ReadOnly] private string[] bundleIDs; // view only
        [SerializeField] private RuntimeAssetBundleData[] runtimeAssetBundles;
        public string[] BundleIDs => bundleIDs;


        public string RootObjTag
        {
            get
            {
#if UNITY_EDITOR

                if (!XynokUtils.AssetUtils.ExistTag(rootObjTag)) XynokUtils.AssetUtils.CreateTag(rootObjTag);
#endif
                return rootObjTag;
            }
        }
#if UNITY_EDITOR
        public static AssetBundleVault Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<AssetBundleVault>("Xynok/AssetBundleVault");
#endif

#if UNITY_EDITOR

        [Button]
        void DuplicateBundles(int row, int col)
        {
            int size = row * col;
            var cache = (RuntimeAssetBundleData)runtimeAssetBundles[0].Clone();
            runtimeAssetBundles = new RuntimeAssetBundleData[size];

            int count = 0;
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    runtimeAssetBundles[count] = (RuntimeAssetBundleData)cache.Clone();
                    runtimeAssetBundles[count].SetBundleID($"mapCell_{i}_{j}");
                    count++;
                }
            }
        }
#endif

        /// <summary>
        /// Replace if exist same id or add new runtime asset bundle data
        /// </summary>
        /// <param name="runtimeAssetBundleData"> lưu trữ dữ liệu của asset bundle khi runtime. Gồm các prefab và các instance của chúng</param>
        public void AddRuntimeAssetBundle(RuntimeAssetBundleData runtimeAssetBundleData)
        {
            var newData = (RuntimeAssetBundleData)runtimeAssetBundleData.Clone();
            var element = runtimeAssetBundles.ToList().Find(e => e.GetHashCode() == newData.GetHashCode());
            if (element != null)
            {
                runtimeAssetBundles[runtimeAssetBundles.ToList().IndexOf(element)] = newData;
                return;
            }

            runtimeAssetBundles = runtimeAssetBundles.Append(newData).ToArray();

            bundleIDs = runtimeAssetBundles.Select(e => e.BundleID).ToArray();
        }

        public RuntimeAssetBundleData GetBundleData(int bundleID)
        {
            var bundleData = runtimeAssetBundles.ToList().FirstOrDefault(e => e.GetHashCode() == bundleID);
            if (bundleData == null)
                Debug.LogError($"not found bundle data for {bundleID}");
            return bundleData;
        }
    }
}