﻿using System;
using System.Collections.Generic;
using Lean.Pool;
using UnityEngine;
using XynokAssetBundle.Data;
using XynokConvention.Patterns;

namespace XynokAssetBundle.Pooling
{
    public abstract class APool<T> : ASingleton<APool<T>>
    {
        protected Dictionary<T, LeanGameObjectPool> pools = new();

        private Action _onDispose;


        public GameObject Spawn(T resourceID, Vector3 spawnPos, Transform parent = null)
        {
            // check if resource's pool existed
            if (!pools.ContainsKey(resourceID))
            {
                Debug.LogError($"not exist pool for {resourceID}");
                return null;
            }

            // spawn resource
            var obj = pools[resourceID].Spawn(spawnPos, Quaternion.identity, parent);

            // sync data with current world
            Physics.SyncTransforms();
            Physics2D.SyncTransforms();


            return obj;
        }

        public void RemovePool(T resourceID)
        {
            if (!pools.ContainsKey(resourceID))
            {
                Debug.LogError($"does not exist pool of {resourceID}");
                return;
            }

            _onDispose -= pools[resourceID].DespawnAll;
            pools[resourceID].DespawnAll();
            LeanPool.Links.Remove(gameObject);
            Destroy(pools[resourceID].gameObject);
            pools.Remove(resourceID);
        }

        protected bool ExistPool(T resourceID)
        {
            return pools.ContainsKey(resourceID);
        }

        protected bool CanCreatePool(T resourceID)
        {
            if (pools.ContainsKey(resourceID))
            {
                Debug.LogWarning($"pool of {resourceID} existed !");
                return false;
            }

            return true;
        }

        public void ChangePoolData(T resourceID, PoolData poolData)
        {
            if (!pools.ContainsKey(resourceID))
            {
                Debug.LogError($"does not exist pool of {resourceID}");
                return;
            }

            var pool = pools[resourceID];
            pool.DespawnAll();
            pool.Clean();
            pool.Capacity = poolData.capacity;
            pool.Recycle = poolData.recycle;
            pool.Preload = poolData.preload;
            pool.PreloadAll();
        }

        public void CreatePool(T resourceID, GameObject source, PoolData poolData)
        {
            if (!CanCreatePool(resourceID)) return;

            GameObject prefab = source;

            if (prefab == null)
            {
                Debug.LogError($"not found prefab of {resourceID}");
                return;
            }

            LeanGameObjectPool pool = new GameObject($"pool of {resourceID}").AddComponent<LeanGameObjectPool>();
            pool.transform.SetParent(transform);

            pool.Prefab = prefab;
            pool.Capacity = poolData.capacity;
            pool.Recycle = poolData.recycle;
            pool.Preload = poolData.preload;
            pool.PreloadAll();
            pools.Add(resourceID, pool);
            _onDispose += pool.DespawnAll;
        }

        public LeanGameObjectPool GetPool(T resourceID)
        {
            if (!pools.ContainsKey(resourceID))
            {
                Debug.LogError($"does not exist pool of {resourceID}");
                return null;
            }

            return pools[resourceID];
        }

        public bool DeSpawn(T resourceID, GameObject obj)
        {
            var pool = GetPool(resourceID);
            if (pool == null) return false;

            pools[resourceID].Despawn(obj);
            return true;
        }

        protected void DeSpawnAll()
        {
            foreach (var poolPair in pools)
            {
                poolPair.Value.DespawnAll();
            }
        }

        public void Dispose()
        {
            _onDispose?.Invoke();
            _onDispose = default;
            OnDispose();
        }

        protected virtual void OnDispose()
        {
        }

        private void OnDestroy()
        {
            Dispose();
        }
    }
}