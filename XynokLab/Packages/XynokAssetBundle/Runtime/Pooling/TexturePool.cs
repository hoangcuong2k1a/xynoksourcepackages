﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using XynokAssetBundle.Manager;
using XynokConvention.Patterns;
using XynokDebugger;


namespace XynokAssetBundle.Pooling
{
    public class TexturePool : ASingleton<TexturePool>, IBundleHolder
    {
        [SerializeField] private TextureManaged textureManaged;

        private Dictionary<string, Sprite> _sprites = new();
        private bool _loading;
        private Action<string> _bundleStartLoad;
        private Action<string> _variantLoad;
        private Action<IBundleHolder> _bundleLoadComplete;

        public async void LoadBundle()
        {
            foreach (var textureCategoryPoolData in textureManaged.preload)
            {
                var category = textureCategoryPoolData.category;
                _bundleStartLoad?.Invoke($"{category} texture");

                foreach (var textureName in textureCategoryPoolData.textureNames)
                {
                    var address = textureManaged.GetGuiAssetReference(category, textureName);
                    if (address == null) continue;

                    await UniTask.WaitUntil(() => !_loading);

                    _loading = true;
                    _variantLoad?.Invoke(textureName);

                    void LoadPool(AsyncOperationHandle<Sprite> source)
                    {
                        var key = $"{category}/{textureName}";
                        if (_sprites.ContainsKey(key))
                        {
                            Xylog.LogWarning($"{key} is already loaded !!! Please check that texture is multiple or single. Nếu Multiple thì đổi thành single.");
                            _loading = false;
                            return;
                        }

                        _sprites.Add($"{category}/{textureName}", source.Result);
                        _loading = false;
                    }

                    if (address.IsValid()) address.ReleaseAsset();
                    var result = address.LoadAssetAsync<Sprite>();
                    result.Completed += LoadPool;
                }
            }

            await UniTask.WaitUntil(() => !_loading);
            _bundleLoadComplete?.Invoke(this);
        }

        public async UniTask<Sprite> GetSprite(string category, string textureName)
        {
            await UniTask.WaitUntil(() => !_loading);
            var idAddress = $"{category}/{textureName}";

            void LoadPool(AsyncOperationHandle<Sprite> source)
            {
                _sprites.Add(idAddress, source.Result);
                _loading = false;
            }

            if (_sprites.TryGetValue(idAddress, out var sprite))
            {
                return sprite;
            }

            var address = textureManaged.GetGuiAssetReference(category, textureName);
            if (address == null) return null;
            _loading = true;
            address.LoadAssetAsync<Sprite>().Completed += LoadPool;

            return null;
        }


        public void Register(Action<string> bundleStartLoad, Action<string> variantLoad,
            Action<IBundleHolder> bundleLoadComplete)
        {
            _bundleStartLoad = bundleStartLoad;
            _variantLoad = variantLoad;
            _bundleLoadComplete = bundleLoadComplete;
        }


        public int AssetCount
        {
            get
            {
                int count = 0;
                foreach (var fxCategoryPoolData in textureManaged.preload)
                {
                    count += fxCategoryPoolData.textureNames.Length;
                }

                return count;
            }
        }
    }
}