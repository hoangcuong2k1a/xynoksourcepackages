﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using XynokAssetBundle.Manager;
using XynokConvention;
using XynokConvention.Procedural;

namespace XynokAssetBundle.Pooling
{
    public class FxPool : APool<string>, IBundleHolder
    {
        [SerializeField] private FxPrefabManaged fxPrefabManaged;
        private bool _isLoading;
        private Action<string> _bundleStartLoad;
        private Action<string> _variantLoad;
        private Action<IBundleHolder> _bundleLoadComplete;

        public async void LoadBundle()
        {
            foreach (var fxCategoryPoolData in fxPrefabManaged.preload)
            {
                _bundleStartLoad?.Invoke(fxCategoryPoolData.category);
                foreach (var fx in fxCategoryPoolData.variants)
                {
                    var assetReference = fxPrefabManaged.GetGuiAssetReference(fxCategoryPoolData.category, fx.fxName);

                    if (assetReference == null) continue;
                    await UniTask.WaitUntil(() => !_isLoading);

                    void LoadPool(AsyncOperationHandle<GameObject> source)
                    {
                        if (!CanCreatePool(fx.fxName))
                        {
                            _isLoading = false;
                            return;
                        }

                        CreatePool(fx.fxName, source.Result, fx.poolData);
                        _isLoading = false;
                    }

                    _isLoading = true;
                    if (assetReference.IsValid()) assetReference.ReleaseAsset();
                    _variantLoad?.Invoke(fx.fxName);
                    assetReference.LoadAssetAsync<GameObject>().Completed += LoadPool;
                }
            }

            await UniTask.WaitUntil(() => !_isLoading);
            _bundleLoadComplete?.Invoke(this);
            EventManager.StartListening(XynokGuiEvent.StartLoadScene.ToString(), DeSpawnAll);
        }


        protected override void OnDispose()
        {
            EventManager.StopListening(XynokGuiEvent.StartLoadScene.ToString(), DeSpawnAll);
        }


        public void Register(Action<string> bundleStartLoad, Action<string> variantLoad,
            Action<IBundleHolder> bundleLoadComplete)
        {
            _bundleStartLoad = bundleStartLoad;
            _variantLoad = variantLoad;
            _bundleLoadComplete = bundleLoadComplete;
        }


        public int AssetCount
        {
            get
            {
                int count = 0;
                foreach (var fxCategoryPoolData in fxPrefabManaged.preload)
                {
                    count += fxCategoryPoolData.variants.Length;
                }

                return count;
            }
        }
    }
}