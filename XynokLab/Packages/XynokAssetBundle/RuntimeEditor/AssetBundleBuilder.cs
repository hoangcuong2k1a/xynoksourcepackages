﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using XynokAssetBundle.Data;
using XynokUtils;
using PrefabType = XynokUtils.PrefabType;

namespace XynokAssetBundle.Editor
{
 
    /// <summary>
    /// define how to load asset bundle in runtime
    /// </summary>
    public class AssetBundleBuilder : MonoBehaviour
    {
        [SerializeField] private RuntimeAssetBundleData runtimeAssetBundleData;
        [SerializeField] private bool includeInactive = true;
        [SerializeField] [ReadOnly] private float buildProgress;


        [Button]
        void StopBuild()
        {
            EditorUtility.ClearProgressBar();
        }

        [Button]
        void BuildRuntimeData()
        {
            // show progress bar
            EditorUtility.DisplayProgressBar("Asset Bundle Builder", "Building...", buildProgress);

            var allGameObjects = FindObjectsOfType<GameObject>(includeInactive);

            var instancePrefabDict = new Dictionary<string, List<GameObjectData>>();

            for (int i = 0; i < allGameObjects.Length; i++)
            {
                buildProgress = (float)i / allGameObjects.Length;

                var currentGameObject = allGameObjects[i];

                bool isRoot = currentGameObject.CompareTag(AssetBundleVault.Instance.RootObjTag);

                if (!isRoot) continue;

                PrefabType prefabType = AssetUtils.GetPrefabType(currentGameObject);

                var originPath = AssetUtils.GetOriginPrefabPath(currentGameObject);

                if (prefabType != PrefabType.Instance) continue;


                if (!instancePrefabDict.ContainsKey(originPath))
                {
                    instancePrefabDict.Add(originPath, new List<GameObjectData>());
                }

                foreach (var pair in instancePrefabDict)
                {
                    if (pair.Key != originPath) continue;

                    bool existed = pair.Value.Any(instanceData =>
                        instanceData.instanceName == currentGameObject.name);

                    if (existed) continue;

                    var instanceTransform = currentGameObject.transform;

                    pair.Value.Add(new GameObjectData
                    {
                        instanceName = currentGameObject.name,
                        active = currentGameObject.activeSelf,
                        position = instanceTransform.position,
                        localScale = instanceTransform.localScale,
                        rotation = instanceTransform.rotation
                    });
                }
            }

            // build runtimeAssetBundleData
            runtimeAssetBundleData.prefabs = new PrefabData[instancePrefabDict.Count];
            int count = 0;
            foreach (var pair in instancePrefabDict)
            {
                var asset = AssetDatabase.LoadAssetAtPath<GameObject>(pair.Key);
                runtimeAssetBundleData.prefabs[count] = new PrefabData
                {
                    source = new AssetReference(AssetDatabase.AssetPathToGUID(pair.Key)),
                    sourceAddress = pair.Key,
                    instances = pair.Value.ToArray(),
                    sourceName = asset.name
                };
                
                count++;
            }


            // push to asset bundle manager
            AssetBundleVault.Instance.AddRuntimeAssetBundle(runtimeAssetBundleData);

            // hide progress bar
            EditorUtility.ClearProgressBar();
        }

     
    }
}
#endif