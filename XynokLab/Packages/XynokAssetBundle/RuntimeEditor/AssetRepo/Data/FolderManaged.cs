﻿using System;
using System.IO;
using Sirenix.OdinInspector;
using UnityEngine.AddressableAssets;

namespace XynokAssetBundle.RuntimeEditor.AssetRepo.Data
{
    [Serializable]
    public class GuiPrefabData
    {
        [ReadOnly] public string sourceName;
        [ReadOnly] public AssetReference source;
    }

    [Serializable]
    public class SubFolderManaged
    {
        public string folderPath;
        [ReadOnly] public string folderName;
        [ReadOnly] public GuiPrefabData[] prefabs;
        public string assetType;
#if UNITY_EDITOR

        public void Refresh()
        {
            folderName = folderPath.Substring(folderPath.LastIndexOf("/", StringComparison.Ordinal) + 1);
            var prefabNames =
                XynokUtils.AssetUtils.GetAllAssetAtPath(file: assetType, path: folderPath, includePath: true);

            prefabs = new GuiPrefabData[prefabNames.Length];
            for (int i = 0; i < prefabNames.Length; i++)
            {
                prefabs[i] = new GuiPrefabData
                {
                    sourceName = Path.GetFileNameWithoutExtension(prefabNames[i]),
                    source = new AssetReference(UnityEditor.AssetDatabase.AssetPathToGUID(prefabNames[i]))
                };
            }
        }
#endif
    }

    [Serializable]
    public class FolderManaged
    {
        public string folderPath;
        [ReadOnly] public string folderName;
        [ReadOnly] public SubFolderManaged[] subFolders;
        [ReadOnly] public string assetType;
#if UNITY_EDITOR


        public void Refresh()
        {
            var allSubFolder = XynokUtils.AssetUtils.GetAllSubFolder(folderPath);
            if (allSubFolder != null)
            {
                subFolders = new SubFolderManaged[allSubFolder.Length];
                for (int i = 0; i < subFolders.Length; i++)
                {
                    subFolders[i] = new SubFolderManaged
                    {
                        folderPath = allSubFolder[i],
                        assetType = assetType
                    };
                    subFolders[i].Refresh();
                }
            }
            else subFolders = Array.Empty<SubFolderManaged>();

            folderName = folderPath.Substring(folderPath.LastIndexOf("/", StringComparison.Ordinal) + 1);
        }
#endif
    }
}