﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using XynokAssetBundle.RuntimeEditor.AssetRepo.Data;

namespace XynokAssetBundle.RuntimeEditor.AssetRepo
{
    public abstract class AAssetManagedContainer : ScriptableObject
    {
        [Header("ROOT FOLDER")] public FolderManaged rootFolder;
        protected virtual string AssetType => "prefab";
        [ReadOnly] public string[] categories;
        [ReadOnly] public string[] currentPrefabs; // change match with current selected category

#if UNITY_EDITOR

        [Button]
        public virtual void Refresh()
        {
            rootFolder.assetType = AssetType;
            rootFolder.Refresh();
            categories = new string[rootFolder.subFolders.Length];

            for (var i = 0; i < categories.Length; i++)
            {
                categories[i] = rootFolder.subFolders[i].folderName;
            }
        }
#endif

        public void EmitCurrentCategory(string category)
        {
            var index = Array.IndexOf(categories, category);
            if (index < 0) return;
            var folder = rootFolder.subFolders.FirstOrDefault(e => e.folderName == category);
            if (folder == null)
            {
                currentPrefabs = Array.Empty<string>();
                return;
            }

            currentPrefabs = folder.prefabs.Select(e => e.sourceName).ToArray();
        }

        public AssetReference GetGuiAssetReference(string category, string assetName)
        {
            var folder = rootFolder.subFolders.FirstOrDefault(e => e.folderName == category);
            if (folder == null)
            {
                Debug.LogError($"[{GetType().Name}]: does not have category: " + category);
                return null;
            }

            var result = folder.prefabs.FirstOrDefault(e => e.sourceName == assetName)?.source;

            if (result == null)
            {
                Debug.LogError(
                    $"[{GetType().Name}]: category <color=cyan>{category}</color> does not have asset reference of: {assetName}");
                return null;
            }

            return result;
        }
    }
}