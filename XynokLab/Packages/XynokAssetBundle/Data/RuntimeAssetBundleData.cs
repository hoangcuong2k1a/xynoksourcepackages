﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace XynokAssetBundle.Data
{
    [Serializable]
    public struct GameObjectData
    {
        public string instanceName;
        public bool active;
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 localScale;
    }

    [Serializable]
    public class PrefabData
    {
        [ReadOnly] public AssetReference source;
        [ReadOnly] public string sourceName;
        [ReadOnly] public string sourceAddress;
        [ReadOnly] public GameObjectData[] instances;

        private string SoundAddress => sourceAddress;

        public override int GetHashCode()
        {
            return Animator.StringToHash(SoundAddress);
        }

        public PoolData GetPoolData()
        {
            var poolData = new PoolData();
            if (instances == null || instances.Length == 0) return poolData;
            poolData.capacity = instances.Length;
            poolData.preload = instances.Length;
            poolData.recycle = true;
            return poolData;
        }
    }

    [Serializable]
    public class RuntimeAssetBundleData : ICloneable
    {
        [SerializeField] private string bundleID;
        public string BundleID => bundleID;
        public PrefabData[] prefabs;

        public PrefabData GetPrefabData(int prefabHash)
        {
            var prefab = prefabs.FirstOrDefault(e => e.GetHashCode() == prefabHash);
            if (prefab == null)
            {
                Debug.LogError($"Prefab hash {prefabHash} not found in bundle {bundleID}");
            }

            return prefab;
        }


#if UNITY_EDITOR
        public void SetBundleID(string txt) => bundleID = txt;
#endif
        public object Clone() => MemberwiseClone();
        public string[] GetAllPrefabAddresses() => prefabs.Select(e => e.sourceAddress).ToArray();

        public int[] GetAllPrefabHashedAddresses()
        {
            var addresses = GetAllPrefabAddresses();
            var hashedAddresses = new int[addresses.Length];
            for (int i = 0; i < addresses.Length; i++)
            {
                hashedAddresses[i] = Animator.StringToHash(addresses[i]);
            }

            return hashedAddresses;
        }

        public override int GetHashCode()
        {
            return Animator.StringToHash(BundleID);
        }
    }
}