﻿using System;
using UnityEngine;


namespace XynokAssetBundle.Data
{
    [Serializable]
    public struct PoolData
    {
        public bool recycle;
        public int capacity;
        public int preload;
    }

    [Serializable]
    public struct RuntimePoolData
    {
        private int _key;

        public int Key
        {
            get
            {
                if (_key == default) _key = Animator.StringToHash(sourceAddress);
                return _key;
            }
        }
       

        public string sourceName;
        public string sourceAddress;
        public PoolData poolData;
    }
}