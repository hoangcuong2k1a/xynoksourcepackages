﻿using Enums;
using UnityEngine;
using XynokSourceGenerator.Generated.Behaviors;


   
    public class EnemyRun: ACharacterGoalHandler
    {
        protected override AbilityType AbilityType => AbilityType.Run;
        protected override bool Matched()
        {
            return true;
        }

        protected override void Init()
        {
            Debug.Log($"{AbilityType} is init !");
        }

        protected override void InternalExecute()
        {
            Debug.Log($"{AbilityType} is running");
        }

        protected override void InternalStop()
        {
            Debug.Log($"{AbilityType} is stopped");
        }

        protected override void InternalReset()
        {
            Debug.Log($"{AbilityType} is reset");
        }

        public override void Continue()
        {
            Debug.Log($"{AbilityType} is Continue");
        }
    }
    
  
