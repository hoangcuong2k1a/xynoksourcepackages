﻿using Enums.Character;
using XynokSourceGenMarker.SourceGenMarkers;

namespace Enums
{
    [EntityItemMaker( typeof(CharacterID),typeof(AbilityType), typeof(StatType), typeof(StateType), typeof(TriggerType))]
    public enum WeaponID
    {
        None=0,
        Melee,
        Range
    }
}