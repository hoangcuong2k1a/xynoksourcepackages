﻿using System;
using System.Numerics;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokConvention.BigData;
using XynokSourceGenerator.Generated.Behaviors;
using XynokSourceGenerator.Generated.Entities;
using XynokSourceGenerator.Generated.Entities.Item;

namespace Enums
{
    public class TestEnemy : ACharacterGoalKeeper, IWeaponOwner
    {
        [SerializeField] private BigInteger bigData;
        [SerializeField] private BigDouble bigDataDobule;
        [ShowInInspector] public string BigVisual => bigData.ToString();
        [ShowInInspector] public string BigVisualDouble => bigDataDobule.ToString();

        [Button]
        void LogBigDataDouble(int size = 48)
        {
            string testVal = "1";
            string result = "";
            BigDouble big = BigDouble.Parse(testVal);
            result += $"{testVal} : {big.GetStringRepresentation()}\n";

            for (int i = 0; i < size; i += 3)
            {
                testVal += "000";
                big = BigDouble.Parse(testVal);
                result += $"{testVal} : {big.GetStringRepresentation()}\n";
            }

            Debug.Log(result);
        }

        [Button]
        void LogBigData(int size = 48)
        {
            string testVal = "1";
            string result = "";
            BigInteger big = BigInteger.Parse(testVal);
            result += $"{testVal} : {big.GetStringRepresentation()}\n";

            for (int i = 0; i < size; i += 3)
            {
                testVal += "000";
                big = BigInteger.Parse(testVal);
                result += $"{testVal} : {big.GetStringRepresentation()}\n";
            }

            Debug.Log(result);
        }

        [Button]
        void Assign(string size)
        {
            BigInteger big = BigInteger.Parse(size);


            Debug.Log(big.GetStringRepresentation());
        }
        [Button]
        void AssignBigDouble(string size)
        {
            BigDouble big = BigDouble.Parse(size);


            Debug.Log(big.GetStringRepresentation());
        }

        private void Start()
        {
            SetDependency(Data);
        }


        public CharacterWeaponCollection xxxxx;
        public CharacterWeaponCollection CharacterWeaponCollection => xxxxx;

        public bool CanInitAbilityOf(WeaponResource item)
        {
            throw new NotImplementedException();
        }

        public bool CanDisposeAbilityOf(WeaponResource item)
        {
            throw new NotImplementedException();
        }
    }
}