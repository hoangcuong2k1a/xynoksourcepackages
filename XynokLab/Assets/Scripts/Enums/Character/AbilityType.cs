﻿namespace Enums
{
    public enum AbilityType
    {
        None = 0,
        Run = 1,
        Jump = 2,
        Fly = 3,
        Dash = 4,
    }
}