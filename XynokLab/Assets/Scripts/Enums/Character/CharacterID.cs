﻿using XynokSourceGenMarker.SourceGenMarkers;

namespace Enums.Character
{
    [GoalMaker(typeof(StatType), typeof(StateType),typeof(AbilityType))]
    [DOTEntityMaker(typeof(AbilityType), typeof(StatType), typeof(StateType), typeof(TriggerType))]
    [EntityMaker(typeof(AbilityType), typeof(StatType), typeof(StateType), typeof(TriggerType))]
    public enum CharacterID
    {
        None = 0,
        Player = 1,
        Enemy = 2,
        Minion = 3,
        Boss = 4,
    }
}