﻿// // Xynok Lab ~ HoangCuongZk1 ♙♙ auto generated - time: 9/25/2023 7:29:51 PM
// using Enums.Character;
// using Enums;
// using System;
// using UnityEngine;
// using Sirenix.OdinInspector;
// using System.Collections.Generic;
// using Random = UnityEngine.Random;
// using System.Linq;
// using XynokAsync;
// using XynokGUI.Modulers.Txt;
// using XynokUtils;
// using XynokUtils.APIs;
// using XynokCombatOop.APIs;
// using XynokConvention.APIs;
// using XynokConvention.Data;
// using XynokCombatOop.Goap.Data;
// using XynokCombatOop.Goap.APIs;
// using XynokSourceGenerator.Generated.Entities;
//
// namespace XynokSourceGenerator.Generated.Behaviors
// {
//     [Serializable]
//     public class CharacterGoalStatOperator : AGoalStatOperator<StatType, CharacterStatData>
//     {
//     }
//
//     [Serializable]
//     public class CharacterGoalStateOperator : AGoalStateOperator<StateType, CharacterStateData>
//     {
//     }
//
//     [Serializable]
//     public class CharacterGoalStatSelfCompareOperator : AStatSelfCompareOperator<StatType, CharacterStatData>
//     {
//     }
//
//     [Serializable]
//     public class CharacterGoalStatCompareAnotherOneOperator : AStatCompareAnotherOneOperator<StatType, CharacterStatData>
//     {
//     }
//
//     public class CharacterGoalStatCompareAnotherOneOperatorProperties : AStatCompareAnotherOneOperatorProperties<StatType, CharacterStatData>
//     {
//     }
//
//     [Serializable]
//     public class CharacterGoalCondition
//     {
//         [HorizontalGroup]
//         [HideLabel]
//         public GoalConditionType conditionType;
//         [HorizontalGroup]
//         [ShowIf("conditionType", GoalConditionType.Stat)]
//         [HideLabel]
//         public CharacterGoalStatOperator stat;
//         [HorizontalGroup]
//         [ShowIf("conditionType", GoalConditionType.State)]
//         [HideLabel]
//         public CharacterGoalStateOperator state;
//         [HorizontalGroup]
//         [ShowIf("conditionType", GoalConditionType.StatSelfCompare)]
//         [HideLabel]
//         public CharacterGoalStatSelfCompareOperator statSelfCompare;
//         [HorizontalGroup]
//         [ShowIf("conditionType", GoalConditionType.StatCompareAnotherOne)]
//         [HideLabel]
//         public CharacterGoalStatCompareAnotherOneOperator statCompareAnotherOne;
//         public bool Satisfy()
//         {
//             switch (conditionType)
//             {
//                 case GoalConditionType.Stat:
//                     return stat.Satisfy();
//                 case GoalConditionType.State:
//                     return state.Satisfy();
//                 case GoalConditionType.StatSelfCompare:
//                     return statSelfCompare.Satisfy();
//                 case GoalConditionType.StatCompareAnotherOne:
//                     return statCompareAnotherOne.Satisfy();
//                 default:
//                     return true;
//             }
//         }
//     }
//
//     [Serializable]
//     public class CharacterGoalHandler
//     {
//         [LabelText("", SdfIconType.Clock)]
//         [HorizontalGroup]
//         public TaskDurationData schedule;
//         [HorizontalGroup]
//         [HideLabel]
//         public AbilityType handler1;
//         [HorizontalGroup]
//         [LabelText("hoặc")]
//         [HideLabel]
//         public bool or;
//         [ShowIf("or")]
//         [HorizontalGroup]
//         [HideLabel]
//         public AbilityType[] anotherWays;
//     }
//
//     [Serializable]
//     public class CharacterGoalData
//     {
//         [LabelText("tên của task")]
//         public string name = "???";
//         [LabelText("trạng thái task")]
//         [ReadOnly]
//         [HideLabel]
//         public GoalStateType state;
//         [HideInInspector]
//         public ExecutorType executor;
//         [HideInInspector]
//         public ResponsibilityType responsibilityType;
//         [LabelText("độ lớn của task")]
//         [ShowIf("executor", ExecutorType.ByCost)]
//         [SuffixLabel("= tổng (điều kiện execute và complete) + số handler", Overlay = true)]
//         [ReadOnly]
//         public int cost;
//         [Space(21)]
//         [TabGroup("tab", "requirements", SdfIconType.PuzzleFill)]
//         [Title("execute condition", "điều kiện để có thể thực hiện goal này")]
//         public CharacterGoalCondition[] executeConditions;
//         [TabGroup("tab", "requirements")]
//         [ShowIf("responsibilityType", ResponsibilityType.UntilCompleted)]
//         [Title("complete condition", "điều kiện để có thể được coi là đã hoàn thành")]
//         public CharacterGoalCondition[] completedConditions;
//         [TabGroup("tab", "to do list", SdfIconType.JournalCheck)]
//         public CharacterGoalHandler[] handlers;
//         public void OnValidate()
//         {
//             if (executeConditions == null)
//                 executeConditions = Array.Empty<CharacterGoalCondition>();
//             if (completedConditions == null)
//                 completedConditions = Array.Empty<CharacterGoalCondition>();
//             if (handlers == null)
//                 handlers = Array.Empty<CharacterGoalHandler>();
//             cost = executeConditions.Length + completedConditions.Length + handlers.Length;
//         }
//
//         public bool CanExecute()
//         {
//             if (executeConditions == null || executeConditions.Length < 1)
//                 return true;
//             for (int i = 0; i < executeConditions.Length; i++)
//             {
//                 if (!executeConditions[i].Satisfy())
//                     return false;
//             }
//
//             return true;
//         }
//
//         public bool Completed()
//         {
//             if (completedConditions == null || completedConditions.Length < 1)
//                 return true;
//             for (int i = 0; i < completedConditions.Length; i++)
//             {
//                 if (!completedConditions[i].Satisfy())
//                     return false;
//             }
//
//             return true;
//         }
//     }
//
//     [Serializable]
//     public class CharacterBehaviorData
//     {
//         [Title("Cài đặt", "Cách chủ thể ra quyết định và cách chủ thể làm việc", TitleAlignments.Centered)]
//         [HideLabel]
//         [InfoBox("Cách lựa chọn task để làm")]
//         [TabGroup("tab", "Settings", SdfIconType.Gear, TextColor = "blue")]
//         public ExecutorType executor;
//         [Space(10)]
//         [HideLabel]
//         [InfoBox("Mặc cả,tính toán , cân đo đong đếm, khi thấy sức nặng của công việc")]
//         [TabGroup("tab", "Settings")]
//         [ShowIf("executor", ExecutorType.ByCost)]
//         public BargainType bargainType;
//         [Space(10)]
//         [HideLabel]
//         [InfoBox("Tinh thần, trách nhiệm khi làm việc")]
//         [TabGroup("tab", "Settings")]
//         public ResponsibilityType responsibilityType;
//         [Space(10)]
//         [HideLabel]
//         [InfoBox("Cách ra quyết định khi gặp cản trở trong công việc")]
//         [TabGroup("tab", "Settings")]
//         public InterruptHandler interruptHandler;
//         [Title("Nhiệm vụ", "việc cần làm", TitleAlignments.Centered)]
//         [TabGroup("tab", "Missions", SdfIconType.Question, TextColor = "purple")]
//         public CharacterGoalData[] goals;
//         public void OnValidate()
//         {
//             if (goals == null)
//                 return;
//             // syncs executor to goals and protectors
//             for (int i = 0; i < goals.Length; i++)
//             {
//                 goals[i].executor = executor;
//                 goals[i].responsibilityType = responsibilityType;
//                 goals[i].OnValidate();
//             }
//         }
//
//         public bool Ready()
//         {
//             for (int i = 0; i < goals.Length; i++)
//             {
//                 if (goals[i].CanExecute())
//                     return true;
//             }
//
//             return false;
//         }
//     }
//
//     [Serializable]
//     public class CharacterGoalContainer
//     {
//         [FoldoutGroup("Các hoạt động thường ngày")]
//         [Title("SỐ PHẬN - ĐỊNH MỆNH", "một chuỗi các nhiệm vụ mà chủ thể phải thực hiện trong suốt vòng đời", TitleAlignments.Centered)]
//         [HideLabel]
//         public CharacterBehaviorData mission;
//         [Space(21)]
//         [FoldoutGroup("Các hoạt động khi có biến !!!")]
//         [Title("TỰ VỆ - BẢN NĂNG", "khi có sự tác động từ bên ngoài, gây cản trở việc làm nhiệm vụ, chủ thể sẽ thực hiện các hành động này", TitleAlignments.Centered)]
//         [HideLabel]
//         public CharacterBehaviorData selfDefence;
//         public void OnValidate()
//         {
//             mission.OnValidate();
//             selfDefence.OnValidate();
//         }
//     }
//
//     public interface ICharacterGoalKeeper
//     {
//         CharacterResource GoalKeeperData { get; }
//
//         CharacterGoalContainer GoalContainer { get; }
//
//         ICharacterGoalHandler[] GoalHandlers { get; }
//
//         CharacterGoalActionMonitor GoalActionMonitor { get; }
//     }
//
//     /// <summary>
//     /// abstract class for entity has goals
//     /// </summary>  
//     public abstract class ACharacterGoalKeeper : ACharacterEntity, ICharacterGoalKeeper, IGoapAgentSequenceProtector
//     {
//         [Header("GOAL KEEPER SETUP")]
//         [SerializeField]
//         protected CharacterGoalContainer goalContainer;
//         [SerializeField]
//         protected CharacterGoalActionMonitor goalActionMonitor;
//         public CharacterResource GoalKeeperData => Data;
//         public CharacterGoalContainer GoalContainer => goalContainer;
//         public CharacterGoalActionMonitor GoalActionMonitor => goalActionMonitor;
//         public ICharacterGoalHandler[] GoalHandlers => GetGoalHandlers();
//         public CharacterGoalPlanner CurrentPlanner => _currentPlanner;
//
//         private CharacterGoalInitializer _goalInitializer;
//         private CharacterGoalPlanner _currentPlanner;
//         protected virtual void OnValidate()
//         {
//             goalContainer.OnValidate();
//         }
//
//         protected override void Init()
//         {
//             _goalInitializer ??= new();
//             _goalInitializer.SetDependency(this);
//             InitGoalMonitor();
//         }
//
//         void InitGoalMonitor()
//         {
//             for (int i = 0; i < abilityController.Abilities.Length; i++)
//             {
//                 int index = i;
//                 var ability = abilityController.Abilities[index];
//                 if (ability is ICharacterGoalHandler handler)
//                 {
//                     goalActionMonitor.GetIn(handler);
//                 }
//             }
//
//             _goalInitializer.PlannerSelfDefence.OnCompleted += MissionExecute;
//             _goalInitializer.PlannerMission.OnCompleted += DefenceExecute;
//         }
//
//         protected override void OnDispose()
//         {
//             goalActionMonitor?.Dispose();
//             if (_goalInitializer != null)
//             {
//                 _goalInitializer.Dispose();
//                 _goalInitializer.PlannerSelfDefence.OnCompleted -= MissionExecute;
//                 _goalInitializer.PlannerMission.OnCompleted -= DefenceExecute;
//             }
//         }
//
//         void MissionExecute()
//         {
//             _currentPlanner = _goalInitializer.PlannerMission;
//             _goalInitializer.PlannerMission.Execute();
//         }
//
//         void DefenceExecute()
//         {
//             _currentPlanner = _goalInitializer.PlannerSelfDefence;
//             _goalInitializer.PlannerSelfDefence.Execute();
//         }
//
//         [Button]
//         public virtual void Execute()
//         {
//             if (goalContainer.selfDefence.Ready())
//             {
//                 _goalInitializer.PlannerMission.Stop();
//                 _currentPlanner = _goalInitializer.PlannerSelfDefence;
//                 _goalInitializer.PlannerSelfDefence.Execute();
//                 return;
//             }
//
//             _goalInitializer.PlannerSelfDefence.Stop();
//             _currentPlanner = _goalInitializer.PlannerMission;
//             _goalInitializer.PlannerMission.Execute();
//         }
//
//         [Button]
//         public virtual void Continue()
//         {
//             if (goalContainer.selfDefence.Ready())
//             {
//                 _goalInitializer.PlannerSelfDefence.Continue();
//                 return;
//             }
//
//             _goalInitializer.PlannerMission.Continue();
//         }
//
//         [Button]
//         public virtual void Stop()
//         {
//             _goalInitializer.PlannerSelfDefence.Stop();
//             _goalInitializer.PlannerMission.Stop();
//         }
//
//         [Button]
//         public virtual void Reset()
//         {
//             _goalInitializer.PlannerSelfDefence.Reset();
//             _goalInitializer.PlannerMission.Reset();
//         }
//
//         ICharacterGoalHandler[] GetGoalHandlers()
//         {
//             if (abilityController == null)
//                 return Array.Empty<ICharacterGoalHandler>();
//             var handlers = new List<ICharacterGoalHandler>();
//             for (int i = 0; i < abilityController.Abilities.Length; i++)
//             {
//                 int index = i;
//                 var ability = abilityController.Abilities[index];
//                 if (ability is ICharacterGoalHandler handler)
//                 {
//                     handlers.Add(handler);
//                 }
//             }
//
//             return handlers.ToArray();
//         }
//     }
//
//     public interface ICharacterGoalHandler : ICharacterAbilityControllable, IGoalHandler<AbilityType, ICharacterGoalHandler>, IChain<ICharacterGoalHandler>
//     {
//     }
//
//     public abstract class ACharacterGoalHandler : ACharacterAbility, ICharacterGoalHandler
//     {
//         public AbilityType HandleID => AbilityType;
//         public ICharacterGoalHandler Next => _nextHandler;
//
//         public event Action<ICharacterGoalHandler> OnExecute;
//         public event Action<ICharacterGoalHandler> OnStop;
//         public event Action<ICharacterGoalHandler> OnReset;
//         private ICharacterGoalHandler _nextHandler;
//         protected abstract void InternalExecute();
//         protected abstract void InternalStop();
//         protected abstract void InternalReset();
//         public abstract void Continue();
//         public void Execute()
//         {
//             OnExecute?.Invoke(this);
//             InternalExecute();
//         }
//
//         public void Stop()
//         {
//             OnStop?.Invoke(this);
//             InternalStop();
//         }
//
//         public void Reset()
//         {
//             OnReset?.Invoke(this);
//             InternalReset();
//         }
//
//         public void SetNext(ICharacterGoalHandler next)
//         {
//             _nextHandler = next;
//         }
//
//         public override void Dispose()
//         {
//             OnExecute = default;
//             OnStop = default;
//             OnReset = default;
//             _nextHandler = default;
//         }
//     }
//
//     /// <summary>
//     /// init planners for entity.
//     /// </summary>
//     public class CharacterGoalInitializer : IInjectable<ICharacterGoalKeeper>
//     {
//         private ICharacterGoalKeeper _goalKeeper;
//         private CharacterResource Data => _goalKeeper.GoalKeeperData;
//         private CharacterGoalContainer GoalContainer => _goalKeeper.GoalContainer;
//
//         private Action _onDispose;
//         // planners
//         private CharacterGoalPlanner _plannerMission;
//         private CharacterGoalPlanner _plannerSelfDefence;
//         public CharacterGoalPlanner PlannerMission => _plannerMission;
//         public CharacterGoalPlanner PlannerSelfDefence => _plannerSelfDefence;
//
//         public void SetDependency(ICharacterGoalKeeper dependency)
//         {
//             if (_goalKeeper != null)
//                 Dispose();
//             _goalKeeper = dependency;
//             BindingGoalConditionToSource();
//             _plannerMission = InitPlannerFor(GoalContainer.mission);
//             _plannerSelfDefence = InitPlannerFor(GoalContainer.selfDefence);
//         }
//
//         CharacterGoalPlanner InitPlannerFor(CharacterBehaviorData behaviorData)
//         {
//             var plannerProperties = new CharacterGoalPlannerProperties
//             {
//                 GoalKeeper = _goalKeeper,
//                 BehaviorData = behaviorData
//             };
//             var planner = new CharacterGoalPlanner();
//             planner.SetDependency(plannerProperties);
//             _onDispose += planner.Dispose;
//             return planner;
//         }
//
//         void BindingGoalConditionToSource()
//         {
//             BindingGoals(GoalContainer.mission.goals);
//             BindingGoals(GoalContainer.selfDefence.goals);
//         }
//
//         void BindingGoals(CharacterGoalData[] goals)
//         {
//             for (int i = 0; i < goals.Length; i++)
//             {
//                 var goal = goals[i];
//                 BindingConditions(goal.executeConditions);
//                 BindingConditions(goal.completedConditions);
//             }
//         }
//
//         void BindingConditions(CharacterGoalCondition[] conditions)
//         {
//             for (int i = 0; i < conditions.Length; i++)
//             {
//                 int index = i;
//                 var condition = conditions[index];
//                 var type = condition.conditionType;
//                 if (type == GoalConditionType.Stat)
//                 {
//                     var source = Data.GetStat(condition.stat.stat);
//                     condition.stat.SetDependency(source);
//                     _onDispose += condition.stat.Dispose;
//                     continue;
//                 }
//
//                 if (type == GoalConditionType.State)
//                 {
//                     var source = Data.GetState(condition.state.state);
//                     condition.state.SetDependency(source);
//                     _onDispose += condition.state.Dispose;
//                     continue;
//                 }
//
//                 if (type == GoalConditionType.StatSelfCompare)
//                 {
//                     var source = Data.GetStat(condition.statSelfCompare.stat);
//                     condition.statSelfCompare.SetDependency(source);
//                     _onDispose += condition.statSelfCompare.Dispose;
//                     continue;
//                 }
//
//                 if (type == GoalConditionType.StatCompareAnotherOne)
//                 {
//                     var source = Data.GetStat(condition.statCompareAnotherOne.stat);
//                     var another = Data.GetStat(condition.statCompareAnotherOne.another);
//                     var properties = new CharacterGoalStatCompareAnotherOneOperatorProperties
//                     {
//                         Stat = source,
//                         Another = another
//                     };
//                     condition.statCompareAnotherOne.SetDependency(properties);
//                     _onDispose += condition.statCompareAnotherOne.Dispose;
//                     continue;
//                 }
//             }
//         }
//
//         public void Dispose()
//         {
//             _onDispose?.Invoke();
//             _onDispose = default;
//             _goalKeeper = default;
//         }
//     }
//
//     public class CharacterGoalSingleActionBakerProperties
//     {
//         public ICharacterGoalKeeper GoalKeeper;
//         public CharacterGoalHandler Handler;
//     }
//
//     /// <summary>
//     /// init task and event catch for single action of entity.
//     /// </summary>
//     public class CharacterGoalSingleActionBaker : IExecuteLite, IInjectable<CharacterGoalSingleActionBakerProperties>, IChain<CharacterGoalSingleActionBaker>
//     {
//         // externals
//         public event Action<CharacterGoalSingleActionBaker> OnCompleted;
//         public ICharacterGoalHandler CurrentHandler => _currentHandler;
//         public CharacterGoalSingleActionBaker Next => _next;
//         public bool Completed => _taskDuration == null || _taskDuration.Completed;
//
//         // internals
//         private CharacterGoalSingleActionBakerProperties _properties;
//         private CharacterGoalSingleActionBaker _next;
//         private CharacterGoalHandler HandlerDefine => _properties.Handler;
//         private ICharacterGoalKeeper GoalKeeper => _properties.GoalKeeper;
//
//         private ICharacterGoalHandler[] _runtimeHandlers;
//         private ICharacterGoalHandler _currentHandler;
//         private TaskDuration _taskDuration;
//         private Action _onDispose;
//         public void SetDependency(CharacterGoalSingleActionBakerProperties dependency)
//         {
//             if (_properties != null)
//                 Dispose();
//             _properties = dependency;
//             _runtimeHandlers = GetMatchingRuntimeHandlers();
//         }
//
//         public void Reset()
//         {
//             Stop();
//             // reset all actions valid, not only current action. note: tạm thời sử dụng như vầy, sau nên tìm giải thuật tốt hơn
//             if (_runtimeHandlers != null && _runtimeHandlers.Length > 0)
//             {
//                 for (int i = 0; i < _runtimeHandlers.Length; i++)
//                 {
//                     _runtimeHandlers[i].Reset();
//                 }
//             }
//
//             if (_currentHandler != null)
//                 HandlerDefine.schedule.RemoveAction(_currentHandler.Execute);
//             _taskDuration ??= new();
//             _taskDuration.SetDependency(HandlerDefine.schedule);
//             _taskDuration.OnCompleted -= Complete;
//             _taskDuration.OnCompleted += Complete;
//         }
//
//         public void Continue()
//         {
//             _taskDuration?.Activate(true);
//         }
//
//         public void Execute()
//         {
//             if (_runtimeHandlers == null || _runtimeHandlers.Length < 1)
//             {
//                 Debug.LogError($"[GoalSingleActionBaker - {GoalKeeper.GoalKeeperData.resourceId}]: _runtimeHandlers is null or empty");
//                 return;
//             }
//
//             _currentHandler = GetHandlerForExecuting();
//             GoalKeeper.GoalActionMonitor.RequestFor(_currentHandler, RequestExecute);
//         }
//
//         void RequestExecute(bool requestResult)
//         {
//             if (requestResult)
//             {
//                 HandlerDefine.schedule.AddAction(_currentHandler.Execute);
//                 _taskDuration.Activate(true);
//                 return;
//             }
//
//             Complete();
//         }
//
//         public void Stop()
//         {
//             _taskDuration?.Activate(false);
//             _currentHandler?.Stop();
//         }
//
//         void Complete()
//         {
//             Stop();
//             if (_currentHandler != null)
//                 HandlerDefine.schedule.RemoveAction(_currentHandler.Execute);
//             _currentHandler = null;
//             OnCompleted?.Invoke(this);
//         }
//
//         ICharacterGoalHandler GetHandlerForExecuting()
//         {
//             if (!HandlerDefine.or)
//                 return _runtimeHandlers[0];
//             if (HandlerDefine.or && _runtimeHandlers.Length == 1)
//                 return _runtimeHandlers[0];
//             var random = Random.Range(0, _runtimeHandlers.Length);
//             return _runtimeHandlers[random];
//         }
//
//         public void Dispose()
//         {
//             _taskDuration?.Dispose();
//             _onDispose?.Invoke();
//             _onDispose = default;
//             _properties = default;
//             OnCompleted = default;
//         }
//
//         ICharacterGoalHandler[] GetMatchingRuntimeHandlers()
//         {
//             var runtimeHandlers = new List<ICharacterGoalHandler>();
//             for (int i = 0; i < GoalKeeper.GoalHandlers.Length; i++)
//             {
//                 int index = i;
//                 var runtimeHandler = GoalKeeper.GoalHandlers[index];
//                 // always add handler1 if match
//                 if (runtimeHandler.HandleID == HandlerDefine.handler1 && !runtimeHandlers.Contains(runtimeHandler))
//                 {
//                     runtimeHandlers.Add(runtimeHandler);
//                 }
//
//                 // if not has variants option, skip
//                 if (!HandlerDefine.or)
//                     continue;
//                 var contain = HandlerDefine.anotherWays.Any(e => e == runtimeHandler.HandleID);
//                 if (contain && !runtimeHandlers.Contains(runtimeHandler))
//                     runtimeHandlers.Add(runtimeHandler);
//             }
//
//             return runtimeHandlers.ToArray();
//         }
//
//         public void SetNext(CharacterGoalSingleActionBaker next)
//         {
//             _next = next;
//         }
//     }
//
//     /// <summary>
//     /// sequence chain for a list of handlers belong to a goal, each handler has only single or random of multiple of action.
//     /// NOTE: This is only focus for a goal (only 1).
//     /// </summary>
//     public class CharacterGoalHandlerBaker : IInjectable<ICharacterGoalKeeper>, IExecuteLite, IChain<CharacterGoalHandlerBaker>
//     {
//         public event Action<CharacterGoalHandlerBaker> OnCompleted;
//         public CharacterGoalSingleActionBaker CurrentAction => _currentAction;
//         public CharacterGoalData GoalData => _goalData;
//         public int Cost => _goalData.cost;
//         public CharacterGoalHandlerBaker Next => _nextGoal;
//
//         private ICharacterGoalKeeper _goalKeeper;
//         private CharacterGoalSingleActionBaker[] _handleBakers;
//         private Action _onDispose;
//         private CharacterGoalSingleActionBaker _currentAction;
//         private CharacterGoalData _goalData;
//         private CharacterGoalHandlerBaker _nextGoal;
//         bool Completed()
//         {
//             var responsibilityType = _goalData.responsibilityType;
//             if (responsibilityType == ResponsibilityType.UntilCompleted)
//                 return _goalData.Completed();
//             if (responsibilityType == ResponsibilityType.TimeOut)
//             {
//                 if (_currentAction == null)
//                     return true;
//                 var index = Array.IndexOf(_handleBakers, _currentAction);
//                 return index == _handleBakers.Length - 1 && _currentAction.Completed;
//             }
//
//             return false;
//         }
//
//         public void SetDependency(ICharacterGoalKeeper dependency)
//         {
//             if (_goalKeeper != null)
//                 Dispose();
//             _goalKeeper = dependency;
//         }
//
//         public void SetNext(CharacterGoalHandlerBaker next)
//         {
//             _nextGoal = next;
//         }
//
//         public void Dispose()
//         {
//             _onDispose?.Invoke();
//             _onDispose = default;
//         }
//
//         public void InitFor(CharacterGoalData goalData)
//         {
//             _goalData = goalData;
//             Reset();
//         }
//
//         void ChainTheActions()
//         {
//             var amount = _handleBakers.Length;
//             for (int i = 0; i < amount; i++)
//             {
//                 int index = i;
//                 var next = index < amount - 1 ? _handleBakers[index + 1] : null;
//                 _handleBakers[index].SetNext(next);
//                 _handleBakers[index].OnCompleted += ExecuteNextHandler;
//             }
//         }
//
//         void ExecuteNextHandler(CharacterGoalSingleActionBaker handler)
//         {
//             if (handler.Next == null || Completed())
//             {
//                 _currentAction = null;
//                 OnCompleted?.Invoke(this);
//                 return;
//             }
//
//             _currentAction = handler.Next;
//             handler.Next.Execute();
//         }
//
//         public void Execute()
//         {
//             if (_handleBakers == null || _handleBakers.Length < 1)
//             {
//                 Debug.LogError($"[GoalHandlerBaker - {_goalKeeper.GoalKeeperData.resourceId}]: _handleBakers is null or empty");
//                 return;
//             }
//
//             //   Stop();
//             //  _handleBakers = GetBakersOf(_goalData);
//             _currentAction = _handleBakers[0];
//             _currentAction.Execute();
//         }
//
//         public void Continue()
//         {
//             _currentAction?.Continue();
//         }
//
//         public void Stop()
//         {
//             _currentAction?.Stop();
//         }
//
//         /// <summary>
//         /// setup the chain & reset all single action, it does not replay. Call execute() for play.
//         /// </summary>
//         public void Reset()
//         {
//             Stop();
//             _handleBakers = GetBakersOf(_goalData);
//             if (_handleBakers != null && _handleBakers.Length > 0)
//             {
//                 for (int i = 0; i < _handleBakers.Length; i++)
//                 {
//                     _handleBakers[i].Reset();
//                 }
//             }
//
//             ChainTheActions();
//         }
//
//         CharacterGoalSingleActionBaker[] GetBakersOf(CharacterGoalData goal)
//         {
//             var bakers = new CharacterGoalSingleActionBaker[goal.handlers.Length];
//             for (int i = 0; i < goal.handlers.Length; i++)
//             {
//                 int index = i;
//                 var handler = goal.handlers[index];
//                 var bakerProperties = new CharacterGoalSingleActionBakerProperties
//                 {
//                     GoalKeeper = _goalKeeper,
//                     Handler = handler
//                 };
//                 var baker = new CharacterGoalSingleActionBaker();
//                 baker.SetDependency(bakerProperties);
//                 _onDispose -= baker.Dispose; // remove old, because reset can be called many times.
//                 _onDispose += baker.Dispose;
//                 bakers[index] = baker;
//             }
//
//             return bakers;
//         }
//     }
//
//     public class CharacterGoalPlannerProperties
//     {
//         public ICharacterGoalKeeper GoalKeeper;
//         public CharacterBehaviorData BehaviorData;
//     }
//
//     /// <summary>
//     /// init goal array and sort by config.
//     /// </summary>
//     public class CharacterGoalPlanner : IInjectable<CharacterGoalPlannerProperties>, IExecuteLite
//     {
//         public event Action OnCompleted;
//         public CharacterGoalHandlerBaker CurrentHandler => _currentHandler;
//
//         private CharacterGoalPlannerProperties _properties;
//         private ICharacterGoalKeeper GoalKeeper => _properties.GoalKeeper;
//         private CharacterResource GoalKeeperData => GoalKeeper.GoalKeeperData;
//
//         private Action _onDispose;
//         private CharacterGoalHandlerBaker[] _goalHandlerBakers;
//         private CharacterGoalHandlerBaker _currentHandler;
//         public void SetDependency(CharacterGoalPlannerProperties dependency)
//         {
//             if (_properties != null)
//                 Dispose();
//             _properties = dependency;
//             _goalHandlerBakers = GetBakersOf(_properties.BehaviorData.goals);
//             SortGoals();
//         }
//
// #region Executings
//         public void Execute()
//         {
//             if (_goalHandlerBakers == null || _goalHandlerBakers.Length < 1)
//             {
//                 Debug.LogError($"[GoalPlanner - {GoalKeeperData.resourceId}]: _goalHandlerBakers is null or empty");
//                 OnCompleted?.Invoke();
//                 return;
//             }
//
//             Stop();
//             SortGoals();
//             _currentHandler = _goalHandlerBakers[0];
//             _currentHandler.Execute();
//         }
//
//         public void Continue()
//         {
//             if (_currentHandler == null)
//             {
//                 Execute();
//                 return;
//             }
//
//             _currentHandler.Continue();
//         }
//
//         public void Stop()
//         {
//             _currentHandler?.Stop();
//         }
//
//         public void Reset()
//         {
//             if (_goalHandlerBakers == null || _goalHandlerBakers.Length < 1)
//                 return;
//             for (int i = 0; i < _goalHandlerBakers.Length; i++)
//             {
//                 _goalHandlerBakers[i].Reset();
//             }
//         }
//
// #endregion
// #region Internals
//         void SortGoals()
//         {
//             var executor = _properties.BehaviorData.executor;
//             if (executor == ExecutorType.Sequence)
//                 Sequence();
//             if (executor == ExecutorType.Random)
//                 MakeRandom();
//             if (executor == ExecutorType.ByCost)
//                 SortByCost();
//         }
//
//         void SortByCost()
//         {
//             var sortType = _properties.BehaviorData.bargainType;
//             if (sortType == BargainType.Cheapest)
//                 _goalHandlerBakers = _goalHandlerBakers.OrderBy(e => e.Cost).ToArray();
//             if (sortType == BargainType.Expensive)
//                 _goalHandlerBakers = _goalHandlerBakers.OrderByDescending(e => e.Cost).ToArray();
//             Sequence();
//         }
//
//         void Sequence()
//         {
//             var amount = _goalHandlerBakers.Length;
//             for (int i = 0; i < amount; i++)
//             {
//                 // init chains
//                 var handler = _goalHandlerBakers[i];
//                 var next = i < amount - 1 ? _goalHandlerBakers[i + 1] : null;
//                 handler.SetNext(next);
//                 handler.OnCompleted -= AfterGoalCompleted;
//                 handler.OnCompleted += AfterGoalCompleted;
//             }
//         }
//
//         void MakeRandom()
//         {
//             _goalHandlerBakers = CollectionUtil.Shuffle(_goalHandlerBakers);
//             Sequence();
//         }
//
//         void AfterGoalCompleted(CharacterGoalHandlerBaker goalBaker)
//         {
//             if (goalBaker.Next == null)
//             {
//                 _currentHandler = null;
//                 OnCompleted?.Invoke();
//                 return;
//             }
//
//             _currentHandler = goalBaker.Next;
//             _currentHandler.Execute();
//         }
//
//         CharacterGoalHandlerBaker[] GetBakersOf(CharacterGoalData[] goals)
//         {
//             var bakers = new CharacterGoalHandlerBaker[goals.Length];
//             for (int i = 0; i < goals.Length; i++)
//             {
//                 int index = i;
//                 var goal = goals[index];
//                 var baker = new CharacterGoalHandlerBaker();
//                 baker.SetDependency(GoalKeeper);
//                 baker.InitFor(goal);
//                 bakers[index] = baker;
//                 _onDispose += baker.Dispose;
//             }
//
//             return bakers;
//         }
//
// #endregion
//         public void Dispose()
//         {
//             _onDispose?.Invoke();
//             _onDispose = default;
//         }
//     }
//
//     [Serializable]
//     public class CharacterAbilityExecuteConditionDict : SerializableDict<AbilityType, CharacterAbilityExecuteConditionWrapper>
//     {
//     }
//
//     [Serializable]
//     public class CharacterAbilityExecuteConditionWrapper
//     {
//         // [Title("General")] public CharacterGoalCondition[] general;
//         public CharacterGoalCondition[] actionExecuteRequirements;
//         private ICharacterAbilityControllable _source;
//         private CharacterResource Data => _source.ControllableData;
//
//         public void SetDependency(ICharacterAbilityControllable member)
//         {
//             _source = member;
//             for (int i = 0; i < actionExecuteRequirements.Length; i++)
//             {
//                 int index = i;
//                 var condition = actionExecuteRequirements[index];
//                 var type = condition.conditionType;
//                 if (type == GoalConditionType.Stat)
//                 {
//                     var source = Data.GetStat(condition.stat.stat);
//                     condition.stat.SetDependency(source);
//                     continue;
//                 }
//
//                 if (type == GoalConditionType.State)
//                 {
//                     var source = Data.GetState(condition.state.state);
//                     condition.state.SetDependency(source);
//                     continue;
//                 }
//
//                 if (type == GoalConditionType.StatSelfCompare)
//                 {
//                     var source = Data.GetStat(condition.statSelfCompare.stat);
//                     condition.statSelfCompare.SetDependency(source);
//                     continue;
//                 }
//
//                 if (type == GoalConditionType.StatCompareAnotherOne)
//                 {
//                     var source = Data.GetStat(condition.statCompareAnotherOne.stat);
//                     var another = Data.GetStat(condition.statCompareAnotherOne.another);
//                     var properties = new CharacterGoalStatCompareAnotherOneOperatorProperties
//                     {
//                         Stat = source,
//                         Another = another
//                     };
//                     condition.statCompareAnotherOne.SetDependency(properties);
//                     continue;
//                 }
//             }
//         }
//
//         public bool IsSatisfied()
//         {
//             // for (int i = 0; i < general.Length; i++)
//             //     if (!general[i].Satisfy())
//             //         return false;
//             for (int i = 0; i < actionExecuteRequirements.Length; i++)
//                 if (!actionExecuteRequirements[i].Satisfy())
//                     return false;
//             return true;
//         }
//     }
//
//     [Serializable]
//     public class CharacterGoalActionMonitor : IMonitor<AbilityType, CharacterResource, ICharacterAbilityControllable>, IDisposable
//     {
//         [SerializeField]
//         private CharacterAbilityExecuteConditionDict dictRequirement;
//         private Dictionary<int, CharacterAbilityExecuteConditionWrapper> _members;
//         public void GetIn(ICharacterAbilityControllable member)
//         {
//             if (member == null)
//                 throw new("CharacterGoalActionMonitor - GetIn: member is null");
//             var hash = member.GetHashCode();
//             var id = member.IdControllable;
//             if (!dictRequirement.ContainsKey(id))
//             {
//                 Debug.LogWarning($"Monitor not found {member.IdControllable} for get in !");
//                 return;
//             }
//
//             _members ??= new();
//             if (_members.ContainsKey(hash))
//             {
//                 Debug.LogWarning($"Monitor already has {id} !");
//                 return;
//             }
//
//             var runtimeBaker = dictRequirement[id].Clone();
//             runtimeBaker.SetDependency(member);
//             _members.Add(hash, runtimeBaker);
//         }
//
//         public void GetOut(ICharacterAbilityControllable member)
//         {
//             if (member == null)
//                 throw new("CharacterGoalActionMonitor - GetOut: member is null");
//             _members?.Remove(member.GetHashCode());
//         }
//
//         public void RequestFor(ICharacterAbilityControllable member, Action<bool> request)
//         {
//             if (member == null)
//             {
//                 throw new("CharacterGoalActionMonitor-RequestFor: member is null");
//             }
//
//             var hash = member.GetHashCode();
//             var id = member.IdControllable;
//             if (!dictRequirement.ContainsKey(id))
//             {
//                 Debug.LogWarning($"Monitor not found {id} for Requesting !");
//                 request?.Invoke(true);
//                 return;
//             }
//
//             if (!_members.ContainsKey(hash))
//             {
//                 Debug.LogError($"{id} did not get in but is Requesting !");
//                 return;
//             }
//
//             var result = _members[hash].IsSatisfied();
//             request?.Invoke(result);
//         }
//
//         public void Dispose()
//         {
//             _members?.Clear();
//         }
//     }
//
// #region State Entity Tracker
//     public abstract class ACharacterGoalKeeperStateTracker : MonoBehaviour
//     {
//         [SerializeField]
//         protected TextBase stateTxt;
//         [SerializeField]
//         protected TextBase goalTxt;
//         [SerializeField]
//         protected TextBase actTxt;
//         [SerializeField]
//         protected ACharacterGoalKeeper goalKeeper;
//         private Action _onDispose;
//         private void Start()
//         {
//             BindingState();
//             BindingTrigger();
//         }
//
//         void Update()
//         {
//             if (goalKeeper.CurrentPlanner == null)
//                 return;
//             var currentGoalHandler = goalKeeper.CurrentPlanner.CurrentHandler;
//             if (currentGoalHandler == null)
//                 return;
//             goalTxt.SetText($"{currentGoalHandler.GoalData.name}");
//             var currentAction = currentGoalHandler.CurrentAction;
//             if (currentAction == null)
//                 return;
//             actTxt.SetText($"{currentAction.CurrentHandler.HandleID}");
//         }
//
//         void BindingState()
//         {
//             for (int i = 0; i < goalKeeper.Data.states.Length; i++)
//             {
//                 int index = i;
//                 var state = goalKeeper.Data.states[index];
//                 state.OnChanged += UpdateStateTxt;
//                 void UpdateStateTxt(bool value)
//                 {
//                     if (!value)
//                         return;
//                     stateTxt.SetText($"{state.Key}");
//                 }
//
//                 _onDispose += () => state.OnChanged -= UpdateStateTxt;
//             }
//         }
//
//         void BindingTrigger()
//         {
//             for (int i = 0; i < goalKeeper.Data.triggers.Length; i++)
//             {
//                 int index = i;
//                 var state = goalKeeper.Data.triggers[index];
//                 state.OnChanged += UpdateStateTxt;
//                 void UpdateStateTxt(bool value)
//                 {
//                     if (!value)
//                         return;
//                     stateTxt.SetText($"{state.Key}");
//                 }
//
//                 _onDispose += () => state.OnChanged -= UpdateStateTxt;
//             }
//         }
//
//         private void OnDestroy()
//         {
//             _onDispose?.Invoke();
//         }
//     }
// #endregion
// }
// //Xynok Lab ~ HoangCuongZk1 ♙♙ auto generated - time: 9/25/2023 7:29:51 PM