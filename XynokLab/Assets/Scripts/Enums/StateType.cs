﻿using XynokSourceGenMarker.SourceGenMarkers;

namespace Enums
{
    //[TestSourceGen]
    public enum StateType
    {
        None = 0,
        AttackReady = 1,
        Attacking = 2,
        MoveReady = 3,
        Moving = 4,
    }
}