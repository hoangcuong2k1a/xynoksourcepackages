﻿

using XynokSourceGenMarker.SourceGenMarkers;

namespace Enums
{
    [StatComponentCreator]
    public enum StatType
    {
        Hp,
        AttackSpeed,
        MoveSpeed,
        AttackDamage,
        MyTargetHp ,
    }
}