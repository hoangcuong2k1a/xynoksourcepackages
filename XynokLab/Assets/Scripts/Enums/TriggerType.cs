﻿namespace Enums
{
    public enum TriggerType
    {
        None = 0,
        ForceAttack = 1,
        ForceMove = 2,
        Show = 3,
        Hide = 4,
    }
}