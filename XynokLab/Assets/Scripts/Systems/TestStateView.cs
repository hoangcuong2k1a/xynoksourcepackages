﻿using Enums;
using XynokGUI.Entities.StatView;
using XynokSourceGenerator.Generated.Entities;

namespace Systems
{
    public class TestStateView: ACharacterStateView
    {
        public CharacterStateData state;
        protected override CharacterStateData GetSourceData(StateType key)
        {
            return state;
        }

        protected override string GetStateName(StateType key)
        {
            return state.Key.ToString();
        }
    }
}