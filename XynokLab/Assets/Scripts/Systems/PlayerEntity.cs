﻿using System;
using Enums;
using Enums.Character;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokCombatOop.Physic.Raycast;
using XynokConvention.BigData;
using XynokGUI.Modulers.Txt;
using XynokSourceGenerator.Dot.Generated.Components;
using XynokSourceGenerator.Dot.Generated.Systems;
using XynokSourceGenerator.Generated.Entities;
using XynokSourceGenerator.Generated.Entities.Item;
using XynokSourceGenMarker.SourceGenMarkers;

namespace Systems
{
    [DOTEntitySystemMaker(typeof(CharacterID), typeof(AbilityType), typeof(StatType), typeof(StateType),
        typeof(TriggerType), "Assets/Scripts/Systems/Generated")]
    public enum EmmitCmdSystem
    {
        None = 0
    }


    public class PlayerEntity : ACharacterEntity, IWeaponOwner
    {
        [SerializeField] private Raycaster2d raycaster2D;
        [SerializeField] private Transform prefab;
        [SerializeField] private Transform target;

        private void Start()
        {
            BigDouble bigDouble = 1;
          //
            raycaster2D.OnHit += OnHit;
        }

        [Button]
        void TestRaycast()
        {
            var dir = target.position - transform.position;
            raycaster2D.Raycast(transform.position, dir);
        }

        void OnHit(Raycast2dHitData data)
        {
            Instantiate(prefab, data.hit.point, Quaternion.identity);
        }

        protected override void Init()
        {
            CharacterHpAspectCmd cmd;
        }

        protected override void OnDispose()
        {
        }

        public class PlayerBaker : ACharacterBaker<PlayerEntity>
        {
        }


        // public abstract class MyBaker<T> : Baker<T> where T : ACharacterEntity
        // {
        //     public override void Bake(T authoring)
        //     {
        //         var entity = GetEntity(TransformUsageFlags.Dynamic);
        //         AddComponent(entity, new Disabled());
        //
        //
        //         #region Entity Tag
        //
        //         switch (authoring.Data.resourceId)
        //         {
        //             case CharacterID.Boss:
        //                 AddComponent(entity, new CharacterTagBoss());
        //                 break;
        //         }
        //
        //         #endregion
        //         
        //         #region Stats
        //
        //         for (int i = 0; i < authoring.Data.stats.Length; i++)
        //         {
        //             var comp = authoring.Data.stats[i];
        //             if (comp.Key == StatType.Hp)
        //             {
        //                 AddComponent(entity, new CharacterHp
        //                 {
        //                     baseValue = comp.Value,
        //                     lastValue = comp.Value,
        //                     Value = comp.Value
        //                 });
        //                 continue;
        //             }
        //         }
        //
        //         #endregion
        //         
        //         
        //         #region States
        //
        //         for (int i = 0; i < authoring.Data.states.Length; i++)
        //         {
        //             var comp = authoring.Data.states[i];
        //             if (comp.Key == StateType.Moving)
        //             {
        //                 AddComponent(entity, new CharacterMoving
        //                 {
        //                     baseValue = comp.Value,
        //                     lastValue = comp.Value,
        //                     Value = comp.Value
        //                 });
        //                 continue;
        //             }
        //         }
        //
        //         #endregion
        //
        //         #region Triggers
        //
        //         for (int i = 0; i < authoring.Data.triggers.Length; i++)
        //         {
        //             var comp = authoring.Data.triggers[i];
        //             if (comp.Key == TriggerType.ForceAttack)
        //             {
        //                 AddComponent(entity, new CharacterForceAttack
        //                 {
        //                     baseValue = comp.Value,
        //                     lastValue = comp.Value,
        //                     Value = comp.Value
        //                 });
        //                 continue;
        //             }
        //         }
        //
        //         #endregion
        //
        //         
        //         #region Abilities
        //         for (int i = 0; i < authoring.Data.abilities.Count; i++)
        //         {
        //             var comp = authoring.Data.abilities[i];
        //             if (comp == AbilityType.Jump)
        //             {
        //                 AddComponent(entity, new CharacterAbilityJump());
        //                 continue;
        //             }
        //         }
        //         #endregion
        //     }
        // }
        // public CharacterGoalContainer CharacterGoalContainer;
        [SerializeField] private CharacterWeaponCollection _characterWeaponCollection;
        public CharacterWeaponCollection CharacterWeaponCollection => _characterWeaponCollection;

        public bool CanInitAbilityOf(WeaponResource item)
        {
            throw new NotImplementedException();
        }

        public bool CanDisposeAbilityOf(WeaponResource item)
        {
#if XYNOK_DEBUGGER_THROW_LOG
            throw new NotImplementedException();
#else
            Debug.LogError("UNITY_64");
#endif


            throw new NotImplementedException();
        }
    }

    // [RequireMatchingQueriesForUpdate]
    // [UpdateInGroup(typeof(XynokSimulationGroup))]
    // public partial struct Mysysyem : ISystem
    // {
    //     private float _cache;
    //
    //     readonly partial struct MyAspectStateCmd : IAspect
    //     {
    //         public readonly Entity Self;
    //         public readonly RefRO<CharacterTagPlayer> Tag;
    //         public readonly RefRW<CharacterHp> Source;
    //
    //         public void Sync(ref SystemState state, float newValue)
    //         {
    //             Source.ValueRW.Value = newValue;
    //             state.EntityManager.SetComponentEnabled<CharacterHpCmdChange>(Self, true);
    //         }
    //     }
    //
    //     public void OnUpdate(ref SystemState state)
    //     {
    //         if (Input.GetKeyDown(KeyCode.Space))
    //         {
    //             _cache++;
    //             foreach (var aspect in SystemAPI.Query<MyAspectStateCmd>())
    //             {
    //                 aspect.Sync(ref state, _cache);
    //             }
    //         }
    //        
    //     }
    // }
}