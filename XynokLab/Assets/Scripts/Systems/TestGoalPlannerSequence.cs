﻿using System;
using System.Collections.Generic;
using UnityEngine;
using XynokCombatOop.Goap.Data;
using XynokConvention.APIs;
using XynokSourceGenerator.Generated.Behaviors;
using XynokSourceGenerator.Generated.Entities;
using XynokUtils.APIs;

namespace Systems
{
    // /// <summary>
    // /// sequence chain for a list of handlers belong to a goal, each handler has only single or random of multiple of action.
    // /// NOTE: This is only focus for a goal (only 1).
    // /// </summary>
    // public class GoalHandlerBaker : IInjectable<ICharacterGoalKeeper>, IExecuteLite
    // {
    //     public event Action<GoalHandlerBaker> OnCompleted;
    //     private ICharacterGoalKeeper _goalKeeper;
    //     private CharacterGoalSingleActionBaker[] _handleBakers;
    //     private Action _onDispose;
    //     private CharacterGoalSingleActionBaker _currentAction;
    //
    //     private CharacterGoalData _goalData;
    //
    //     public void SetDependency(ICharacterGoalKeeper dependency)
    //     {
    //         if (_goalKeeper != null) Dispose();
    //         _goalKeeper = dependency;
    //     }
    //
    //     public void Dispose()
    //     {
    //         _onDispose?.Invoke();
    //         _onDispose = default;
    //     }
    //
    //     public void InitFor(CharacterGoalData goalData)
    //     {
    //         _goalData = goalData;
    //         Reset();
    //     }
    //
    //     void ChainTheActions()
    //     {
    //         var amount = _handleBakers.Length;
    //         for (int i = 0; i < amount; i++)
    //         {
    //             int index = i;
    //             var next = index < amount - 1 ? _handleBakers[index + 1] : null;
    //             _handleBakers[index].SetNext(next);
    //             _handleBakers[index].OnCompleted += ExecuteNextHandler;
    //         }
    //     }
    //
    //     void ExecuteNextHandler(CharacterGoalSingleActionBaker handler)
    //     {
    //         if (handler.Next == null)
    //         {
    //             OnCompleted?.Invoke(this);
    //             return;
    //         }
    //
    //         handler.Next.Execute();
    //     }
    //
    //     public void Execute()
    //     {
    //         if (_handleBakers == null || _handleBakers.Length < 1)
    //         {
    //             Debug.LogError(
    //                 $"[GoalHandlerBaker - {_goalKeeper.GoalKeeperData.resourceId}]: _handleBakers is null or empty");
    //             return;
    //         }
    //
    //         Stop();
    //         _currentAction = _handleBakers[0];
    //         _currentAction.Execute();
    //     }
    //
    //     public void Stop()
    //     {
    //         _currentAction?.Stop();
    //     }
    //
    //     /// <summary>
    //     /// just re setup the chain, it does not replay. Call execute() for play.
    //     /// </summary>
    //     public void Reset()
    //     {
    //         Stop();
    //         _handleBakers = GetBakersOf(_goalData);
    //         ChainTheActions();
    //     }
    //
    //     CharacterGoalSingleActionBaker[] GetBakersOf(CharacterGoalData goal)
    //     {
    //         var bakers = new CharacterGoalSingleActionBaker[goal.handlers.Length];
    //
    //         for (int i = 0; i < goal.handlers.Length; i++)
    //         {
    //             int index = i;
    //             var handler = goal.handlers[index];
    //             var bakerProperties = new CharacterGoalSingleActionBakerProperties
    //             {
    //                 GoalKeeper = _goalKeeper,
    //                 Handler = handler
    //             };
    //             var baker = new CharacterGoalSingleActionBaker();
    //             baker.SetDependency(bakerProperties);
    //             _onDispose += baker.Dispose;
    //
    //             bakers[index] = baker;
    //         }
    //
    //         return bakers;
    //     }
    // }

    // public class GoalPlannerProperties
    // {
    //     public ICharacterGoalKeeper GoalKeeper;
    //     public CharacterBehaviorData BehaviorData;
    // }
    //
    // public class TestGoalPlanner : IInjectable<GoalPlannerProperties>
    // {
    //     private GoalPlannerProperties _properties;
    //     private ICharacterGoalKeeper GoalKeeper => _properties.GoalKeeper;
    //     private CharacterResource GoalKeeperData => GoalKeeper.GoalKeeperData;
    //     private Action _onDispose;
    //
    //     private CharacterGoalHandlerBaker[] _goalHandlerBakers;
    //     private CharacterGoalHandlerBaker _currentHandler;
    //
    //     public void SetDependency(GoalPlannerProperties dependency)
    //     {
    //         if (GoalKeeper != null) Dispose();
    //         _properties = dependency;
    //
    //         _goalHandlerBakers = GetBakersOf(_properties.BehaviorData.goals);
    //
    //         if (_properties.BehaviorData.goalExecutor == ExecutorType.Sequence) Sequence();
    //         if (_properties.BehaviorData.goalExecutor == ExecutorType.ByCost) Sequence();
    //         if (_properties.BehaviorData.goalExecutor == ExecutorType.Random) Sequence();
    //     }
    //
    //
    //     void Sequence()
    //     {
    //         var amount = _goalHandlerBakers.Length;
    //         for (int i = 0; i < amount; i++)
    //         {
    //             // init chains
    //             var handler = _goalHandlerBakers[i];
    //             var next = _goalHandlerBakers[i < amount - 1 ? i + 1 : 0];
    //             handler.SetNext(next);
    //             handler.OnCompleted -= BindingAfterGoalCompleted;
    //             handler.OnCompleted += BindingAfterGoalCompleted;
    //         }
    //     }
    //
    //     void BindingAfterGoalCompleted(CharacterGoalHandlerBaker goalBaker)
    //     {
    //         goalBaker.Next?.Execute();
    //     }
    //
    //     CharacterGoalHandlerBaker[] GetBakersOf(CharacterGoalData[] goals)
    //     {
    //         var bakers = new CharacterGoalHandlerBaker[goals.Length];
    //
    //         for (int i = 0; i < goals.Length; i++)
    //         {
    //             int index = i;
    //             var goal = goals[index];
    //
    //             var baker = new CharacterGoalHandlerBaker();
    //             baker.SetDependency(GoalKeeper);
    //             baker.InitFor(goal);
    //             bakers[index] = baker;
    //             _onDispose += baker.Dispose;
    //         }
    //
    //         return bakers;
    //     }
    //
    //     public void Dispose()
    //     {
    //         _onDispose?.Invoke();
    //         _onDispose = default;
    //     }
    // }
}