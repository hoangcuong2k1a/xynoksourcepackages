// Xynok Lab ~ HoangCuongZk1 ♙♙ auto generated - time: 9/23/2023 6:38:50 PM
using Unity.Entities;
using XynokSourceGenerator.Generated.Entities;
using XynokECS.SystemGroup;
using UnityEngine;
using XynokSourceGenerator.Dot.Generated.Components;
using Enums;

namespace XynokSourceGenerator.Dot.Generated.Systems
{
#region Stats 
    readonly partial struct CharacterHpAspectCmd : IAspect
    {
        public readonly Entity Self;
        public readonly RefRW<CharacterHp> Source;
        public readonly RefRO<CharacterHpCmdChange> Cmd;
        public void Sync(ref SystemState state)
        {
            Source.ValueRW.Value = Cmd.ValueRO.newValue;
            state.EntityManager.SetComponentEnabled<CharacterHpCmdChange>(Self, false);
        }
    }

    [RequireMatchingQueriesForUpdate]
    [UpdateInGroup(typeof(XynokSimulationGroup))]
    public partial struct TrackCmdChangeCharacterHp : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            foreach (var aspect in SystemAPI.Query<CharacterHpAspectCmd>())
            {
                aspect.Sync(ref state);
            }
        }
    }

    readonly partial struct CharacterAttackSpeedAspectCmd : IAspect
    {
        public readonly Entity Self;
        public readonly RefRW<CharacterAttackSpeed> Source;
        public readonly RefRO<CharacterAttackSpeedCmdChange> Cmd;
        public void Sync(ref SystemState state)
        {
            Source.ValueRW.Value = Cmd.ValueRO.newValue;
            state.EntityManager.SetComponentEnabled<CharacterAttackSpeedCmdChange>(Self, false);
        }
    }

    [RequireMatchingQueriesForUpdate]
    [UpdateInGroup(typeof(XynokSimulationGroup))]
    public partial struct TrackCmdChangeCharacterAttackSpeed : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            foreach (var aspect in SystemAPI.Query<CharacterAttackSpeedAspectCmd>())
            {
                aspect.Sync(ref state);
            }
        }
    }

    readonly partial struct CharacterMoveSpeedAspectCmd : IAspect
    {
        public readonly Entity Self;
        public readonly RefRW<CharacterMoveSpeed> Source;
        public readonly RefRO<CharacterMoveSpeedCmdChange> Cmd;
        public void Sync(ref SystemState state)
        {
            Source.ValueRW.Value = Cmd.ValueRO.newValue;
            state.EntityManager.SetComponentEnabled<CharacterMoveSpeedCmdChange>(Self, false);
        }
    }

    [RequireMatchingQueriesForUpdate]
    [UpdateInGroup(typeof(XynokSimulationGroup))]
    public partial struct TrackCmdChangeCharacterMoveSpeed : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            foreach (var aspect in SystemAPI.Query<CharacterMoveSpeedAspectCmd>())
            {
                aspect.Sync(ref state);
            }
        }
    }

    readonly partial struct CharacterAttackDamageAspectCmd : IAspect
    {
        public readonly Entity Self;
        public readonly RefRW<CharacterAttackDamage> Source;
        public readonly RefRO<CharacterAttackDamageCmdChange> Cmd;
        public void Sync(ref SystemState state)
        {
            Source.ValueRW.Value = Cmd.ValueRO.newValue;
            state.EntityManager.SetComponentEnabled<CharacterAttackDamageCmdChange>(Self, false);
        }
    }

    [RequireMatchingQueriesForUpdate]
    [UpdateInGroup(typeof(XynokSimulationGroup))]
    public partial struct TrackCmdChangeCharacterAttackDamage : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            foreach (var aspect in SystemAPI.Query<CharacterAttackDamageAspectCmd>())
            {
                aspect.Sync(ref state);
            }
        }
    }

    readonly partial struct CharacterMyTargetHpAspectCmd : IAspect
    {
        public readonly Entity Self;
        public readonly RefRW<CharacterMyTargetHp> Source;
        public readonly RefRO<CharacterMyTargetHpCmdChange> Cmd;
        public void Sync(ref SystemState state)
        {
            Source.ValueRW.Value = Cmd.ValueRO.newValue;
            state.EntityManager.SetComponentEnabled<CharacterMyTargetHpCmdChange>(Self, false);
        }
    }

    [RequireMatchingQueriesForUpdate]
    [UpdateInGroup(typeof(XynokSimulationGroup))]
    public partial struct TrackCmdChangeCharacterMyTargetHp : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            foreach (var aspect in SystemAPI.Query<CharacterMyTargetHpAspectCmd>())
            {
                aspect.Sync(ref state);
            }
        }
    }

#endregion
    [UpdateInGroup(typeof(XynokSimulationGroup))]
    public partial struct Mysysyem : ISystem
    {
        private float _cache;
        readonly partial struct MyAspectStateCmd : IAspect
        {
            public readonly Entity Self;
            public readonly RefRO<CharacterTagPlayer> Tag;
            public readonly RefRO<CharacterHp> Source;
            public void Sync(ref SystemState state, float newValue)
            {
                var cmd = new CharacterHpCmdChange
                {
                    newValue = newValue
                };
                state.EntityManager.SetComponentData(Self, cmd);
                state.EntityManager.SetComponentEnabled<CharacterHpCmdChange>(Self, true);
            }
        }

        public void OnUpdate(ref SystemState state)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _cache++;
                foreach (var aspect in SystemAPI.Query<MyAspectStateCmd>())
                {
                    aspect.Sync(ref state, _cache);
                }
            }
        }
    }
}
//Xynok Lab ~ HoangCuongZk1 ♙♙ auto generated - time: 9/23/2023 6:38:50 PM