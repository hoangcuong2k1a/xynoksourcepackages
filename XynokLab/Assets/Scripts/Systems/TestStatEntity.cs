﻿using Enums;
using XynokGUI.Entities.StatView;
using XynokSourceGenerator.Generated.Entities;

namespace Systems
{
    public class TestStatEntity: ACharacterStatView
    {
        public CharacterStatData stat;
        protected override CharacterStatData GetSourceData(StatType key)
        {
            return stat;
        }

        protected override string GetStatName(StatType key)
        {
            return stat.Key.ToString();
        }
    }
}