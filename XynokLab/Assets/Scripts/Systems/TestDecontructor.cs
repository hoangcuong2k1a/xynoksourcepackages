﻿using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Systems
{
   

    // [ASerializable]
    public interface IMove
    {
        void Move();
    }

    [Serializable]
    public class Hero : IMove
    {
        public string name = "Hero";

        [JsonIgnore] public Component conga;
        [JsonIgnore] public Transform sds;
        public string unityDataCache;

       [SerializeReference] public IMove mover;
       

        public void Move()
        {
            Debug.Log("Hero move");
        }
    }

    public class Monster : IMove
    {
        public string name = "Monster";

        public virtual void Move()
        {
            Debug.Log("Monster move");
        }
    }

    public class Killer : Monster
    {
        public string name = "Killer";

        public override void Move()
        {
            Debug.Log("Killer move");
        }
    }

    public class Hannibal : Killer
    {
        public string name = "Hannibal";

        public override void Move()
        {
            Debug.Log("Hannibal move");
        }
    }

    public class WillGrahmn : Hannibal
    {
        public string name2 = "WillGrahmn";

        public override void Move()
        {
            Debug.Log("Hannibal move");
        }
    }

    // [Serializable]
    // public class TestSer : IMoveSerializable
    // {
    //     [JsonIgnore] public Transform something;
    // }

    public class TestDecontructor : MonoBehaviour
    {
        [SerializeReference] public IMove mover2;

       
    }
}