﻿using Enums;
using UnityEngine;
using XynokSourceGenerator.Generated.Entities;

namespace Systems.MyAbility.Character
{
    public class RunAbi : ACharacterAbility
    {
        protected override AbilityType AbilityType => AbilityType.Run;

        protected override bool Matched()
        {
            return true;
        }

        protected override void Init()
        {
            Debug.Log($"{AbilityType} init");
        }

        public override void Dispose()
        {
            Debug.Log($"{AbilityType} Dispose");
        }
    }
    public class AbilityDeath : ACharacterAbility
    {
        protected override AbilityType AbilityType => AbilityType.Dash;

        private CharacterStatData hpStat;

        private CharacterStateData death;
        protected override void Init()
        {
            hpStat = this.owner.Data.GetStat(StatType.Hp);

            death = this.owner.Data.GetState(StateType.Moving);

            hpStat.OnChanged += OnHpChange;
        }

        public override void Dispose()
        {
            hpStat.OnChanged -= OnHpChange;
        }

        private void OnHpChange(float obj)
        {
            if (obj <= 0)
            {
                death.Value = true;
            } else
            {
                death.Value = false;
            }
        }

        protected override bool Matched()
        {
            return true;
        }
    }
    public class RunAbixxx : RunAbi
    {
        protected override AbilityType AbilityType => AbilityType.Jump;

        protected override bool Matched()
        {
            return true;
        }

        protected override void Init()
        {
            Debug.Log($"{AbilityType} Init");
        }

        public override void Dispose()
        {
            Debug.Log($"{AbilityType} Dispose");
        }
    }
}