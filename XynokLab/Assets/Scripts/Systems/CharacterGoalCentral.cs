﻿using System;
using System.Linq;
using Enums;
using Enums.Character;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokCombatOop.Goap.Data;
using XynokConvention.APIs;
using XynokConvention.Data;
using XynokSourceGenerator.Generated.Behaviors;
using XynokSourceGenerator.Generated.Entities;

namespace Systems
{
    // [Serializable]
    // public class CharacterGoalKeeperDict : SerializableDict<CharacterID, CharacterGoalContainer>
    // {
    // }

    [CreateAssetMenu(fileName = "CharacterGoalCentral", menuName = "Xynok/Combat/CharacterGoalCentral")]
    public class CharacterGoalCentral : ScriptableObject
    {
        [HideLabel] public CharacterGoalKeeperMetaDataDict goalKeeperDict;

#if UNITY_EDITOR
        public static CharacterGoalCentral Instance =>
            XynokUtils.AssetUtils.GetInstanceOfSo<CharacterGoalCentral>("Xynok/AssetBundle/FxPrefabManaged");
#endif
        private void OnValidate()
        {
            foreach (var pair in goalKeeperDict)
            {
                pair.Value.goalContainer.OnValidate();
                pair.Value.data.resourceId = pair.Key;
            }
        }
    }


    /// <summary>
    /// init goal array and sort by config.
    /// </summary>
    public class XGoalPlanner : IInjectable<CharacterGoalPlannerProperties>
    {
        private CharacterGoalPlannerProperties _properties;
        private ICharacterGoalKeeper GoalKeeper => _properties.GoalKeeper;
        private CharacterResource GoalKeeperData => GoalKeeper.GoalKeeperData;
        private Action _onDispose;
        private CharacterGoalHandlerBaker[] _goalHandlerBakers;
        private CharacterGoalHandlerBaker _currentHandler;

        public void SetDependency(CharacterGoalPlannerProperties dependency)
        {
            if (GoalKeeper != null)
                Dispose();
            _properties = dependency;
            _goalHandlerBakers = GetBakersOf(_properties.BehaviorData.goals);
            SortGoals();
        }

        void SortGoals()
        {
            var executor = _properties.BehaviorData.executor;
            if (executor == ExecutorType.Sequence)
                Sequence();
            if (executor == ExecutorType.Random)
                MakeRandom();
            if (executor == ExecutorType.ByCost)
                SortByCost();
        }

        void SortByCost()
        {
            var sortType = _properties.BehaviorData.bargainType;
            if (sortType == BargainType.Cheapest)
                _goalHandlerBakers = _goalHandlerBakers.OrderBy(e => e.Cost).ToArray();
            if (sortType == BargainType.Expensive)
                _goalHandlerBakers = _goalHandlerBakers.OrderByDescending(e => e.Cost).ToArray();
            Sequence();
        }

        void Sequence()
        {
            var amount = _goalHandlerBakers.Length;
            for (int i = 0; i < amount; i++)
            {
                // init chains
                var handler = _goalHandlerBakers[i];
                var next = i < amount - 1 ? _goalHandlerBakers[i + 1] : null;
                handler.SetNext(next);
                handler.OnCompleted -= BindingAfterGoalCompleted;
                handler.OnCompleted += BindingAfterGoalCompleted;
            }
        }

        void MakeRandom()
        {
            _goalHandlerBakers = XynokUtils.CollectionUtil.Shuffle(_goalHandlerBakers);
            Sequence();
        }

        void BindingAfterGoalCompleted(CharacterGoalHandlerBaker goalBaker)
        {
            if (goalBaker.Next == null)
            {
                SortGoals();
                return;
            }

            goalBaker.Next.Execute();
        }

        CharacterGoalHandlerBaker[] GetBakersOf(CharacterGoalData[] goals)
        {
            var bakers = new CharacterGoalHandlerBaker[goals.Length];
            for (int i = 0; i < goals.Length; i++)
            {
                int index = i;
                var goal = goals[index];
                var baker = new CharacterGoalHandlerBaker();
                baker.SetDependency(GoalKeeper);
                // goal.completedConditions
                baker.InitFor(goal);
                bakers[index] = baker;
                _onDispose += baker.Dispose;
            }

            return bakers;
        }

        public void Dispose()
        {
            _onDispose?.Invoke();
            _onDispose = default;
        }
    }
}