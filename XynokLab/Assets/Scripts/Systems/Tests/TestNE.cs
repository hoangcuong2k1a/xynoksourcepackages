﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using XynokUtils.Gacha;

namespace Systems.Tests
{
    [Serializable]
    public class RandomData
    {
        public string id;
        public float chance;
    }

    [Serializable]
    public class RandomDataTrack
    {
        public string id;
        public int count;
        public float dropRate;
    }

    public class TestNE : MonoBehaviour
    {
        public List<RandomData> data;
        public List<RandomDataTrack> dataTrack;

        [Button]
        void TestRandomFloat(int sizeTest = 100)
        {
            var selector = new ProportionalRandomSelector<RandomData>();
            dataTrack = new();
            var dict = new Dictionary<RandomData, float>();
            foreach (var randomData in data)
            {
                dataTrack.Add(new RandomDataTrack
                {
                    id = randomData.id,
                    count = 0,
                    dropRate = 0,
                });
                dict.Add(randomData, randomData.chance);
            }

            selector.Init(dict);
            
            for (int i = 0; i < sizeTest; i++)
            {
                var result = selector.SelectItem();
                if (result == null) continue;
                var pair = dataTrack.FirstOrDefault(x => x.id == result.id);
                if (pair != null) pair.count++;
            }

            foreach (var randomDataTrack in dataTrack)
            {
                randomDataTrack.dropRate = (float)randomDataTrack.count / sizeTest;
            }
        }
    }
}