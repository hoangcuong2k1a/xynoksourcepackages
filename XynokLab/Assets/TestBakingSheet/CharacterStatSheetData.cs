﻿using Cathei.BakingSheet;
using Newtonsoft.Json;
using UnityEngine;
using XynokBakingSheet.Runner;

namespace TestBakingSheet
{
    public enum CurrencyType
    {
        ETH,
        NFT,
        MiningFarm,
        CryptoWallet,
    }

    public class ConsumableSheet : Sheet< ConsumableSheet.Row>
    {
        public class Row : SheetRow
        {
            // use name of matching column
            public string Business { get; private set; }
            public string Number { get; private set; }
            public int BonusSpeed { get; private set; }
            public int BonusProfit { get; private set; }
            public string BonusForBusiness { get; private set; }
        }
    }

    public class Shheet : SheetContainerBase
    {
        public ConsumableSheet Sheet3 { get; private set; }
    }

    [CreateAssetMenu(fileName = "CharacterStatSheetData", menuName = "TestBakingSheet/CharacterStatSheetData")]
    public class CharacterStatSheetData : ASheetPage<Shheet>
    {
        protected override void AfterPull(bool pullResult, Shheet container)
        {
            Debug.Log(JsonConvert.SerializeObject(container));
            foreach (var row in container.Sheet3)
            {
                Debug.Log(row.Id);
            }
        }
    }
}